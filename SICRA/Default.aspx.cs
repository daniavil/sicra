﻿using SICRA.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SICRA
{
    public partial class _Default : Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        UsuarioController _uCtrl = new UsuarioController();
        public readonly string app = ConfigurationManager.AppSettings["app"];

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "Pagina Principal";

            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            if (!_uCtrl.GetActivoSicra(_usrLog.user))
            {
                spnMsj.InnerText = "ATENCIÓN:  USUARIO INACTIVO EN SICRA";
            }

        }
    }
}