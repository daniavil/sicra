﻿using SICRA.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Data.Common;
using Dapper;
using SICRA.Dto;
using System.Data.SqlClient;
using System.Data;

namespace SICRA.Controllers
{
    public class SoeehController : Controller
    {
        private static readonly string TokenSys = ConfigurationManager.AppSettings["TokenSys"];
        private static readonly DbConnection ConnEehApps = Fun.GetConnectionSQL("EEHAppsConn");

        public ObjClass GetExisteOS(int os)
        {
            var resp = new ObjClass();

            try
            {
                var qry = $"select Os from ATCOs where Os={os}";
                var osQry = SqlMapper.Query<int?>(ConnEehApps, qry, commandTimeout: 0).FirstOrDefault();

                if (osQry != null)
                {
                    resp.Texto1 = "1";
                    resp.Texto2 = "OS existe";
                    return resp;
                }
                else
                {
                    resp.Texto1 = "0";
                    resp.Texto2 = "OS NO existe";
                    return resp;
                }
            }
            catch (Exception ex)
            {
                resp.Texto1 = "0";
                resp.Texto2 = "OS NO existe.";
                return resp;
            }
        }

        public OS_Obj PutOSSOEEH(OS_Obj CreaOs)
        {
            try
            {
                List<SqlParameter> lista = new List<SqlParameter>();

                lista.Add(new SqlParameter("@clave", CreaOs.Clave));
                lista.Add(new SqlParameter("@comentario", CreaOs.Comentario));
                lista.Add(new SqlParameter("@UsuarioCreador", CreaOs.UsuarioCrea));

                List<SqlParameter> listaOutput = new List<SqlParameter>();
                listaOutput.Add(new SqlParameter("@cod_resultado", SqlDbType.Int));
                listaOutput.Add(new SqlParameter("@txt_resultado", SqlDbType.VarChar, 200));

                List<ObjClass> resp = Fun.ExecPaQueryResult("[eehapps].[dbo].[sp_os_sicra_revision]", lista, listaOutput, 0, "EEHAppsConn");

                int codOs = int.Parse(resp[0].Texto2.Trim());
                string msjResp = resp[1].Texto2.Trim();

                CreaOs.IdOS = codOs;
                CreaOs.Resultadotext = msjResp;
                CreaOs.Correcto = codOs < 1 ? false : true;
            }
            catch (Exception ex)
            {
                CreaOs.IdOS = null;
                CreaOs.Resultadotext = $"Ocurrió un error en el proceso de creación de OS: {ex.Message}";
                CreaOs.Correcto = false;
            }
            return CreaOs;
        }

        public void PutFACTRefactura(FACTRefacturaDto r)
        {
            try
            {
                string qry = $@"
    INSERT INTO [dbo].[FACTRefactura]
               ([NIS_RAD]
               ,[CICLO]
               ,[DIAL]
               ,[CO_AN]
               ,[LECT_ACT_ACTIVA]
               ,[LECT_ACT_REACTIVA]
               ,[LECT_ACT_DEMANDA]
               ,[COD_ESTADO]
               ,[MESES]
               ,[TXT_COMENTARIO_INICIAL]
               ,[COD_ORIGEN]
               ,[COD_TIPO_CAMBIO]
               ,[FEC_REGISTRO]
               ,[COD_USUARIO_REGISTRO]
               ,[IdRowMuestraCritica])
         VALUES
               ({(!string.IsNullOrEmpty(r.NIS_RAD.ToString()) ? r.NIS_RAD.ToString() : "null")}
               ,{(!string.IsNullOrEmpty(r.CICLO.ToString()) ? r.CICLO.ToString() : "null")}
               ,{(!string.IsNullOrEmpty(r.DIAL.ToString()) ? r.DIAL.ToString() : "null")}
               , '{(!string.IsNullOrEmpty(r.CO_AN.ToString()) ? r.CO_AN.ToString() : "null")}'
               ,{(!string.IsNullOrEmpty(r.LECT_ACT_ACTIVA.ToString()) ? r.LECT_ACT_ACTIVA.ToString() : "null")}
               ,{(!string.IsNullOrEmpty(r.LECT_ACT_REACTIVA.ToString()) ? r.LECT_ACT_REACTIVA.ToString() : "null")}
               ,{(!string.IsNullOrEmpty(r.LECT_ACT_DEMANDA.ToString()) ? r.LECT_ACT_DEMANDA.ToString() : "null")}
               ,{(!string.IsNullOrEmpty(r.COD_ESTADO.ToString()) ? r.COD_ESTADO.ToString() : "null")}
               ,{(!string.IsNullOrEmpty(r.MESES.ToString()) ? r.MESES.ToString() : "null")}
               , '{(!string.IsNullOrEmpty(r.TXT_COMENTARIO_INICIAL.ToString()) ? r.TXT_COMENTARIO_INICIAL.ToString() : "null")}'
               ,{(!string.IsNullOrEmpty(r.COD_ORIGEN.ToString()) ? r.COD_ORIGEN.ToString() : "null")}
               ,{(!string.IsNullOrEmpty(r.COD_TIPO_CAMBIO.ToString()) ? r.COD_TIPO_CAMBIO.ToString() : "null")}
               , '{(!string.IsNullOrEmpty(r.FEC_REGISTRO.ToString()) ? r.FEC_REGISTRO.ToString() : "null")}'
               , '{(!string.IsNullOrEmpty(r.COD_USUARIO_REGISTRO.ToString()) ? r.COD_USUARIO_REGISTRO.ToString() : "null")}'
               , '{(!string.IsNullOrEmpty(r.IdRowMuestraCritica.ToString()) ? r.IdRowMuestraCritica.ToString() : "null")}')";

                var ExeQry = SqlMapper.ExecuteScalar(ConnEehApps, qry, commandTimeout: 0);


            }
            catch (Exception ex)
            {
            }
        }
    }
}