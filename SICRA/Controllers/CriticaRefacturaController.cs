﻿using Dapper;
using Newtonsoft.Json;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace SICRA.Controllers
{
    public class CriticaRefacturaController : Controller
    {
        private static readonly string TokenSys = ConfigurationManager.AppSettings["TokenSys"];
        private static readonly DbConnection conStrReplica = Fun.GetConnectionSQL("ReplicaConn");
        private static readonly DbConnection conStrEehApps = Fun.GetConnectionSQL("SegConn");
        private static readonly DbConnection conStrSicra = Fun.GetConnectionSQL("SicraModel");
        private static readonly DbConnection ConnINCMS = Fun.GetConnectionORA("INCMSBD");
        private UsuarioController _uCtrl = new UsuarioController();


        /*****************************************asignacion de critica***********************************************/
        public List<CriticaRefacturaDtoInfo> GetMuestraBd(int Ciclo,int dial, string Origen, string Observ, string usuariosRefa, int porcentaje, List<string> users)
        {
            try
            {
                if (users.Count<1)
                {
                    return null;
                }

                string where = " where 1=1 ";
                if (!string.IsNullOrEmpty(Origen))
                {
                    where += $" and CodOrigen in ({Origen}) ";
                }

                if (!string.IsNullOrEmpty(Observ))
                {
                    where += $" and CodObs in ({Observ}) ";
                }

                if (!string.IsNullOrEmpty(usuariosRefa))
                {
                    where += $" and codUsuarioRef in ({usuariosRefa}) ";
                }

                string sql = $@"DECLARE @Dial int
                                DECLARE @CantClaves int
                                declare @Ciclo int
                                declare @porcentaje decimal(18,2)
                                set @Dial = '{dial}'; --->enviado desde interfaz

                                set @Ciclo={Ciclo}
                                /*********************************calculo porcentaje**********************************/
                                set @porcentaje= convert(decimal(18,2),({porcentaje}))/convert(decimal(18,2),100)

                                DROP TABLE IF EXISTS  #TempMuestraCriRefa
                                select aa.*

                                into #TempMuestraCriRefa
                                from 
                                (
                                select 
                                a.NIS_RAD as nisrad,
                                a.CICLO,
                                a.DIAL dial,
                                a.COD_USUARIO_RESUELTA codUsuarioRef,
                                LOWER(a.LECT_ACT_ACTIVA) LectActiva,
                                a.LECT_ACT_REACTIVA LectReactiva,
                                a.LECT_ACT_DEMANDA LectDemanda,
                                a.COD_OBSERVACION CodObs,
                                b.txt_desc as Observacion,
                                a.COD_ORIGEN CodOrigen,
                                c.txt_desc Origen
                                from [eehApps].[dbo].FACTRefactura a with(nolock)
                                left join [eehApps].[dbo].FACTRefacturaObservacion b on a.COD_OBSERVACION=b.cod_observacion
                                left join FACTRefacturaOrigen c on a.COD_ORIGEN=c.cod_origen
                                where a.COD_ESTADO=4 and a.DIAL=@Dial and a.ciclo=@Ciclo
                                ) as aa
                                {where}


                                set @CantClaves=(select convert(integer,round (count(nisrad)*@porcentaje,0)) as cantMuestra from #TempMuestraCriRefa)

                                select top (@CantClaves) * from #TempMuestraCriRefa ORDER BY NEWID()";

                var muestra = SqlMapper.Query<CriticaRefacturaDtoInfo>(conStrEehApps, sql).ToList();

                int conteoPorUsuario = 0;
                conteoPorUsuario = muestra.Count() / users.Count();

                List<CriticaRefacturaDtoInfo> listaFinal = new List<CriticaRefacturaDtoInfo>();

                foreach (var usr in users)
                {
                    var listSinUsrs = muestra
                        .FindAll(x => !listaFinal.Select(y => y.nisrad).Contains(x.nisrad))
                        .Take(conteoPorUsuario)
                        .ToList();

                    foreach (var mues in listSinUsrs)
                    {
                        mues.codUsuario = usr;
                        listaFinal.Add(mues);
                    }
                }
                return listaFinal;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<UsuariosFacturacionInfo> GetUsuariosFacturacionRefactura()
        {
            string sql = "";
            sql = $@"select Usuario as codUsr, CONCAT(Usuario,' - ',NomPersona) as nombre 
                    from [dbo].[Usuarios] a
                    join [dbo].[PerfilModulo] b on a.IdPerfilResoRepo=b.IdPerfilResoRepo
                    join [dbo].[Modulo] c on c.idModulo=b.idModulo
                    where c.Nombre='MuestraRefactura' and a.Activo=1";
            var usuarios = SqlMapper.Query<UsuariosFacturacionInfo>(conStrSicra, sql).ToList();
            return usuarios;
        }

        public List<ObjIdValor> GetOrigenRefactura()
        {
            string sql = "";
            //sql = $@"select CODIGOANOMALIA as id,NOMBRE from SGC_ANOMALIAS order by NOMBRE";
            sql = $@"select cod_origen as id, txt_desc as valor from FACTRefacturaOrigen where activo=1 order by 2";
            var lista = SqlMapper.Query<ObjIdValor>(conStrEehApps, sql).ToList();
            return lista;
        }

        public List<int> GetPeriodosRefactura()
        {
            string sql = "";
            //sql = $@"select CODIGOANOMALIA as id,NOMBRE from SGC_ANOMALIAS order by NOMBRE";
            sql = $@"select distinct top 12 CICLO FROM eehApps.dbo.FACTRefactura order by 1 desc";
            var periodos = SqlMapper.Query<int>(conStrEehApps, sql).ToList();
            return periodos;
        }

        public bool GetCountMuestrasbyFecha(int dial, bool esTipo)
        {
            using (var db = new SicraModel())
            {
                var conteo = db.MuestraRefactura
                    .Where(x => x.dial== dial && x.EsTotal == esTipo)
                    .ToList()
                    .Count;

                if (conteo > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public RespMsj PutAsingarMuestras(List<MuestraRefactura> listaMuestras)
        {
            using (var db = new SicraModel())
            {
                db.MuestraRefactura.AddRange(listaMuestras);

                if (db.SaveChanges() > 0)
                {
                    return new RespMsj { estado = "ok", mensaje = "Muestra de Refactura guardada correctamente." };
                }
                else
                {
                    return new RespMsj { estado = "error", mensaje = "Error al guardar la muestra de Refactura." };
                }
            }
        }

        /*****************************************resolver de critica***********************************************/

        public List<MuestraRefactura> GetAsignacionByUser(string user, int ciclo, int dial)
        {
            using (var db = new SicraModel())
            {
                var lista = db.MuestraRefactura
                    .Where(x => x.usuarioResuelve.Equals(user)
                    && x.Ciclo == ciclo
                    && x.dial == dial
                    && !x.Resuelto)
                    .ToList();
                return lista;
            }
        }

        public List<int> GetCiclosByUser(string user)
        {
            using (var db = new SicraModel())
            {
                var lista = db.MuestraRefactura
                    //.Where(x => x.usuarioResuelve.Equals(user) && !x.Resuelto)
                    .Where(x => !x.Resuelto && x.usuarioResuelve.Equals(user))
                    .Select(y => y.Ciclo)
                    .Distinct()
                    .ToList();
                return lista;
            }
        }

        public List<string> GetDialesByUser(string user, int ciclo)
        {
            string sql = "";
            sql = $@"select distinct dial from [MuestraRefactura]
                    where usuarioResuelve='{user}' and Resuelto=0 and Ciclo={ciclo} order by 1";
            var fechasDb = SqlMapper.Query<string>(conStrSicra, sql).ToList();
            return fechasDb;
        }

        public MuestraRefactura GetAsignacionById(string idRow)
        {
            using (var db = new SicraModel())
            {
                var muestra = db.MuestraRefactura.Where(x => x.idRow.ToString().Equals(idRow)).FirstOrDefault();
                return muestra;
            }
        }

        public CriticaRefacturaDtoInfo GetCriticaFactInfo(string clave, int periodo)
        {
            try
            {
                string qry = $@"SELECT 
                                a.NIS_RAD as nisrad,
                                a.CICLO,
                                a.DIAL dial,
                                a.COD_USUARIO_RESUELTA codUsuarioRef,
                                LOWER(a.LECT_ACT_ACTIVA) LectActiva,
                                a.LECT_ACT_REACTIVA LectReactiva,
                                a.LECT_ACT_DEMANDA LectDemanda,
                                a.COD_OBSERVACION CodObs,
                                b.txt_desc as Observacion,
                                a.COD_ORIGEN CodOrigen,
                                c.txt_desc Origen
                                from [eehApps].[dbo].FACTRefactura a with(nolock)
                                left join [eehApps].[dbo].FACTRefacturaObservacion b on a.COD_OBSERVACION=b.cod_observacion
                                left join FACTRefacturaOrigen c on a.COD_ORIGEN=c.cod_origen 
                                where a.CICLO={periodo} and NIS_RAD={clave}";
                var obj = SqlMapper.Query<CriticaRefacturaDtoInfo>(conStrEehApps, qry, commandTimeout: 0).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //public List<ddlGenral> GetCriticaObservaciones()
        //{
        //    try
        //    {
        //        string qry = $@"select cod_observacion as id,txt_desc valor from [dbo].[FACTCriticaObservaciones] where activo=1";
        //        var lista = SqlMapper.Query<ddlGenral>(conStrEehApps, qry, commandTimeout: 0).ToList();
        //        return lista;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        public List<ObjIdValor> GetListaUsuarioSoeehRefa(string ciclo, string dial)
        {
            try
            {
                string qry = $@"select  distinct
                                a.COD_USUARIO_RESUELTA as id,
                                concat(TRIM(b.Nombres),' ',TRIM(b.PrimerApellido),' ',TRIM(b.SegundoApellido))valor
                                from EEHApps.dbo.FACTRefactura a 
                                join GENUsuarios b on a.COD_USUARIO_RESUELTA=b.Usuario
                                where CICLO={ciclo} and COD_ESTADO=4 and DIAL={dial}
                                Order by 2 asc";
                var lista = SqlMapper.Query<ObjIdValor>(conStrEehApps, qry, commandTimeout: 0).ToList();
                return lista;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ObjIdValor> GetListaObservaciones(string ciclo, string dial)
        {
            try
            {
                string qry = $@"select  distinct
                                a.COD_OBSERVACION as id,
                                TRIM(b.txt_desc)valor
                                from EEHApps.dbo.FACTRefactura a 
                                join FACTRefacturaObservacion b on a.COD_OBSERVACION=b.cod_observacion
                                where CICLO={ciclo} and COD_ESTADO=4 and DIAL={dial} order by 2";
                var lista = SqlMapper.Query<ObjIdValor>(conStrEehApps, qry, commandTimeout: 0).ToList();
                return lista;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void PutMuestraCritica(MuestraRefactura m)
        {
            using (var db = new SicraModel())
            {
                db.Entry(m).State = EntityState.Modified;
                db.SaveChanges();
            }
        }


        /*****************************************resolucion (vista) de critica***********************************************/
        public List<int> GetCiclosMuestras()
        {
            using (var db = new SicraModel())
            {
                var listaCiclos = db.MuestraRefactura.Select(y => y.Ciclo).Distinct().ToList();
                return listaCiclos.OrderByDescending(x => x).ToList();
            }
        }
        public List<string> GetDialesResueltos(int ciclo)
        {
            string sql = "";
            sql = $@"select distinct dial from MuestraRefactura
                    where Resuelto=1 and Ciclo={ciclo} order by 1";
            var fechas = SqlMapper.Query<string>(conStrSicra, sql).ToList();
            return fechas;

            //using (var db = new SicraModel())
            //{
            //    var lista = db.MuestraCritica
            //        .Where(x => x.Ciclo == ciclo)
            //        .Select(y => y.dial).Distinct().ToList();
            //    return lista;
            //}
        }

        public MuestrasInfo GetMuestrasUsuariosEstados(int ciclo, int dial)
        {
            var muestra = new MuestrasInfo();

            using (var db = new SicraModel())
            {
                var muestrasusr = new List<ListaMuestrasUsuarios>();
                var lmu = db.MuestraRefactura
                    .Where(x => x.Ciclo == ciclo
                    && x.dial == dial)
                    .Distinct()
                    .ToList();

                foreach (var usr in lmu.Select(x => x.usuarioResuelve).Distinct())
                {
                    var muestras = db.MuestraRefactura
                            .Where(x => x.Ciclo == ciclo
                            && x.dial == dial
                            && x.usuarioResuelve.Equals(usr))
                            .ToList();

                    var muesusr = new ListaMuestrasUsuarios
                    {
                        usuario = usr,
                        NomUsr = _uCtrl.GetUsuarioByUser(usr).NomPersona,
                        CantResueltas = muestras.Where(x => x.Resuelto).Count(),
                        CantNoResueltas = muestras.Where(x => !x.Resuelto).Count()
                    };

                    muestrasusr.Add(muesusr);
                }

                muestra.CantTotalGen = lmu.Count();
                muestra.CantResueltas = lmu.Where(x => x.Resuelto).Count();
                muestra.CantNoResueltas = lmu.Where(x => !x.Resuelto).Count();
                muestra.ListaMuestras = muestrasusr.OrderBy(x => x.NomUsr).ToList();

                return muestra;
            }
        }

        public List<MuestraRefactura> GetResueltasByUser(List<string> users, int ciclo, int dial)
        {
            using (var db = new SicraModel())
            {
                var lista = db.MuestraRefactura
                    .Where(x => users.Contains(x.usuarioResuelve)
                    && x.Ciclo == ciclo
                    && x.dial == dial
                    && x.Resuelto)
                    .ToList();
                return lista;
            }
        }

        /*****************************************Reportería de critica***********************************************/
        public object GetDatosGeneral(FiltroRefa f)
        {
            var datos = new DatosRptGeneral
            {
                FiltrosRefa = new FiltroRefa(),
                SeriesDatos = new List<SerieGeneral>()
            };
            string sql = "", where = " ";

            where = !string.IsNullOrWhiteSpace(f.Dial.ToString()) ? $" and Dial in ({f.Dial}) " : " ";


            sql = $@"
                    declare @TbTemp as table(id int,Concepto nvarchar(max),Valor int)
                    DECLARE @CantMuestra int, @PromCalidadMuestra decimal(18,2) ,@CantRefacturaCorrecta int,@CantDocCorrecta int,@CantObsCierreCorrecto int,@CantProcesoAutorizacionCorrecto int,@CantDatosResoCorrecto int

                    set @CantMuestra = (SELECT COUNT(*) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where})
                    set @PromCalidadMuestra = (SELECT AVG(Calificacion) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where} and Resuelto=1)
                    set @CantRefacturaCorrecta = (SELECT COUNT(*) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where} and P1RefacturaCorrecta='SI')
                    set @CantDocCorrecta = (SELECT COUNT(*) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where} and P6DocCorrecta='SI')
                    set @CantObsCierreCorrecto = (SELECT COUNT(*) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where} and P7ObsCierreCorrecto='SI')
                    set @CantProcesoAutorizacionCorrecto = (SELECT COUNT(*) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where} and P3ProcesoAutorizacionCorrecto='SI')
                    set @CantDatosResoCorrecto = (SELECT COUNT(*) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where} and P5DatosResoCorrecto='SI')


                    insert into @TbTemp values (1,'Cantidad Muestra Generada',@CantMuestra)
                    insert into @TbTemp values (2,'Promedio % Muestra Evaluada', @PromCalidadMuestra)
                    insert into @TbTemp values (3,'Cantidad de Refacturas Correctas', @CantRefacturaCorrecta)
                    insert into @TbTemp values (4,'Cantidad de Refacturas con Documentación Soporte Correcta', @CantDocCorrecta)
                    insert into @TbTemp values (5,'Cantidad de Refacturas con Observación Cierre Correcta', @CantObsCierreCorrecto)
                    insert into @TbTemp values (6,'Cantidad de Refacturas con Proceso de Autorización Correcto', @CantProcesoAutorizacionCorrecto)
                    insert into @TbTemp values (7,'Cantidad de Refacturas con Datos de Resolución Correctos', @CantDatosResoCorrecto)

                    select * from @TbTemp order by 1";

            datos.SeriesDatosRefa = SqlMapper.Query<SerieGeneral>(conStrSicra, sql).ToList();

            return datos;
        }

        public object GetDatosDetalle(FiltroRefa f)
        {
            var datos = new DatosRptDetalle
            {
                FiltrosRefa = new FiltroRefa(),
                SeriesDatosRefa = new List<SerieDetalleRefa>()
            };
            string sql = "", where = " ";

            where = !string.IsNullOrWhiteSpace(f.Dial.ToString()) ? $" and dial in ({f.Dial}) " : " ";


            sql = $@"
                    DECLARE @FechaLect date
                    DECLARE @UsrResCri VARCHAR(MAX)
                    declare @totalMuestra int
                    set @totalMuestra = ( SELECT count(*) FROM MuestraRefactura  where Ciclo = {f.Ciclo} {where}  )
                    declare @tablaDatos as table(
                    NomPersona nvarchar(50)
                    ,PromCalidadMuestra int
                    ,CantRefacturaCorrecta int
                    ,CantDocCorrecta int
                    ,CantObsCierreCorrecto int
                    ,CantProcesoAutorizacionCorrecto int
                    ,CantDatosResoCorrecto int
                    ,TotalMuestra int)

                    DECLARE cursor_Usr CURSOR
                    FOR SELECT DISTINCT usuarioResuelveRefa FROM MuestraRefactura where Ciclo = {f.Ciclo} {where}
                    OPEN cursor_Usr;
                    FETCH NEXT FROM cursor_Usr INTO @UsrResCri 

                    WHILE @@FETCH_STATUS = 0
                    BEGIN
                    DECLARE @NomPersona nvarchar(50), @PromCalidadMuestra decimal(18,2) ,@CantRefacturaCorrecta int,@CantDocCorrecta int,@CantObsCierreCorrecto int,@CantProcesoAutorizacionCorrecto int, @CantDatosResoCorrecto int 

                    set @NomPersona = (SELECT DISTINCT isnull(b.NomPersona,a.usuarioResuelveRefa) FROM MuestraRefactura a left join sicra.dbo.Usuarios b on a.usuarioResuelveRefa=b.Usuario where a.usuarioResuelveRefa=@UsrResCri)
                    set @PromCalidadMuestra = (SELECT AVG(Calificacion) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where} and Resuelto=1 and usuarioResuelveRefa = @UsrResCri)
                    set @CantRefacturaCorrecta = (SELECT COUNT(*) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where} and usuarioResuelveRefa = @UsrResCri and P1RefacturaCorrecta='SI')
                    set @CantDocCorrecta = (SELECT COUNT(*) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where} and usuarioResuelveRefa = @UsrResCri and P6DocCorrecta='SI')
                    set @CantObsCierreCorrecto = (SELECT COUNT(*) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where} and usuarioResuelveRefa = @UsrResCri and P7ObsCierreCorrecto='SI')
                    set @CantProcesoAutorizacionCorrecto = (SELECT COUNT(*) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where} and usuarioResuelveRefa = @UsrResCri and P3ProcesoAutorizacionCorrecto='SI')
                    set @CantDatosResoCorrecto = (SELECT COUNT(*) FROM MuestraRefactura where Ciclo = {f.Ciclo} {where} and usuarioResuelveRefa = @UsrResCri and P5DatosResoCorrecto='SI')

                    insert into @tablaDatos
                    select @NomPersona,@PromCalidadMuestra,@CantRefacturaCorrecta
                    ,@CantDocCorrecta,@CantObsCierreCorrecto,@CantProcesoAutorizacionCorrecto,
                    @CantDatosResoCorrecto,@totalMuestra

                    FETCH NEXT FROM cursor_Usr INTO @UsrResCri 
                    END;
                    CLOSE cursor_Usr;
                    DEALLOCATE cursor_Usr;
                    select * from @tablaDatos";
            datos.SeriesDatosRefa = SqlMapper.Query<SerieDetalleRefa>(conStrSicra, sql).ToList();
            return datos;
        }

        public object GetDatosControl(FiltroRefa f)
        {
            var datos = new DatosRptControl
            {
                FiltrosRefa = new FiltroRefa(),
                SeriesDatosRefa = new List<SerieControl>()
            };
            string sql = "", where = " ";

            where = !string.IsNullOrWhiteSpace(f.Dial.ToString()) ? $" and Dial in ({f.Dial}) " : " ";


            sql = $@"
                    DECLARE @UsrResuelve VARCHAR(MAX)
                    declare @totalMuestra int
                    set @totalMuestra = (SELECT count(*) FROM MuestraRefactura where Ciclo={f.Ciclo} {where} )
                    declare @tablaDatos as table(NomPersona nvarchar(50),Asignados int,CantNoResueltos int,CantResueltos int)
                    DECLARE cursor_Usr CURSOR
                    FOR SELECT DISTINCT usuarioResuelve FROM MuestraRefactura where Ciclo={f.Ciclo} {where} 

                    OPEN cursor_Usr;
                    FETCH NEXT FROM cursor_Usr INTO @UsrResuelve

                    WHILE @@FETCH_STATUS = 0
                    BEGIN
                    DECLARE @Asignados int, @NoResueltos decimal(18,2),@Resueltos decimal(18,2)
                    ,@noresporcent decimal(18,2),@resueltoporcent decimal(18,2), @NomPersona nvarchar(50)

                    set @Asignados = (SELECT count(*) FROM MuestraRefactura where Ciclo={f.Ciclo} {where}  and usuarioResuelve = @UsrResuelve)
                    set @NoResueltos = (SELECT count(*) FROM MuestraRefactura where Ciclo={f.Ciclo} {where}  and usuarioResuelve = @UsrResuelve and Resuelto=0)
                    set @Resueltos = (SELECT count(*) FROM MuestraRefactura where Ciclo={f.Ciclo} {where}  and usuarioResuelve = @UsrResuelve and Resuelto=1)
                    set @NomPersona = (SELECT DISTINCT isnull(b.NomPersona,a.usuarioResuelve) FROM MuestraRefactura a 
                    left join Usuarios b on a.usuarioResuelve=b.Usuario where a.usuarioResuelve=@UsrResuelve)

                    insert into @tablaDatos
                    select @NomPersona,@Asignados,@NoResueltos,@Resueltos

                    FETCH NEXT FROM cursor_Usr INTO @UsrResuelve
                    END;

                    CLOSE cursor_Usr;
                    DEALLOCATE cursor_Usr;
                    select * from @tablaDatos";
            datos.SeriesDatosRefa = SqlMapper.Query<SerieControl>(conStrSicra, sql).ToList();

            return datos;
        }
    }
}
