﻿using Dapper;
using DevExpress.DataProcessing;
using Newtonsoft.Json;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace SICRA.Controllers
{
    public class SoportesController : Controller
    {
        private static readonly DbConnection conStrSicra = Fun.GetConnectionSQL("SicraModel");
        private static readonly DbConnection ConnINCMS = Fun.GetConnectionORA("INCMSBD");
        
        public List<ObjSoporteRefacturacion> GetSoportesFacturacionWs(string nisrad, string periodo, bool EsRefarefa)
        {
            try
            {
                var listaSoportes = new List<ObjSoporteRefacturacion>();

                if (EsRefarefa)
                {
                    listaSoportes = GetData4(nisrad, periodo);
                }
                else
                {
                    listaSoportes = GetData1(nisrad, periodo);
                }
                return listaSoportes;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private List<ObjSoporteRefacturacion> GetData4(string nisrad, string periodo)
        {
            try
            {
                string sql = $@"select * from
                            (
                            SELECT ms.nis_rad, ms.nombre_cliente,(ms.ruta ||  '' || ms.num_itin || '' || ms.acometida)as ubicacion,t.desc_tar as nom_tarifa,
                            r.sec_rec,R.SEC_NIS, R.F_FACT,r.f_fact_anul,r.sec_rec_anul, R.IMP_TOT_REC,H.CSMO_FACT, H.CSMO_REACT, H.POT_FACT, H.POT_LEIDA,LEVEL,
                            R.EST_ACT, R.F_FACT_ANT,
                            --CONCAT(SUBSTR(r.periodo_fact,3,2),SUBSTR(r.periodo_fact,5,2))cicloFact
                            to_char(r.f_fact,'yymm')cicloFact
                            FROM RECIBOS R, HFACTURACION H,maestro_suscriptores ms,mtarifas t,sumcon s
                            WHERE 
                            /******************LLAVE COMPUESTA ENTRE RECIBOS Y HFACTURACION********************/
                            R.NIS_RAD = H.NIS_RAD
                            AND R.SEC_REC = H.SEC_REC
                            AND R.SEC_NIS = H.SEC_NIS
                            AND R.F_FACT = H.F_FACT
                            /**********************************************************************************/
                            AND ms.nis_rad = R.NIS_RAD
                            AND s.nis_rad = ms.NIS_RAD
                            and s.cod_tar=t.cod_tar
                            --START WITH  R.PERIODO_FACT = '{periodo}' comentado para cambio de campo
                            START WITH  to_char(r.f_fact,'yyyymm') = '{periodo}'
                            AND R.F_FACT_ANUL = TO_DATE('29991231','yyyymmdd')
                            AND R.TIP_REC = 'TR010'
                            AND R.NIS_RAD = {nisrad}
                            connect by prior r.nis_rad = r.nis_Rad 
                            and prior r.sec_nis = r.sec_nis 
                            and prior r.sec_rec = r.sec_rec_anul 
                            and prior r.f_fact = r.f_fact_anul
                            order by LEVEL asc)";

                var soportes = SqlMapper.Query<ObjSoporteRefacturacion>(ConnINCMS, sql, commandTimeout: 0).ToList();

                if (soportes.Count() > 0)
                {
                    foreach (var soporte in soportes)
                    {
                        ///************************LECTURAS************************///

                        string mes = soporte.f_fact.Month.ToString().PadLeft(2, '0');
                        string dia = soporte.f_fact.Day.ToString().PadLeft(2, '0');

                        sql = $@"SELECT a.nis_rad,a.tip_csmo,a.lect,a.f_lect,a.csmo,a.cte,
                            a.tip_lect,a.f_fact,a.sec_rec,a.num_rue
                            FROM apmedida_co a
                            where a.nis_rad={soporte.nis_rad}
                            AND a.SEC_REC = {soporte.sec_rec}
                            AND a.F_FACT = to_date('{soporte.f_fact.Year}{mes}{dia}','yyyymmdd')";

                        try
                        {
                            var soporte_lect = SqlMapper.Query<SoporteRefacturacion_lecturas>(ConnINCMS, sql, commandTimeout: 0).ToList();
                            if (soporte_lect != null)
                            {
                                soporte.soporteRefacturacion_Lecturas = soporte_lect.ToList();
                            }
                        }
                        catch (Exception ex)
                        {
                            soporte.soporteRefacturacion_Lecturas = null;
                        }

                        sql = $@"SELECT a.sec_rec,a.nis_rad,a.sec_nis,a.f_fact,a.co_concepto
                                ,a.csmo_fact,SUM(a.imp_concepto) AS imp_concepto, b.desc_cod
                                 FROM imp_concepto a
                                inner join codigos b on a.co_concepto = b.cod
                                where a.nis_rad = {soporte.nis_rad}
                                AND a.SEC_REC = {soporte.sec_rec}
                                AND a.SEC_NIS = {soporte.sec_nis}
                                AND a.F_FACT = to_date('{soporte.f_fact.Year}{mes}{dia}', 'yyyymmdd')
                                group by a.sec_rec,a.nis_rad,a.sec_nis,a.f_fact,a.co_concepto
                                ,a.csmo_fact,b.desc_cod
                                order by b.desc_cod";

                        try
                        {
                            var soporte_imp = SqlMapper.Query<SoporteRefacturacion_importes>(ConnINCMS, sql, commandTimeout: 0).ToList();
                            if (soporte_imp != null)
                            {
                                soporte.soporteRefacturacion_Importes = soporte_imp.ToList();
                            }
                        }
                        catch (Exception ex)
                        {
                            soporte.soporteRefacturacion_Lecturas = null;
                        }
                    }
                }

                return soportes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private List<ObjSoporteRefacturacion> GetData1(string nisrad, string periodo)
        {
            try
            {
                string sql = $@"select * from
                            (
                            SELECT ms.nis_rad, ms.nombre_cliente,(ms.ruta ||  '' || ms.num_itin || '' || ms.acometida)as ubicacion,t.desc_tar as nom_tarifa,
                            r.sec_rec,R.SEC_NIS, R.F_FACT,r.f_fact_anul,r.sec_rec_anul, R.IMP_TOT_REC,H.CSMO_FACT, H.CSMO_REACT, H.POT_FACT, H.POT_LEIDA,LEVEL,
                            R.EST_ACT, R.F_FACT_ANT,
                            --CONCAT(SUBSTR(r.periodo_fact,3,2),SUBSTR(r.periodo_fact,5,2))cicloFact
                            to_char(r.f_fact,'yymm')cicloFact
                            FROM RECIBOS R, HFACTURACION H,maestro_suscriptores ms,mtarifas t,sumcon s
                            WHERE 
                            /******************LLAVE COMPUESTA ENTRE RECIBOS Y HFACTURACION********************/
                            R.NIS_RAD = H.NIS_RAD
                            AND R.SEC_REC = H.SEC_REC
                            AND R.SEC_NIS = H.SEC_NIS
                            AND R.F_FACT = H.F_FACT
                            /**********************************************************************************/
                            AND ms.nis_rad = R.NIS_RAD
                            AND s.nis_rad = ms.NIS_RAD
                            and s.cod_tar=t.cod_tar
                            --START WITH  R.PERIODO_FACT = '{periodo}' comentado para cambio de campo
                            START WITH  to_char(r.f_fact,'yyyymm') = '{periodo}'
                            AND R.F_FACT_ANUL = TO_DATE('29991231','yyyymmdd')
                            AND R.TIP_REC = 'TR010'
                            AND R.NIS_RAD = {nisrad}
                            connect by prior r.nis_rad = r.nis_Rad 
                            and prior r.sec_nis = r.sec_nis 
                            and prior r.sec_rec = r.sec_rec_anul 
                            and prior r.f_fact = r.f_fact_anul
                            order by LEVEL desc)
                            where ROWNUM <= 2";

                var soportes = SqlMapper.Query<ObjSoporteRefacturacion>(ConnINCMS, sql, commandTimeout: 0).ToList();

                if (soportes.Count() > 0)
                {
                    foreach (var soporte in soportes)
                    {
                        ///************************LECTURAS************************///

                        string mes = soporte.f_fact.Month.ToString().PadLeft(2, '0');
                        string dia = soporte.f_fact.Day.ToString().PadLeft(2, '0');

                        sql = $@"SELECT a.nis_rad,a.tip_csmo,a.lect,a.f_lect,a.csmo,a.cte,
                            a.tip_lect,a.f_fact,a.sec_rec,a.num_rue
                            FROM apmedida_co a
                            where a.nis_rad={soporte.nis_rad}
                            AND a.SEC_REC = {soporte.sec_rec}
                            AND a.F_FACT = to_date('{soporte.f_fact.Year}{mes}{dia}','yyyymmdd')";

                        try
                        {
                            var soporte_lect = SqlMapper.Query<SoporteRefacturacion_lecturas>(ConnINCMS, sql, commandTimeout: 0).ToList();
                            if (soporte_lect != null)
                            {
                                soporte.soporteRefacturacion_Lecturas = soporte_lect.ToList();
                            }
                        }
                        catch (Exception ex)
                        {
                            soporte.soporteRefacturacion_Lecturas = null;
                        }

                        sql = $@"SELECT a.sec_rec,a.nis_rad,a.sec_nis,a.f_fact,a.co_concepto
                                ,a.csmo_fact,SUM(a.imp_concepto) AS imp_concepto, b.desc_cod
                                 FROM imp_concepto a
                                inner join codigos b on a.co_concepto = b.cod
                                where a.nis_rad = {soporte.nis_rad}
                                AND a.SEC_REC = {soporte.sec_rec}
                                AND a.SEC_NIS = {soporte.sec_nis}
                                AND a.F_FACT = to_date('{soporte.f_fact.Year}{mes}{dia}', 'yyyymmdd')
                                group by a.sec_rec,a.nis_rad,a.sec_nis,a.f_fact,a.co_concepto
                                ,a.csmo_fact,b.desc_cod
                                order by b.desc_cod";

                        try
                        {
                            var soporte_imp = SqlMapper.Query<SoporteRefacturacion_importes>(ConnINCMS, sql, commandTimeout: 0).ToList();
                            if (soporte_imp != null)
                            {
                                soporte.soporteRefacturacion_Importes = soporte_imp.ToList();
                            }
                        }
                        catch (Exception ex)
                        {
                            soporte.soporteRefacturacion_Lecturas = null;
                        }
                    }
                }

                return soportes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        /// <summary>
        /// metodo devuelve importes
        /// </summary>
        /// <param name="soportes">Parametro de lista de soportes JSON</param>
        /// <returns></returns>
        public DataSet GetImportesBySoporte(List<ObjSoporteRefacturacion> soportes)
        {
            try
            {
                var listaTotal = new List<ListadoImportes>();

                var listaFactura = soportes
                    .Where(s => s.level == (soportes.Min(x => x.level)))
                    .Select(s => s.soporteRefacturacion_Importes)
                    .FirstOrDefault();

                var listaFactura2 = (from l1 in listaFactura
                                     group l1 by l1.desc_cod into g
                                     select new
                                     {
                                         NomImporte = g.Key,
                                         valor = g.Sum(x => x.imp_concepto)
                                     });



                var listaRefactura = soportes
                    .Where(s => s.level == (soportes.Max(x => x.level)))
                    .Select(s => s.soporteRefacturacion_Importes)
                    .FirstOrDefault();

                var listaRefactura2 = (from l2 in listaRefactura
                                       group l2 by l2.desc_cod into g
                                     select new
                                     {
                                         NomImporte = g.Key,
                                         valor = g.Sum(x => x.imp_concepto)
                                     });





                //listado iguales
                var listaIn = (from l1 in listaFactura2
                               join l2 in listaRefactura2 on l1.NomImporte equals l2.NomImporte
                               select new
                               {
                                   importe = l1.NomImporte,
                                   corregido = l2.valor,
                                   facturado = l1.valor,
                                   diferencia = l2.valor - l1.valor
                               })
                               .ToList();

                               //group new { l1, l2 } by new { l1.desc_cod } into t
                               //select new
                               //{
                               //    importe = t.Key.desc_cod,
                               //    corregido = t.Sum(x => x.l1.imp_concepto),
                               //    facturado = t.Sum(x => x.l2.imp_concepto),
                               //    diferencia = t.Sum(x => x.l1.imp_concepto) - t.Sum(x => x.l2.imp_concepto)
                               //})
                               //.ToList();


                //select new
                //{
                //    importe = l2.desc_cod,
                //    corregido = l2.imp_concepto,
                //    facturado = l1.imp_concepto,
                //    diferencia = l2.imp_concepto - l1.imp_concepto
                //})
                //.GroupJoin()

                foreach (var x in listaIn)
                {
                    listaTotal.Add(
                    new ListadoImportes()
                    {
                        Importe = x.importe,
                        Corregido = x.corregido,
                        Facturado = x.facturado,
                        Diferencia = x.diferencia
                    });
                }


                var listaNot1 = listaFactura
                    .Where(p => !listaRefactura.Any(p2 => p2.co_concepto.Equals(p.co_concepto)))
                    .ToList();
                foreach (var x in listaNot1)
                {
                    listaTotal.Add(
                    new ListadoImportes()
                    {
                        Importe = x.desc_cod,
                        Corregido = 0,
                        Facturado = x.imp_concepto,
                        Diferencia = 0 - x.imp_concepto
                    });
                }


                var listaNot2 = listaRefactura
                    .Where(p => !listaFactura.Any(p2 => p2.co_concepto.Equals(p.co_concepto)))
                    .ToList();
                foreach (var x in listaNot2)
                {
                    listaTotal.Add(
                    new ListadoImportes()
                    {
                        Importe = x.desc_cod,
                        Corregido = x.imp_concepto,
                        Facturado = 0,
                        Diferencia = x.imp_concepto
                    });
                }

                


                DataTable tabla = new DataTable("soportes");
                tabla.Columns.Add("dato1");
                tabla.Columns.Add("dato2");
                tabla.Columns.Add("dato3");
                tabla.Columns.Add("dato4");

                //Importe , Corregido , Facturado , Diferencia
                foreach (var i in listaTotal.OrderBy(x=>x.Importe))
                {
                    tabla.Rows.Add(i.Importe.ToUpper(),i.Corregido,i.Facturado,i.Diferencia);
                }

                tabla.Rows.Add(
                    "TOTAL"
                    , listaTotal.Sum(x=> x.Corregido)
                    , listaTotal.Sum(x => x.Facturado)
                    , listaTotal.Sum(x => x.Diferencia)
                    );

                DataSet dsSoportes = new DataSet("dsSoportes");
                dsSoportes.Tables.Add(tabla);

                return dsSoportes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //---------------->  funcion de prueba toma solo importes del ciclo
        public DataSet GetImportesBySoporte(List<ObjSoporteRefacturacion> soportes,int ciclo)
        {
            try
            {
                var listaTotal = new List<ListadoImportes>();

                var listaFactura = soportes
                    .Where(s => s.level == (soportes.Min(x => x.level)))
                    .Select(s => s.soporteRefacturacion_Importes)
                    .FirstOrDefault();


                var listaRefactura = soportes
                    .Where(s => s.level == (soportes.Max(x => x.level)))
                    .Select(s => s.soporteRefacturacion_Importes)
                    .FirstOrDefault();

                //listado iguales
                var listaIn = (from l1 in listaFactura
                               join l2 in listaRefactura on l1.co_concepto equals l2.co_concepto
                               select new
                               {
                                   importe = l2.desc_cod,
                                   corregido = l2.imp_concepto,
                                   facturado = l1.imp_concepto,
                                   diferencia = l2.imp_concepto - l1.imp_concepto
                               }).ToList();

                foreach (var x in listaIn)
                {
                    listaTotal.Add(
                    new ListadoImportes()
                    {
                        Importe = x.importe,
                        Corregido = x.corregido,
                        Facturado = x.facturado,
                        Diferencia = x.diferencia
                    });
                }


                var listaNot1 = listaFactura
                    .Where(p => !listaRefactura.Any(p2 => p2.co_concepto.Equals(p.co_concepto)))
                    .ToList();
                foreach (var x in listaNot1)
                {
                    listaTotal.Add(
                    new ListadoImportes()
                    {
                        Importe = x.desc_cod,
                        Corregido = 0,
                        Facturado = x.imp_concepto,
                        Diferencia = 0 - x.imp_concepto
                    });
                }


                var listaNot2 = listaRefactura
                    .Where(p => !listaFactura.Any(p2 => p2.co_concepto.Equals(p.co_concepto)))
                    .ToList();
                foreach (var x in listaNot2)
                {
                    listaTotal.Add(
                    new ListadoImportes()
                    {
                        Importe = x.desc_cod,
                        Corregido = x.imp_concepto,
                        Facturado = 0,
                        Diferencia = x.imp_concepto
                    });
                }




                DataTable tabla = new DataTable("soportes");
                tabla.Columns.Add("dato1");
                tabla.Columns.Add("dato2");
                tabla.Columns.Add("dato3");
                tabla.Columns.Add("dato4");

                //Importe , Corregido , Facturado , Diferencia
                foreach (var i in listaTotal.OrderBy(x => x.Importe))
                {
                    tabla.Rows.Add(i.Importe.ToUpper(), i.Corregido, i.Facturado, i.Diferencia);
                }

                tabla.Rows.Add(
                    "TOTAL"
                    , listaTotal.Sum(x => x.Corregido)
                    , listaTotal.Sum(x => x.Facturado)
                    , listaTotal.Sum(x => x.Diferencia)
                    );

                DataSet dsSoportes = new DataSet("dsSoportes");
                dsSoportes.Tables.Add(tabla);

                return dsSoportes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        /// <summary>
        /// metodo devuelve importes
        /// </summary>
        /// <param name="soportes">Parametro cuando ya esta guardado en base de datos</param>
        /// <returns>Devuelve importes cuando ya estan guardados en base de datos</returns>
        public DataSet GetImportesBySoporte(SoporteRefactura soportes)
        {
            try
            {
                var listaImportes = soportes.SoporteRefacturaDetalle;

                DataTable tabla = new DataTable("soportes");
                tabla.Columns.Add("dato1");
                tabla.Columns.Add("dato2");
                tabla.Columns.Add("dato3");
                tabla.Columns.Add("dato4");

                //Importe , Corregido , Facturado , Diferencia
                foreach (var i in listaImportes.OrderBy(x => x.importe))
                {
                    tabla.Rows.Add(i.importe.ToUpper(), i.corregido, i.facturado, i.diferencia);
                }

                tabla.Rows.Add(
                    "TOTAL"
                    , listaImportes.Sum(x => x.corregido)
                    , listaImportes.Sum(x => x.facturado)
                    , listaImportes.Sum(x => x.diferencia)
                    );

                DataSet dsSoportes = new DataSet("dsSoportes");
                dsSoportes.Tables.Add(tabla);

                return dsSoportes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public RespMsj PutSoporteRefactura(SoporteRefactura soporteFull)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    db.Entry(soporteFull).State = soporteFull.idSoporte == 0 ? EntityState.Added : EntityState.Modified;
                    //db.Entry(soporteFull).State = soporteFull.idSoporte == 0 ? EntityState.Added : EntityState.Unchanged;

                    if (db.SaveChanges() > 0)
                    {
                        if (soporteFull.idSoporte == 0)
                        {
                            return new RespMsj { estado = "ok", mensaje = "Soporte Guardado Correctamente.!" };
                        }
                        else
                        {
                            return new RespMsj { estado = "ok", mensaje = "Soporte Actualizado Correctamente.!" };
                        }
                    }
                    else
                    {
                        return new RespMsj { estado = "error", mensaje = "Error al guardar Soporte." };
                    }
                }
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                return new RespMsj { estado = "error", mensaje = $"Ocurrió una excepción: {msj}" };
            }

        }

        public bool VerificarExisteSoporte(decimal nisrad, int cicloFact)
        {
            using (var db = new SicraModel())
            {
                var existe = db.SoporteRefactura
                    .Where(x => x.nisrad == nisrad && x.cicloFact == cicloFact)
                    .FirstOrDefault();

                if (existe!=null)
                {
                    return true;
                }
            }
            return false;
        }

        public SoporteRefactura GetSoporteFacturacionBd(decimal nisrad, int cicloFact)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    var soporte = db.SoporteRefactura.Include("SoporteRefacturaDetalle")
                        .Where(x => x.nisrad == nisrad && x.cicloFact == cicloFact)
                        .FirstOrDefault();
                    return soporte;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public SoporteRefactura GetSoporteFacturacionBd(int idSoporte)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    var soporte = db.SoporteRefactura.Include("SoporteRefacturaDetalle")
                        .Where(s => s.idSoporte == idSoporte)
                        .FirstOrDefault();
                    return soporte;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<SoporteRefactura> GetSoportesFacturacionBd(decimal nisrad, string usr)
        {
            try
            {
                //using (var db = new SicraModel())
                //{
                //    var soportes = db.SoporteRefactura.Include("SoporteRefacturaDetalle")
                //        .Where(x => x.nisrad == nisrad && x.CodAgrupacion==null)
                //        .OrderByDescending(x=>x.cicloFact).ThenBy(x => x.Orden)
                //        .ToList();
                //    return soportes;
                //}

                string sql = "";
                sql = $@"select * 
                        from SoporteRefactura 
                        where nisrad={nisrad} and nomUsuario='{usr}'
                        and CodAgrupacion is null
                        order by cicloFact desc, Orden desc";

                var soportes = SqlMapper.Query<SoporteRefactura>(conStrSicra, sql).ToList();
                return soportes;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int GetConteoSoporteSubidoByNisrad(decimal nis)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    var conteo = db.BitacoraCargaSoporte.Where(x => x.nisrad == nis).Count();

                    return conteo;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public List<BitacoraCargaSoporte> GetSoporteSubidoByNisrad(decimal nis)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    var lista = db.BitacoraCargaSoporte.Where(x => x.nisrad == nis).ToList();
                    return lista;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GetFirmaByIdSoporte(int idSoporte, string path)
        {
            try
            {
                string sql = "";
                sql = $@"select c.firma
                        from SoporteRefactura a
                        join Usuarios b on a.idUsuario=b.idUsuario
                        left join FirmaUsuario c on b.idUsuario=c.idUsuario
                        where idSoporte={idSoporte}";

                var firma = SqlMapper.Query<string>(conStrSicra, sql).FirstOrDefault();
                string nombreArchivo = $"{path}Firma\\{firma}";

                return nombreArchivo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //-------------refa varios---------------------------
        public bool VerificarExisteSoporte(ObjSoporteRefacturacion soporte)
        {

            using (var db = new SicraModel())
            {
                var existe = db.SoporteRefactura
                    .Where(x => x.nisrad == soporte.nis_rad 
                    && x.cicloFact == soporte.cicloFact
                    && x.SecNis==soporte.sec_nis
                    && x.SecRec==soporte.sec_rec
                    && x.FFact==soporte.f_fact)
                    .FirstOrDefault();

                if (existe != null)
                {
                    return true;
                }
            }
            return false;
        }

        public DataSet GetImportesBySoporte(List<ObjSoporteRefacturacion> soportes,int maxLvl,int minLvl)
        {
            try
            {
                var listaTotal = new List<ListadoImportes>();

                var listaFactura = soportes
                    .Where(s => s.level == minLvl)
                    .Select(s => s.soporteRefacturacion_Importes)
                    .FirstOrDefault();


                var listaRefactura = soportes
                    .Where(s => s.level == maxLvl)
                    .Select(s => s.soporteRefacturacion_Importes)
                    .FirstOrDefault();

                //listado iguales
                var listaIn = (from l1 in listaFactura
                               join l2 in listaRefactura on l1.co_concepto equals l2.co_concepto
                               select new
                               {
                                   importe = l2.desc_cod,
                                   corregido = l2.imp_concepto,
                                   facturado = l1.imp_concepto,
                                   diferencia = l2.imp_concepto - l1.imp_concepto
                               }).ToList();

                foreach (var x in listaIn)
                {
                    listaTotal.Add(
                    new ListadoImportes()
                    {
                        Importe = x.importe,
                        Corregido = x.corregido,
                        Facturado = x.facturado,
                        Diferencia = x.diferencia
                    });
                }


                var listaNot1 = listaFactura
                    .Where(p => !listaRefactura.Any(p2 => p2.co_concepto.Equals(p.co_concepto)))
                    .ToList();
                foreach (var x in listaNot1)
                {
                    listaTotal.Add(
                    new ListadoImportes()
                    {
                        Importe = x.desc_cod,
                        Corregido = 0,
                        Facturado = x.imp_concepto,
                        Diferencia = (0 - x.imp_concepto)
                    });
                }


                var listaNot2 = listaRefactura
                    .Where(p => !listaFactura.Any(p2 => p2.co_concepto.Equals(p.co_concepto)))
                    .ToList();
                foreach (var x in listaNot2)
                {
                    listaTotal.Add(
                    new ListadoImportes()
                    {
                        Importe = x.desc_cod,
                        Corregido = x.imp_concepto,
                        Facturado = 0,
                        Diferencia = x.imp_concepto
                    });
                }




                DataTable tabla = new DataTable("soportes");
                tabla.Columns.Add("dato1");
                tabla.Columns.Add("dato2");
                tabla.Columns.Add("dato3");
                tabla.Columns.Add("dato4");

                //Importe , Corregido , Facturado , Diferencia
                foreach (var i in listaTotal.OrderBy(x => x.Importe))
                {
                    tabla.Rows.Add(i.Importe.ToUpper(), i.Corregido, i.Facturado, i.Diferencia);
                }

                tabla.Rows.Add(
                    "TOTAL"
                    , listaTotal.Sum(x => x.Corregido)
                    , listaTotal.Sum(x => x.Facturado)
                    , listaTotal.Sum(x => x.Diferencia)
                    );

                DataSet dsSoportes = new DataSet("dsSoportes");
                dsSoportes.Tables.Add(tabla);

                return dsSoportes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public RespMsj PutBitacoraSoporte(BitacoraCargaSoporte bitacora)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    db.Entry(bitacora).State = bitacora.Id == 0 ? EntityState.Added : EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        return new RespMsj { estado = "ok", mensaje = "soporte subido Correctamente.!" };
                    }
                    else
                    {
                        return new RespMsj { estado = "error", mensaje = "Error al subir Soporte." };
                    }
                }
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                return new RespMsj { estado = "error", mensaje = $"Ocurrió una excepción: {msj}" };
            }
        }
    }
}
