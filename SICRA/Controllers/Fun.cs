﻿using Dapper;
using SICRA.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
//using ModuloRCM.DataTransferObject;

namespace SICRA.Controllers
{
    public class Fun
    {
        private static readonly DbConnection conStrSicra = GetConnectionSQL("SicraModel");

        /// <summary>
        /// Funcion para generar una consulta a base de datos enviado como parametro strind la consulta a realizar.
        /// </summary>
        /// <param name="pQuery"></param>
        /// <param name="pCmdTimeOut"></param>
        /// <param name="pConnectionString"></param>
        /// <returns>Retorna un dataset como respuesta</returns>
        public static DataSet GetData(string pQuery, int pCmdTimeOut = 30, string pConnectionString = "SAConn")
        {
            DataSet ds = new DataSet();
            string conStr = "";
            conStr=ConfigurationManager.ConnectionStrings[pConnectionString].ToString();

            SqlConnection sqlCon=new SqlConnection(conStr);
            sqlCon.Open();
            SqlCommand sqlComm=new SqlCommand(pQuery,sqlCon);

            SqlDataAdapter sqlAdap = new SqlDataAdapter(sqlComm);
            sqlAdap.SelectCommand.CommandTimeout = pCmdTimeOut;
            sqlAdap.Fill(ds);
            sqlCon.Close();
            sqlCon.Dispose();
            return ds;
        }

        public static DataSet GetPaData(string paName,List<ObjClass> parametros, int pCmdTimeOut = 30, string pConnectionString = "SAConn")
        {
            DataSet ds = new DataSet();
            string conStr = "";
            conStr = ConfigurationManager.ConnectionStrings[pConnectionString].ToString();

            SqlConnection sqlCon = new SqlConnection(conStr);
            sqlCon.Open();
            SqlCommand sqlComm = new SqlCommand(paName, sqlCon);
            sqlComm.CommandType = CommandType.StoredProcedure;

            foreach (ObjClass objeto in parametros)
            {
                sqlComm.Parameters.Add(new SqlParameter("@" + objeto.Texto1, objeto.Texto2));
            }
            
            SqlDataAdapter sqlAdap = new SqlDataAdapter(sqlComm);
            sqlAdap.SelectCommand.CommandTimeout = pCmdTimeOut;
            sqlAdap.Fill(ds);
            sqlCon.Close();
            sqlCon.Dispose();
            return ds;
        }

        public static string ExecPaQuery(string paName, List<ObjClass> parametros, int pCmdTimeOut = 30, string pConnectionString = "SAConn")
        {
            string conStr = "";
            int result = 0;
            conStr = ConfigurationManager.ConnectionStrings[pConnectionString].ToString();
            string estado = "";

            using (SqlConnection con = new SqlConnection(conStr))
            {
                using (SqlCommand cmd = new SqlCommand(paName, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (ObjClass objeto in parametros)
                    {
                        cmd.Parameters.Add(new SqlParameter("@" + objeto.Texto1, objeto.Texto2));
                    }

                    con.Open();
                    result = cmd.ExecuteNonQuery();
                    estado = result > 0 ? "ok" : "error";
                }
            }
            return estado;
        }

        public static List<ObjClass> ExecPaQueryResult(string paName, List<SqlParameter> parametros, List<SqlParameter> parametrosOutput, int pCmdTimeOut = 300, string pConnectionString = "SegConn")
        {
            string conStr = "";
            int result = 0;
            conStr = ConfigurationManager.ConnectionStrings[pConnectionString].ToString();
            string estado = "";
            List<ObjClass> lst = new List<ObjClass>();

            using (SqlConnection con = new SqlConnection(conStr))
            {
                using (SqlCommand cmd = new SqlCommand(paName, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = pCmdTimeOut;
                    foreach (SqlParameter paramIn in parametros)
                    {
                        cmd.Parameters.Add(paramIn);
                    }
                    foreach (SqlParameter paramOut in parametrosOutput)
                    {
                        cmd.Parameters.Add(paramOut).Direction = ParameterDirection.Output;                        
                    }
                   
                    con.Open();
                    result = cmd.ExecuteNonQuery();
                    estado = result > 0 ? "ok" : "error";

                    
                    foreach (SqlParameter paramOut in parametrosOutput)
                    {
                        lst.Add(new ObjClass() { Texto1 = paramOut.ParameterName, Texto2 = Convert.ToString(cmd.Parameters[paramOut.ParameterName].Value.ToString()) });
                    }
                  
                }
                                
            }

            return lst;

        }

        public static ObjetoGenerico RetornarCamposIdNombreCualquierTabla(string nombreTabla)
        {
            string qry ="select COLUMN_NAME as id,'Nombre' as nombre From information_schema.key_column_usage WHERE TABLE_NAME = '"+ nombreTabla + "'";
            string cs = "SAConn";
            ObjetoGenerico campos =new ObjetoGenerico();

            DataSet ds = Fun.GetData(qry,30,cs);
            campos = (from DataRow row in ds.Tables[0].Rows
                    select new ObjetoGenerico
                    {
                        Dato1 = nombreTabla,
                        Dato2 = row["id"].ToString(),
                        Dato3 = row["Nombre"].ToString()
                    }).FirstOrDefault();
        
        

            return campos;
        }

        public static string ExecQuery(string pQuery, int pCmdTimeOut = 30, string pConnectionString = "SAConn")
        {

            int result = 0;
            string estado = "";
            string conStr = "";
            conStr = ConfigurationManager.ConnectionStrings[pConnectionString].ToString();
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                using (SqlCommand command = new SqlCommand(pQuery, connection))
                {
                    connection.Open();
                    result = command.ExecuteNonQuery();
                    estado = result > 0 ? "ok" : "error";
                }
            }
            return estado;
        }

        public static string CodeKeyStringEncode(string value, string key)
        {
            string ReturnValue = null;
            System.Security.Cryptography.MACTripleDES mac3des = new System.Security.Cryptography.MACTripleDES();
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            mac3des.Key = md5.ComputeHash(System.Text.Encoding.UTF32.GetBytes(key));

            ReturnValue = Convert.ToBase64String(System.Text.Encoding.UTF32.GetBytes(value)) + '-' + Convert.ToBase64String(mac3des.ComputeHash(System.Text.Encoding.UTF32.GetBytes(value)));

            return ReturnValue;
        }

        public static string CodeKeyStringDecode(string value, string key)
        {
            string dataValue = "";
            string calcHash = "";
            string storedHash = "";

            System.Security.Cryptography.MACTripleDES mac3des = new System.Security.Cryptography.MACTripleDES();
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            mac3des.Key = md5.ComputeHash(System.Text.Encoding.UTF32.GetBytes(key));

            try
            {
                dataValue = System.Text.Encoding.UTF32.GetString(Convert.FromBase64String(value.Split('-')[0]));
                storedHash = System.Text.Encoding.UTF32.GetString(Convert.FromBase64String(value.Split('-')[1]));
                calcHash = System.Text.Encoding.UTF32.GetString(mac3des.ComputeHash(System.Text.Encoding.UTF32.GetBytes(dataValue)));

                if (storedHash != calcHash)
                {
                    throw new ArgumentException("Información Invalida");
                }
            }
            catch (Exception ex)
            {
                //Throw New ArgumentException("Llave Invalida")
                return "Llave Invalida";
            }

            return dataValue;

        }

        public static string QueryStringEncode(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return "";
            }
            else
            {
                string encodeKey = ConfigurationManager.AppSettings["TokenSys"];
                return HttpUtility.UrlEncode(CodeKeyStringEncode(value, encodeKey));
            }
        }

        public static string QueryStringDecode(string value)
        {
            if (value == null)
            {
                return "";
            }
            else
            {
                string encodeKey = ConfigurationManager.AppSettings["TokenSys"];
                return HttpUtility.UrlDecode(CodeKeyStringDecode(value, encodeKey));
            }
        }

        public static string encrypt(string plainText, string Token)
        {
            string key = "";
            string iv = "";
            key = Token.Substring(0, 108);
            iv = Token.Substring(108, 64);
            byte[] _key = Convert.FromBase64String(key);
            byte[] _iv = Convert.FromBase64String(iv);
            int keySize = 32;
            int ivSize = 16;
            Array.Resize(ref _key, keySize);
            Array.Resize(ref _iv, ivSize);
            Rijndael RijndaelAlg = new RijndaelManaged();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, RijndaelAlg.CreateEncryptor(_key, _iv), CryptoStreamMode.Write);
            byte[] plainMessageBytes = UTF8Encoding.UTF8.GetBytes(plainText);
            cryptoStream.Write(plainMessageBytes, 0, plainMessageBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherMessageBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(cipherMessageBytes);
        }

        public static string decrypt(string plainText, string Token)
        {
            string key = "";
            string iv = "";
            key = Token.Substring(0, 108);
            iv = Token.Substring(108, 64);
            byte[] _key = Convert.FromBase64String(key);
            byte[] _iv = Convert.FromBase64String(iv);
            int keySize = 32;
            int ivSize = 16;
            Array.Resize(ref _key, keySize);
            Array.Resize(ref _iv, ivSize);
            byte[] cipherTextBytes = Convert.FromBase64String(plainText);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            Rijndael RijndaelAlg = new RijndaelManaged();
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, RijndaelAlg.CreateDecryptor(_key, _iv), CryptoStreamMode.Read);
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }

        public static void InsertSQLBulkCopy(DataTable csvFileData, string tablaDestino, string pConnectionString = "FactConn")
        {
            string conStr = "";

            if (string.IsNullOrEmpty(pConnectionString))
            {
                conStr = ConfigurationManager.ConnectionStrings["SICRAModel"].ConnectionString;
            }
            else
            {
                conStr = ConfigurationManager.ConnectionStrings[pConnectionString].ConnectionString;
            }

            using (SqlConnection dbConnection = new SqlConnection(conStr))
            {
                dbConnection.Open();
                using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                {
                    s.DestinationTableName = tablaDestino;

                    foreach (var column in csvFileData.Columns)
                        s.ColumnMappings.Add(column.ToString(), column.ToString());

                    s.WriteToServer(csvFileData);
                }
            }
        }

        public static DbConnection GetConnectionSQL(string _conn)
        {
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings[_conn].ConnectionString;
                var factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
                var conn = factory.CreateConnection();
                conn.ConnectionString = connectionString;
                conn.Open();
                return conn;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static DbConnection GetConnectionORA(string _conn)
        {
            try
            {
                string connectionString = ConfigurationManager.AppSettings[_conn].ToString();
                var factory = DbProviderFactories.GetFactory("Oracle.DataAccess.Client");
                var conn = factory.CreateConnection();
                conn.ConnectionString = connectionString;
                conn.Open();
                return conn;
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }

        public static string Serialize<T>(T dataToSerialize)
        {
            try
            {
                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }
            catch
            {
                throw;
            }
        }

        public static T Deserialize<T>(string xmlText)
        {
            try
            {
                var stringReader = new System.IO.StringReader(xmlText);
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// METODO PARA LIMPIAR CAMPOS DE UN FORMULARIO
        /// </summary>
        /// <param name="parent">THIS -> webform </param>
        public static void ClearControls(Control parent)
        {
            foreach (Control c in parent.Controls)
            {
                if ((c.GetType() == typeof(TextBox)))  //Clear TextBox
                {
                    ((TextBox)(c)).Text = "";
                }
                if ((c.GetType() == typeof(DropDownList)))  //Clear DropDownList
                {
                    ((DropDownList)(c)).ClearSelection();
                }
                if ((c.GetType() == typeof(CheckBox)))  //Clear CheckBox
                {
                    ((CheckBox)(c)).Checked = false;
                }
                if ((c.GetType() == typeof(CheckBoxList)))  //Clear CheckBoxList
                {
                    ((CheckBoxList)(c)).ClearSelection();
                    ((CheckBoxList)(c)).Items.Clear();
                }
                if ((c.GetType() == typeof(RadioButton)))  //Clear RadioButton
                {
                    ((RadioButton)(c)).Checked = false;
                }
                if ((c.GetType() == typeof(RadioButtonList)))  //Clear RadioButtonList
                {
                    ((RadioButtonList)(c)).ClearSelection();
                }
                if ((c.GetType() == typeof(HiddenField)))  //Clear HiddenField
                {
                    ((HiddenField)(c)).Value = "";
                }
                if ((c.GetType() == typeof(Label)))  //Clear Label
                {
                    ((Label)(c)).Text = "";
                }
                if (c.HasControls())
                {
                    ClearControls(c);
                }
            }
        }
        public static int GetCicloActual()
        {
            string sql = "select CONCAT(FORMAT(GETDATE(),'yy'),FORMAT(GETDATE(),'MM'))";
            var cicloFact = SqlMapper.Query<int>(conStrSicra, sql).FirstOrDefault();
            return cicloFact;
        }
        public static DateTime? FormatearFecha(string fecha)
        {
            try
            {
                DateTime date;
                date = DateTime.ParseExact(fecha,"dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                return date;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static void HideControls(Control parent)
        {
            foreach (Control c in parent.Controls)
            {
                c.Visible = false;
            }
        }
    }
}