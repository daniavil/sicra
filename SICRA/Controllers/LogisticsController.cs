﻿using SICRA.Models.EEHLogistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SICRA.Controllers
{
    public class LogisticsController:Controller
    {
        /// <summary>
        /// Obtener Listado de Anomalias desde Logistics
        /// </summary>
        /// <returns></returns>
        public List<tipoLecturasAnomalia> GetListaAnomalias()
        {
            using (var db = new LogisticsModel())
            {
                var listaAnomalias = db.tipoLecturasAnomalia.ToList();
                return listaAnomalias;
            }
        }
    }
}