﻿using ClosedXML.Excel;
using Dapper;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.EEHLogistics;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace SICRA.Controllers
{
    public class RepoAnomaliaController : Controller
    {
        private static readonly DbConnection conStrSicra = Fun.GetConnectionSQL("SicraModel");
        private static readonly DbConnection conStrEehApps = Fun.GetConnectionSQL("SegConn");
        private UsuarioController _uCtrl = new UsuarioController();


        public REPOANOMALIA_M GetReporteAnomaliaByClave(decimal? nisrad, int ciclo, int tipoRepo)
        {
            using (var db = new SicraModel())
            {
                var reporte = db.REPOANOMALIA_M.Include("REPOANOMALIA_D")
                    .Where(m => m.Nis_Rad == nisrad && m.Ciclo == ciclo && m.esCritica == tipoRepo)
                    .FirstOrDefault();

                return reporte;
            }
        }

        public DateTime GetFechaResolucion(int tipo, int diasSum)
        {
            using (var db = new SicraModel())
            {
                var diasSumNul = 0;
                int diaCri = int.Parse(ConfigurationManager.AppSettings["dialResCritica"]);
                int diaRefa = int.Parse(ConfigurationManager.AppSettings["dialResRefa"]);
                int diaPostFact = int.Parse(ConfigurationManager.AppSettings["dialResPostFact"]);

                //refactura
                if (tipo == 0)
                {
                    diasSumNul = diaRefa;
                }
                //critica
                else if (tipo == 1)
                {
                    diasSumNul = diaCri;
                }
                //postfactura
                else if (tipo == 2)
                {
                    diasSumNul = diaPostFact;
                }

                string sql = $@"select isnull(max(convert(datetime,DATEADD(HOUR,23,CONVERT(VARCHAR(10),fecha_lectura,110)))),convert(datetime,DATEADD(day,{diasSumNul},( DATEADD(HOUR,23,CONVERT(VARCHAR(10),GETDATE(),110)))))) f_resol
                                from
                                (
                                    select distinct top {diasSum} convert(date,FECHA_LECTURA) fecha_lectura
                                    from InCMS.[dbo].[CALENDARIO_EEH]
                                    where convert(date,FECHA_LECTURA)> convert(date,GETDATE())
                                    order by convert(date,FECHA_LECTURA) asc
                                )res";
                var f_resol = SqlMapper.Query<DateTime>(conStrEehApps, sql).FirstOrDefault();

                return f_resol;
            }
        }

        public List<REPOANOMALIA_D> RepoDetailAnomaliaByIdM(long IdM)
        {
            using (var db = new SicraModel())
            {

                var detalle = db.REPOANOMALIA_D
                    .Where(d => d.IdRow_M == IdM)
                    .OrderByDescending(d => d.FechaResolucion)
                    .ToList();

                return detalle;
            }
        }

        public List<CategoriaResolucion> GetListaCateResolucion()
        {
            using (var db = new SicraModel())
            {

                var lista = db.CategoriaResolucion
                    .OrderBy(x => x.descripcion)
                    .ToList();

                return lista;
            }
        }

        public int GetCicloFactura()
        {
            using (var db = new SicraModel())
            {

                var ciclo = db.CicloFacturacion
                    .Where(x => x.Actual)
                    .FirstOrDefault();

                return ciclo.Ciclo;
            }
        }

        public RespMsj PutReporteAnomalia(REPOANOMALIA_M RepoMaster)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    bool GuardadoNuevo = false;

                    REPOANOMALIA_M exiteRepo = db.REPOANOMALIA_M
                        .Include("ImagenReporteAnomalia")
                        .Where(x => x.IdRow_M == RepoMaster.IdRow_M)
                        .FirstOrDefault();

                    if (exiteRepo != null)
                    {
                        db.Entry(exiteRepo).State = EntityState.Detached;

                        foreach (var repoDeta in RepoMaster.REPOANOMALIA_D)
                        {
                            db.Entry(repoDeta).State = repoDeta.IdRow_D == 0 ? EntityState.Added : EntityState.Unchanged;
                        }

                        try
                        {
                            foreach (var img in RepoMaster.ImagenReporteAnomalia)
                            {
                                db.Entry(img).State = !db.ImagenReporteAnomalia.Any(f => f.idRow == img.idRow) ? EntityState.Added : EntityState.Unchanged;
                            }
                        }
                        catch (Exception)
                        {

                        }

                        db.Entry(RepoMaster).State = EntityState.Modified;
                    }
                    else
                    {
                        db.Entry(RepoMaster).State = EntityState.Added;
                        GuardadoNuevo = true;
                    }

                    if (db.SaveChanges() > 0)
                    {
                        //***************Ejecutar SP EEHAPPS*******************
                        if (GuardadoNuevo)
                        {
                            try
                            {
                                var par = new List<ObjClass>();
                                par.Add(new ObjClass { Texto1 = "IdRow_M", Texto2 = RepoMaster.IdRow_M.ToString() });
                                var resp = Fun.ExecPaQuery("sp_crear_seguimiento_cuenta_sa", par, 0, "EEHAppsConn");
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        //***************Fin Ejecutar SP EEHAPPS***************
                        return new RespMsj { estado = "ok", mensaje = "Clave Reportada Correctamente.!" };
                    }
                    else
                    {
                        return new RespMsj { estado = "error", mensaje = "Error al Reportar Clave" };
                    }
                }
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                return new RespMsj { estado = "error", mensaje = $"Ocurrió una excepción: {msj}" };
            }

        }

        public ReporteClaveDTO GetRepoMaestroAnomaliaDTOById(long idM)
        {
            using (var db = new SicraModel())
            {
                using (var dbLogistics = new LogisticsModel())
                {
                    var repo = db.REPOANOMALIA_M
                        .Include("REPOANOMALIA_D")
                        .Where(r => r.IdRow_M == idM).FirstOrDefault();

                    int idAnomalia = int.Parse(repo.Anomalia);
                    string AnomaliaText = dbLogistics.tipoLecturasAnomalia
                        .Where(a => a.idLecturaAnomalia == idAnomalia)
                        .FirstOrDefault().descripcion;

                    string tiporepo = "";
                    if (repo.esCritica == 0)
                    {
                        tiporepo = "Refactura";
                    }
                    else if (repo.esCritica == 1)
                    {
                        tiporepo = "Crítica";
                    }
                    else if (repo.esCritica == 2)
                    {
                        tiporepo = "PostFacturación";
                    }

                    var repoDto = new ReporteClaveDTO
                    {
                        IdRow_M = repo.IdRow_M,
                        Nis_Rad = repo.Nis_Rad,
                        IdAnomalia = repo.Anomalia,
                        Anomalia = AnomaliaText,
                        LecturaActiva = repo.REPOANOMALIA_D.Where(d => d.esRespTec == true).OrderByDescending(d => d.FechaResolucion).FirstOrDefault()?.LecturaActiva != null ? repo.REPOANOMALIA_D.Where(d => d.esRespTec == true).OrderByDescending(d => d.FechaResolucion).FirstOrDefault().LecturaActiva : repo.REPOANOMALIA_D.Where(d => d.esRespTec == false).OrderBy(d => d.FechaResolucion).FirstOrDefault().LecturaActiva, //repo.LecturaActiva,
                        LecturaReactiva = repo.REPOANOMALIA_D.Where(d => d.esRespTec == true).OrderByDescending(d => d.FechaResolucion).FirstOrDefault()?.LecturaReactiva != null ? repo.REPOANOMALIA_D.Where(d => d.esRespTec == true).OrderByDescending(d => d.FechaResolucion).FirstOrDefault().LecturaReactiva : repo.REPOANOMALIA_D.Where(d => d.esRespTec == false).OrderBy(d => d.FechaResolucion).FirstOrDefault().LecturaReactiva,
                        LecturaDemanda = repo.REPOANOMALIA_D.Where(d => d.esRespTec == true).OrderByDescending(d => d.FechaResolucion).FirstOrDefault()?.LecturaDemanda != null ? repo.REPOANOMALIA_D.Where(d => d.esRespTec == true).OrderByDescending(d => d.FechaResolucion).FirstOrDefault().LecturaDemanda : repo.REPOANOMALIA_D.Where(d => d.esRespTec == false).OrderBy(d => d.FechaResolucion).FirstOrDefault().LecturaDemanda,
                        Causa = repo.SolcitudCausaVerificacion,
                        multiplicador = repo.Multiplicador.ToString(),
                        numResolucion = (repo.REPOANOMALIA_D.Count() + 1).ToString(),
                        ciclo = repo.Ciclo,
                        Solicitante = _uCtrl.GetUsuarioByUser(repo.UsuarioFacturacion).NomPersona,
                        IntTipoRepo = repo.esCritica,
                        TipoRepo = tiporepo,
                        OS = repo.OS,
                        MsjOs = repo.MsjOS,
                        EnEspera = repo.EnEspera
                    };

                    return repoDto;
                }
            }
        }

        public List<RepoExport> listaRepoMaestroAnomaliaReporte(Usuarios usr, FiltroObj filtro)
        {
            string sql = "";

            sql = $@"select
                    convert(nvarchar(max),a.Nis_Rad)NIS,
                    convert(nvarchar(max),a.Region)Region,
                    convert(nvarchar(max),a.Sector)Sector,
                    convert(nvarchar(max),a.LecturaActiva)LecturaActiva,
                    convert(nvarchar(max),a.LecturaReactiva)LecturaReactiva,
                    convert(nvarchar(max),a.LecturaDemanda)LecturaDemanda,
                    convert(nvarchar(max),a.UsuarioFacturacion)Usuario_Reporta,
                    convert(nvarchar(max),a.FechaRegistro)Fecha_Reporte,
                    convert(nvarchar(max),a.Anomalia)Anomalia,
                    convert(nvarchar(max),a.dial)dial
                    from REPOANOMALIA_M a 
                    where 1=1 ";

            if (filtro != null)
            {
                ///FILTO DE REGION Y SECTOR
                if (filtro.Region == "" && filtro.Sector == "")
                {
                    sql = sql + $@" and a.Sector in (select Sector from Usuarios a 
                                inner join Usuario_Sector b on a.idUsuario=b.idUsuario 
                                inner join Sectores c on b.idSector=c.idSector 
                                where a.Usuario='{usr.Usuario}') ";
                }
                else
                {
                    if (filtro.Region != "" && filtro.Sector == "")
                    {
                        sql = sql + $@" and a.Region ='{filtro.Region}' ";
                    }
                    else if (filtro.Region == "" && filtro.Sector != "")
                    {
                        sql = sql + $@" and a.Sector ='{filtro.Sector}' ";
                    }
                }

                ///FILTO DE PERFIL RESOLUCION
                if (filtro.idPerfilReso != "")
                {
                    sql = sql + $@" and a.idPerfilResoRepo ={filtro.idPerfilReso} ";
                }

                ///FILTO DE MEDIDA
                if (filtro.Medida == "")
                {
                    //if (usr.MedidaEspecial && !usr.MedidaDirecta)
                    //{
                    //    sql = sql + $@" and a.Medida ='Medida Especial' ";
                    //}
                    //else if (!usr.MedidaEspecial && usr.MedidaDirecta)
                    //{
                    //    sql = sql + $@" and a.Medida ='Medida Directa' ";
                    //}
                    //else if (usr.MedidaEspecial && usr.MedidaDirecta)
                    //{
                    //    sql = sql + $@" and a.Medida IN('Medida Directa','Medida Especial') ";
                    //}

                    //cambio 06/06/2020 -> campo medida pasa a ser un campo informativo
                    sql = sql + $@" and a.Medida IN('Medida Directa','Medida Especial') ";
                }
                else
                {
                    sql = sql + $@" and a.Medida ='{filtro.Medida}' ";
                }

                ///FILTO DE NISRAD
                if (filtro.Nisrad != "")
                {
                    sql = sql + $@" and a.Nis_Rad ={filtro.Nisrad} ";
                }

                ///FILTO DE ESTADO
                if (filtro.Resuelto != "")
                {
                    sql = sql + $@" and a.Resuelto ={filtro.Resuelto} ";
                }

                ///FILTO DE ANOMALIA
                if (filtro.idAnomalia != "")
                {
                    sql = sql + $@" and a.Anomalia ={filtro.idAnomalia} ";
                }

                ///FILTO DE DIAL
                if (filtro.Dial != "")
                {
                    sql = sql + $@" and a.dial ={filtro.Dial} ";
                }

                sql = sql + $@" order by a.Resuelto asc";
            }
            else
            {
                //if (usr.MedidaEspecial && !usr.MedidaDirecta)
                //{
                //    sql = sql + $@" and a.Medida ='Medida Especial' ";
                //}
                //else if (!usr.MedidaEspecial && usr.MedidaDirecta)
                //{
                //    sql = sql + $@" and a.Medida ='Medida Directa' ";
                //}
                //else if (usr.MedidaEspecial && usr.MedidaDirecta)
                //{
                sql = sql + $@" and a.Medida IN('Medida Directa','Medida Especial') ";
                //}
                //else
                //{
                //    return null;
                //}
                sql = sql + $@" and a.Sector in (select Sector from Usuarios a 
                                inner join Usuario_Sector b on a.idUsuario=b.idUsuario 
                                inner join Sectores c on b.idSector=c.idSector 
                                where a.Usuario='{usr.Usuario}') order by a.Resuelto asc";
            }

            using (var dbSicra = new SicraModel())
            {
                var ds1 = SqlMapper.Query<RepoExport>(conStrSicra, sql).ToList();

                using (var dbLogistics = new LogisticsModel())
                {
                    foreach (var reporte in ds1)
                    {
                        try
                        {
                            int idAnomalia = int.Parse(reporte.Anomalia);
                            string AnomaliaText = dbLogistics.tipoLecturasAnomalia
                                .Where(a => a.idLecturaAnomalia == idAnomalia)
                                .FirstOrDefault().descripcion;

                            reporte.Anomalia = AnomaliaText;
                        }
                        catch (Exception)
                        {
                            reporte.Anomalia = "N/A";
                        }

                    }
                }
                return ds1;
            }
        }

        public List<tipoLecturasAnomalia> GetAnomalias()
        {
            using (var db = new LogisticsModel())
            {
                var anomalias = db.tipoLecturasAnomalia.ToList();
                return anomalias;
            }
        }

        /// <summary>
        /// Filtro para fechas
        /// </summary>
        /// <param name="modulo">Parámetro modulo: A: Anomalía, O:Operativa </param>
        /// <returns></returns>
        public List<DateTime> GetFechasRegistro(string modulo)
        {
            string sql = "";
            if (modulo == "a")
            {
                sql = $@"select distinct CONVERT(date,FechaRegistro) as FechaRegistro from REPOANOMALIA_M order by 1 asc";
            }
            else if (modulo == "o")
            {
                sql = $@"select distinct CONVERT(date,FechaRegistro) as FechaRegistro from Rep_Operativa_M order by 1 desc";
            }
            var lista = SqlMapper.Query<DateTime>(conStrSicra, sql).ToList();
            return lista;
        }

        public List<DateTime> GetFechasVencimiento(string modulo)
        {
            string sql = "";
            if (modulo == "a")
            {
                sql = $@"select distinct CONVERT(date,FechaMaxResol) as FechaMaxReso from REPOANOMALIA_M order by 1 asc";
            }
            else if (modulo == "o")
            {
                sql = $@"select distinct CONVERT(date,FechaMaxResol) as FechaMaxReso from Rep_Operativa_M order by 1 asc";
            }

            var lista = SqlMapper.Query<DateTime>(conStrSicra, sql).ToList();
            return lista;
        }

        public List<Usuarios> GetUsuariosReporte()
        {
            using (var db = new SicraModel())
            {
                string sql = $@"select distinct b.* 
                                from [dbo].[REPOANOMALIA_M] a
                                join Usuarios b on a.UsuarioFacturacion=b.Usuario
                                where Activo=1 order by 3";
                var lista = SqlMapper.Query<Usuarios>(conStrSicra, sql).ToList();
                return lista;
            }
        }

        public List<Usuarios> GetUsuariosRespuesta()
        {
            using (var db = new SicraModel())
            {
                string sql = $@"select distinct b.* 
                                from [dbo].[REPOANOMALIA_D]  a
                                join Usuarios b on a.UsuarioResuelve=b.Usuario
                                where Activo=1 order by 3";
                var lista = SqlMapper.Query<Usuarios>(conStrSicra, sql).ToList();
                return lista;
            }
        }

        public List<RepoAnomaliaMaestroDto> listaRepoMaestroAnomalia(Usuarios usr, FiltroObj filtro)
        {
            string sql = "";

            if (filtro != null)
            {
                sql = $@"select a.* 
                        from REPOANOMALIA_M a 
                        where 1=1 ";

                ///FILTO DE REGION Y SECTOR
                if (filtro.Region != "" && filtro.Sector == "")
                {
                    sql = sql + $@" and a.Region ='{filtro.Region}' ";
                }
                else if (filtro.Region == "" && filtro.Sector != "")
                {
                    sql = sql + $@" and a.Sector ='{filtro.Sector}' ";
                }

                ///FILTO DE PERFIL RESOLUCION
                if (filtro.idPerfilReso != "")
                {
                    sql = sql + $@" and a.idPerfilResoRepo ={filtro.idPerfilReso} ";
                }

                ///FILTO DE MEDIDA
                if (filtro.Medida == "")
                {
                    //cambio 06/06/2020 -> campo medida pasa a ser un campo informativo
                    sql = sql + $@" and a.Medida IN('Medida Directa','Medida Especial') ";
                }
                else
                {
                    sql = sql + $@" and a.Medida ='{filtro.Medida}' ";
                }

                ///FILTO DE NISRAD
                if (filtro.Nisrad != "")
                {
                    sql = sql + $@" and a.Nis_Rad ={filtro.Nisrad} ";
                }

                ///FILTO DE ESTADO
                if (filtro.Resuelto != "")
                {
                    sql = sql + $@" and a.Resuelto ={filtro.Resuelto} ";
                }

                ///FILTO DE ANOMALIA
                if (filtro.idAnomalia != "")
                {
                    sql = sql + $@" and a.Anomalia ={filtro.idAnomalia} ";
                }

                ///FILTO DE DIAL
                if (filtro.Dial != "")
                {
                    sql = sql + $@" and a.dial ={filtro.Dial} ";
                }

                ///FILTO DE FECHA VENCIMIENTO
                if (filtro.FechaMaxResol != "")
                {
                    sql = sql + $@" and CONVERT(date,a.FechaMaxResol) = CONVERT(date,'{Convert.ToDateTime(filtro.FechaMaxResol).ToString("yyyy-MM-dd")}') ";
                }

                ///FILTO DE FECHA REGISTRO
                if (filtro.FechaRegistro != "")
                {
                    sql = sql + $@" and CONVERT(date,a.FechaRegistro) = CONVERT(date,'{Convert.ToDateTime(filtro.FechaRegistro).ToString("yyyy-MM-dd")}') ";
                }

                ///FILTO DE USUARIO REPORTA
                if (filtro.UsuarioFacturacion != "")
                {
                    sql = sql + $@" and a.UsuarioFacturacion='{filtro.UsuarioFacturacion}' ";
                }

                ///FILTO DE USUARIO RESUELVE
                if (filtro.UsuarioResuelve != "")
                {
                    sql = sql + $@" and a.IdRow_M in(select b.IdRow_M from [dbo].[REPOANOMALIA_D] b where b.UsuarioResuelve='{filtro.UsuarioResuelve}') ";
                }

                ///FILTO DE OS
                if (filtro.OS != "")
                {
                    sql = sql + $@" and a.OS ={filtro.OS} ";
                }

                ///FILTO Tiene OS
                if (filtro.TieneOS != "")
                {
                    sql = sql + $@" and '{filtro.TieneOS}' = (case when OS is null then 'NO' when OS<1 then 'NO' when OS>1 then 'SI'end) ";
                }

                //*****************APLICANDO FILTRO VENCIDO O NO*****************
                if (filtro.Vencido == 1)
                {
                    // VENCIDO -> fecha resolucion menor que la fecha actual
                    sql = sql + $@" and format(GETDATE(),'yyy-MM-dd HH:mm') > format(a.FechaMaxResol,'yyy-MM-dd HH:mm') ";
                }
                else if (filtro.Vencido == 2)
                {
                    // NO VENCIDO -> fecha resolucion mayor que la fecha actual
                    sql = sql + $@" and format(FechaMaxResol,'yyy-MM-dd HH:mm') > format(GETDATE(),'yyy-MM-dd HH:mm') ";
                }

                ///FIN QUERY
                sql = sql + $@" and a.Sector in (select Sector from Usuarios a 
                                inner join Usuario_Sector b on a.idUsuario=b.idUsuario 
                                inner join Sectores c on b.idSector=c.idSector  
                                where a.Usuario='{usr.Usuario}') order by a.Resuelto asc";
            }
            else
            {
                sql = $@"select a.* from REPOANOMALIA_M a order by a.Resuelto asc";
            }

            using (var dbSicra = new SicraModel())
            {
                var lista = SqlMapper.Query<RepoAnomaliaMaestroDto>(conStrSicra, sql).ToList();

                using (var dbLogistics = new LogisticsModel())
                {
                    foreach (var reporte in lista)
                    {
                        try
                        {
                            string state = "";
                            switch (reporte.Resuelto)
                            {
                                case 0:
                                    state = "NO";
                                    break;
                                case 1:
                                    state = "SI";
                                    break;
                                case 2:
                                    state = "ESPERA";
                                    break;
                                default:
                                    state = "ERROR";
                                    break;
                            }

                            reporte.ResueltoStng = state;

                            var usrRepo = _uCtrl.GetUsuarioByUser(reporte.UsuarioFacturacion);
                            reporte.UsuarioFacturacion = usrRepo == null ? reporte.UsuarioFacturacion : usrRepo.NomPersona;

                            switch (reporte.esCritica)
                            {
                                case 0:
                                    reporte.TipoRepo = "Refactura";
                                    break;
                                case 1:
                                    reporte.TipoRepo = "Crítica";
                                    break;
                                case 2:
                                    reporte.TipoRepo = "PostFacturación";
                                    break;
                                default:
                                    break;
                            }


                            if (string.IsNullOrEmpty(reporte.FechaMaxResol.ToString()))
                            {
                                reporte.VisibleResolver = false;
                            }
                            else
                            {
                                //Evaluar fecha, hora minutos.
                                int result = DateTime.Compare(DateTime.Now, (DateTime)reporte.FechaMaxResol);
                                if (result >= 1)
                                    reporte.VisibleResolver = false;
                                else
                                    reporte.VisibleResolver = true;
                            }

                            reporte.PerfilAsignado = _uCtrl.GetPerfilRepoById(reporte.IdPerfilResoRepo).NombrePerfil;
                        }
                        catch (Exception ex)
                        {
                            reporte.VisibleResolver = false;
                        }
                        try
                        {
                            int idAnomalia = int.Parse(reporte.Anomalia);
                            string AnomaliaText = dbLogistics.tipoLecturasAnomalia
                                .Where(a => a.idLecturaAnomalia == idAnomalia)
                                .FirstOrDefault().descripcion;

                            reporte.Anomalia = AnomaliaText;
                        }
                        catch (Exception)
                        {
                            reporte.Anomalia = "N/A";
                        }

                    }
                }

                //***************************************
                return lista;
            }
        }

        public REPOANOMALIA_M GetRepoMaestroAnomaliaById(long idM)
        {
            using (var db = new SicraModel())
            {
                var repo = db.REPOANOMALIA_M.Include("REPOANOMALIA_D")
                    .Where(r => r.IdRow_M == idM).FirstOrDefault();
                return repo;
            }
        }

        public DataSet ListaRepoAnomaliaExport(Usuarios usr, FiltroObj filtro)
        {
            string sql = "";

            if (filtro != null)
            {
                sql = $@"SELECT 
                        IIF(b.esRespTec=0,'Reporte','Respuesta') as TipoRegistro
                        ,a.[Region]
                        ,a.[Sector]
                        ,a.[Nis_Rad]
                        ,a.[Ubicacion]
                        ,a.[Multiplicador]
                        ,a.[Medida]
                        ,a.[LecturaActiva]
                        ,a.[LecturaReactiva]
                        ,a.[LecturaDemanda]
                        ,a.[Anomalia] IdAnomalia
                        ,g.descripcion as Anomalia
                        ,b.Resolucion
                        ,a.[dial]
                        ,a.[Ciclo]
                        ,a.[FechaRegistro]
                        ,a.[FechaMaxResol]
                        ,(case when a.Resuelto=1 then 'Resuelto' when a.Resuelto=0 then 'No resuelto' else 'En Espera' end ) as Estado
                        ,B.UsuarioResuelve
                        ,C.NomPersona
                        ,E.NombrePerfil
                        ,(case when a.esCritica=1 then 'CRITICA' when a.esCritica=0 then 'POSTFACTURACIÓN' else 'REFACTURA' end ) as TipoReporte
                        ,D.descripcion AS Categoria
                        ,a.[OS]
                        ,a.[MsjOS]
                        FROM [dbo].[REPOANOMALIA_M] a
                        JOIN dbo.REPOANOMALIA_D b ON a.IdRow_M=b.IdRow_M
                        JOIN DBO.Usuarios C ON B.UsuarioResuelve=C.Usuario
                        JOIN DBO.CategoriaResolucion D ON A.idCategoria=D.idCategoria
                        JOIN DBO.PerfilResolucionRepo E ON C.IdPerfilResoRepo=E.IdPerfilResoRepo
                        JOIN [192.168.100.28].[EEHLogistics].dbo.tipoLecturasAnomalia g on a.Anomalia=g.idLecturaAnomalia
                        WHERE 1=1  ";

                ///FILTO DE REGION Y SECTOR
                if (filtro.Region != "" && filtro.Sector == "")
                {
                    sql = sql + $@" and a.Region ='{filtro.Region}' ";
                }
                else if (filtro.Region == "" && filtro.Sector != "")
                {
                    sql = sql + $@" and a.Sector ='{filtro.Sector}' ";
                }

                ///FILTO DE PERFIL RESOLUCION
                if (filtro.idPerfilReso != "")
                {
                    sql = sql + $@" and a.idPerfilResoRepo ={filtro.idPerfilReso} ";
                }

                ///FILTO DE MEDIDA
                if (filtro.Medida == "")
                {
                    //cambio 06/06/2020 -> campo medida pasa a ser un campo informativo
                    sql = sql + $@" and a.Medida IN('Medida Directa','Medida Especial') ";
                }
                else
                {
                    sql = sql + $@" and a.Medida ='{filtro.Medida}' ";
                }

                ///FILTO DE NISRAD
                if (filtro.Nisrad != "")
                {
                    sql = sql + $@" and a.Nis_Rad ={filtro.Nisrad} ";
                }

                ///FILTO DE ESTADO
                if (filtro.Resuelto != "")
                {
                    sql = sql + $@" and a.Resuelto ={filtro.Resuelto} ";
                }

                ///FILTO DE ANOMALIA
                if (filtro.idAnomalia != "")
                {
                    sql = sql + $@" and a.Anomalia ={filtro.idAnomalia} ";
                }

                ///FILTO DE DIAL
                if (filtro.Dial != "")
                {
                    sql = sql + $@" and a.dial ={filtro.Dial} ";
                }

                ///FILTO DE FECHA VENCIMIENTO
                if (filtro.FechaMaxResol != "")
                {
                    sql = sql + $@" and CONVERT(date,a.FechaMaxResol) = CONVERT(date,'{Convert.ToDateTime(filtro.FechaMaxResol).ToString("yyyy-MM-dd")}') ";
                }

                ///FILTO DE FECHA REGISTRO
                if (filtro.FechaRegistro != "")
                {
                    sql = sql + $@" and CONVERT(date,a.FechaRegistro) = CONVERT(date,'{Convert.ToDateTime(filtro.FechaRegistro).ToString("yyyy-MM-dd")}') ";
                }

                ///FILTO DE USUARIO REPORTA
                if (filtro.UsuarioFacturacion != "")
                {
                    sql = sql + $@" and a.UsuarioFacturacion='{filtro.UsuarioFacturacion}' ";
                }

                ///FILTO DE USUARIO RESUELVE
                if (filtro.UsuarioResuelve != "")
                {
                    sql = sql + $@" and a.IdRow_M in(select b.IdRow_M from [dbo].[REPOANOMALIA_D] b where b.UsuarioResuelve='{filtro.UsuarioResuelve}') ";
                }

                ///FILTO DE OS
                if (filtro.OS != "")
                {
                    sql = sql + $@" and a.OS ={filtro.OS} ";
                }

                ///FILTO Tiene OS
                if (filtro.TieneOS != "")
                {
                    sql = sql + $@" and '{filtro.TieneOS}' = (case when OS is null then 'NO' when OS<1 then 'NO' when OS>1 then 'SI'end) ";
                }

                //*****************APLICANDO FILTRO VENCIDO O NO*****************
                if (filtro.Vencido == 1)
                {
                    // VENCIDO -> fecha resolucion menor que la fecha actual
                    sql = sql + $@" and format(GETDATE(),'yyy-MM-dd HH:mm') > format(a.FechaMaxResol,'yyy-MM-dd HH:mm') ";
                }
                else if (filtro.Vencido == 2)
                {
                    // NO VENCIDO -> fecha resolucion mayor que la fecha actual
                    sql = sql + $@" and format(FechaMaxResol,'yyy-MM-dd HH:mm') > format(GETDATE(),'yyy-MM-dd HH:mm') ";
                }
            }

            var ds = Fun.GetData(sql, 0, "SicraModel");
            return ds;
        }

        public RepoAnomaliaMaestroDto GetRepoAnomaliaDTO(long idRowM)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    var r = db.REPOANOMALIA_M
                        .Include("REPOANOMALIA_D")
                        .Include("ImagenReporteAnomalia")
                        .Include("CategoriaResolucion")
                        .Where(x => x.IdRow_M == idRowM).FirstOrDefault();

                    if (r != null)
                    {
                        var repoMDto = new RepoAnomaliaMaestroDto();
                        using (var dbLogistics = new LogisticsModel())
                        {
                            int idAnomalia = int.Parse(r.Anomalia);
                            string AnomaliaText = dbLogistics.tipoLecturasAnomalia
                                .Where(a => a.idLecturaAnomalia == idAnomalia)
                                .FirstOrDefault().descripcion;

                            string state = "";
                            switch (r.Resuelto)
                            {
                                case 0:
                                    state = "NO";
                                    break;
                                case 1:
                                    state = "SI";
                                    break;
                                case 2:
                                    state = "ESPERA";
                                    break;
                                default:
                                    state = "ERROR";
                                    break;
                            }

                            string TipoRepo = "";
                            switch (r.esCritica)
                            {
                                case 0:
                                    TipoRepo = "Refactura";
                                    break;
                                case 1:
                                    TipoRepo = "Crítica";
                                    break;
                                case 2:
                                    TipoRepo = "PostFacturación";
                                    break;
                                default:
                                    break;
                            }


                            repoMDto = new RepoAnomaliaMaestroDto
                            {
                                IdRow_M = r.IdRow_M,
                                UsuarioFacturacion = _uCtrl.GetUsuarioByUser(r.UsuarioFacturacion).NomPersona,
                                Region = r.Region,
                                Sector = r.Sector,
                                Nis_Rad = r.Nis_Rad,
                                Ubicacion = r.Ubicacion,
                                Anomalia = AnomaliaText,
                                SolcitudCausaVerificacion = r.SolcitudCausaVerificacion,
                                Multiplicador = r.Multiplicador,
                                Medida = r.Medida,
                                LecturaActiva = r.LecturaActiva,
                                LecturaReactiva = r.LecturaReactiva,
                                LecturaDemanda = r.LecturaDemanda,
                                FechaRegistro = r.FechaRegistro,
                                FechaMaxResol = r.FechaMaxResol,
                                dial = r.dial,
                                Ciclo = r.Ciclo,
                                Resuelto = r.Resuelto,
                                ResueltoStng = state,
                                //r.Resuelto ? "SI" : "NO",
                                VisibleResolver = false,
                                tipo = TipoRepo,
                                categoriaResol = r.CategoriaResolucion != null ? r.CategoriaResolucion.descripcion : null,
                                OS = r.OS,
                                MsjOs = r.MsjOS
                            };


                            repoMDto.repoAnomaliaDetalleDto = new List<RepoAnomaliaDetalleDto>();
                            foreach (var d in r.REPOANOMALIA_D)
                            {
                                var usr = _uCtrl.GetUsuarioByUser(d.UsuarioResuelve);

                                string nomPer = usr == null ? d.UsuarioResuelve : usr.NomPersona;


                                repoMDto.repoAnomaliaDetalleDto.Add(new RepoAnomaliaDetalleDto
                                {
                                    IdRow_M = d.IdRow_M,
                                    IdRow_D = d.IdRow_D,
                                    UsuarioResuelve = nomPer,
                                    LecturaActiva = d.LecturaActiva,
                                    LecturaReactiva = d.LecturaReactiva,
                                    LecturaDemanda = d.LecturaDemanda,
                                    Resolucion = d.Resolucion,
                                    FechaResolucion = d.FechaResolucion,
                                    tipo = d.Tipo,
                                    Ciclo = d.Ciclo,
                                    esRespTec = d.esRespTec,
                                    esRespTecStng = d.esRespTec ? "SI" : "NO",
                                    PerfilUsuario = _uCtrl.GetPerfilRepoByUser(d.UsuarioResuelve).NombrePerfil
                                });
                            }

                            repoMDto.imagenReporteAnomalias = new List<ImagenReporteAnomalia>();
                            repoMDto.imagenReporteAnomalias = r.ImagenReporteAnomalia.ToList();
                        }
                        return repoMDto;
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<int> GetCiclosAnomaliaHistorico()
        {
            string sql = $@"select distinct Ciclo 
                            from [dbo].[REPOANOMALIA_M_HISTORICO] 
                            order by Ciclo desc";

            var ciclo = SqlMapper.Query<int>(conStrSicra, sql).ToList();
            return ciclo;
        }

        public List<RepoAnomaliaMaestroDto> listaRepoMaestroAnomaliaHistorico(FiltroObj filtro)
        {
            string sql = "";

            if (filtro != null)
            {
                sql = $@"select a.* from REPOANOMALIA_M_HISTORICO a where 1=1 ";

                ///FILTO DE REGION
                if (filtro.Region != "")
                {
                    sql = sql + $@" and a.Region ='{filtro.Region}' ";
                }

                ///FILTO DE SECTOR
                if (filtro.Sector != "")
                {
                    sql = sql + $@" and a.Sector ='{filtro.Sector}' ";
                }

                ///FILTO DE PERFIL RESOLUCION
                if (filtro.idPerfilReso != "" && filtro.idPerfilReso != null)
                {
                    sql = sql + $@" and a.idPerfilResoRepo ={filtro.idPerfilReso} ";
                }

                ///FILTO DE MEDIDA
                if (filtro.Medida != "")
                {
                    sql = sql + $@" and a.Medida ='{filtro.Medida}' ";
                }

                ///FILTO DE NISRAD
                if (filtro.Nisrad != "")
                {
                    sql = sql + $@" and a.Nis_Rad ={filtro.Nisrad} ";
                }

                ///FILTO DE ESTADO
                if (filtro.Resuelto != "")
                {
                    sql = sql + $@" and a.Resuelto ={filtro.Resuelto} ";
                }

                ///FILTO DE ANOMALIA
                if (filtro.idAnomalia != "")
                {
                    sql = sql + $@" and a.Anomalia ={filtro.idAnomalia} ";
                }

                ///FILTO DE CICLO
                if (filtro.CicloFact != "")
                {
                    sql = sql + $@" and a.Ciclo ={filtro.CicloFact} ";
                }

                ///FILTO DE DIAL
                if (filtro.Dial != "")
                {
                    sql = sql + $@" and a.dial ={filtro.Dial} ";
                }

                sql = sql + $@" order by a.Ciclo desc,FechaRegistro desc";
            }
            else
            {
                return null;
            }

            using (var dbSicra = new SicraModel())
            {
                var ds1 = SqlMapper.Query<RepoAnomaliaMaestroDto>(conStrSicra, sql).ToList();

                using (var dbLogistics = new LogisticsModel())
                {
                    foreach (var reporte in ds1)
                    {
                        try
                        {
                            string state = "";
                            switch (reporte.Resuelto)
                            {
                                case 0:
                                    state = "NO";
                                    break;
                                case 1:
                                    state = "SI";
                                    break;
                                case 2:
                                    state = "ESPERA";
                                    break;
                                default:
                                    state = "ERROR";
                                    break;
                            }
                            reporte.ResueltoStng = state;
                            var usrRepo = _uCtrl.GetUsuarioByUser(reporte.UsuarioFacturacion);
                            reporte.UsuarioFacturacion = usrRepo == null ? reporte.UsuarioFacturacion : usrRepo.NomPersona;

                            int idAnomalia = int.Parse(reporte.Anomalia);
                            string AnomaliaText = dbLogistics.tipoLecturasAnomalia
                                .Where(a => a.idLecturaAnomalia == idAnomalia)
                                .FirstOrDefault().descripcion;
                            reporte.Anomalia = AnomaliaText;
                        }
                        catch (Exception ex)
                        {
                            reporte.Anomalia = "N/A";
                        }
                    }
                }
                return ds1;
            }
        }

        public List<REPOANOMALIA_D_HISTORICO> RepoDetailAnomaliaByIdMHistorico(long IdM, int ciclo)
        {
            using (var db = new SicraModel())
            {
                string sql = "";
                sql = $@"SELECT 
                        b.[idrow]
                        ,b.[IdRow_M]
                        ,b.[IdRow_D]
                        ,isnull(c.NomPersona,b.[UsuarioResuelve])UsuarioResuelve
                        ,b.[LecturaActiva]
                        ,b.[LecturaReactiva]
                        ,b.[LecturaDemanda]
                        ,b.[Resolucion]
                        ,b.[FechaResolucion]
                        ,b.[Tipo]
                        ,b.[Ciclo]
                        ,b.[esRespTec]
                        FROM REPOANOMALIA_M_HISTORICO a
                        join REPOANOMALIA_D_HISTORICO b on a.IdRow_M=b.IdRow_M and a.Ciclo=b.Ciclo
                        left join Usuarios c on c.Usuario=b.UsuarioResuelve
                        where b.IdRow_M={IdM} and b.Ciclo={ciclo} order by 9 desc";
                var lista = SqlMapper.Query<REPOANOMALIA_D_HISTORICO>(conStrSicra, sql).ToList();

                return lista;
            }
        }

        public RepoAnomaliaMaestroDto GetRepoAnomaliaDTOHistorico(long nis, int ciclo)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    var rHisto = new ReportesAnomaliaHisto();
                    //.Include("REPOANOMALIA_D_HISTORICO")
                    //.Include("ImagenReporteAnomalia")
                    //.Include("CategoriaResolucion")
                    //.Where(x => x.IdRow_M == idRowM).FirstOrDefault();
                    string sql = "";

                    ////reporte Maestro
                    sql = $@"select * from [dbo].[REPOANOMALIA_M_HISTORICO] a where a.Nis_Rad={nis} and a.Ciclo={ciclo}";
                    rHisto.repoMHisto = SqlMapper.Query<REPOANOMALIA_M_HISTORICO>(conStrSicra, sql).FirstOrDefault();

                    if (rHisto.repoMHisto != null)
                    {
                        sql = $@"select * from [dbo].[REPOANOMALIA_D_HISTORICO] a where a.IdRow_M={rHisto.repoMHisto.IdRow_M} and a.Ciclo={ciclo}";
                        rHisto.repoDHisto = SqlMapper.Query<REPOANOMALIA_D_HISTORICO>(conStrSicra, sql).ToList();

                        sql = $@"select * from [dbo].[ImagenReporteAnomalia] where IdRow_M={rHisto.repoMHisto.IdRow_M} and ciclo={ciclo}";
                        rHisto.imgsRepoHisto = SqlMapper.Query<ImagenReporteAnomalia>(conStrSicra, sql).ToList();

                    }

                    if (rHisto.repoMHisto != null)
                    {
                        var repoMDto = new RepoAnomaliaMaestroDto();
                        using (var dbLogistics = new LogisticsModel())
                        {
                            string AnomaliaText = "Sin Dato Historico";
                            if (rHisto.repoMHisto.Anomalia != null)
                            {
                                int idAnomalia = int.Parse(rHisto.repoMHisto.Anomalia);
                                AnomaliaText = dbLogistics.tipoLecturasAnomalia
                                    .Where(a => a.idLecturaAnomalia == idAnomalia)
                                    .FirstOrDefault().descripcion;

                            }

                            string state = "";
                            switch (rHisto.repoMHisto.Resuelto)
                            {
                                case 0:
                                    state = "NO";
                                    break;
                                case 1:
                                    state = "SI";
                                    break;
                                case 2:
                                    state = "ESPERA";
                                    break;
                                default:
                                    state = "ERROR";
                                    break;
                            }


                            repoMDto = new RepoAnomaliaMaestroDto
                            {
                                IdRow_M = rHisto.repoMHisto.IdRow_M,
                                UsuarioFacturacion = rHisto.repoMHisto.UsuarioFacturacion,
                                Region = rHisto.repoMHisto.Region,
                                Sector = rHisto.repoMHisto.Sector,
                                Nis_Rad = rHisto.repoMHisto.Nis_Rad,
                                Ubicacion = rHisto.repoMHisto.Ubicacion,
                                Anomalia = AnomaliaText,
                                SolcitudCausaVerificacion = rHisto.repoMHisto.SolcitudCausaVerificacion,
                                Multiplicador = rHisto.repoMHisto.Multiplicador,
                                Medida = rHisto.repoMHisto.Medida,
                                LecturaActiva = rHisto.repoMHisto.LecturaActiva,
                                LecturaReactiva = rHisto.repoMHisto.LecturaReactiva,
                                LecturaDemanda = rHisto.repoMHisto.LecturaDemanda,
                                FechaRegistro = rHisto.repoMHisto.FechaRegistro,
                                FechaMaxResol = rHisto.repoMHisto.FechaMaxResol,
                                dial = rHisto.repoMHisto.dial,
                                Ciclo = rHisto.repoMHisto.Ciclo,
                                Resuelto = rHisto.repoMHisto.Resuelto,
                                ResueltoStng = state,
                                VisibleResolver = false,
                                tipo = "Reporte",
                                categoriaResol = ""
                            };

                            repoMDto.repoAnomaliaDetalleDto = new List<RepoAnomaliaDetalleDto>();
                            foreach (var d in rHisto.repoDHisto)
                            {
                                var usr = _uCtrl.GetUsuarioByUser(d.UsuarioResuelve);

                                repoMDto.repoAnomaliaDetalleDto.Add(new RepoAnomaliaDetalleDto
                                {
                                    IdRow_M = d.IdRow_M,
                                    IdRow_D = d.IdRow_D,
                                    UsuarioResuelve = usr != null ? usr.NomPersona : d.UsuarioResuelve,
                                    LecturaActiva = d.LecturaActiva,
                                    LecturaReactiva = d.LecturaReactiva,
                                    LecturaDemanda = d.LecturaDemanda,
                                    Resolucion = d.Resolucion,
                                    FechaResolucion = d.FechaResolucion,
                                    tipo = d.Tipo,
                                    Ciclo = d.Ciclo,
                                    esRespTec = d.esRespTec == null ? false : Convert.ToBoolean(d.esRespTec),
                                    esRespTecStng = d.esRespTec == null ? "NULL" : Convert.ToBoolean(d.esRespTec) ? "SI" : "NO"
                                });
                            }

                            repoMDto.imagenReporteAnomalias = new List<ImagenReporteAnomalia>();
                            repoMDto.imagenReporteAnomalias = rHisto.imgsRepoHisto.ToList();
                        }
                        return repoMDto;
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
