﻿//using Contratos_EEH_Web.Model;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace SICRA.Controllers
{
    internal class MenuController
    {
        static string entornoSys = ConfigurationManager.AppSettings["entorno"];
        public static DataSet GetAllWebformsByAccesos(List<String> lista, string appId)
        {
            string ids = "";
            string strQuery = string.Empty;

            foreach (string id in lista)
            {
                ids += "'" + id + "',";
            }

            if (entornoSys == "Prueba")
            {
                strQuery = $@"select a.MenuParentID,a.MenuID,(Case WHEN b.Nombre is null THEN a.MenuName ELSE b.Nombre END)as 'Nombre',b.URL,a.Orden
                            FROM [ServiceAccount].[dbo].[MenuGU] a
                            inner join [ServiceAccount].[dbo].[WebForms] b on a.WebFormID=b.WebFormID
                            where b.app_id={appId}
                            order by a.Orden asc";
            }
            else
            {
                strQuery = $@"select a.MenuParentID,a.MenuID,(Case WHEN b.Nombre is null THEN a.MenuName ELSE b.Nombre END)as 'Nombre',b.URL,a.Orden
                            FROM [ServiceAccount].[dbo].[MenuGU] a
                            inner join [ServiceAccount].[dbo].[WebForms] b on a.WebFormID=b.WebFormID
                            where b.app_id={appId}
                            and (b.id in ({ids.Substring(0, ids.Length - 1)}) or b.id is null)
                            and a.Habilitado=1
                            order by a.Orden asc";
            }

            DataSet ds = Fun.GetData(strQuery, 30, "SAConn");

            return ds;
        }

        public static bool GetWebformByAccesos(List<String> lista, string appId, string url)
        {
            string ids = "";
            string strQuery = string.Empty;

            if (lista != null)
            {
                foreach (string id in lista)
                {
                    ids += "'" + id + "',";
                }

                if (entornoSys == "Prueba")
                {
                    strQuery = $@"SELECT count(0) from[ServiceAccount].[dbo].[WebForms] f 
                                where f.app_id = { appId} 
                                and REPLACE(REPLACE(url,'~',''),'.aspx','')='{url.Replace(".aspx", "")}'";
                }
                else
                {
                    strQuery = $@"SELECT count(0) from[ServiceAccount].[dbo].[WebForms] f 
                                  where f.app_id = { appId} and f.id in ({ ids.Substring(0, ids.Length - 1)})
                                   and REPLACE(REPLACE(url,'~',''),'.aspx','')='{url.Replace(".aspx", "")}'";
                } 

                DataSet ds = Fun.GetData(strQuery, 30, "SAConn");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    int count = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0].ToString());
                    if (count > 0) return true;
                    else return false;
                }
                else
                    return false;
            } else return false;

        }        


    }
}