﻿using SICRA.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Data.Common;
using Dapper;
using SICRA.Models.SicraBD;

namespace SICRA.Controllers
{
    public class ClienteController : Controller
    {
        private static readonly string TokenSys = ConfigurationManager.AppSettings["TokenSys"];
        private static readonly DbConnection ConnINCMS = Fun.GetConnectionORA("INCMSBD");
        private static readonly DbConnection ConnSQL = Fun.GetConnectionSQL("energis");

        public class PrintConfigPreviewViewModel
        {
            public string Name { get; set; }
            public byte[] Picture { get; set; }
            public string clave { get; set; }
        }
        public class imagePreview
        {
            public string Name { get; set; }
            public string url { get; set; }
        }

        public ClienteRoot GetInfoCliente(string clave)
        {
            var cli = new ClienteRoot();

            string sql = $@"SELECT a.pan_cuenta_2 as clave,
                            a.nis_rad,
                            a.num_apa as num_medidor,
                            (select num_apa from hapmedida_ap where nis_rad=a.nis_rad
                            and f_lvto = (select max(f_lvto) from hapmedida_ap where nis_rad=a.nis_rad)
                            and f_inst = (select max(f_inst) from hapmedida_ap where nis_rad=a.nis_rad)) as num_medidor_ant,
                            (a.ruta ||  '' || a.num_itin || '' || a.acometida)as ubicacion,
                            d.desc_region as region,
                            e.COD_AREA as codarea,
                            e.NOM_AREA as area,
                            a.nombre_cliente as nom_cli,
                            a.ref_dir as direccion,
                            a.cod_tar as cod_tarifa,
                            b.desc_tar as nom_tarifa,
                            c.TIP_MERCADO as codmercado,
                            g.DESC_TIPO as mercado,
                            NVL(a.num_rue,0)num_agujas,
                            (case when a.tip_lect= 'RA005'  then '0' when  a.tip_lect ='RA000' then  '2' else '5' end)   as  cod_lectura_actual,
                            NVL(a.csmo_total_1,0) as consumo_activo_actual,
                            NVL(a.csmo_total_2,0) as consumo_activo_anterior,
                            NVL(a.lect_act_1,0) AS lectura_activa_actual,
                            NVL(a.lect_act_2,0) AS lectura_activa_anterior,
                            NVL(a.csmo_react_1,0) consumo_reactivo_actual ,
                            NVL(a.csmo_react_2,0) consumo_reactivo_anterior,
                            NVL(a.lect_react_1,0) as lectura_reactiva_actual,
                            NVL(a.lect_react_2,0) as lectura_reactiva_anterior,
                            NVL(a.lect_pot_1,0) as lectura_demanda_actual,
                            NVL(a.lect_pot_2,0) as lectura_demanda_anterior,
                            a.cte as multiplicador,
                            NVL(a.NUM_Dias_rec_1,0) as num_dias_fact,
                            f.NUM_ITIN as dial
                            FROM maestro_suscriptores a 
                            LEFT JOIN mtarifas b on a.cod_tar=b.cod_tar
                            LEFT JOIN sumcon c on a.nis_rad=c.nis_rad
                            LEFT JOIN region d on c.cod_region=d.cod_region
                            LEFT JOIN AREAS e on a.cod_area=e.cod_area
                            LEFT JOIN FINCAS_PER_LECT f ON f.NIF = a.NIF
                            LEFT JOIN tipos g on c.TIP_MERCADO=g.TIPO
                            WHERE a.nis_rad={clave}";

            try
            {
                var clienteInCMS = SqlMapper.Query<Cliente>(ConnINCMS, sql, commandTimeout: 0).FirstOrDefault();

                if (clienteInCMS != null)
                {
                    sql = $@"select top 1 
                        FECHAHORA fecha_hora_lectura,
                        isnull(CODIGOLECTURA,-1) cod_lectura_campo,
                        iif(CONSUMOACTIVA='',null,convert(float,CONSUMOACTIVA))consumo_activo_campo,
                        iif(LECTURAACTIVA='',null,convert(float,LECTURAACTIVA))lectura_activa_campo,
                        iif(CONSUMOREACTIVA='',null,convert(float,CONSUMOREACTIVA))consumo_reactivo_campo,
                        iif(LECTURAREACTIVA='',null,convert(float,LECTURAREACTIVA))lectura_reactiva_campo,
                        isnull(ORIGEN,'') entidad_lector,
                        isnull(ANOMALIA,'') anomalia,
                        CONCAT(SUBSTRING(convert(nvarchar,DATEPART(year,FECHAHORA)),3,2),facturacion.dbo.F_Integer(DATEPART(month,FECHAHORA),2,'N')) COLLATE Modern_Spanish_CI_AS cicloLectura,
                        CONCAT(SUBSTRING(convert(nvarchar,DATEPART(year,GETDATE())),3,2),facturacion.dbo.F_Integer(DATEPART(month,GETDATE()),2,'N')) COLLATE Modern_Spanish_CI_AS cicloActual
                        FROM [ENERGIS].[dbo].[V_CONSULTA_LECTURAS_TEST] Modern_Spanish_CI_AS
                        WHERE CLAVE={clienteInCMS.clave} order by FECHAHORA desc";

                    try
                    {
                        //var clienteEEH = SqlMapper.QuerySingle<Cliente>(_connSqlFactory.GetConnection("energis"), sql, commandTimeout: 300);
                        var clienteEEH = SqlMapper.Query<Cliente>(ConnSQL, sql, commandTimeout: 0).FirstOrDefault();

                        if (clienteEEH != null)
                        {
                            clienteInCMS.fecha_hora_lectura = clienteEEH.fecha_hora_lectura;
                            clienteInCMS.cod_lectura_campo = clienteEEH.cod_lectura_campo;
                            clienteInCMS.consumo_activo_campo = clienteEEH.consumo_activo_campo;
                            clienteInCMS.lectura_activa_campo = clienteEEH.lectura_activa_campo;
                            clienteInCMS.consumo_reactivo_campo = clienteEEH.consumo_reactivo_campo;
                            clienteInCMS.lectura_reactiva_campo = clienteEEH.lectura_reactiva_campo;
                            clienteInCMS.entidad_lector = clienteEEH.entidad_lector;
                            clienteInCMS.anomalia = clienteEEH.anomalia;
                            clienteInCMS.cicloLectura = clienteEEH.cicloLectura;
                            clienteInCMS.cicloActual = clienteEEH.cicloActual;
                        }
                        else
                        {
                            clienteEEH = new Cliente();
                            clienteInCMS.cicloLectura = "9999";
                            clienteInCMS.cicloActual = "9999";
                        }
                    }
                    catch (Exception ex)
                    {
                        clienteInCMS.fecha_hora_lectura = null;
                        clienteInCMS.cod_lectura_campo = null;
                        clienteInCMS.consumo_activo_campo = null;
                        clienteInCMS.lectura_activa_campo = null;
                        clienteInCMS.consumo_reactivo_campo = null;
                        clienteInCMS.lectura_reactiva_campo = null;
                        clienteInCMS.entidad_lector = null;
                        clienteInCMS.anomalia = null;
                        clienteInCMS.cicloLectura = "0";
                        clienteInCMS.cicloActual = $"{DateTime.Now.ToString("yy")}{DateTime.Now.Month.ToString("00")}"; //clienteEEH.cicloActual;
                    }
                }

                cli.cliente = clienteInCMS;
                return cli;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        
        [HttpPost]
        public string CalculaConsumo(string data)
        {
            var parametro = JsonConvert.DeserializeObject<ParamData>(data);
            try
            {
                parametro.consumoAct = calculo(int.Parse(parametro.NumAgu), decimal.Parse(parametro.Multi),long.Parse(parametro.LectActAnt),long.Parse(parametro.LectActActual)).ToString();
            }
            catch (Exception)
            {
                parametro.consumoAct = "N/A";
            }
            try
            {
                parametro.consumoRea = calculo(int.Parse(parametro.NumAgu), decimal.Parse(parametro.Multi), long.Parse(parametro.LectReaAnt), long.Parse(parametro.LectReaActual)).ToString();
            }
            catch (Exception)
            {
                parametro.consumoRea = "N/A";
            }
            return JsonConvert.SerializeObject(parametro);
        }
        private decimal calculo(int numAgu, decimal multi,long LectAnt,long LectActual)
        {
            long difLect;
            decimal consumo;

            difLect = LectActual - LectAnt;
            consumo = (difLect) * (multi);

            if (numAgu == 4)
            {
                if (difLect < 0)
                {
                    difLect = (10000 + difLect);
                    consumo = (difLect * multi);
                }
                else if (difLect > 10000)
                {
                    consumo = (difLect * multi);
                }

                if (difLect < 0)
                {
                    difLect = (90000 + difLect);
                    consumo = (difLect * multi);
                }
            }
            else
            {
                if (difLect < 0)
                {
                    difLect = (100000 + difLect);
                    consumo = (difLect * multi);
                }
                else if (difLect > 100000)
                {
                    consumo = (difLect * multi);
                }
                if (difLect < 0)
                {
                    difLect = (900000 + difLect);
                    consumo = (difLect * multi);
                }
            }
            return consumo;
        }
        public List<imagePreview> GuardarCopiaImagenes(List<PrintConfigPreviewViewModel> imagenes, string clave)
        {
            try
            {
                List<imagePreview> ImgFiles = new List<imagePreview>();
                //foreach (var item in imagenes)
                foreach (var (item, index) in imagenes.Select((v, i) => (v, i)))
                {
                    string path = Request.PhysicalApplicationPath;
                    string nombreArchivo = $"{path}Imagenes\\img{index}_{item.Name}_C{item.clave}.Jpeg";

                    string resolvedUrl = System.Web.HttpContext.Current.Request.Url.Authority.ToString();//VirtualPathUtility.ToAbsolute("~/");

                    ImgFiles.Add(new imagePreview() { url = $"img{index}_{item.Name}_C{item.clave}.Jpeg", Name = $"img{index}_{item.Name}_C{item.clave}" });

                    Bitmap objBitMap = new Bitmap(byteArrayToImage(item.Picture));
                    objBitMap.Save(nombreArchivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                return ImgFiles;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
                ms.Position = 0;
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
            catch (Exception)
            {
                return null;
            }            
        }
        public LiteralControl GetImagenesMosaico(List<imagePreview> images)
        {
            LiteralControl html = new LiteralControl();

            string folderProject = Request.Url.AbsolutePath.ToString().Trim();

            string content = @"<ul class='pictures'>";

            try
            {
                int Dividendo = images.Count(), divisor = 6, q, r, totalFilas = 0;
                q = (int)(Dividendo / divisor);
                r = Dividendo % divisor;
                totalFilas = q + (r > 0 ? 1 : 0);

                string imageneshtml = "";

                foreach (var (item, index) in images.Select((v, i) => (v, i)))
                {
                    if (item.url != "")
                    {
                        string path = System.Web.HttpContext.Current.Request.Url.Authority.ToString();
                        imageneshtml += $@"<li><img data-original='{item.Name}' src ='{folderProject}/Imagenes/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                    }
                }
                content += imageneshtml;
                content += " </ul>";
            }
            catch (Exception)
            {
                content = "<H3>SIN IMÁGENES</H3>";
            }

            
            html.Text = content;
            return html;
        }
        public Mercado GetMercadoByIdIncms(string CodIncms)
        {
            using (var db = new SicraModel())
            {
                var m = db.Mercado.Where(x => x.CodIncms.Equals(CodIncms)).FirstOrDefault();
                return m;
            }

        }
        public Sectores GetSectorByIdIncms(int? CodIncms)
        {
            using (var db = new SicraModel())
            {
                var m = db.Sectores.Where(x => x.CodAreaInCms==CodIncms).FirstOrDefault();
                return m;
            }
        }
    }
}