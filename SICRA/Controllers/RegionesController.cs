﻿using Dapper;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SICRA.Controllers
{
    public class RegionesController
    {
        private static readonly DbConnection conStrSicra = Fun.GetConnectionSQL("SicraModel");
        public List<Regiones> GetRegionesByUsuario(string usr)
        {
            string sql = "";

            if (usr != "")
            {
                sql = $@"SELECT distinct a.Region,a.idRegion
                        FROM [dbo].[Regiones] a
                        INNER JOIN [dbo].[Sectores] b on a.idRegion=b.idRegion
                        inner join [dbo].[Usuario_Sector] c on b.idSector=c.idSector
                        inner join [dbo].[Usuarios] d on c.idUsuario=d.idUsuario
                        where d.Usuario ='{usr}' ";
            }
            else
            {
                sql = "SELECT [idRegion],[Region] FROM [Regiones]";
            }

            var regiones = SqlMapper.Query<Regiones>(conStrSicra, sql).ToList();
            return regiones;
        }

        public List<Sectores> GetSectoresByUsuario(string usr)
        {
            string sql = "";

            if (usr != "")
            {
                sql = $@"SELECT b.*
                        FROM [dbo].[Sectores] b
                        inner join [dbo].[Usuario_Sector] c on b.idSector=c.idSector
                        inner join [dbo].[Usuarios] d on c.idUsuario=d.idUsuario
                        where d.Usuario ='{usr}' ";
            }
            else
            {
                sql = "SELECT idSector,Sector FROM Sectores";
            }

            var sectores = SqlMapper.Query<Sectores>(conStrSicra, sql).ToList();
            return sectores;
        }

        public List<string> GetMedidaByUsuario(string usr)
        {
            string sql = "";

            if (usr != "")
            {
                sql = $@"select distinct Medida from [dbo].[REPOANOMALIA_M]";
            }
            else
            {
                sql = "select distinct Medida from [dbo].[REPOANOMALIA_M_HISTORICO]";
            }
            var sectores = SqlMapper.Query<string>(conStrSicra, sql).ToList();
            return sectores;
        }

        public Sectores GetSectorByNombre(string nomSector)
        {
            using (var db = new SicraModel())
            {
                var sector = db.Sectores.Where(s => s.Sector.Equals(nomSector)).FirstOrDefault();
                return sector;
            }
        }

        public Sectores GetFirstSectorByUser(int idUsr)
        {
            using (var db = new SicraModel())
            {
                var sector = db.Usuario_Sector
                    .Include("Sectores")
                    .Where(u => u.idUsuario == idUsr)
                    .FirstOrDefault().Sectores;
                return sector;
            }
        }
    }
}
