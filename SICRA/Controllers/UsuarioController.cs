﻿using SICRA.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using SICRA.Models.SicraBD;
using System.Data.Entity;
using SICRA.Dto;
using Dapper;
using System.Data.Common;

namespace SICRA.Controllers
{
    public class UsuarioController 
    {
        private static readonly DbConnection conStrSicra = Fun.GetConnectionSQL("SicraModel");

        public List<Usuarios> GetListaUsuarios()
        {
            using (var db = new SicraModel())
            {
                var lista = db.Usuarios.ToList();
                return lista;
            }
        }

        public bool GetActivoSicra(string userName)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    var esActivo = db.Usuarios.Any(x => x.Usuario.Equals(userName) && x.Activo);
                    return esActivo;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public Usuarios GetUsuarioByUser(string userName)
        {
            using (var db = new SicraModel())
            {
                var USR = db.Usuarios
                    .Include("PerfilResolucionRepo")
                    .Include("PerfilAutorizacion")
                    .Where(x => x.Usuario.Equals(userName))
                    .FirstOrDefault();
                return USR;
            }
        }

        public Usuarios GetUsuarioById(int IdUser)
        {
            using (var db = new SicraModel())
            {
                var USR = db.Usuarios.Find(IdUser);
                return USR;
            }
        }

        public RespMsj PutFirmaUsuario(FirmaUsuario firma)
        {
            using (var db = new SicraModel())
            {
                bool existeFirma = db.FirmaUsuario.Any(x => x.idUsuario==firma.idUsuario);

                db.Entry(firma).State = !existeFirma ? EntityState.Added : EntityState.Modified;
                if (db.SaveChanges() > 0)
                {
                    return new RespMsj { estado = "ok", mensaje = $"Firma {(existeFirma? "Modificada":"Guardada")} Correctamente.!" };
                }
                else
                {
                    return new RespMsj { estado = "error", mensaje = "Error al Guardar Firma." };
                }
            }
        }

        public List<PerfilAutorizacion> GetListaPerfilAutoziacionSicra()
        {
            using (var db = new SicraModel())
            {
                var perfiles = db.PerfilAutorizacion.OrderBy(x => x.Nombre).ToList();
                return perfiles;
            }
        }

        public List<PerfilResolucionRepo> GetListaPerfilRepo()
        {
            using (var db = new SicraModel())
            {
                var perfiles = db.PerfilResolucionRepo.ToList();
                return perfiles;
            }
        }

        public List<PerfilResolucionRepo> GetListaPerfilRepoById(int? idPer)
        {
            using (var db = new SicraModel())
            {
                var perfiles = db.PerfilResolucionRepo
                    .Where(x=>x.IdPerfilResoRepo==idPer)
                    .ToList();
                return perfiles;
            }
        }

        public PerfilResolucionRepo GetPerfilRepoById(int? idPer)
        {
            using (var db = new SicraModel())
            {
                var perfiles = db.PerfilResolucionRepo
                    .Where(x => x.IdPerfilResoRepo == idPer)
                    .FirstOrDefault();
                return perfiles;
            }
        }

        public PerfilResolucionRepo GetPerfilRepoByUser(string Usuario)
        {
            using (var db = new SicraModel())
            {
                var usr = db.Usuarios
                    .Include("PerfilResolucionRepo")
                    .Where(x => x.Usuario.Equals(Usuario))
                    .FirstOrDefault();
                return usr.PerfilResolucionRepo;
            }
        }

        public Usuarios PutUsuario(Usuarios usuario)
        {
            using (var db = new SicraModel())
            {
                usuario.PerfilResolucionRepo = null;
                usuario.PerfilAutorizacion = null;
                db.Entry(usuario).State = usuario.idUsuario == 0 ? EntityState.Added : EntityState.Modified;

                if (db.SaveChanges() > 0)
                {
                    return usuario;
                }
                else
                {
                    return null;
                }
            }
        }

        public bool PutSectoresUsuarios(List<int> sectoresUsuario,int idUsr)
        {
            try
            {
                //borrado de sectores
                string sql = $"delete from Usuario_Sector where idUsuario={idUsr}";
                var resp = Fun.ExecQuery(sql, 0, "SicraModel");

                //inserción de sectores
                foreach (var sec in sectoresUsuario)
                {
                    sql = $@"INSERT INTO [dbo].[Usuario_Sector]([idUsuario],[idSector])
                            select {idUsr},{sec}";
                    resp = Fun.ExecQuery(sql, 0, "SicraModel");
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public bool PutMercadosUsuario(List<int> MercadosUsuario, int idUsr)
        {
            try
            {
                //borrado de mercados
                string sql = $"delete from Usuario_Mercado where idUsuario={idUsr}";
                var resp = Fun.ExecQuery(sql, 0, "SicraModel");

                //inserción de mercados
                foreach (var m in MercadosUsuario)
                {
                    sql = $@"INSERT INTO [dbo].[Usuario_Mercado]([idUsuario],[IdMercado])
                            select {idUsr},{m}";
                    resp = Fun.ExecQuery(sql, 0, "SicraModel");
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public List<SectoresUsuarioDto> GetSectoresUsuarioDto(string usuario)
        {
            string sql = $@"select A.idSector,a.Sector,
                            ISNULL((
                            select 1
                            from Usuario_Sector b
                            left join Usuarios c on c.idUsuario=b.idUsuario
                            where Usuario ='{usuario}' and b.idSector=a.idSector
                            ),0) as Asig
                            from Sectores a";

            var sectoresUsuarios = SqlMapper.Query<SectoresUsuarioDto>(conStrSicra, sql).ToList();
            return sectoresUsuarios;
        }

        public List<MercadosUsuarioDto> GetMercadosUsuarioDto(string usuario)
        {
            string sql = $@"select A.IdMercado,a.Nombre,
                            ISNULL((
                            select 1
                            from Usuario_Mercado b
                            left join Usuarios c on c.idUsuario=b.idUsuario
                            where Usuario ='{usuario}' and b.IdMercado=a.IdMercado
                            ),0) as Asig
                            from Mercado a";

            var mercadosUsuario = SqlMapper.Query<MercadosUsuarioDto>(conStrSicra, sql).ToList();
            return mercadosUsuario;
        }

        public FirmaUsuario GetFirmaUsuario(int idUsr)
        {
            using (var db = new SicraModel())
            {
                var firma = db.FirmaUsuario
                    .Where(x => x.idUsuario == idUsr)
                    .FirstOrDefault();
                return firma;
            }
        }

        public List<Usuarios> GetUsuariosByIdPerfilResol(int idPerfilReso)
        {
            using (var db = new SicraModel())
            {
                var lista = db.Usuarios.Where(x => x.IdPerfilResoRepo == idPerfilReso).ToList();
                return lista;
            }
        }
    }
}