﻿using ClosedXML.Excel;
using Dapper;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.EEHLogistics;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace SICRA.Controllers
{
    public class RepoOperativaController : Controller
    {
        private static readonly DbConnection conStrSicra = Fun.GetConnectionSQL("SicraModel");
        private UsuarioController _uCtrl = new UsuarioController();

        public List<Rep_Operativa_D> RepoDetaOpeByIdM(long IdM)
        {
            using (var db = new SicraModel())
            {
                string sql = $@"select 
                                [IdRow_D]
                                ,[Estado]
                                ,[Resolucion]
                                ,[FechaResolucion]
                                ,c.NomPersona UsuarioResuelve
                                from
                                (
                                    select * from Rep_Operativa_D a where IdRow_M={IdM}
                                    union
                                    select NULL,IdRow_M,'Reporte',SolcitudCausaVerificacion,FechaRegistro,UsuarioReporta 
                                    from Rep_Operativa_M b
                                    where IdRow_M={IdM}
                                ) 
                                r join Usuarios c on r.UsuarioResuelve=c.Usuario
                                order by FechaResolucion desc";

                var detalle = SqlMapper.Query<Rep_Operativa_D>(conStrSicra, sql).ToList();

                return detalle;
            }
        }

        public DataSet rptRepoMaestroOperativa(Usuarios usr, FiltroObj filtro)
        {
            string sql = $@"select
                            IIF( isnull(b.Estado,'reporte') = 'reporte','REPORTE','RESPUESTA' )    AS TIPO
                            ,a.dial
                            ,a.Nis_Rad
                            ,a.Medida
                            ,e.Region
                            ,c.Sector
                            ,a.Tarifa
                            ,a.MultiplicadorAct
                            ,a.MedidorAct
                            ,IIF( isnull(b.Estado,'reporte') = 'reporte',a.UsuarioReporta,b.UsuarioResuelve) UsuarioRepoResp
                            ,IIF( isnull(b.Estado,'reporte') = 'reporte',f.NomPersona,d.NomPersona) NomPersona
                            ,IIF( isnull(b.Estado,'reporte') = 'reporte',f.IdPerfilResoRepo,d.IdPerfilResoRepo) IdPerfilResoRepo
                            ,IIF( isnull(b.Estado,'reporte') = 'reporte',ff.NombrePerfil,dd.NombrePerfil) Perfil
                            ,a.FechaRegistro FechaRegistroReporte
                            ,a.FechaMaxResol FechaMaxResolucion
                            ,b.FechaResolucion FechaRegistroRespuesta
                            ,a.Ciclo
                            ,IIF( isnull(b.Estado,'reporte') = 'reporte',a.SolcitudCausaVerificacion,b.resolucion ) as SolicitudRespuesta
                            ,IIF( isnull(b.Estado,'reporte') = 'reporte','Reporte',b.Estado ) Realiza
                            ,a.LecturaActiva
                            ,a.ConsumoActivaFact
                            ,a.LecturaReactiva
                            ,a.ConsumoReactivaFact
                            ,a.LecturaDemanda
                            ,a.ConsumoDemandaFact
                            ,a.Anomalia
                            ,g.descripcion
                            ,a.OS
                            ,iif(a.Resuelto=0,'No','Si') EsResuelto
                            from [dbo].[Rep_Operativa_M] a
                            left join dbo.Rep_Operativa_D b on a.IdRow_M=b.IdRow_M
                            left join dbo.Sectores c on a.idSector=c.idSector
                            left join dbo.Usuarios d on b.UsuarioResuelve=d.Usuario
                            left join dbo.PerfilResolucionRepo dd on dd.IdPerfilResoRepo=d.IdPerfilResoRepo
                            left join dbo.Regiones e on c.idRegion=e.idRegion
                            left join dbo.Usuarios f on a.UsuarioReporta=f.Usuario
                            left join dbo.PerfilResolucionRepo ff on ff.IdPerfilResoRepo=f.IdPerfilResoRepo
                            left join [192.168.100.28].[EEHLogistics].dbo.tipoLecturasAnomalia g on a.Anomalia=g.idLecturaAnomalia
                            where 1=1 ";

            if (filtro != null)
            {
                ///FILTO DE NISRAD
                if (filtro.Nisrad != "")
                {
                    sql = sql + $@" and a.Nis_Rad ={filtro.Nisrad} ";
                }

                ///FILTO DE REGION Y SECTOR
                if (filtro.Region == "" && filtro.Sector == "")
                {
                    sql = sql + $@" and a.idSector in (select idSector from Sectores)";
                }
                else
                {
                    if (filtro.Region != "" && filtro.Sector == "")
                    {
                        sql = sql + $@" and c.Region ='{filtro.Region}' ";
                    }
                    else if (filtro.Region == "" && filtro.Sector != "")
                    {
                        sql = sql + $@" and b.Sector ='{filtro.Sector}' ";
                    }
                }

                ///FILTO DE MEDIDA
                if (filtro.Medida == "")
                {
                    sql = sql + $@" and a.Medida IN('Medida Directa','Medida Especial') ";
                }
                else
                {
                    sql = sql + $@" and a.Medida ='{filtro.Medida}' ";
                }

                ///FILTO DE ESTADO
                if (filtro.Resuelto != "")
                {
                    sql = sql + $@" and a.Resuelto ={filtro.Resuelto} ";
                }

                ///FILTO DE ANOMALIA
                if (filtro.idAnomalia != "")
                {
                    sql = sql + $@" and a.Anomalia ={filtro.idAnomalia} ";
                }

                ///FILTO DE DIAL
                if (filtro.Dial != "")
                {
                    sql = sql + $@" and a.dial ={filtro.Dial} ";
                }

                ///FILTO DE PERFIL REPORTE
                if (filtro.idPerfilReso != "")
                {
                    sql = sql + $@" and a.idPerfilResoRepo ={filtro.idPerfilReso} ";
                }

                ///FILTO DE FECHA REGISTRO
                if (filtro.FechaRegistro != "")
                {
                    sql = sql + $@" and CONVERT(date,a.FechaRegistro) = CONVERT(date,'{Convert.ToDateTime(filtro.FechaRegistro).ToString("yyyy-MM-dd")}') ";
                }

                ///FILTO DE FECHA VENCIMIENTO
                if (filtro.FechaMaxResol != "")
                {
                    sql = sql + $@" and CONVERT(date,a.FechaMaxResol) = CONVERT(date,'{Convert.ToDateTime(filtro.FechaMaxResol).ToString("yyyy-MM-dd")}') ";
                }

                ///FILTO DE USUARIO REPORTA
                if (filtro.UsuarioFacturacion != "")
                {
                    sql = sql + $@" and a.UsuarioReporta='{filtro.UsuarioFacturacion}' ";
                }

                ///FILTO DE USUARIO RESUELVE
                if (filtro.UsuarioResuelve != "")
                {
                    sql = sql + $@" and a.IdRow_M in(select b.IdRow_M from [dbo].[Rep_Operativa_D] b where b.UsuarioResuelve='{filtro.UsuarioResuelve}') ";
                }

                ///FILTO DE OS
                if (filtro.OS != "")
                {
                    sql = sql + $@" and a.OS ={filtro.OS} ";
                }

                ///FILTO Tiene OS
                if (filtro.TieneOS != "")
                {
                    sql = sql + $@" and '{filtro.TieneOS}' = (case when OS is null then 'NO' when OS<1 then 'NO' when OS>1 then 'SI'end) ";
                }

                //*****************APLICANDO FILTRO VENCIDO O NO*****************
                if (filtro.Vencido == 1)
                {
                    // VENCIDO -> fecha resolucion menor que la fecha actual
                    sql = sql + $@" and format(GETDATE(),'yyy-MM-dd HH:mm') > format(a.FechaMaxResol,'yyy-MM-dd HH:mm') ";
                }
                else if (filtro.Vencido == 2)
                {
                    // NO VENCIDO -> fecha resolucion mayor que la fecha actual
                    sql = sql + $@" and format(FechaMaxResol,'yyy-MM-dd HH:mm') > format(GETDATE(),'yyy-MM-dd HH:mm') ";
                }

                ///FILTO DE CICLO
                if (filtro.CicloFact != "")
                {
                    sql = sql + $@" and a.Ciclo ={filtro.CicloFact} ";
                }

                ///FILTO DE CICLO REPORDADO
                if (filtro.CicloRepo != "")
                {
                    sql = sql + $@" and FORMAT(a.FechaRegistro,'yyMM') ={filtro.CicloRepo} ";
                }
            }
            sql = sql + $@" order by a.Ciclo desc";

            var ds = Fun.GetData(sql, 0, "SicraModel");
            return ds;
        }

        public List<ImagenReporteOperativa> GetImgRepoOperativa(long idRowM)
        {
            using (var db = new SicraModel())
            {
                var archivo = db.ImagenReporteOperativa
                            .Include("Rep_Operativa_M")
                            .Where(x => x.Rep_Operativa_M.IdRow_M == idRowM)
                            .ToList();

                return archivo;
            }
        }

        public List<tipoLecturasAnomalia> GetAnomalias()
        {
            using (var db = new LogisticsModel())
            {
                var anomalias = db.tipoLecturasAnomalia.ToList();
                return anomalias;
            }
        }

        public List<int> GetCiclosOperativaHistorico()
        {
            string sql = $@"select distinct Ciclo 
                            from [dbo].[Rep_Operativa_M] 
                            order by Ciclo desc";

            var ciclo = SqlMapper.Query<int>(conStrSicra, sql).ToList();
            return ciclo;
        }

        /// <summary>
        /// Filtro para fechas
        /// </summary>
        /// <param name="modulo">Parámetro modulo: A: Anomalía, O:Operativa </param>
        /// <returns></returns>
        public List<DateTime> GetFechasRegistro(string modulo)
        {
            string sql = "";
            if (modulo == "a")
            {
                sql = $@"select distinct CONVERT(date,FechaRegistro) as FechaRegistro from REPOANOMALIA_M order by 1 asc";
            }
            else if (modulo == "o")
            {
                sql = $@"select distinct CONVERT(date,FechaRegistro) as FechaRegistro from Rep_Operativa_M order by 1 desc";
            }
            var lista = SqlMapper.Query<DateTime>(conStrSicra, sql).ToList();
            return lista;
        }

        public List<DateTime> GetFechasVencimiento(string modulo)
        {
            string sql = "";
            if (modulo == "a")
            {
                sql = $@"select distinct CONVERT(date,FechaMaxResol) as FechaMaxReso from REPOANOMALIA_M order by 1 asc";
            }
            else if (modulo == "o")
            {
                sql = $@"select distinct CONVERT(date,FechaMaxResol) as FechaMaxReso from Rep_Operativa_M order by 1 asc";
            }

            var lista = SqlMapper.Query<DateTime>(conStrSicra, sql).ToList();
            return lista;
        }

        public List<Usuarios> GetUsuariosReporteOp()
        {
            using (var db = new SicraModel())
            {
                string sql = $@"select distinct b.* 
                                from [dbo].[Rep_Operativa_M] a
                                join Usuarios b on a.UsuarioReporta =b.Usuario
                                where Activo=1 order by 3";
                var lista = SqlMapper.Query<Usuarios>(conStrSicra, sql).ToList();
                return lista;
            }
        }

        public List<Usuarios> GetUsuariosRespuestaOp()
        {
            using (var db = new SicraModel())
            {
                string sql = $@"select distinct b.* 
                                from [dbo].[Rep_Operativa_D]  a
                                join Usuarios b on a.UsuarioResuelve=b.Usuario
                                where Activo=1 order by 3";
                var lista = SqlMapper.Query<Usuarios>(conStrSicra, sql).ToList();
                return lista;
            }
        }

        public List<int> GetCiclosRegistro()
        {
            string sql = $@"select distinct FORMAT(FechaRegistro,'yyMM') as CicloReporte from Rep_Operativa_M order by 1 desc";
            var lista = SqlMapper.Query<int>(conStrSicra, sql).ToList();
            return lista;
        }

        public List<RepoAnomaliaMaestroDto> listaRepoMaestroOperativa(FiltroObj filtro)
        {
            string sql = "";

            if (filtro != null)
            {
                sql = $@"select a.*
                        ,b.Sector
                        ,c.Region,a.UsuarioReporta as UsuarioFacturacion
                        ,FechaCambioMedidor
                        ,(select COUNT(d.IdRow_M) Tf from ImagenReporteOperativa d where d.IdRow_M=a.IdRow_M) as Tf
                        from [Rep_Operativa_M] a 
                        inner join Sectores b on a.idSector=b.idSector
                        inner join Regiones c on b.idRegion=c.idRegion
                        where 1=1 ";

                ///FILTO DE NISRAD
                if (filtro.Nisrad != "")
                {
                    sql = sql + $@" and a.Nis_Rad ={filtro.Nisrad} ";
                }

                ///FILTO DE REGION Y SECTOR
                if (filtro.Region == "" && filtro.Sector == "")
                {
                    sql = sql + $@" and a.idSector in (select idSector from Sectores)";
                }
                else
                {
                    if (filtro.Region != "" && filtro.Sector == "")
                    {
                        sql = sql + $@" and c.Region ='{filtro.Region}' ";
                    }
                    else if (filtro.Region == "" && filtro.Sector != "")
                    {
                        sql = sql + $@" and b.Sector ='{filtro.Sector}' ";
                    }
                }

                ///FILTO DE MEDIDA
                if (filtro.Medida == "")
                {
                    sql = sql + $@" and a.Medida IN('Medida Directa','Medida Especial') ";
                }
                else
                {
                    sql = sql + $@" and a.Medida ='{filtro.Medida}' ";
                }

                ///FILTO DE ESTADO
                if (filtro.Resuelto != "")
                {
                    sql = sql + $@" and a.Resuelto ={filtro.Resuelto} ";
                }

                ///FILTO DE ANOMALIA
                if (filtro.idAnomalia != "")
                {
                    sql = sql + $@" and a.Anomalia ={filtro.idAnomalia} ";
                }

                ///FILTO DE DIAL
                if (filtro.Dial != "")
                {
                    sql = sql + $@" and a.dial ={filtro.Dial} ";
                }

                ///FILTO DE PERFIL REPORTE
                if (filtro.idPerfilReso != "")
                {
                    sql = sql + $@" and a.idPerfilResoRepo ={filtro.idPerfilReso} ";
                }

                ///FILTO DE FECHA REGISTRO
                if (filtro.FechaRegistro != "")
                {
                    sql = sql + $@" and CONVERT(date,a.FechaRegistro) = CONVERT(date,'{Convert.ToDateTime(filtro.FechaRegistro).ToString("yyyy-MM-dd")}') ";
                }

                ///FILTO DE FECHA VENCIMIENTO
                if (filtro.FechaMaxResol != "")
                {
                    sql = sql + $@" and CONVERT(date,a.FechaMaxResol) = CONVERT(date,'{Convert.ToDateTime(filtro.FechaMaxResol).ToString("yyyy-MM-dd")}') ";
                }

                ///FILTO DE USUARIO REPORTA
                if (filtro.UsuarioFacturacion != "")
                {
                    sql = sql + $@" and a.UsuarioReporta='{filtro.UsuarioFacturacion}' ";
                }

                ///FILTO DE USUARIO RESUELVE
                if (filtro.UsuarioResuelve != "")
                {
                    sql = sql + $@" and a.IdRow_M in(select b.IdRow_M from [dbo].[Rep_Operativa_D] b where b.UsuarioResuelve='{filtro.UsuarioResuelve}') ";
                }

                ///FILTO DE OS
                if (filtro.OS != "")
                {
                    sql = sql + $@" and a.OS ={filtro.OS} ";
                }

                ///FILTO Tiene OS
                if (filtro.TieneOS != "")
                {
                    sql = sql + $@" and '{filtro.TieneOS}' = (case when OS is null then 'NO' when OS<1 then 'NO' when OS>1 then 'SI'end) ";
                }

                //*****************APLICANDO FILTRO VENCIDO O NO*****************
                if (filtro.Vencido == 1)
                {
                    // VENCIDO -> fecha resolucion menor que la fecha actual
                    sql = sql + $@" and format(GETDATE(),'yyy-MM-dd HH:mm') > format(a.FechaMaxResol,'yyy-MM-dd HH:mm') ";
                }
                else if (filtro.Vencido == 2)
                {
                    // NO VENCIDO -> fecha resolucion mayor que la fecha actual
                    sql = sql + $@" and format(FechaMaxResol,'yyy-MM-dd HH:mm') > format(GETDATE(),'yyy-MM-dd HH:mm') ";
                }

                ///FILTO DE CICLO
                if (filtro.CicloFact != "")
                {
                    sql = sql + $@" and a.Ciclo ={filtro.CicloFact} ";
                }

                ///FILTO DE CICLO REPORDADO
                if (filtro.CicloRepo != "")
                {
                    sql = sql + $@" and FORMAT(a.FechaRegistro,'yyMM') ={filtro.CicloRepo} ";
                }

                sql = sql + $@" order by a.Ciclo desc";
            }
            else
            {
                sql = sql + $@"select a.*,
                            b.Sector,
                            c.Region,a.UsuarioReporta as UsuarioFacturacion,
                            FechaCambioMedidor
                            ,(select COUNT(d.IdRow_M) Tf from ImagenReporteOperativa d where d.IdRow_M=a.IdRow_M) as Tf
                            from [Rep_Operativa_M] a 
                            inner join Sectores b on a.idSector=b.idSector
                            inner join Regiones c on b.idRegion=c.idRegion
                            where 1=1 order by a.Ciclo desc";
            }

            using (var dbSicra = new SicraModel())
            {
                var ds1 = SqlMapper.Query<RepoAnomaliaMaestroDto>(conStrSicra, sql).ToList();

                using (var dbLogistics = new LogisticsModel())
                {
                    foreach (var reporte in ds1)
                    {
                        try
                        {
                            var usr = _uCtrl.GetUsuarioByUser(reporte.UsuarioFacturacion);

                            if (usr.IdPerfilResoRepo == null)
                            {
                                reporte.PerfilAsignado = "S/D";
                            }
                            else
                            {
                                reporte.PerfilAsignado = usr.PerfilResolucionRepo.NombrePerfil;
                            }

                            //reporte.PerfilAsignado= usr != null ? usr.PerfilResolucionRepo.NombrePerfil : "S/D";
                            reporte.UsuarioFacturacion = usr != null ? usr.NomPersona : reporte.UsuarioFacturacion;

                            if (string.IsNullOrEmpty(reporte.FechaMaxResol.ToString()))
                            {
                                reporte.VisibleResolver = false;
                            }
                            else
                            {
                                //Evaluar fecha, hora minutos.
                                int result = DateTime.Compare(DateTime.Now, (DateTime)reporte.FechaMaxResol);
                                if (result >= 1)
                                    reporte.VisibleResolver = false;
                                else
                                    reporte.VisibleResolver = true;
                            }
                        }
                        catch (Exception)
                        {
                            reporte.VisibleResolver = false;
                        }
                        try
                        {
                            string AnomaliaText = "Sin dato historico";

                            if (!string.IsNullOrWhiteSpace(reporte.Anomalia))
                            {
                                int idAnomalia = int.Parse(reporte.Anomalia);
                                AnomaliaText = dbLogistics.tipoLecturasAnomalia
                                    .Where(a => a.idLecturaAnomalia == idAnomalia)
                                    .FirstOrDefault().descripcion;
                            }
                            reporte.Anomalia = AnomaliaText;
                        }
                        catch (Exception)
                        {
                            reporte.Anomalia = "N/A";
                        }
                    }
                }
                return ds1;
            }
        }

        public Rep_Operativa_M GetReporteOperativaByClaveCiclo(decimal nisrad, int ciclo)
        {
            using (var db = new SicraModel())
            {
                var reporte = db.Rep_Operativa_M
                    .Include("Rep_Operativa_D")
                    .Include("Sectores")
                    .Include("Sectores.Regiones")
                    .Where(m => m.Nis_Rad == nisrad && m.Ciclo == ciclo)
                    .FirstOrDefault();
                return reporte;
            }
        }

        public Rep_Operativa_M PutReporteOperativaM(Rep_Operativa_M RepoMaster)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    //db.Entry(RepoMaster.Rep_Operativa_D).State = RepoMaster.Rep_Operativa_D.IdRow_D == 0 ? EntityState.Added : EntityState.Modified;
                    db.Entry(RepoMaster).State = RepoMaster.IdRow_M == 0 ? EntityState.Added : EntityState.Modified;

                    if (db.SaveChanges() > 0)
                    {
                        return RepoMaster;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Rep_Operativa_D PutReporteOperativaDetalle(Rep_Operativa_D RepoDetalle)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    db.Entry(RepoDetalle).State = RepoDetalle.IdRow_D == 0 ? EntityState.Added : EntityState.Unchanged;

                    if (db.SaveChanges() > 0)
                    {
                        return RepoDetalle;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool PutImgReporteOperativa(ImagenReporteOperativa img)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    db.Entry(img).State = db.ImagenReporteOperativa.Find(img.idRow) == null ? EntityState.Added : EntityState.Unchanged;
                    if (db.SaveChanges() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public RepoOperativaDTO GetRepoMaeOpeDTOById(long idM)
        {
            using (var db = new SicraModel())
            {
                using (var dbLogistics = new LogisticsModel())
                {
                    var r = db.Rep_Operativa_M
                    .Include("Rep_Operativa_D")
                    .Include("Sectores")
                    .Include("Sectores.Regiones")
                    .Where(m => m.IdRow_M == idM).FirstOrDefault();

                    string AnomaliaText = dbLogistics.tipoLecturasAnomalia
                        .Where(a => a.idLecturaAnomalia == r.Anomalia)
                        .FirstOrDefault().descripcion;

                    var usr = _uCtrl.GetUsuarioByUser(r.UsuarioReporta);

                    var repoDto = new RepoOperativaDTO
                    {
                        IdRow_M = r.IdRow_M,
                        Ubicacion = r.Ubicacion,
                        Region = r.Sectores.Regiones.Region,
                        Sector = r.Sectores.Sector,
                        Tarifa = r.Tarifa,
                        MultiplicadorAnt = r.MultiplicadorAnt,
                        MultiplicadorAct = r.MultiplicadorAct,
                        MedidorAnt = r.MedidorAnt,
                        MedidorAct = r.MedidorAct,
                        Medida = r.Medida,
                        Anomalia = AnomaliaText,
                        FechaCambioMedidor = r.FechaCambioMedidor,
                        LecturaActiva = r.LecturaActiva,
                        LecturaActivaRetira = r.LecturaActivaRetira,
                        ConsumoActivaFact = r.ConsumoActivaFact,
                        LecturaReactiva = r.LecturaReactiva,
                        LecturaReactivaRetira = r.LecturaReactivaRetira,
                        ConsumoReactivaFact = r.ConsumoReactivaFact,
                        LecturaDemanda = r.LecturaDemanda,
                        ConsumoDemandaFact = r.ConsumoDemandaFact,
                        dial = r.dial,
                        SolcitudCausaVerificacion = r.SolcitudCausaVerificacion,
                        Nis_Rad = r.Nis_Rad,
                        //rep_Operativa_D = r.Rep_Operativa_D,
                        Ciclo = r.Ciclo,
                        Solicitante = usr.NomPersona,
                        PerfilUsuario = usr.PerfilResolucionRepo.NombrePerfil,
                        OS = r.OS
                    };

                    return repoDto;
                }
            }
        }

        /// <summary>
        /// GUARDAR REPORTE MAESTRO, DETALLE, IMAGEN
        /// </summary>
        /// <param name="RepoMaster"></param>
        /// <returns></returns>
        public Rep_Operativa_M PutReporteOperativaMDI(Rep_Operativa_M RepoMaster)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    foreach (var RepoDeta in RepoMaster.Rep_Operativa_D)
                    {
                        db.Entry(RepoDeta).State = RepoDeta.IdRow_D == 0 ? EntityState.Added : EntityState.Unchanged;
                    }
                    //foreach (var RepoImg in RepoMaster.ImagenReporteOperativa)
                    //{
                    //    db.Entry(RepoImg).State = EntityState.Added;
                    //}
                    db.Entry(RepoMaster).State = RepoMaster.IdRow_M == 0 ? EntityState.Added : EntityState.Modified;

                    if (db.SaveChanges() > 0)
                    {
                        return RepoMaster;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
