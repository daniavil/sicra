﻿using Dapper;
using Newtonsoft.Json;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace SICRA.Controllers
{
    public class CriticaFacturacionController : Controller
    {
        private static readonly string TokenSys = ConfigurationManager.AppSettings["TokenSys"];
        private static readonly DbConnection conStrReplica = Fun.GetConnectionSQL("ReplicaConn");
        private static readonly DbConnection conStrEehApps = Fun.GetConnectionSQL("SegConn");
        private static readonly DbConnection conStrSicra = Fun.GetConnectionSQL("SicraModel");
        private static readonly DbConnection ConnINCMS = Fun.GetConnectionORA("INCMSBD");
        private UsuarioController _uCtrl = new UsuarioController();


        /*****************************************asignacion de critica***********************************************/
        public List<DatosCriticaInfo> GetMuestraBd(string fecha, string concatAnomalias, string concatObservs, string UsuCri, int porcentaje,List<string> users)
        {
            try
            {
                string where = "";
                if (!string.IsNullOrEmpty(concatAnomalias))
                {
                    where = $" and CO_AN in ({concatAnomalias}) ";
                }

                if (!string.IsNullOrEmpty(concatObservs))
                {
                    where += $" and d.cod_observacion in ({concatObservs}) ";
                }

                if (!string.IsNullOrEmpty(UsuCri))
                {
                    where += $" and UsrSoeeh in ({UsuCri}) ";
                }

                string sql = $@"DECLARE @fecha date
                                DECLARE @CantClaves int
                                declare @periodo int
                                declare @porcentaje decimal(18,2)
                                set @fecha = '{fecha}'; --->enviado desde interfaz
                                set @periodo = FORMAT(convert(date,'{fecha}'),'yyMM')
                                /*********************************calculo porcentaje**********************************/
                                set @porcentaje= convert(decimal(18,2),{porcentaje})/convert(decimal(18,2),100)

                                DROP TABLE IF EXISTS  #TempMuestraCri
                                select 
                                aa.nisrad,
                                aa.codAnoCri,
                                aa.DIAL,
                                aa.codUsuario,
                                aa.LectActiva,
                                aa.LectReactiva,
                                aa.LectDemanda,
                                aa.ciclo,
                                aa.COD_ESTADO,
                                aa.CO_AN,
                                aa.F_GEN,
                                aa.cod_observacion,
                                b.DESC_COD as nomAnoCri,
                                c.SUBCLASE as codAnoLec,
                                c.DESCRIPCION as nomAnoLec,
                                d.txt_desc as DescripObserv,
                                aa.UsrSoeeh

                                into #TempMuestraCri
                                from 
                                (select a.NIS_RAD as nisrad,
                                a.CO_AN as codAnoCri,
                                a.DIAL,
                                null as codUsuario,
                                a.LECT_ACT_ACTIVA LectActiva,
                                a.LECT_ACT_REACTIVA LectReactiva,
                                a.LECT_ACT_DEMANDA LectDemanda,
                                @periodo as ciclo,
                                a.COD_ESTADO,
                                a.CO_AN,
                                a.F_GEN,
                                a.cod_observacion,
                                a.COD_USUARIO_RESUELTA UsrSoeeh,
                                row_number() over (partition by a.NIS_RAD order by a.CO_AN) as 'ROWNR' 
                                from [eehApps].[dbo].[FACTCritica] a with(nolock) ) as aa
                                left join InCMS.dbo.CODIGOS b with(nolock) on CONVERT(VARCHAR,aa.CO_AN)=CONVERT(VARCHAR,b.cod) COLLATE SQL_Latin1_General_CP1_CI_AS
                                left join (SELECT Aa.CLAVE,aa.codigoanomalia as SUBCLASE,cc.DESCRIPCION,FORMAT(aa.FECHA_PROGRAMADA,'yyyyMM')fechaProgram
			                                from EEHAPPS.[dbo].sigcom_lectura_LOCAL as aa with(nolock)
			                                inner join EEHAPPS.[dbo].[sgc_clases_local] cc with(nolock) on aa.codigoanomalia=cc.CODIGOCLASE COLLATE SQL_Latin1_General_CP1_CI_AS
			                                where convert(date,AA.FECHA_PROGRAMADA) = @fecha ) c on aa.nisrad=IIF(c.CLAVE<1000000,c.CLAVE+3000000,c.CLAVE)
                                left join [eehApps].[dbo].FACTCriticaObservaciones d on aa.COD_OBSERVACION=d.COD_OBSERVACION_INCMS
                                where ROWNR = 1 and aa.COD_ESTADO=4 and aa.F_GEN=@fecha 
                                {where} 

                                set @CantClaves=(select convert(integer,round (count(nisrad)*@porcentaje,0)) as cantMuestra from #TempMuestraCri)

                                select top (@CantClaves) * from #TempMuestraCri ORDER BY NEWID()";

                var muestra = SqlMapper.Query<DatosCriticaInfo>(conStrEehApps, sql).ToList();

                int conteoPorUsuario = 0;
                conteoPorUsuario = muestra.Count() / users.Count();

                List<DatosCriticaInfo> listaFinal = new List<DatosCriticaInfo>();

                foreach (var usr in users)
                {
                    var listSinUsrs = muestra
                        .FindAll(x=> !listaFinal.Select(y=>y.nisrad).Contains(x.nisrad))
                        .Take(conteoPorUsuario)
                        .ToList();

                    foreach (var mues in listSinUsrs)
                    {
                        mues.codUsuario = usr;

                        listaFinal.Add(mues);

                    }
                }

                return listaFinal;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<UsuariosFacturacionInfo> GetUsuariosFacturacion()
        {
            string sql = "";
            sql = $@"select Usuario as codUsr, CONCAT(Usuario,' - ',NomPersona) as nombre 
                    from [dbo].[Usuarios] a
                    join [dbo].[PerfilModulo] b on a.IdPerfilResoRepo=b.IdPerfilResoRepo
                    join [dbo].[Modulo] c on c.idModulo=b.idModulo
                    where c.Nombre='MuestraFacturacion' and a.Activo=1";
            var usuarios = SqlMapper.Query<UsuariosFacturacionInfo>(conStrSicra, sql).ToList();
            return usuarios;
        }

        public List<AnomaliasFacturacionInfo> GetAnomaliasFacturacion()
        {
            string sql = "";
            sql = $@"select COD as id,DESC_COD as nombre from InCMS.dbo.CODIGOS where COD like 'AT%' order by DESC_COD";
            var anomalias = SqlMapper.Query<AnomaliasFacturacionInfo>(conStrEehApps, sql).ToList();
            return anomalias;
        }

        public List<int> GetPeriodosLectura()
        {
            string sql = "";

            sql = $@"select distinct periodo FROM eehApps.dbo.infoitinerariolocal
                where periodo>=202001 order by 1 desc";
            var periodos = SqlMapper.Query<int>(conStrEehApps, sql).ToList();
            return periodos;

        }

        /// <summary>
        /// retornar fechas totales o sesgadas
        /// </summary>
        /// <param name="esTotal"> Parametros define si es total(1) o sesgada(0) </param>
        /// <returns></returns>
        public List<FechaLecturaObj> GetMuestrasFechaTotalSesgada(int esTotal, int periodo)
        {
            string sql = "";
            sql = $@"select distinct CONVERT(DATE,FECHALECTURA) 
                    from MuestraCritica 
                    where EsTotal={esTotal} and Ciclo= concat(SUBSTRING('{periodo}', 1, 2),SUBSTRING('{periodo}', 5, 2))";
            var fechasDb = SqlMapper.Query<string>(conStrSicra, sql).ToList();

            string fechasDbString = "'" + string.Join("','", from item in fechasDb select item) + "'";


            //para muestras que no se hicieron en 2019 y fechas lecturas del 2020
            sql = $@"select distinct CONVERT(VARCHAR(10), fechalectura, 111) as FECHA_LECTURA
                    FROM eehApps.dbo.infoitinerariolocal
                    where periodo={periodo}
                    and CONVERT(DATE,fechalectura) not in ({fechasDbString})
                    order by FECHA_LECTURA desc";

            var fechas = SqlMapper.Query<FechaLecturaObj>(conStrEehApps, sql).ToList();
            return fechas;
        }

        public bool GetCountMuestrasbyFecha(DateTime fechaLect, bool esTipoTotal)
        {
            using (var db = new SicraModel())
            {
                var conteo = db.MuestraCritica
                    .Where(x => x.FechaLectura.Value.Year == fechaLect.Year
                    && x.FechaLectura.Value.Month == fechaLect.Month
                    && x.FechaLectura.Value.Day == fechaLect.Day
                    && x.EsTotal == esTipoTotal)
                    .ToList()
                    .Count;

                if (conteo > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public RespMsj PutAsingarMuestras(List<MuestraCritica> listaMuestras)
        {
            using (var db = new SicraModel())
            {
                db.MuestraCritica.AddRange(listaMuestras);

                if (db.SaveChanges() > 0)
                {
                    return new RespMsj { estado = "ok", mensaje = "Muestra de Lecturas guardada correctamente." };
                }
                else
                {
                    return new RespMsj { estado = "error", mensaje = "Error al guardar la muestra de lecturas." };
                }
            }
        }

        /*****************************************resolver de critica***********************************************/

        public List<MuestraCritica> GetAsignacionByUser(string user, int ciclo, DateTime dial)
        {
            using (var db=new SicraModel())
            {
                var lista = db.MuestraCritica
                    .Where(x => x.usuarioResuelve.Equals(user) 
                    && x.Ciclo == ciclo 
                    && x.FechaLectura == dial
                    && !x.Resuelto)
                    .ToList();
                return lista;
            }
        }

        public List<int> GetCiclosByUser(string user)
        {
            using (var db = new SicraModel())
            {
                var lista = db.MuestraCritica
                    //.Where(x => x.usuarioResuelve.Equals(user) && !x.Resuelto)
                    .Where(x => !x.Resuelto)
                    .Select(y => y.Ciclo)
                    .Distinct()
                    .ToList();
                return lista;
            }
        }

        public List<string> GetDialesByUser(string user,int ciclo)
        {
            string sql = "";
            sql = $@"select distinct CONVERT(nvarchar(10),CONVERT(date,FechaLectura))FechaLectura 
                    from MuestraCritica
                    where usuarioResuelve='{user}' and Resuelto=0 and Ciclo={ciclo} order by 1";
            var fechasDb = SqlMapper.Query<string>(conStrSicra, sql).ToList();
            return fechasDb;
        }

        public MuestraCritica GetAsignacionById(string idRow)
        {
            using (var db = new SicraModel())
            {
                var muestra = db.MuestraCritica.Where(x => x.idRow.ToString().Equals(idRow)).FirstOrDefault();
                return muestra;
            }
        }

        public CriticaFactInfo GetCriticaFactInfo(string clave, int periodo)
        {
            try
            {
                string qry = $@"SELECT STRING_AGG(a.CO_AN, ', ') AS CodAnomalia,
                                STRING_AGG(b.DESC_COD, ', ') AS DescripcionAnomalia,
                                c.txt_desc as ObsCri
                                from [eehApps].[dbo].[FACTCritica] a
                                left join incms.dbo.CODIGOS b on a.CO_AN = b.COD collate SQL_Latin1_General_CP1_CI_AS
                                left join [dbo].[FACTCriticaObservaciones] c on a.cod_observacion=c.COD_OBSERVACION_INCMS
                                where FORMAT(convert(date,A.F_GEN),'yyyyMM') = {periodo}
                                and NIS_RAD={clave}
                                group by txt_desc";
                var obj = SqlMapper.Query<CriticaFactInfo>(conStrEehApps, qry, commandTimeout: 0).FirstOrDefault();

                bool existeEnSicra = false;
                //evalua si en mes actual esta reportado
                using (var db = new SicraModel())
                {
                    decimal nis = decimal.Parse(clave);
                    int ciclo = int.Parse(periodo.ToString().Substring(2, 4));

                    var existe = db.REPOANOMALIA_M
                        .Where(x => x.Nis_Rad == nis
                        && x.Ciclo == ciclo)
                        .Any();

                    existeEnSicra = existe;
                    //obj.repoSicra = existe != null ? "SI" : "NO";
                }

                //evalua si en historico con el ciclo esta reportado
                if (!existeEnSicra)
                {
                    using (var db = new SicraModel())
                    {
                        decimal nis = decimal.Parse(clave);
                        int ciclo = int.Parse(periodo.ToString().Substring(2, 4));

                        var existe = db.REPOANOMALIA_M_HISTORICO
                            .Where(x => x.Nis_Rad == nis
                            && x.Ciclo == ciclo)
                            .Any();

                        existeEnSicra = existe;
                    }
                }
                obj.repoSicra = existeEnSicra ? "SI" : "NO";


                qry = $@"SELECT a.ENER_FACT as csmocri,CSMO_fACT as csmofact, b.IND_REST esprom,a.usuario as usrcri
                        FROM TRABPEND_AF a
                        join HFACTURACION b on a.nis_rad = b.nis_rad
                        where to_char(a.F_GEN,'YYYYMM') = {periodo}
                        and to_char(b.F_FACT,'YYYYMM') = {periodo}
                        and a.nis_rad={clave}
                        and a.est_af in ('SA400','SA311')";
                var obj2 = SqlMapper.Query<CriticaFactInfo>(ConnINCMS, qry, commandTimeout: 0).FirstOrDefault();
                obj.csmocri = obj2.csmocri;
                obj.csmofact = obj2.csmofact;
                obj.esprom = obj2.esprom;
                obj.usrcri = obj2.usrcri;

                if (obj2.esprom == "2")
                {
                    obj.accion = "PROMEDIÓ";
                }
                else
                {
                    if (obj2.csmocri.Trim() == obj2.csmofact.Trim())
                    {
                        obj.accion = "CONFIRMÓ";
                    }
                    else
                    {
                        obj.accion = "MODIFICÓ";
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ObjIdValor> GetCriticaObservaciones()
        {
            try
            {
                string qry = $@"select cod_observacion as id,txt_desc valor from [dbo].[FACTCriticaObservaciones] where activo=1";
                var lista = SqlMapper.Query<ObjIdValor>(conStrEehApps, qry, commandTimeout: 0).ToList();
                return lista;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ObjIdValor> GetUsuariosSoeehCritica(string Fecha)
        {
            try
            {
                string qry = $@"select  distinct
                                a.COD_USUARIO_RESUELTA as id,
                                concat(a.COD_USUARIO_RESUELTA,' - ',  TRIM(b.Nombres),' ',TRIM(b.PrimerApellido),' ',TRIM(b.SegundoApellido))valor,
                                TRIM(b.Nombres) as aa
                                from EEHApps.dbo.[FACTCritica] a 
                                join GENUsuarios b on a.COD_USUARIO_RESUELTA=b.Usuario
                                where COD_ESTADO=4 and a.F_GEN='{Fecha}'
                                Order by aa asc";
                var lista = SqlMapper.Query<ObjIdValor>(conStrEehApps, qry, commandTimeout: 0).ToList();
                return lista;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //public List<ddlGenral> GetListaUsuarioSoeehRefa(string ciclo)
        //{
        //    try
        //    {
        //        string qry = $@"select  distinct
        //                        a.COD_USUARIO_RESUELTA as id,
        //                        concat(TRIM(b.Nombres),' ',TRIM(b.PrimerApellido),' ',TRIM(b.SegundoApellido))valor
        //                        from EEHApps.dbo.FACTRefactura a 
        //                        join GENUsuarios b on a.COD_USUARIO_RESUELTA=b.Usuario
        //                        where CICLO={ciclo.Substring(2,4)} and COD_ESTADO=4
        //                        Order by 2 asc";
        //        var lista = SqlMapper.Query<ddlGenral>(conStrEehApps, qry, commandTimeout: 0).ToList();
        //        return lista;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        public void PutMuestraCritica(MuestraCritica m)
        {
            using (var db = new SicraModel())
            {
                db.Entry(m).State = EntityState.Modified;
                db.SaveChanges();
            }
        }


        /*****************************************resolucion (vista) de critica***********************************************/
        public List<int> GetCiclosMuestras()
        {
            using (var db = new SicraModel())
            {
                var listaCiclos = db.MuestraCritica.Select(y => y.Ciclo ).Distinct().ToList();
                return listaCiclos.OrderByDescending(x => x).ToList();
            }
        }
        
        public List<string> GetDialesResueltos(int ciclo)
        {
            string sql = "";
            sql = $@"select distinct CONVERT(nvarchar(10),CONVERT(date,FechaLectura))FechaLectura 
                    from MuestraCritica
                    where Resuelto=1 and Ciclo={ciclo} order by 1";
            var fechas = SqlMapper.Query<string>(conStrSicra, sql).ToList();
            return fechas;

            //using (var db = new SicraModel())
            //{
            //    var lista = db.MuestraCritica
            //        .Where(x => x.Ciclo == ciclo)
            //        .Select(y => y.dial).Distinct().ToList();
            //    return lista;
            //}
        }

        public MuestrasInfo GetMuestrasUsuariosEstados(int ciclo, DateTime dial)
        {
            var muestra = new MuestrasInfo();

            using (var db = new SicraModel())
            {
                var muestrasusr = new List<ListaMuestrasUsuarios>();
                var lmu = db.MuestraCritica
                    .Where(x => x.Ciclo == ciclo
                    && x.FechaLectura == dial)
                    .Distinct()
                    .ToList();

                foreach (var usr in lmu.Select(x => x.usuarioResuelve).Distinct())
                {
                    var muestras = db.MuestraCritica
                            .Where(x => x.Ciclo == ciclo
                            && x.FechaLectura == dial
                            && x.usuarioResuelve.Equals(usr))
                            .ToList();

                    var muesusr = new ListaMuestrasUsuarios
                    {
                        usuario = usr,
                        NomUsr = _uCtrl.GetUsuarioByUser(usr).NomPersona,
                        CantResueltas = muestras.Where(x => x.Resuelto).Count(),
                        CantNoResueltas = muestras.Where(x => !x.Resuelto).Count()
                    };

                    muestrasusr.Add(muesusr);
                }

                muestra.CantTotalGen = lmu.Count();
                muestra.CantResueltas = lmu.Where(x => x.Resuelto).Count();
                muestra.CantNoResueltas = lmu.Where(x => !x.Resuelto).Count();
                muestra.ListaMuestras = muestrasusr.OrderBy(x => x.NomUsr).ToList();

                return muestra;
            }
        }

        public List<MuestraCritica> GetResueltasByUser(List<string> users, int ciclo, DateTime dial)
        {
            using (var db = new SicraModel())
            {
                var lista = db.MuestraCritica
                    .Where(x => users.Contains(x.usuarioResuelve)
                    && x.Ciclo == ciclo
                    && x.FechaLectura == dial
                    && x.Resuelto)
                    .ToList();
                return lista;
            }
        }

        /*****************************************Reportería de critica***********************************************/
        public object GetDatosGeneral(Filtro f)
        {
            var datos = new DatosRptGeneral
            {
                Filtros = new Filtro(),
                SeriesDatos = new List<SerieGeneral>()
            };
            string sql = "", where = " ";

            where = !string.IsNullOrWhiteSpace(f.FechasLectura) ? $" and FechaLectura in ({f.FechasLectura}) " : " ";


            sql = $@"
                    declare @TbTemp as table(Concepto nvarchar(max),Valor int)
                    DECLARE @CantMuestra int, @PromCalidadMuestra decimal(18,2) ,@CantAccionCorrecta int,@CantObservCorrecta int,@CantSicraSiDebioRepoSi int

                    set @CantMuestra = (SELECT COUNT(*) FROM MuestraCritica where Ciclo = {f.Ciclo} {where})
                    set @PromCalidadMuestra = (SELECT AVG(Calificacion) FROM MuestraCritica where Ciclo = {f.Ciclo} {where} )
                    set @CantAccionCorrecta = (SELECT COUNT(*) FROM MuestraCritica where Ciclo = {f.Ciclo} {where} and P1AccionCorrecta='SI')
                    set @CantObservCorrecta = (SELECT COUNT(*) FROM MuestraCritica where Ciclo = {f.Ciclo} {where} and P2ObservacionCorrecta='SI')
                    set @CantSicraSiDebioRepoSi = (SELECT COUNT(*) FROM MuestraCritica where Ciclo = {f.Ciclo} {where} and P3EnSicra='SI' and P4DebioRepoSicra='SI')


                    insert into @TbTemp values ('Cantidad Muestra Evaluada',@CantMuestra)
                    insert into @TbTemp values ('Promedio % Muestra Evaluada', @PromCalidadMuestra)
                    insert into @TbTemp values ('Cantidad de Cuentas con Acción Correcta', @CantAccionCorrecta)
                    insert into @TbTemp values ('Cantidad de Cuentas con Observación Correcta', @CantObservCorrecta)
                    insert into @TbTemp values ('Cantidad de Cuentas en SICRA y Debió Reportar Correcta', @CantSicraSiDebioRepoSi)

                    select * from @TbTemp";

            datos.SeriesDatos = SqlMapper.Query<SerieGeneral>(conStrSicra, sql).ToList();





            return datos;
        }

        public object GetDatosDetalle(Filtro f)
        {
            var datos = new DatosRptDetalle
            {
                Filtros = new Filtro(),
                SeriesDatos = new List<SerieDetalle>()
            };
            string sql = "", where = " ";

            where = !string.IsNullOrWhiteSpace(f.FechasLectura) ? $" and FechaLectura in ({f.FechasLectura}) " : " ";


            sql = $@"
                    DECLARE @FechaLect date
                    DECLARE @UsrResIncms VARCHAR(MAX)
                    declare @totalMuestra int
                    set @totalMuestra = ( SELECT count(*) FROM MuestraCritica  where Ciclo = {f.Ciclo} {where} )
                    declare @tablaDatos as table(
                    NomPersona nvarchar(50)
                    ,PromCalidadMuestra int
                    ,CantAccionCorrecta int
                    ,CantObservCorrecta int
                    ,CantSicraSiDebioRepoSi int
                    ,TotalMuestra int)

                    DECLARE cursor_Usr CURSOR
                    FOR SELECT DISTINCT usuarioResIncms FROM MuestraCritica where Ciclo = {f.Ciclo} {where}
                    OPEN cursor_Usr;
                    FETCH NEXT FROM cursor_Usr INTO @UsrResIncms 

                    WHILE @@FETCH_STATUS = 0
	                    BEGIN
		                    DECLARE @NomPersona nvarchar(50), @PromCalidadMuestra decimal(18,2)
		                    ,@CantAccionCorrecta int,@CantObservCorrecta int,@CantSicraSiDebioRepoSi int
      
		                    set @NomPersona = (SELECT DISTINCT isnull(b.NomPersona,a.usuarioResIncms) FROM MuestraCritica a 
							                    left join Usuarios b on a.usuarioResIncms=b.Usuario where a.usuarioResIncms=@UsrResIncms)
		                    set @PromCalidadMuestra = (SELECT AVG(Calificacion) FROM MuestraCritica where Ciclo = {f.Ciclo} {where} and usuarioResIncms = @UsrResIncms)
		                    set @CantAccionCorrecta = (SELECT COUNT(*) FROM MuestraCritica where Ciclo = {f.Ciclo} {where} and usuarioResIncms = @UsrResIncms and P1AccionCorrecta='SI')
		                    set @CantObservCorrecta = (SELECT COUNT(*) FROM MuestraCritica where Ciclo = {f.Ciclo} {where} and usuarioResIncms = @UsrResIncms and P2ObservacionCorrecta='SI')
		                    set @CantSicraSiDebioRepoSi = (SELECT COUNT(*) FROM MuestraCritica where Ciclo = {f.Ciclo} {where} and usuarioResIncms = @UsrResIncms and P3EnSicra='SI' and P4DebioRepoSicra='SI')

		                    insert into @tablaDatos
		                    select @NomPersona,@PromCalidadMuestra,@CantAccionCorrecta
                            ,@CantObservCorrecta,@CantSicraSiDebioRepoSi,@totalMuestra
		
		                    FETCH NEXT FROM cursor_Usr INTO @UsrResIncms 
	                    END;
                    CLOSE cursor_Usr;
                    DEALLOCATE cursor_Usr;
                    select * from @tablaDatos";
            datos.SeriesDatos = SqlMapper.Query<SerieDetalle>(conStrSicra, sql).ToList();
            return datos;
        }

        public object GetDatosControl(Filtro f)
        {
            var datos = new DatosRptControl
            {
                Filtros = new Filtro(),
                SeriesDatos = new List<SerieControl>()
            };
            string sql = "", where = " ";

            where = !string.IsNullOrWhiteSpace(f.FechasLectura) ? $" and FechaLectura in ({f.FechasLectura}) " : " ";


            sql = $@"
                    DECLARE @UsrResuelve VARCHAR(MAX)
                    declare @totalMuestra int
                    set @totalMuestra = (SELECT count(*) FROM MuestraCritica where Ciclo = {f.Ciclo} {where})
                    declare @tablaDatos as table(NomPersona nvarchar(50),Asignados int,CantNoResueltos int,CantResueltos int)
                    DECLARE cursor_Usr CURSOR
                    FOR SELECT DISTINCT usuarioResuelve FROM MuestraCritica where Ciclo = {f.Ciclo} {where}

                    OPEN cursor_Usr;
                    FETCH NEXT FROM cursor_Usr INTO @UsrResuelve

                    WHILE @@FETCH_STATUS = 0
	                    BEGIN
		                    DECLARE @Asignados int, @NoResueltos decimal(18,2),@Resueltos decimal(18,2)
		                    ,@noresporcent decimal(18,2),@resueltoporcent decimal(18,2), @NomPersona nvarchar(50)
      
		                    set @Asignados = (SELECT count(*) FROM MuestraCritica where Ciclo = {f.Ciclo} {where} and usuarioResuelve = @UsrResuelve)
		                    set @NoResueltos = (SELECT count(*) FROM MuestraCritica where Ciclo = {f.Ciclo} {where} and usuarioResuelve = @UsrResuelve and Resuelto=0)
		                    set @Resueltos = (SELECT count(*) FROM MuestraCritica where Ciclo = {f.Ciclo} {where} and usuarioResuelve = @UsrResuelve and Resuelto=1)
		                    set @NomPersona = (SELECT DISTINCT isnull(b.NomPersona,a.usuarioResuelve) FROM MuestraCritica a 
							                    left join Usuarios b on a.usuarioResuelve=b.Usuario where a.usuarioResuelve=@UsrResuelve)

		                    insert into @tablaDatos
		                    select @NomPersona,@Asignados,@NoResueltos,@Resueltos
		
		                    FETCH NEXT FROM cursor_Usr INTO @UsrResuelve
	                    END;

                    CLOSE cursor_Usr;
                    DEALLOCATE cursor_Usr;
                    select * from @tablaDatos";
            datos.SeriesDatos = SqlMapper.Query<SerieControl>(conStrSicra, sql).ToList();

            return datos;
        }
    }
}
