﻿using Dapper;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SICRA.Controllers
{
    public class RefacturaController
    {
        private static readonly DbConnection conStrSicra = Fun.GetConnectionSQL("SicraModel");
        private static readonly DbConnection conStrIcmsSQL = Fun.GetConnectionSQL("IncmsConn");
        private static readonly DbConnection conStrIcmsORA = Fun.GetConnectionORA("INCMSBD");

        //*****************************************PRE REFACTURACION - AUTORIZACIONES********************************

        public bool GetPermiteAutorizar(int IdUsuario)
        {
            using (var db = new SicraModel())
            {
                var permite = db.Usuarios.Find(IdUsuario).idPerfil == null ? false : true;
                return permite;
            }
        }

        public List<TipoOrigen> GetAllTiposOrigen()
        {
            using (var db = new SicraModel())
            {
                var lista = db.TipoOrigen.ToList();
                return lista;
            }
        }

        public List<TipoRectificacion> GetAllTiposRectificacion()
        {
            using (var db = new SicraModel())
            {
                var lista = db.TipoRectificacion.ToList();
                return lista;
            }
        }


        /// <summary>
        /// INSERTAR OBJETO GESTION MAESTRA CON 
        /// HIJOS GESTION DETALLE Y DOCUMENTOS GESTION
        /// </summary>
        /// <param name="GesMae"></param>
        /// <returns></returns>
        public RespMsj PutGestionRefactura_Mae_Det_DocGes(GestionRefacturaMae GesMae)
        {
            using (var db = new SicraModel())
            {
                //Insertar Gestion Detalle
                foreach (var GesDet in GesMae.GestionRefacturaDet)
                {
                    if (GesDet.DocGestionSoporteRefactura != null)
                    {
                        //Insertar Gestion Detalle-> Documento Gestión
                        foreach (var DocGes in GesDet.DocGestionSoporteRefactura)
                        {
                            db.Entry(DocGes).State = DocGes.IdDoc == 0 ? EntityState.Added : EntityState.Unchanged;
                        }
                    }
                    db.Entry(GesDet).State = GesDet.IdGesRefDet == 0 ? EntityState.Added : EntityState.Unchanged;
                }

                if (GesMae.CicloRefactura != null)
                {
                    //Insertar Ciclos de refacturacion
                    foreach (var ciclo in GesMae.CicloRefactura)
                    {
                        db.Entry(ciclo).State = ciclo.idCiclo == 0 ? EntityState.Added : EntityState.Unchanged;
                    }
                }

                //Insertar GESTION MAESTRA
                db.Entry(GesMae).State = GesMae.IdGesRefMae == 0 ? EntityState.Added : EntityState.Modified;
                try
                {
                    if (db.SaveChanges() > 0)
                    {
                        return new RespMsj { estado = "ok", mensaje = "Datos Guardados Correctamente." };
                    }
                    else
                    {
                        return new RespMsj { estado = "error", mensaje = "Error al guardar datos." };
                    }
                }
                catch (Exception ex)
                {
                    return new RespMsj { estado = "error", mensaje = $"Exepción: {ex}" };
                }
            }
        }

        public List<RefacturaGrid> GetSolicitudesRefaByPerfil(Usuarios user)
        {
            using (var db =new SicraModel())
            {
                var qry = $@"Select *
                            FROM GestionRefacturaMae A
                            join Usuarios B on A.idUsuario=B.idUsuario
                            where A.IdPerfilActualAsignado={user.idPerfil}
                            AND A.Finalizada=0";

                List<RefacturaGrid> solicitudes = new List<RefacturaGrid>();

                solicitudes = SqlMapper.Query<RefacturaGrid>(conStrSicra, qry).ToList();

                if (solicitudes.Count > 0)
                {
                    return solicitudes;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<RefacturaGrid> GetAllSolicitudesRefaGrid()
        {
            using (var db = new SicraModel())
            {
                var gestiones = db.GestionRefacturaMae
                    .Include("Usuarios")
                    .ToList();

                if (gestiones.Count > 0)
                {
                    List<RefacturaGrid> solicitudes = new List<RefacturaGrid>();
                    foreach (var gMae in gestiones)
                    {
                        var soli = new RefacturaGrid
                        {
                            IdGesRefMae = gMae.IdGesRefMae,
                            nisrad = gMae.nisrad,
                            ComentarioSolicitud = gMae.ComentarioSolicitud,
                            NomPersona = gMae.Usuarios.NomPersona,
                            FechaCreacion = gMae.FechaCreacion,
                            Estado = gMae.Denegada ? "Rechazada" : "Aceptada",
                            Finalizado = gMae.Finalizada ? "SI" : "NO"
                        };
                        solicitudes.Add(soli);
                    }
                    return solicitudes;
                }
                else
                {
                    return null;
                }
            }
        }
        /*
        public RefacturaInfo GetInfoSolicitudRefaByidGestion(int IdGestionRefa,int? IdPerfilUsr)
        {
            using (var db = new SicraModel())
            {
                var g = db.GestionRefacturaMae
                    .Include("GestionRefacturaDet")
                    .Include("GestionRefacturaDet.PerfilAutorizacion")
                    .Include("Usuarios")
                    .Include("GestionRefacturaDet.DocGestionSoporteRefactura")
                    .Include("TipoOrigen")
                    .Where(x => x.IdGesRefMae == IdGestionRefa & !x.Denegada)
                    .FirstOrDefault();

                if (g != null)
                {
                    var resoluciones = new List<Resolucion>();
                    foreach (var GestDet in g.GestionRefacturaDet.Where(x=>x.Resuelto))
                    {
                        var r = new Resolucion
                        {
                            IdGestDet = GestDet.IdGesRefDet,
                            ComentarioResol = GestDet.ComentarioResuelve,
                            FechaHoraResol = GestDet.FechaHoraResuelto,
                            NomResolvio = GestDet.Usuarios.NomPersona,
                            Perfil = GestDet.PerfilAutorizacion.Nombre
                        };
                        resoluciones.Add(r);
                    }

                    RefacturaInfo solicitud = new RefacturaInfo
                    {
                        IdAsigRefa = g.GestionRefacturaDet.Where(x => x.idPerfilAsignado == IdPerfilUsr).FirstOrDefault().IdGesRefDet,
                        IdGestionRefa = g.IdGesRefMae,
                        Nis = g.nisrad,
                        Origen = g.TipoOrigen.nombre,
                        IdGestionOrigen = g.codGestion,
                        TipoRefa = g.TipoRectificacion.nombre,
                        MontoRefa = g.monto.ToString(),
                        Dbcr = g.dbcr == "db" ? "Débito" : "Crédito",
                        Ciclos = g.ciclo,
                        Sector = g.Sectores.Sector,
                        Mercado = g.Mercado.Nombre,
                        NomSolicita = g.Usuarios.NomPersona,
                        RazonSolicita = g.ComentarioSolicitud,
                        //NombreFile = g.DocGestionSoporteRefactura.NomDoc,
                        //UrlFile = g.DocGestionSoporteRefactura.Ruta,
                        FechaHoraCreado = g.FechaCreacion,
                        Resoluciones = resoluciones
                    };

                    return solicitud;
                }
                else
                {
                    return null;
                }
            }
        }
        */
        public RefacturaInfo GetInfoSolicitudRefaByidGestion(int IdGestionRefa, int? IdPerfilUsr = null)
        {
            using (var db = new SicraModel())
            {
                var g = db.GestionRefacturaMae
                    .Include("GestionRefacturaDet")
                    .Include("GestionRefacturaDet.PerfilAutorizacion")
                    .Include("Usuarios")
                    .Include("GestionRefacturaDet.DocGestionSoporteRefactura")
                    .Include("TipoOrigen")
                    .Where(x => x.IdGesRefMae == IdGestionRefa)
                    .FirstOrDefault();

                if (g != null)
                {
                    var resoluciones = new List<Resolucion>();
                    var documentos = new List<DocumentosAutorizacion>();
                    foreach (var a in g.GestionRefacturaDet)
                    {
                        var r = new Resolucion
                        {
                            IdGestDet = a.IdGesRefDet,
                            ComentarioResol = a.ComentarioResuelve,
                            FechaCreacion = a.FechaCreacion,
                            FechaHoraResuelto = a.FechaHoraResuelto,
                            NomResolvio = a?.Usuarios?.NomPersona,
                            Perfil = a.PerfilAutorizacion1.Nombre,
                            Estado = (bool)a.Denego ? "Denegado" : "Aceptado"
                        };
                        resoluciones.Add(r);

                        foreach (var doc in a.DocGestionSoporteRefactura)
                        {
                            documentos.Add(new DocumentosAutorizacion
                            {
                                IdGesRefDet = a.IdGesRefDet,
                                IdDoc = doc.IdDoc,
                                NomPersona = a.Usuarios.NomPersona,
                                NomDoc = doc.NomDoc,
                                Ruta = doc.Ruta
                            });
                        }
                    }

                    string tlCo = $"exec SP_TIMELINE_AUTORIZACION {g.IdGesRefMae},1";
                    var timelineCo = SqlMapper.Query<TimeLineAutorizacion>(conStrSicra, tlCo, commandTimeout: 0).ToList();

                    string tlCe = $"exec SP_TIMELINE_AUTORIZACION {g.IdGesRefMae},2";
                    var timelineCe = SqlMapper.Query<TimeLineAutorizacion>(conStrSicra, tlCe, commandTimeout: 0).ToList();

                    RefacturaInfo solicitud = new RefacturaInfo()
                    {
                        IdGestionRefa = g.IdGesRefMae,
                        Nis = g.nisrad,
                        Origen = g.TipoOrigen.nombre,
                        IdGestionOrigen = g.codGestion,
                        TipoRefa = g.TipoRectificacion.nombre,
                        kWhCv = g.kWhCv.ToString(),
                        kWhRef = g.kWhRef.ToString(),
                        kWhTotal = g.kWhTotal.ToString(),
                        LpsCv = g.LpsCv.ToString(),
                        LpsRef = g.LpsRef.ToString(),
                        LpsTotal = g.LpsTotal.ToString(),
                        Dbcr = g.dbcr == "db" ? "Débito" : "Crédito",
                        Ciclos = g.ciclo,
                        Sector = g.Sectores.Sector,
                        Mercado = g.Mercado.Nombre,
                        NomSolicita = g.Usuarios.NomPersona,
                        RazonSolicita = g.ComentarioSolicitud,
                        FechaHoraCreado = g.FechaCreacion,
                        Resoluciones = resoluciones,
                        Estado = g.Denegada ? "Rechazada" : "Aceptada",
                        FinalizadoString = g.Finalizada ? "SI" : "NO",
                        Finalizado = g.Finalizada,
                        Denegada = g.Denegada,
                        TimeLineCO = timelineCo,
                        TimeLineCE = timelineCe,
                        DocsAuto = new List<DocumentosAutorizacion>(documentos),
                        //PerfilesHijos = GetIdPerfilesAnteriores(g.IdGesRefMae)
                        PerfilesHijos = GetIdPerfilesAnteriores(IdPerfilUsr, null, g.IdArea)
                    };

                    return solicitud;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<DocGestionSoporteRefactura> GetDocumentosByIdGestDet(int idgestDet)
        {
            using (var db = new SicraModel())
            {
                var docs = db.DocGestionSoporteRefactura
                    .Where(x => x.IdGesRefDet == idgestDet)
                    .ToList();
                return docs;
            }
        }

        private List<PerfilAutorizacion> GetIdPerfilesAnteriores(int idGestMae)
        {
            try
            {
                var perfiles = new List<PerfilAutorizacion>();
                var sql = $@"SELECT t.idPerfil,t.Nombre,t.IdPerfilSuperior
                        FROM (
                          SELECT a.idPerfil,a.Nombre,a.IdPerfilSuperior, MAX(b.FechaHoraResuelto) AS fecha
                          from [dbo].[PerfilAutorizacion] a
                          join [dbo].[GestionRefacturaDet] b on a.idPerfil=b.idPerfilAutoriza
                          where b.IdGesRefMae={idGestMae}
                          GROUP BY a.idPerfil,a.Nombre,a.IdPerfilSuperior
                        ) t
                        ORDER BY fecha DESC";
                var p = SqlMapper.Query<PerfilAutorizacion>(conStrSicra, sql, commandTimeout: 0).ToList();
                return p;
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        private List<PerfilAutorizacion> GetIdPerfilesAnteriores(int? idPerfilUsr, List<PerfilAutorizacion> perfiles = null, int? IdAreaPerfil = null)
        {
            try
            {
                if (idPerfilUsr != null)
                {
                    if (perfiles == null)
                    {
                        perfiles = new List<PerfilAutorizacion>();
                    }
                    var sql = $@"select a.* 
                                from  PerfilAutorizacion a
                                join PerfilesAreas b on a.idPerfil=b.idPerfil
                                where b.IdArea={IdAreaPerfil} and a.IdPerfilSuperior={idPerfilUsr}";

                    var p = SqlMapper.Query<PerfilAutorizacion>(conStrSicra, sql, commandTimeout: 0).ToList();

                    foreach (var item in p)
                    {
                        perfiles.Add(item);
                        GetIdPerfilesAnteriores(item.idPerfil, perfiles, IdAreaPerfil);
                    }
                }
                return perfiles;
            }
            catch (Exception)
            {
                return null;
            }
        }


        //public RespMsj PutAsignacion(GestionRefacturaMae gestion)
        //{
        //    using (var db=new SicraModel())
        //    {
        //        string FechaFinalizadaDenegada = " ";
        //        if (gestion.Denegada == true)
        //        {
        //            FechaFinalizadaDenegada = ", FechaFinalizada = GETDATE() ";
        //        }

        //        string sql = $@"update GestionRefactura
        //                        set Denegada = {(gestion.Denegada==true?1:0)}
        //                        {FechaFinalizadaDenegada}
        //                        where IdGestionRefa = {gestion.IdGestionRefa};

        //                        update AsignacionAutorizacionRefa
        //                        set IdUsuarioAutoriza={gestion.AsignacionAutorizacionRefa.FirstOrDefault().IdUsuarioAutoriza},
        //                        ComentarioResuelve='{gestion.AsignacionAutorizacionRefa.FirstOrDefault().ComentarioResuelve}',
        //                        Resuelto=1,
        //                        FechaHoraResuelto = GETDATE()
        //                        where idAsigRefa={gestion.AsignacionAutorizacionRefa.FirstOrDefault().idAsigRefa};";

        //        var resp = Fun.ExecQuery(sql, 0, "SicraModel");

        //        var retorna = new RespMsj
        //        {
        //            estado = resp,
        //            mensaje = resp == "ok" ? "Datos Guardados Correctamente." : "Error al guardados datos."
        //        };
        //        return retorna;
        //    }
        //}

        public GestionRefacturaMae GetGestionByIdGestion(int idges)
        {
            using (var db = new SicraModel())
            {
                var gestion = db.GestionRefacturaMae
                    .Include("GestionRefacturaDet")
                    .Include("GestionRefacturaDet.DocGestionSoporteRefactura")
                    .Include("CicloRefactura")
                    .Include("Usuarios")
                    .Where(x => x.IdGesRefMae == idges)
                    .FirstOrDefault();
                return gestion;
            }
        }

        public GestionRefacturaMae GetGestionByIdAsignacion(int idAsig)
        {
            using (var db = new SicraModel())
            {
                var gestion = db.GestionRefacturaMae
                    .Include("AsignacionAutorizacionRefa")
                    .Where(x => x.GestionRefacturaDet.Any(y => y.IdGesRefMae== idAsig))
                    .FirstOrDefault();

                return gestion;
            }
        }

        public bool GetPermiteMercado(int idUsr,int idMercado)
        {
            using (var db = new SicraModel())
            {
                var permite = db.Usuario_Mercado.Any(x => x.IdMercado == idMercado && x.idUsuario==idUsr);
                return permite;
            }
        }


        //**************************************************POST REFACTURACION*****************************************

        public int GetCicloAnterior(int PrimerCiclo)
        {
            var qry = $@"select to_char(ADD_MONTHS(to_date('{PrimerCiclo}','YYYYMM'), -1),'YYYYMM') CicloAnterior from dual";
            var c = SqlMapper.Query<int>(conStrIcmsORA, qry).FirstOrDefault();
            return c;
        }

        public PeriodosMulti GetCiclosFacturadosYMultiByNis(int nis)
        {
            var obj = new PeriodosMulti();
            try
            {
                var qry = $@"select distinct concat(TO_CHAR(f_fact, 'yyyy'),TO_CHAR(f_fact, 'MM')) as periodo
                        from recibos where nis_rad={nis}
                        order by concat(TO_CHAR(f_fact, 'yyyy'),TO_CHAR(f_fact, 'MM')) desc";
                var ciclos = SqlMapper.Query<int>(conStrIcmsORA, qry).ToList();

                qry = $@"select cte as multiplicador from maestro_suscriptores where nis_rad={nis}";
                var multi = SqlMapper.Query<decimal>(conStrIcmsORA, qry).FirstOrDefault();

                obj.periodo = ciclos;
                obj.multiplicador = multi;
                obj.RespCallBack = "OK";
            }
            catch (Exception ex)
            {
                obj.periodo = new List<int>();
                obj.multiplicador = 0;
                obj.RespCallBack = ex.Message;
            }
            return obj;
        }

        public List<DatosRefactura> GetDatosRefactura(int nisrad, string periodos)
        {
            var qry = $@"select * 
                        from
                        (
                        SELECT 
                        r.nis_rad nisrad,
                        t.desc_tar nombretarifa,
                        t.cod_tar codtarifa,
                        t.cod_tar_ofi codtarifaofi,
                        r.sec_rec secrec,
                        R.SEC_NIS secnis, 
                        (CASE WHEN to_char(r.f_fact_anul,'yyyymmdd') = to_char(TO_DATE('29991231','yyyymmdd'),'yyyymmdd') THEN r.f_fact ELSE r.F_FACT_ANUL END)ffactura,
                        to_char(r.f_fact,'yyyymmdd') ffacturaint,
                        R.F_FACT frefactura,
                        R.IMP_TOT_REC totalimporte,
                        H.CSMO_FACT consumoactivafact, 
                        H.CSMO_REACT consumoreactivafact,
                        R.F_FACT_ANT ffactanterior,
                        (CASE WHEN to_char(r.F_FACT_ANUL,'yyyymmdd') = to_char(TO_DATE('29991231','yyyymmdd'),'yyyymmdd') THEN to_char(r.f_fact,'yyyymm') ELSE to_char(r.F_FACT_ANUL,'yyyymm') END)periodofacturado,
                        to_char(r.f_fact,'yyyymm')periodorefacturado
                        FROM RECIBOS R, HFACTURACION H,maestro_suscriptores ms,mtarifas t,sumcon s
                        WHERE 
                        /******************LLAVE COMPUESTA ENTRE RECIBOS Y HFACTURACION********************/
                        R.NIS_RAD = H.NIS_RAD
                        AND R.SEC_REC = H.SEC_REC
                        AND R.SEC_NIS = H.SEC_NIS
                        AND R.F_FACT = H.F_FACT
                        /**********************************************************************************/
                        AND ms.nis_rad = R.NIS_RAD
                        AND s.nis_rad = ms.NIS_RAD
                        and s.cod_tar = t.cod_tar
                        and R.EST_ACT <> 'ER600'
                        START WITH  to_char(r.f_fact,'yyyymm') in ({periodos})
                        AND R.F_FACT_ANUL = TO_DATE('29991231','yyyymmdd')
                        AND R.TIP_REC = 'TR010'
                        AND R.NIS_RAD = {nisrad}
                        connect by prior r.nis_rad = r.nis_Rad 
                        and prior r.sec_nis = r.sec_nis 
                        and prior r.sec_rec = r.sec_rec_anul 
                        and prior r.f_fact = r.f_fact_anul
                        order by TO_NUMBER(periodofacturado) asc)";

            var resp = SqlMapper.Query<DatosRefactura>(conStrIcmsORA, qry).ToList();
            return resp;
        }

        public List<LecturaPeriodo> GetLecturaByPeriodo(FiltrosLectPeriodo f)
        {
            var qry = $@"select H.CSMO,h.lect,h.cte,h.tip_csmo
                    ,h.tip_lect,t.desc_tipo as Nom_Tip_Lect
                    ,(h.lect || ' - ' || t.desc_tipo) as LectTipo
                    from 
                    (
                    select * from APMEDIDA_CO a
                    where nis_rad={f.Nisrad}
                    union all 
                    select *
                    from hapmedida_co b
                    where nis_rad={f.Nisrad}
                    ) h 
                    join RECIBOS r on h.SEC_REC=R.SEC_REC AND H.NIS_RAD = R.NIS_RAD AND H.F_FACT=R.F_FACT
                    join tipos t on h.tip_lect=t.tipo
                    where to_char(H.f_fact,'yyyymm') = {f.Ciclo}
                    AND R.EST_ACT <> 'ER600'
                    ORDER BY H.F_LECT";

            var l = SqlMapper.Query<LecturaPeriodo>(conStrIcmsORA, qry).ToList();
            return l;
        }

        public List<SoporteRefacturacion_importes> GetImportes(FiltrosImportes filtros)
        {

            var _importes = new List<SoporteRefacturacion_importes>();

            var sql = $@"SELECT a.sec_rec,a.nis_rad,a.sec_nis,a.f_fact,a.co_concepto
            ,a.csmo_fact,SUM(a.imp_concepto) AS imp_concepto, b.desc_cod
            FROM imp_concepto a
            inner join codigos b on a.co_concepto = b.cod
            inner join RECIBOS r on a.SEC_REC = r.sec_rec and a.SEC_NIS=r.SEC_NIS AND to_char(r.f_fact,'yyyymm') = '{filtros.Ciclo}'
            AND a.nis_rad=R.NIS_RAD AND R.TIP_REC = 'TR010' 
            where a.nis_rad = {filtros.Nisrad}
            AND a.F_FACT = to_date('{filtros.FechaFact}', 'yyyymmdd')
            group by a.sec_rec,a.nis_rad,a.sec_nis,a.f_fact,a.co_concepto,a.csmo_fact,b.desc_cod
            order by b.desc_cod";

            try
            {
                _importes = SqlMapper.Query<SoporteRefacturacion_importes>(conStrIcmsORA, sql, commandTimeout: 0).ToList();
            }
            catch (Exception ex)
            {
                _importes = null;
            }

            return _importes;
        }

        public List<DtoCalculoCorregido> GetCalculoCorregidoMes(FiltroCalculoConceptos f)
        {
            var _importes = new List<DtoCalculoCorregido>();
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@NISRAD", f.Nisrad);
                parameters.Add("@PERIODO_FACT", f.PeriodoAct);
                parameters.Add("@CICLO_ANTERIOR", f.CicloAnt);
                parameters.Add("@CSMO_ANTERIOR", f.CsmoAnt);
                parameters.Add("@COD_TARIFA", f.CodTarifa);
                parameters.Add("@FECHA_FACT_INT", f.FechaFactInt);
                parameters.Add("@CSMO_FACT", f.CsmoAct);

                _importes = conStrSicra.Query<DtoCalculoCorregido>(
                    "SP_GET_CARGOS_CORREGIDOS ",
                    parameters, 
                    commandType: CommandType.StoredProcedure).ToList();

            }
            catch (Exception ex)
            {
                _importes = null;
            }


            return _importes;
        }

        public List<SoporteRefacturacion_importes> GetImportesRefa()
        {
            var _importes = new List<SoporteRefacturacion_importes>();

            var sql = $@"select COD co_concepto,UPPER(DESC_COD) desc_cod
                        from [InCMS].dbo.CODIGOS 
                        where COD in (select * from [SICRA].[dbo].[CargosRefactura])
                        order by 2";

            try
            {
                _importes = SqlMapper.Query<SoporteRefacturacion_importes>(conStrSicra, sql, commandTimeout: 0).ToList();
            }
            catch (Exception ex)
            {
                _importes = null;
            }

            return _importes;

        }

        public RepoAutorizaDto GetRptAutorizacion(int idGest)
        {

            var data = new RepoAutorizaDto();

            var sql = $@"select 
                        a.FechaCreacion,
                        a.FechaFinalizada,
                        a.IdGesRefMae,
                        a.codGestion,
                        c.Usuario,
                        c.NomPersona,
                        'descrip pqr' as tipopqr,
                        d.IdArea,
                        d.NombreArea,
                        a.ComentarioSolicitud,
                        a.kWhRef,
                        a.LpsRef,
                        a.kWhCv,
                        a.LpsCv,
                        a.kWhTotal,
                        a.LpsTotal
                        from GestionRefacturaMae a
                        join Usuarios c on a.idUsuario=c.idUsuario
                        join Area d on a.IdAreaCrea=d.IdArea
                        WHERE IdGesRefMae={idGest}";

            try
            {
                data = SqlMapper.Query<RepoAutorizaDto>(conStrSicra, sql, commandTimeout: 0).FirstOrDefault();
            }
            catch (Exception ex)
            {
                data = null;
            }

            return data;
        }
    }
}