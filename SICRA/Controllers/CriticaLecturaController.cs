﻿using Dapper;
using Newtonsoft.Json;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace SICRA.Controllers
{
    public class CriticaLecturaController:Controller
    {
        private static readonly DbConnection conStrReplica = Fun.GetConnectionSQL("ReplicaConn");
        private static readonly DbConnection conStrEehApps = Fun.GetConnectionSQL("SegConn");
        private static readonly DbConnection conStrSicra = Fun.GetConnectionSQL("SicraModel");
        private static readonly DbConnection conStrLogistic = Fun.GetConnectionSQL("LogisticsModel");
        private static readonly DbConnection conStrEnergis = Fun.GetConnectionSQL("energis");
        private ClienteController _CCtrl = new ClienteController();
        

        public List<CriticaLecturaInfo> GetMuestraCriticaLectura(FiltroMuestralectura f)
        {
            try
            {
                var lista = new List<CriticaLecturaInfo>();
                string porcentaje = "";
                string sql = "";

                if (f.TipoCant == "u")
                {
                    porcentaje = f.Porcentaje.ToString();
                }
                else //f.TipoCant = "p"
                {
                    porcentaje = (Convert.ToDecimal(f.Porcentaje) / Convert.ToDecimal(100)).ToString();
                }
                

                if (string.IsNullOrWhiteSpace(f.ConcatIds))
                {
                    f.ConcatIds = "''";
                }

                //sql = $@"EXECUTE [192.168.100.84].EEHAPPS.[dbo].[SP_GENERAR_MUESTRA_LECTURA_SICRA] 
                //        @COD_SECTOR = {f.Sector}
                //        ,@FECHA_LECTURA = '{f.FechaLect}'
                //        ,@PORCENTAJE = {porcentaje}
                //        ,@TIPO_FILTRO = {f.TipoFiltro}
                //        ,@CONCAT_IDS = {f.ConcatIds}
                //        ,@USR = {f.Usr}
                //        ,@OP = 3
                //        ,@CANT = {f.TipoCant} ";

                sql = $@"EXECUTE EEHAPPS.[dbo].[SP_GENERAR_MUESTRA_LECTURA_SICRA] 
                        @COD_SECTOR = {f.Sector}
                        ,@FECHA_LECTURA = '{f.FechaLect}'
                        ,@PORCENTAJE = {porcentaje}
                        ,@TIPO_FILTRO = {f.TipoFiltro}
                        ,@CONCAT_IDS = {f.ConcatIds}
                        ,@USR = {f.Usr}
                        ,@OP = 3
                        ,@CANT = {f.TipoCant} ";

                lista = SqlMapper.Query<CriticaLecturaInfo>(conStrEehApps, sql, commandTimeout: 0).ToList();
                return lista;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ClienteRoot CargarInfoCliente(string clave)
        {
            var CliRoot = _CCtrl.GetInfoCliente(clave);

            if (CliRoot != null)
            {
                string strQuery = "";
                string fechas = "";

                //BUSCAR FOTO POR CLAVE
                List<CliFotos> listaRetorna = null;
                try
                {
                    ///OBTENER IMAGENES DE LOGISTICS.     
                    strQuery = $@"select  STUFF((SELECT  (',' + convert(nvarchar(max),fecha))
                    from (SELECT distinct top 3 left([fecha],6) as fecha FROM [dbo].[generalGestionImagenes] with(nolock) 
                    where idActivity=6 and os = {CliRoot.cliente.clave} and len(imagen)>0 order by left([fecha],6) desc) r for xml path('')), 1, 1, '') as fechas";
                    fechas = SqlMapper.Query<string>(conStrLogistic, strQuery, commandTimeout: 0).FirstOrDefault();
                    if (!string.IsNullOrEmpty(fechas))
                    {
                        strQuery = $@"SELECT * 
                    FROM
                    (
                    SELECT [os] as clave,[imagen],[fecha],'Logistics' as origen
                    FROM [dbo].[generalGestionImagenes] with(nolock)
                    where idActivity=6 and os = {CliRoot.cliente.clave} and len(imagen)>0 
                    ) AS R WHERE left(R.fecha,6) in ({fechas}) order by R.fecha desc";

                        var CliFotosLogistics = SqlMapper.Query<CliFotos>(conStrLogistic, strQuery, commandTimeout: 0).ToList();

                        if (CliFotosLogistics != null)
                        {
                            listaRetorna = new List<CliFotos>();

                            foreach (var item in CliFotosLogistics)
                            {
                                listaRetorna.Add(item);
                            }
                        }
                        else
                        {
                            listaRetorna = null;
                        }
                    }

                    fechas = "";
                    ///OBTENER IMAGENES DE SIGCOM.     
                    strQuery = $@"select  STUFF((SELECT  (',' + substring(CONVERT( VARCHAR, FECHALECTURA, 112 ),1,6))
                    from (SELECT distinct top 3 substring(CONVERT( VARCHAR, FECHALECTURA, 112 ),1,6) as fecha,FECHALECTURA
                    FROM [ENERGIS].[dbo].[V_FOTOS_SIGCOM] with(nolock) 
                    where CLAVE = '{ CliRoot.cliente.clave}' order by FECHALECTURA desc) r for xml path('')), 1, 1, '') as fechas";

                    fechas = SqlMapper.Query<string>(conStrEnergis, strQuery, commandTimeout: 0).FirstOrDefault();
                    if (!string.IsNullOrEmpty(fechas))
                    {
                        strQuery = $@"select CLAVE,UBICACION as imagen,substring(CONVERT( VARCHAR, FECHALECTURA, 112 ),1,6) as fecha,'Sigcom' as origen
                    FROM [ENERGIS].[dbo].[V_FOTOS_SIGCOM] 
                    where clave= '{CliRoot.cliente.clave}' and substring(CONVERT( VARCHAR, FECHALECTURA, 112 ),1,6) in({fechas})
                    order by FECHALECTURA desc";

                        var CliFotosSigcom = SqlMapper.Query<CliFotos>(conStrEnergis, strQuery, commandTimeout: 0).ToList();

                        if (CliFotosSigcom != null)
                        {
                            if (listaRetorna == null)
                            {
                                listaRetorna = new List<CliFotos>();
                            }

                            foreach (var item in CliFotosSigcom)
                            {
                                listaRetorna.Add(item);
                            }
                        }
                    }
                    CliRoot.cliFotos = listaRetorna;
                }
                catch (Exception ex)
                {
                    CliRoot.cliFotos = null;
                }

                CliRoot.ListaHistoricoLectura = null;
                CliRoot.ListaHistoricoConsumo = null;
                return CliRoot;
            }
            else
            {
                return null;
            }

        }

        public Resp_CriticaLecturaInfo GetInfoMuestraWs(string clave, int periodo)
        {
            var respuesta = new Resp_CriticaLecturaInfo();
            try
            {
                string conn = ConfigurationManager.ConnectionStrings["SegConn"].ConnectionString;
                using (var db = new SqlConnection(conn))
                {
                    string qry = $@"exec eehApps.dbo.SP_GET_INFO_NIS_SICRA {clave},{periodo} ";
                    var obj = db.Query<CriticaLecturaInfo>(qry, commandTimeout: 0).FirstOrDefault();
                    respuesta.CriticaLecturaInfo = obj;

                    qry = $@"select distinct top 12 
                            CICLO,
                            CLAVE,
                            LECTURAACTIVA,
                            CONSUMOACTIVA,
                            CONSUMOREACTIVA,
                            ANOMALIA,
                            OBSERVACION
                            from TABLA_CONSULTA_LECTURAS_TEST
                            where CLAVE={clave}
                            order by CICLO desc";
                    var obj1 = db.Query<HistoAnomaliaInfo>(qry, commandTimeout: 0).ToList();

                    if (obj1 != null)
                    {
                        if (obj1.Count>0)
                        {
                            respuesta.histoAnomaliasInfo = new List<HistoAnomaliaInfo>(obj1);
                        }
                    }

                    respuesta.Msj = "(202-OK)";
                    return respuesta;
                }

            }
            catch (Exception ex)
            {
                respuesta.CriticaLecturaInfo = null;
                respuesta.Msj = ex.ToString();
                return respuesta;
            }
        }

        public List<ObjList> GetAnomaliasLectura()
        {
            string sql = "";
            //sql = $@"select CODIGOANOMALIA as id,NOMBRE from SGC_ANOMALIAS order by NOMBRE";
            sql = $@"select a.CODIGOCLASE as id, CONCAT(a.DESCRIPCION, ' --> ', b.DESCRIPCION) as NOMBRE
                    from [ENERGIS].dbo.sgc_clases a
                    join [ENERGIS].dbo.SGC_ANOMALIAS b on a.CODIGOANOMALIA=b.CODIGOANOMALIA
                    where isnull(a.DESCRIPCION,'') <> '' 
                    and a.CODIGOCLASE not in ('AL2_2')
                    order by a.DESCRIPCION";
            var anomalias = SqlMapper.Query<ObjList>(conStrReplica, sql).ToList();
            return anomalias;
        }

        public List<ObjList> GetInspectoresLectura(int? CodSector)
        {
            string sql = "";
            //sql = $@"select CODIGOANOMALIA as id,NOMBRE from SGC_ANOMALIAS order by NOMBRE";
            //sql = $@"select CODIGO_GRUPO_TRABAJO as id, [NOMBRE COMPLETO] as NOMBRE
            //        from EEHAPPS.[dbo].lectores 
            //        where CODIGO_GRUPO_TRABAJO is not null
            //        order by 2";

            sql = $@"SELECT CONCAT( CODIGOGRUPOTRABAJO,'-',CODIGOADMINISTRATIVO) id
                    ,CONCAT('CA-',CODIGOADMINISTRATIVO,' CT-',CODIGOGRUPOTRABAJO,' / ', NOMBREGRUPOTRABAJO) NOMBRE
                    FROM [ENERGIS].[dbo].[GRUPOTRABAJO] a
                    join eehApps.dbo.eeh_sector b on a.CODIGOSEDEOPERATIVA= b.cod_ruta
                    where ACTIVO='S' and CODIGOSEDEOPERATIVA=(select cod_ruta from eehApps.dbo.eeh_sector where cod_sector={CodSector})
                    group by CODIGOGRUPOTRABAJO,CODIGOADMINISTRATIVO,NOMBREGRUPOTRABAJO
                    order by NOMBREGRUPOTRABAJO";
            var anomalias = SqlMapper.Query<ObjList>(conStrReplica, sql).ToList();
            return anomalias;
        }

        public List<ObjList> GetParametrizacionLectura()
        {
            string sql = "";
            //sql = $@"select CODIGOANOMALIA as id,NOMBRE from SGC_ANOMALIAS order by NOMBRE";
            sql = $@"select a.CODIGOCLASE as id, CONCAT(a.DESCRIPCION, ' --> ', b.DESCRIPCION) as NOMBRE
                    from [ENERGIS].dbo.sgc_clases a
                    join [ENERGIS].dbo.SGC_ANOMALIAS b on a.CODIGOANOMALIA=b.CODIGOANOMALIA
                    where isnull(a.DESCRIPCION,'') <> '' order by a.DESCRIPCION";
            var anomalias = SqlMapper.Query<ObjList>(conStrReplica, sql).ToList();
            return anomalias;
        }

        public List<FechaLecturaObj> GetReporteFecha(string usr,int estotal,string periodo)
        {
            string sql = "";
            sql = $@"select distinct CONVERT(DATE,FECHALECTURA) from MuestraLecturasMes where EsTotal={estotal} and usuario='{usr}' and PERIODO={periodo} ";
            var fechasDb = SqlMapper.Query<string>(conStrSicra, sql).ToList();

            string fechasDbString = "'" + string.Join("','", from item in fechasDb select item) + "'";

            sql = $@"select distinct CONVERT(VARCHAR(10), fechalectura, 111) as FECHA_LECTURA
                    FROM eehApps.dbo.infoitinerariolocal
                    where periodo={periodo}
                    and CONVERT(DATE,fechalectura) not in ({fechasDbString})
                    order by FECHA_LECTURA desc";

            var fechas = SqlMapper.Query<FechaLecturaObj>(conStrEehApps, sql).ToList();
            return fechas;
        }

        public bool GetCountMuestrasbyFecha(DateTime fechaLect,bool esTipoTotal,string usr)
        {
            using (var db = new SicraModel())
            {
                var conteo = db.MuestraLecturasMes
                    .Where(x => x.FECHALECTURA.Year == fechaLect.Year
                    && x.FECHALECTURA.Month == fechaLect.Month
                    && x.FECHALECTURA.Day == fechaLect.Day
                    && x.EsTotal == esTipoTotal
                    && x.usuario.Equals(usr))
                    .ToList()
                    .Count;

                if (conteo > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public RespMsj PutAsingarMuestras(List<MuestraLecturasMes> listaMuestras)
        {
            using (var db = new SicraModel())
            {
                db.MuestraLecturasMes.AddRange(listaMuestras);

                if (db.SaveChanges() > 0)
                {
                    return new RespMsj { estado = "ok", mensaje = "Muestra de Lecturas guardada correctamente." };
                }
                else
                {
                    return new RespMsj { estado = "error", mensaje = "Error al guardar la muestra de lecturas." };
                }
            }
        }

        public RespMsj PutCriticaMuestra(MuestraLecturasMes Muestras)
        {
            try
            {
                using (var db = new SicraModel())
                {
                    if (!Muestras.Resuelto)
                    {
                        Muestras.Resuelto = true;
                        db.Entry(Muestras).State = EntityState.Modified;
                    }
                    else
                    {
                        db.Entry(Muestras).State = EntityState.Unchanged;
                    }


                    if (db.SaveChanges() > 0)
                    {
                        return new RespMsj { estado = "ok", mensaje = "Crítica Realizada Correctamente.!" };
                    }
                    else
                    {
                        return new RespMsj { estado = "error", mensaje = "Error al guardar cítica." };
                    }
                }
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                return new RespMsj { estado = "error", mensaje = $"Ocurrió una excepción: {msj}" };
            }
        }

        public List<MuestraLecturaDTO> GetMuestrasByUser(int idUsr, string user, string ciclo, string fecha,int estado, string filtro,bool esCritica)
        {
            string dialWhere = fecha == "0" ? "" : $" and x.FECHALECTURA='{fecha}'";

            string sql;

            if (esCritica)
            {
                sql = $@"select x.* ,u.Usuario UsrResuelveString
                    from MuestraLecturasMes x
                    join Usuarios u on x.UsrResuelve = u.idUsuario
                    where x.Resuelto={estado}
                    and x.UsrResuelve={idUsr}
                    and PERIODO={ciclo} {dialWhere} 
                    and TipoFiltro = '{filtro}' ";

                //if (filtro == "INTERSECTORIAL")
                //{
                //    sql = $@"select x.* ,u.Usuario UsrResuelveString
                //        from MuestraLecturasMes x
                //        join Usuarios u on x.UsrResuelve = u.idUsuario
                //        where x.Resuelto={estado}
                //        and PERIODO={ciclo} {dialWhere} 
                //        and x.UsrResuelve='{idUsr}' ";
                //}
                //else
                //{
                //    sql = $@"select x.* ,u.Usuario UsrResuelveString
                //    from MuestraLecturasMes x
                //    join Usuarios u on x.UsrResuelve = u.idUsuario
                //    where x.Resuelto={estado}
                //    and x.UsrResuelve={idUsr}
                //    and PERIODO={ciclo} {dialWhere} 
                //    and TipoFiltro = '{filtro}' ";
                //}
            }
            else
            {
                sql = $@"select x.* ,u.Usuario UsrResuelveString
                        from MuestraLecturasMes x
                        join Usuarios u on x.UsrResuelve = u.idUsuario
                        where x.Resuelto=1
                        and PERIODO={ciclo} {dialWhere} and TipoFiltro = '{filtro}' ";
            }

            var muestras = SqlMapper.Query<MuestraLecturaDTO>(conStrSicra, sql).ToList();
            return muestras;
        }

        public MuestraLecturasMes GetMuestrasByClave(int clave)
        {
            using (var db = new SicraModel())
            {
                var muestra = db.MuestraLecturasMes
                    .Where(x => x.CLAVE == clave)
                    .FirstOrDefault();
                return muestra;
            }
        }

        public MuestraLecturasMes GetMuestrasByIdRow(int idRow)
        {
            using (var db = new SicraModel())
            {
                var muestra = db.MuestraLecturasMes
                    .Include("ObsFotoMuestraLectura")
                    .Where(x => x.idRow == idRow)
                    .FirstOrDefault();
                return muestra;
            }
        }
        
        public List<int> GetCiclosMuestras(int estado)
        {
            string sql = $@"select distinct periodo from MuestraLecturasMes where Resuelto={estado} order by periodo desc";

            var ciclo = SqlMapper.Query<int>(conStrSicra, sql).ToList();
            return ciclo;
        }

        public List<int> GetPeriodosLectura()
        {
            string sql = "";
            //sql = $@"select CODIGOANOMALIA as id,NOMBRE from SGC_ANOMALIAS order by NOMBRE";
            sql = $@"select distinct periodo FROM eehApps.dbo.infoitinerariolocal
                    where periodo>=202001 order by 1 desc";
            var periodos = SqlMapper.Query<int>(conStrEehApps, sql).ToList();
            return periodos;
        }
        
        public List<string> GetDialesResueltos(int periodo, int estado)
        {
            string sql = "";
            sql = $@"select distinct CONVERT(nvarchar(10),CONVERT(date,FechaLectura))FechaLectura 
                    from MuestraLecturasMes
                    where Resuelto={estado} and PERIODO={periodo} order by 1";
            var fechas = SqlMapper.Query<string>(conStrSicra, sql).ToList();
            return fechas;
        }

        public List<string> GetTiposMuetrasFecha(string Fecha, int idUsr,bool EsCritica)
        {
            string sql = "";

            if (EsCritica)
            {
                sql = $@"select distinct x.TipoFiltro 
                    from MuestraLecturasMes x 
                    where x.Resuelto=0 and x.FECHALECTURA='{Fecha}' 
                    and x.TipoFiltro<>'INTERSECTORIAL'
                    union
                    select distinct x.TipoFiltro 
                    from MuestraLecturasMes x 
                    where x.Resuelto=0 and x.FECHALECTURA='{Fecha}' and x.UsrResuelve={idUsr}";
            }
            else
            {
                sql = $@"select distinct x.TipoFiltro 
                    from MuestraLecturasMes x 
                    where x.FECHALECTURA='{Fecha}' and x.Resuelto=1";
            }

            var tipos = SqlMapper.Query<string>(conStrSicra, sql).ToList();
            return tipos;
        }
        public List<ObsFotoMuestraLectura> GetListaObservacionFoto()
        {
            using (var db = new SicraModel())
            {
                var lista = db.ObsFotoMuestraLectura.ToList();
                return lista;
            }
        }

        public ObsFotoMuestraLectura GetObservacionFoto(int? id)
        {
            using (var db = new SicraModel())
            {
                var obs = db.ObsFotoMuestraLectura.Find(id);
                return obs;
            }
        }
    }
}
