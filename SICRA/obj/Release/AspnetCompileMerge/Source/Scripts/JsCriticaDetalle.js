﻿
function RptCriticaDetalle() {
    RenderDdlFechas();
    $('#DivTbControl').hide();
    $('#DivTbDetalle').hide();
    $('#DivTbGeneral').hide();
}

function GetRepoData() {

    var checked_radio = $("[id*=rbRepo] input:checked");
    var value = checked_radio.val();
    var fechas = $("#hfFechas").val();
    var ciclo = $("#ddlCiclo").val();

    var obj = { Ciclo: ciclo, FechasLectura: fechas, TipoRepo: value };
    var jsn = JSON.stringify(obj);


    $.ajax({
        type: "POST",
        url: "ViewRptCritica.aspx?jsn=" + jsn,
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            RenderTablaDetalle(data.SeriesDatos, value);
        },
        error: function (response) {
            alert('Error En parámetros');
        }
    }).done(function () {
        // Call other function to execute other scripts  
        //alert('aaaaaaaa');
    });  
}


function RenderDdlFechas() {

    var ddl = $(".js-example-basic-multiple").select2({
        placeholder: "Seleccionar Una/Multiple Fecha(s)",
        allowClear: true,
        multiple: "multiple",
        closeOnSelect: false
    });

    ddl.val("");
    ddl.trigger("change");
    ddl.on('change', function (e) {
        $("#hfFechas").val(ddl.val());
    });
}


function RenderTablaDetalle(data,id) {

    if (id == 1) {
        $('#DivTbControl').hide();
        $('#DivTbDetalle').hide();
        $('#DivTbGeneral').show();
        var TablaGen = $("#gvDatosGen").DataTable({
            destroy: true,
            deferRender: true,
            'processing': true,
            'language': {
                'loadingRecords': '&nbsp;',
                'processing': '<div class="spinner"></div>'
            },
            dom: 'lBfrtip',
            buttons: [
                { extend: 'copy', className: 'btn btn-primary glyphicon glyphicon-duplicate' },
                { extend: 'excel', className: 'btn btn-primary glyphicon glyphicon-list-alt' },
                { extend: 'print', className: 'btn btn-primary glyphicon glyphicon-print' }
            ],
            bLengthChange: true,
            scrollX: true,
            "pagingType": "full_numbers",
            data: data,
            columns: [
                { data: "Concepto" },
                { data: "Valor" }
            ],
            fixedColumns: true
        });

        $("div.toolbar").html('<b>Reporte Detallado</b>');
    }

    if (id == 2) {
        $('#DivTbControl').hide();
        $('#DivTbDetalle').show();
        $('#DivTbGeneral').hide();
        var TablaDetalle = $("#gvDatosDetalle").DataTable({
            destroy: true,
            deferRender: true,
            'processing': true,
            'language': {
                'loadingRecords': '&nbsp;',
                'processing': '<div class="spinner"></div>'
            },
            dom: 'lBfrtip',
            buttons: [
                { extend: 'copy', className: 'btn btn-primary glyphicon glyphicon-duplicate' },
                { extend: 'excel', className: 'btn btn-primary glyphicon glyphicon-list-alt' },
                { extend: 'print', className: 'btn btn-primary glyphicon glyphicon-print' }
            ],
            bLengthChange: true,
            scrollX: true,
            "pagingType": "full_numbers",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            data: data,
            columns: [
                { data: "NomPersona" },
                { data: "PromCalidadMuestra" },
                { data: "CantAccionCorrecta" },
                { data: "CantObservCorrecta" },
                { data: "CantSicraSiDebioRepoSi" }
            ],
            fixedColumns: true
        });
        $('[id*=lblTotMuestra]').text(data[0].TotalMuestra);
    }

    if (id == 3) {

        console.log(data);

        $('#DivTbControl').show();
        $('#DivTbDetalle').hide();
        $('#DivTbGeneral').hide();
        var TablaControl = $("#gvDatosControl").DataTable({
            destroy: true,
            deferRender: true,
            'processing': true,
            'language': {
                'loadingRecords': '&nbsp;',
                'processing': '<div class="spinner"></div>'
            },
            dom: 'lBfrtip',
            buttons: [
                { extend: 'copy', className: 'btn btn-primary glyphicon glyphicon-duplicate' },
                { extend: 'excel', className: 'btn btn-primary glyphicon glyphicon-list-alt' },
                { extend: 'print', className: 'btn btn-primary glyphicon glyphicon-print' }
            ],
            bLengthChange: true,
            scrollX: true,
            "pagingType": "full_numbers",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            data: data,
            columns: [
                { data: "NomPersona" },
                { data: "Asignados" },
                { data: "CantNoResueltos" },
                { data: "CantResueltos" }
            ],
            fixedColumns: true
        });

        $("div.toolbar").html('<b>Reporte Detallado</b>');
    }
    
}
