﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CalculoRefacturacion.aspx.cs" Inherits="SICRA.Views.CalculoRefacturacion" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <a href="#"><span>SICRA</span></a> <%--agregar al menu que pertenece--%>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
    <a class="UrlActual" href='<%= ResolveUrl("CalculoRefacturacion.aspx") %>'>Cálculo Refacturación</a>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <%--Tabs y checkbox--%>
    <style>

        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }
        .switch {
            position: relative;
            width: auto;
            margin: 0;
            background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
            background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
            border-radius: 18px;
            box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
            cursor: pointer;
            box-sizing: content-box;
            padding:0 !important;
        }

        .switch-input {
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
            box-sizing: content-box;
        }

        .switch-label {
            position: relative;
            display: block;
            height: inherit;
            font-size: 14px;
            text-transform: uppercase;
            background: #FF7F50;
            border-radius: inherit;
            box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
            box-sizing: content-box;
            text-align:center;
        }

            .switch-label:before, .switch-label:after {
                position: absolute;
                top: 50%;
                margin-top: -.5em;
                line-height: 1;
                -webkit-transition: inherit;
                -moz-transition: inherit;
                -o-transition: inherit;
                transition: inherit;
                box-sizing: content-box;
            }

            .switch-label:before {
                content: attr(data-off);
                right: 11px;
                color: #ffffff;
                text-shadow: 0 1px rgba(255, 255, 255, 0.5);
            }

            .switch-label:after {
                content: attr(data-on);
                left: 11px;
                color: #FFFFFF;
                text-shadow: 0 1px rgba(0, 0, 0, 0.2);
                opacity: 0;
            }

        .switch-input:checked ~ .switch-label {
            background: #E1B42B;
            box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
        }

            .switch-input:checked ~ .switch-label:before {
                opacity: 0;
            }

            .switch-input:checked ~ .switch-label:after {
                opacity: 1;
            }

        .switch-handle {
            position: absolute;
            top: 4px;
            left: 4px;
            width: 28px;
            height: 28px;
            background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
            background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
            border-radius: 100%;
            box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
        }

            .switch-handle:before {
                content: "";
                position: absolute;
                top: 50%;
                left: 50%;
                margin: -6px 0 0 -6px;
                width: 12px;
                height: 12px;
                background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
                background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
                border-radius: 6px;
                box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
            }

        .switch-input:checked ~ .switch-handle {
            left: 74px;
            box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
        }

        /* Switch Slide
==========================*/
        .switch-slide {
            padding: 0;
            background: #FFF;
            border-radius: 0;
            background-image: none;
        }

            .switch-slide .switch-label {
                box-shadow: none;
                background: none;
                overflow: hidden;
            }

                .switch-slide .switch-label:after, .switch-slide .switch-label:before {
                    width: 100%;
                    height: 100%;
                    top: 0px;
                    left: 0;
                    text-align: center;
                    padding-top: 7%;
                    box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.2), inset 0 0 3px rgba(0, 0, 0, 0.1);
                }

                .switch-slide .switch-label:after {
                    color: #FFFFFF;
                    background: #32CD32;
                    left: -100px;
                }

                .switch-slide .switch-label:before {
                    background: #FF7F50;
                }

            .switch-slide .switch-handle {
                display: none;
            }

            .switch-slide .switch-input:checked ~ .switch-label {
                background: #FFF;
                border-color: #0088cc;
            }

                .switch-slide .switch-input:checked ~ .switch-label:before {
                    left: 100px;
                }

                .switch-slide .switch-input:checked ~ .switch-label:after {
                    left: 0;
                }

        /* Transition ============================================================ */
        .switch-label, .switch-handle {
            transition: All 0.3s ease;
            -webkit-transition: All 0.3s ease;
            -moz-transition: All 0.3s ease;
            -o-transition: All 0.3s ease;
        }


        /* TABS ============================================================ */
        
        .panel.with-nav-tabs .panel-heading{
            padding: 5px 5px 0 5px;
        }
        .panel.with-nav-tabs .nav-tabs{
	        border-bottom: none;
        }
        .panel.with-nav-tabs .nav-justified{
	        margin-bottom: -1px;
        }
        /********************************************************************/
        /*** PANEL DEFAULT ***/
        .with-nav-tabs.panel-default .nav-tabs > li > a,
        .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li > a:focus {
            color: #777;
        }
        .with-nav-tabs.panel-default .nav-tabs > .open > a,
        .with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-default .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li > a:focus {
            color: #777;
	        background-color: #ddd;
	        border-color: transparent;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.active > a,
        .with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
	        color: #555;
	        background-color: #fff;
	        border-color: #ddd;
	        border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #f5f5f5;
            border-color: #ddd;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #777;   
        }
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #ddd;
        }
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #555;
        }
        /********************************************************************/
        /*** PANEL PRIMARY ***/
        .with-nav-tabs.panel-primary .nav-tabs > li > a,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
            color: #fff;
        }
        .with-nav-tabs.panel-primary .nav-tabs > .open > a,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	        color: #fff;
	        background-color: #3071a9;
	        border-color: transparent;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	        color: #428bca;
	        background-color: #fff;
	        border-color: #428bca;
	        border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #428bca;
            border-color: #3071a9;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #fff;   
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #3071a9;
        }
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            background-color: #4a9fe9;
        }
        /********************************************************************/
        /*** PANEL SUCCESS ***/
        .with-nav-tabs.panel-success .nav-tabs > li > a,
        .with-nav-tabs.panel-success .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	        color: #3c763d;
        }
        .with-nav-tabs.panel-success .nav-tabs > .open > a,
        .with-nav-tabs.panel-success .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-success .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	        color: #3c763d;
	        background-color: #d6e9c6;
	        border-color: transparent;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.active > a,
        .with-nav-tabs.panel-success .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
	        color: #3c763d;
	        background-color: #fff;
	        border-color: #d6e9c6;
	        border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #dff0d8;
            border-color: #d6e9c6;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #3c763d;   
        }
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #d6e9c6;
        }
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #3c763d;
        }
        /********************************************************************/
        /*** PANEL INFO ***/
        .with-nav-tabs.panel-info .nav-tabs > li > a,
        .with-nav-tabs.panel-info .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	        color: #31708f;
        }
        .with-nav-tabs.panel-info .nav-tabs > .open > a,
        .with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-info .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	        color: #31708f;
	        background-color: #bce8f1;
	        border-color: transparent;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.active > a,
        .with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
	        color: #31708f;
	        background-color: #fff;
	        border-color: #bce8f1;
	        border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #d9edf7;
            border-color: #bce8f1;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #31708f;   
        }
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #bce8f1;
        }
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #31708f;
        }
        /********************************************************************/
        /*** PANEL WARNING ***/
        .with-nav-tabs.panel-warning .nav-tabs > li > a,
        .with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	        color: #8a6d3b;
        }
        .with-nav-tabs.panel-warning .nav-tabs > .open > a,
        .with-nav-tabs.panel-warning .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	        color: #8a6d3b;
	        background-color: #faebcc;
	        border-color: transparent;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.active > a,
        .with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
	        color: #8a6d3b;
	        background-color: #fff;
	        border-color: #faebcc;
	        border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #fcf8e3;
            border-color: #faebcc;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #8a6d3b; 
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #faebcc;
        }
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff;
            background-color: #8a6d3b;
        }
        /********************************************************************/
        /*** PANEL DANGER ***/
        .with-nav-tabs.panel-danger .nav-tabs > li > a,
        .with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	        color: #a94442;
        }
        .with-nav-tabs.panel-danger .nav-tabs > .open > a,
        .with-nav-tabs.panel-danger .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	        color: #a94442;
	        background-color: #ebccd1;
	        border-color: transparent;
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.active > a,
        .with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
	        color: #a94442;
	        background-color: #fff;
	        border-color: #ebccd1;
	        border-bottom-color: transparent;
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
            background-color: #f2dede; /* bg color */
            border-color: #ebccd1; /* border color */
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
            color: #a94442; /* normal text color */  
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
            background-color: #ebccd1; /* hover bg color */
        }
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a,
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
        .with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
            color: #fff; /* active text color */
            background-color: #a94442; /* active bg color */
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    
    <script src="../Scripts/plugins/select2/select2.full.min.js"></script>
    <link href="../Content/plugins/select2/select2.min.css" rel="stylesheet" />
    <script src="../Scripts/JsRefacturacion.js"></script>
    
    <script type="text/javascript">        
        $(function () {
            control = new renderLocal();
        });
    </script>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-sm-12">
        <h2 class="titulo">Cálculo de Refacturación - V1</h2>
        <div class="panel">
            <div class="panel-body">
                <div class="form-horizontal">

                    <div class="table-responsive col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-striped table-bordered table-hover" id="tbCalculoCdr" style="width: 100%; text-align: center">
                            <tr>
                                <td>NIS :</td>
                                <td colspan="5">
                                    <asp:TextBox runat="server" Width="20%" TextMode="Number" ID="txtClave" CssClass="form-control" OnTextChanged="txtClave_TextChanged" AutoPostBack="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Ciclos refacturación :</td>
                                <td colspan="5">
                                    <asp:HiddenField runat="server" ID="hfCiclos" ClientIDMode="Static" />
                                    <asp:DropDownList ID="ddlCiclos" runat="server" CssClass="form-control js-example-basic-multiple" ToolTip="Seleccionar Ciclo de refacturación"></asp:DropDownList>
                                    <asp:TextBox runat="server" ID="txtCiclos" CssClass="form-control" Visible="false" ReadOnly="true" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Cálculos</label></td>
                                <td>
                                    <label>Fecha</label></td>
                                <td>
                                    <label>Lectura Activa</label></td>
                                <td>
                                    <label>Lectura Reactiva</label></td>
                                <td>
                                    <label>RM kWh</label></td>
                                <td>
                                    <label>Multiplicador</label></td>
                            </tr>

                            <tr>
                                <td>
                                    <span>Inicio de referencia</span>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtFechaIniCdr" CssClass="form-control" TextMode="Date" ClientIDMode="Static" onchange="javascript:CalcularDifDias();"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtLecActIniCdr" CssClass="form-control" TextMode="Number" Text="0"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="rfLecActIniCdr" runat="server" ErrorMessage="***Valor obligatorio***" ControlToValidate="txtLecActIniCdr" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtLecReaIniCdr" CssClass="form-control" TextMode="Number" Text="0"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="rfLecReaIniCdr" runat="server" ErrorMessage="***Valor obligatorio***" ControlToValidate="txtLecReaIniCdr" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <span id="spRmKwh">*****</span>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblMultip"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>Última Lectura</span>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtFechaFinCdr" CssClass="form-control" TextMode="Date" ClientIDMode="Static" onchange="javascript:CalcularDifDias();"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtLecActFinCdr" CssClass="form-control" TextMode="Number" Text="0"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtLecReaFinCdr" CssClass="form-control" TextMode="Number" Text="0"></asp:TextBox>
                                </td>
                                <td colspan="2">
                                    <label>¿Calcular Alumbrado?</label>
                                    <label class="switch switch-slide form-control">
                                        <input class="switch-input" type="checkbox" id="chkCalculaAlumbrado" runat="server" />
                                        <%--<asp:CheckBox class="switch-input" runat="server" ID="chkCalculaAlumbrado"/>--%>
                                        <span class="switch-label" data-on="SI" data-off="NO"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>Cantidad Días</span>
                                </td>
                                <td>
                                    <asp:HiddenField runat="server" ID="hfCantDiasCdr" ClientIDMode="Static" />
                                    <span>
                                        <asp:Label runat="server" ID="lblCantDiasCdr" ClientIDMode="Static"></asp:Label>
                                    </span>
                                </td>
                                <td>
                                    <span>
                                        <asp:Label runat="server" ID="lblDifLectAct"></asp:Label></span>
                                </td>
                                <td>
                                    <span>
                                        <asp:Label runat="server" ID="lblDifLectRea"></asp:Label></span>
                                </td>
                                <td colspan="2">
                                    <label>Ciclos</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>CDR</label>
                                </td>
                                <td>
                                    <label>¿Calcular Por CDR?</label><br />
                                    <label class="switch switch-slide form-control">
                                        <input class="switch-input" type="checkbox" id="chkCdr" runat="server" />
                                        <%--<asp:CheckBox class="switch-input" runat="server" ID="chkCalculaAlumbrado"/>--%>
                                        <span class="switch-label" data-on="SI" data-off="NO"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </td>
                                <td>
                                    <asp:HiddenField runat="server" ID="hfCdr" ClientIDMode="Static" />
                                    <span>
                                        <asp:Label runat="server" ID="lblCdr"></asp:Label>
                                    </span>
                                </td>
                                <td>
                                    <asp:HiddenField runat="server" ID="hfCdrReact" ClientIDMode="Static" />
                                    <span>
                                        <asp:Label runat="server" ID="lblCdrReac"></asp:Label>
                                    </span>
                                </td>
                                <td>
                                    <asp:HiddenField runat="server" ID="hfCicloIni" ClientIDMode="Static" />
                                    <span>
                                        <asp:Label runat="server" ID="lblCicloIni" ClientIDMode="Static"></asp:Label></span>
                                </td>
                                <td>
                                    <asp:HiddenField runat="server" ID="hfCicloFin" ClientIDMode="Static" />
                                    <span>
                                        <asp:Label runat="server" ID="lblCicloFin" ClientIDMode="Static"></asp:Label></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <asp:Button runat="server" ID="btnCalcular" CssClass="btn bg-success" Text="Calcular Datos" OnClick="btnCalcular_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <%--formulario flotante de actualizacion--%>
    


    



    <asp:UpdatePanel runat="server" ID="updPanReportar">
        <ContentTemplate>
            <div class="col-sm-12">
                <asp:Label ID="lblMsjError" runat="server" Visible="False" Style="font-size: medium" CssClass="label label-danger"></asp:Label>
            </div>
            <div class="col-sm-12">
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="table-responsive col-sm-12 col-md-12 col-lg-12">
                            <table class="table table-striped table-bordered table-hover table-responsive" style="background-color: lightgoldenrodyellow; width: 100%" runat="server" id="tbInfoMesAnt">
                                <tr>
                                    <td>
                                        <label>Fecha Anterior:</label>
                                    </td>
                                    <td>
                                        <span>
                                            <asp:Label runat="server" ID="lblFechaAnterior"></asp:Label></span>
                                    </td>
                                    <td>
                                        <label>Lectura Reactiva Anterior:</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlLecRea" runat="server" CssClass="form-control" ToolTip="Seleccionar Lectura Rectiva"></asp:DropDownList>
                                        <asp:TextBox ID="txtLecRea" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                    </td>
                                    <td>
                                        <label>Consumo Reactiva Anterior:</label>
                                    </td>
                                    <td>
                                        <span>
                                            <asp:Label runat="server" ID="lblConReaAnt"></asp:Label></span>
                                    </td>
                                    <td>
                                        <label>Lectura Activa Anterior:</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlLecAct" runat="server" CssClass="form-control" ToolTip="Seleccionar Lectura Activa" OnSelectedIndexChanged="ddlLecAct_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        <asp:TextBox ID="txtLecAct" TextMode="Number" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                        <asp:Button runat="server" ID="btnCalActivaGrid" CssClass="btn bg-info" Text="Calcular" OnClick="btnCalActivaGrid_Click" Visible="false" />
                                    </td>
                                    <td>
                                        <label>Consumo Activa Anterior:</label>
                                    </td>
                                    <td>
                                        <span>
                                            <asp:Label runat="server" ID="lblConActAnt"></asp:Label>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField runat="server" ID="hfMetodo" Value="" />


                            <div class="panel with-nav-tabs panel-primary">
                                <div class="panel-heading">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab1primary" id="first_tab" data-toggle="tab">ENERGÍA</a></li>
                                        <li><a href="#tab2primary" id="second_tab" data-toggle="tab">FACTURADO</a></li>
                                        <li><a href="#tab3primary" id="third_tab" data-toggle="tab">CORREGIDO</a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <%--**********************Primer Pestaña**********************--%>
                                        <div class="tab-pane fade in active" id="tab1primary">
                                            <asp:Panel ID="pnlEnergia" runat="server" Width="100%" ScrollBars="Horizontal">
                                                <asp:GridView ID="gvEnergia" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                                    class="table table-striped table-bordered table-hover table-responsive" ShowFooter="true"
                                                    Width="100%" Font-Size="Small" OnRowDataBound="gvEnergia_RowDataBound" OnDataBound="gvEnergia_DataBound">
                                                    <HeaderStyle Font-Bold="True" />
                                                    <EditRowStyle BackColor="Yellow" />
                                                    <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                                    <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                                    <Columns>
                                                        <%--col0--%>
                                                        <asp:BoundField DataField="secrec" HeaderText="secrec" Visible="false" />
                                                        <%--col1--%>
                                                        <asp:BoundField DataField="periodofacturado" HeaderText="Periodo" />
                                                        <%--col2--%>
                                                        <asp:BoundField DataField="ffactanterior" HeaderText="Fecha Anterior" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" Visible="true" />
                                                        <%--col3--%>
                                                        <asp:BoundField DataField="ffactura" HeaderText="Fecha Factura" DataFormatString="{0:dd/MM/yyyy}" />
                                                        <%--col4--%>
                                                        <asp:TemplateField HeaderText="Días">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="colDias" ReadOnly="true" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col5--%>
                                                        <asp:BoundField DataField="consumoactivafact" HeaderText="kWh Facturado" />
                                                        <%--col6--%>
                                                        <asp:TemplateField HeaderText="KWh Reconstuido">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="colKwhRec" ReadOnly="true" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col7--%>
                                                        <asp:TemplateField HeaderText="Diferencia kWh Ref">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="colDifRefact" ReadOnly="true" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col8--%>
                                                        <asp:TemplateField HeaderText="Lectura Activa">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtGridLecAct" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col9--%>
                                                        <asp:TemplateField HeaderText="Unidades Activa" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtUnidades" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col10--%>
                                                        <asp:TemplateField HeaderText="ActivaPorDia" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtActivaPorDia" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col11--%>
                                                        <asp:TemplateField HeaderText="Lec Act construida" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtLectActConst" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col12--%>
                                                        <asp:BoundField DataField="consumoreactivafact" HeaderText="kVARh Facturado Reactiva" />
                                                        <%--col13--%>
                                                        <asp:TemplateField HeaderText="kVARh Reactiva Reconstuido">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="colKwhReacRec" ReadOnly="true" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col14--%>
                                                        <asp:TemplateField HeaderText="Diferencia kVARh Reactiva">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="colDifRefactReact" ReadOnly="true" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col15--%>
                                                        <asp:TemplateField HeaderText="Lectura Reactiva">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtGridLecRea" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col16--%>
                                                        <asp:TemplateField HeaderText="Unidades Reactiva" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtUnidadesRect" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col17--%>
                                                        <asp:TemplateField HeaderText="ReactivaPorDia" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtReactivaPorDia" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col18--%>
                                                        <asp:TemplateField HeaderText="Lec Reac construida" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtLectReacConst" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--col19--%>
                                                        <asp:BoundField DataField="codtarifa" HeaderText="Código Tarifa" Visible="true" />
                                                        <%--col20--%>
                                                        <asp:BoundField DataField="nombretarifa" HeaderText="Nombre Tarifa" Visible="true" />
                                                        <%--col21--%>
                                                        <asp:BoundField DataField="ffactura" HeaderText="Ffact" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                                        <%--col22--%>
                                                        <asp:BoundField DataField="ffactanterior" HeaderText="FAnterior" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                                    </Columns>
                                                </asp:GridView>

                                                <asp:Panel runat="server" ID="pnlCalculaAlumbrado" Visible="false" Width="100%">
                                                    <table class="table-responsive" style="background-color: lightcoral; width: 100%">
                                                        <tr>
                                                            <td>
                                                                <label>Total Días</label>
                                                            </td>
                                                            <td>
                                                                <label>KWh Facturado Real</label>
                                                            </td>
                                                            <td>
                                                                <label>KWh Reconstuido Real</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" ID="lblTotDias"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label runat="server" ID="lblFacturadoReal"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label runat="server" ID="lblReconstruidoReal"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </div>
                                        <%--**********************Segunda Pestaña**********************--%>
                                        <div class="tab-pane fade in" id="tab2primary">
                                            <asp:GridView ID="gvFacturado" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                                class="table table-striped table-bordered table-hover table-responsive" ShowFooter="False"
                                                Width="100%" Font-Size="Small" OnRowDataBound="gvFacturado_RowDataBound">
                                                <HeaderStyle Font-Bold="True" />
                                                <EditRowStyle BackColor="Yellow" />
                                                <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                                <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                                <Columns>
                                                    <%--col0--%>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <img alt="" class="imgPlusRow" style="cursor: pointer" src="../Imagenes/plus.png" />
                                                            <asp:Panel ID="pnlRegistros" runat="server" Style="display: none">
                                                                <asp:GridView ID="gvFacturadoDet" runat="server" CssClass="Grid" AutoGenerateColumns="false" Width="100%">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="sec_rec" HeaderText="sec_rec" ReadOnly="True" Visible="false" />
                                                                        <asp:BoundField DataField="sec_nis" HeaderText="sec_nis" ReadOnly="True" Visible="false" />
                                                                        <asp:BoundField DataField="nis_rad" HeaderText="nis_rad" ReadOnly="True" Visible="false" />
                                                                        <asp:BoundField DataField="f_fact" HeaderText="f_fact" ReadOnly="True" Visible="false" DataFormatString="{0:dd/MM/yyyy}" />
                                                                        <asp:BoundField DataField="co_concepto" HeaderText="Código Concepto" ReadOnly="True" Visible="true" />
                                                                        <asp:BoundField DataField="csmo_fact" HeaderText="CSMO Facturado" ReadOnly="True" Visible="true" />
                                                                        <asp:BoundField DataField="imp_concepto" HeaderText="Importe" ReadOnly="True" Visible="true" />
                                                                        <asp:BoundField DataField="desc_cod" HeaderText="Descripción" ReadOnly="True" Visible="true" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--col1--%>
                                                    <asp:BoundField DataField="nisrad" HeaderText="NIS" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                                    <%--col2--%>
                                                    <asp:BoundField DataField="periodofacturado" HeaderText="Periodo" />
                                                    <%--col3--%>
                                                    <asp:BoundField DataField="ffactura" HeaderText="Fecha Factura" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <%--col4--%>
                                                    <asp:BoundField DataField="nombretarifa" HeaderText="Nombre Tarifa" />
                                                    <%--col5--%>
                                                    <asp:BoundField DataField="consumoactivafact" HeaderText="CSMO Activa" />
                                                    <%--col6--%>
                                                    <asp:BoundField DataField="consumoreactivafact" HeaderText="CSMO Reactiva" />
                                                    <%--col7--%>
                                                    <asp:BoundField DataField="totalimporte" HeaderText="Total Importe" />
                                                    <%--col8--%>
                                                    <asp:BoundField DataField="codtarifa" HeaderText="codtarifa" Visible="true" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"/>
                                                    <%--col9--%>
                                                    <asp:BoundField DataField="JsonImp" HeaderText="Importes Json" Visible="true" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"/>
                                                    <%--col10--%>
                                                    <asp:BoundField DataField="ffacturaint" HeaderText="Fecha Int" Visible="true" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <%--**********************Tercera Pestaña**********************--%>
                                        <div class="tab-pane fade in" id="tab3primary" style="text-align:right">
                                            <asp:LinkButton ID="lnkNewCargo" runat="server" Text="Agregar Cargo" ForeColor="blue" OnClick="lnkNewCargo_Click" OnClientClick="$('#third_tab').trigger('click');" ></asp:LinkButton>
                                            <br />
                                            <asp:GridView ID="gvCorrecion" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                                class="table table-striped table-bordered table-hover table-responsive" ShowFooter="false"
                                                Width="100%" Font-Size="Small" OnRowDataBound="gvCorrecion_RowDataBound">
                                                <HeaderStyle Font-Bold="True" />
                                                <EditRowStyle BackColor="Yellow" />
                                                <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                                <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                                <Columns>
                                                    <%--col0--%>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <img alt="" class="imgPlusRow" style="cursor: pointer" src="../Imagenes/plus.png" />
                                                            <asp:Panel ID="pnlRegistrosCorregidos" runat="server" Style="display: none">
                                                                <asp:GridView ID="gvCorrecionDet" runat="server" CssClass="Grid" AutoGenerateColumns="false" Width="100%" ShowFooter="false"
                                                                    OnRowDataBound="gvCorrecionDet_RowDataBound">
                                                                    <Columns>
                                                                        <%--col0--%>
                                                                        <asp:BoundField DataField="codconcepto" HeaderText="Código Concepto" ReadOnly="True" Visible="true" />
                                                                        <%--col1--%>
                                                                        <asp:BoundField DataField="DescripcionImporte" HeaderText="Concepto" ReadOnly="True" Visible="true" />
                                                                        <%--col2--%>
                                                                        <asp:BoundField DataField="ValorCobrar" HeaderText="Importe" ReadOnly="True" Visible="true" />
                                                                        <%--col3--%>
                                                                        <asp:BoundField DataField="IsNew" HeaderText="Es Nuevo" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"/>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--col1--%>
                                                    <asp:BoundField DataField="periodofacturado" HeaderText="Periodo" />
                                                    <%--col2--%>
                                                    <asp:BoundField DataField="ffactura" HeaderText="Fecha Factura" DataFormatString="{0:dd/MM/yyyy}" />
                                                    <%--col3--%>
                                                    <asp:BoundField DataField="nombretarifa" HeaderText="Nombre Tarifa" />
                                                    <%--col4--%>
                                                    <asp:BoundField DataField="consumoactivafact" HeaderText="CSMO Activa" />
                                                    <%--col5--%>
                                                    <asp:BoundField DataField="consumoreactivafact" HeaderText="CSMO Reactiva" />
                                                    <%--col6--%>
                                                    <asp:BoundField DataField="totalimporte" HeaderText="Total Importe" />
                                                    <%--col7--%>
                                                    <asp:BoundField DataField="codtarifa" HeaderText="codtarifa" Visible="true" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <cc1:ModalPopupExtender ID="mpUpdCargo" ClientIDMode="Static" runat="server" Enabled="true"
                CancelControlID="btnCancel" OkControlID="btnCancel" PopupControlID="pnResolver"
                TargetControlID="hfIdM" BackgroundCssClass="ModalPopupBG">
            </cc1:ModalPopupExtender>
            <%--<asp:Panel ID="pnResolver" Style="display:none; overflow-x:scroll;overflow-y:scroll; max-width:800px; max-height:400px;" runat="server">--%>
    <%--<asp:Panel ID="pnResolver" runat="server">--%>
        <div ID="pnResolver" runat="server" class="panel panel-success" style="max-width:800px; overflow-y:scroll; top:10px !important; height:-webkit-fill-available !important;">
            <div class="panel-heading">Agregar Cargo</div>
            <input id="hfIdM" type="hidden" name="hddclick" runat="server" />
            <div class="panel-body">
                <div class="form-horizontal">
                    <div>
                        <div class="row">
                            <fieldset class="col-sm-4">
                                <div class="form-group">
                                    <div class="form-group col-sm-12 col-md-12 col-lg-12" style="margin-left:5px">
                                        <label for="cblCiclos">Período Cargo:</label>
                                        <div style="overflow-y: scroll; width: 98%; height:182px" border: dashed 1px;">
                                            <asp:CheckBoxList runat="server" ID="cblCiclos"></asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="col-sm-8">
                                <div class="form-group">
                                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                        <label for="ddlNewImporte">Nuevo Importe:</label>
                                        <asp:DropDownList runat="server" class="form-control" ID="ddlNewImporte"></asp:DropDownList>
                                    </div>
                                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                        <label for="txtValImporte">Valor Importe:</label>
                                        <asp:TextBox runat="server" class="form-control" ID="txtValImporte" TextMode="Number" ></asp:TextBox>
                                    </div>
                                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                        <label for="txtObsAdi">Observación adicional: </label>
                                        <asp:TextBox runat="server" class="form-control" ID="txtObsAdi"></asp:TextBox>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="col-sm-12" style="text-align:right">
                                <asp:LinkButton ID="lbAddNewCrgo" runat="server" Text="Agregar Cargo" ForeColor="blue" OnClick="lbAddNewCrgo_Click" ></asp:LinkButton>
                                <br />
                                <asp:GridView ID="gvNewCargo" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                    class="table table-striped table-bordered table-hover table-responsive" ShowFooter="false" Width="100%" Font-Size="Small">
                                    <HeaderStyle Font-Bold="True" />
                                    <EditRowStyle BackColor="Yellow" />
                                    <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                    <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                    <Columns>
                                        <%--col0--%>
                                        <asp:BoundField DataField="RowNumber" HeaderText="Row Number" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"/>
                                        <%--col1--%>
                                        <asp:TemplateField HeaderText="Período(s)" HeaderStyle-Width="45%">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txt0" runat="server" ReadOnly="true" TextMode="MultiLine" Width="100%" Rows="1"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--col2--%>
                                        <asp:TemplateField HeaderText="Código" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txt1" runat="server" ReadOnly="true"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--col3--%>
                                        <asp:TemplateField HeaderText="Cargo" HeaderStyle-Width="35%">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txt2" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--col4--%>
                                        <asp:TemplateField HeaderText="Valor" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txt3" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                            <fieldset class="col-sm-12">
                                <table style="width: 100%" class="table table-responsive">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnAceptar" Text="Finalizar" runat="server" CssClass="btn btn-success active" OnClientClick="$('#third_tab').trigger('click')" OnClick="btnAceptar_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCancel" Text="Cancelar" runat="server" CssClass="btn btn-danger active" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <%--</asp:Panel>--%>
    <%--FIN formulario flotante de actualizacion--%>


        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlLecAct" EventName="SelectedIndexChanged" />
            <%--<asp:AsyncPostBackTrigger ControlID="ddlLecRea" EventName="SelectedIndexChanged" />--%>
            <asp:PostBackTrigger ControlID="btnCalActivaGrid" />
            <%--<asp:PostBackTrigger ControlID="btnCalReactGrid" />--%>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

