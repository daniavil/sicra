﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="AsignacionMuestra.aspx.cs" Inherits="SICRA.Views.AsignacionMuestra" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <a href="#"><span>SICRA</span></a>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>   
    <a class="UrlActual" href='<%= ResolveUrl("AsignacionMuestra.aspx") %>'>Asignación de Muestra</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>

    <div class="col-sm-12">
        <h2 class="titulo">Asignación de Muestra de Lecturas</h2>
        <div class="panel panel-primary">
            <div class="panel-body">
                <%--********************************formulario de filtros**************************--%>
                <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#demoFiltros">FILTROS</button>
                <div id="demoFiltros" class="collapse">
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <asp:RadioButton ID="ckbSesgada" runat="server" AutoPostBack="True" Text="Muestra Sesgada" OnCheckedChanged="ckbSesgada_CheckedChanged" />
                                        </div>
                                        <div class="form-group col-sm-8 col-md-8 col-lg-8">
                                            <asp:RadioButton ID="ckbTotal" runat="server" AutoPostBack="True" Text="Muestra Total" OnCheckedChanged="ckbTotal_CheckedChanged" />
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlCiclo">Período:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlCiclo" Width="50%" OnSelectedIndexChanged="ddlCiclo_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlFecha">Fecha Lectura:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlFecha" Width="50%" OnSelectedIndexChanged="ddlFecha_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3" runat="server" id="divPorcentaje">
                                            <asp:RadioButtonList runat="server" ID="rbTipoCount" RepeatDirection="Horizontal">
                                                <asp:ListItem Selected="True" Value="p">Porcentaje</asp:ListItem>
                                                <asp:ListItem Value="u">Unidades</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <asp:TextBox ID="txtPorcentaje" TextMode="Number" class="form-control" runat="server" min="1" step="1" Width="50%" Text="2"/>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3" runat="server" id="filtroTipo">
                                            <label for="ddlTipoFiltro">Típo Filtro:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlTipoFiltro" OnSelectedIndexChanged="ddlTipoFiltro_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="***Seleccionar***" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="MÚLTIPLE" Value="0" ></asp:ListItem>
                                                <asp:ListItem Text="ANOMALÍA" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="INSPECTOR" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="CAMBIO PARAMETRIZACIÓN" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="INTERSECTORIAL" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblMsj" runat="server" Visible="true" Style="font-size: medium" CssClass="label label-danger"></asp:Label>
                                        </div>
                                        <div class="form-group col-sm-6 col-md-6 col-lg-6" runat="server" id="divTipoFiltro">
                                            <asp:Label ID="lblTipoFiltro" runat="server"></asp:Label>
                                            <asp:CheckBox runat="server" ID="chlTipoFiltro" Text="Seleccionar Todos" Checked="false" AutoPostBack="true" OnCheckedChanged="chlTipoFiltro_CheckedChanged" />
                                            <div style="max-height:200px; overflow-y:scroll;top:10px !important;height:-webkit-fill-available !important; width:95%; margin-right:20px;">
                                                <asp:CheckBoxList ID="ckbTipoFiltro" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ckbTipoFiltro_SelectedIndexChanged" ></asp:CheckBoxList>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-5 col-md-5 col-lg-5" runat="server" id="divAnomalia2">
                                            <span><br /></span>
                                            <asp:TextBox runat="server" ID="txtTipoFiltro" Visible="false" TextMode="MultiLine" ReadOnly="true" style="max-height:200px; width:100%; height:100%;"></asp:TextBox>
                                        </div>
                                        <asp:HiddenField runat="server" ID="hfFiltra" Value="0" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <%--<asp:AsyncPostBackTrigger ControlID="ddlFecha" EventName="SelectedIndexChanged" />--%>
                                        <asp:AsyncPostBackTrigger ControlID="ckbSesgada" EventName="CheckedChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ckbTotal" EventName="CheckedChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <asp:Button ID="btnFiltrar" runat="server" Text="Buscar" CssClass="btn btn-warning" OnClick="btnFiltrar_Click" />
                    </div>
                </div>
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <asp:Label ID="lblBusqueda" runat="server" Visible="False" Style="font-size:large" CssClass="label label-danger"></asp:Label>
                    </div>
                <%--********************************FIN formulario de filtros**************************--%>
                <br />
                <br />
                <br />
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <asp:Label ID="lblFiltros" runat="server" Style="font-size: small" CssClass="label label-default"></asp:Label>
                </div>

                <asp:Panel ID="pnlAsignado" runat="server" Visible="false">
                    <div class="form-group col-sm-2 col-md-2 col-lg-2">
                        <span style="background-color: lightcoral">Conteo Muestra:</span>
                        <span id="spnConteo" runat="server" style="font-weight: bold"></span>
                    </div>
                    <div class="form-group col-sm-2 col-md-2 col-lg-2">
                        <span style="background-color: lightblue">Fecha:</span>
                        <span id="spnFecha" runat="server" style="font-weight: bold"></span>
                    </div>
                    <div class="form-group col-sm-2 col-md-2 col-lg-2">
                        <span style="background-color: lightgreen">Tipo:</span>
                        <span id="spnTipo" runat="server" style="font-weight: bold"></span>
                    </div>
                    <div class="form-group col-sm-6 col-md-6 col-lg-6" style="text-align: left">
                        <asp:Button ID="btnAsignar" runat="server" CssClass="btn btn-primary" Text="Asignar" OnClick="btnAsignar_Click" OnClientClick="Confirm()" />
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <asp:Label ID="lblMsjSave" runat="server" Visible="False" Style="font-size:large" CssClass="label label-success"></asp:Label>
                    </div>

                    <asp:GridView ID="gvRepoM" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                        CssClass="mydatagrid" Width="100%" DataKeyNames="CLAVE"
                        Font-Size="Small" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                        <HeaderStyle Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="Yellow" />
                        <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                        <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--columna 1--%>
                            <asp:BoundField DataField="CLAVE" HeaderText="CLAVE" />
                            <%--columna 2--%>
                            <asp:BoundField DataField="CODIGODIAL" HeaderText="DIAL" />
                            <%--columna 3--%>
                            <asp:BoundField DataField="SUBCLASE" HeaderText="Cod. Anomalía" />
                            <%--columna 4--%>
                            <asp:BoundField DataField="DESCRIPCION" HeaderText="Nombre Anomalía" />
                            <%--columna 5--%>
                            <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"  DataField="CODIGOGRUPOTRABAJO" HeaderText="CodLector" />
                            <%--columna 6--%>
                            <asp:BoundField DataField="NomLector" HeaderText="Lector" />
                            <%--columna 7--%>
                            <asp:BoundField DataField="ParamAnterior" HeaderText="Parametrización Anterior" />
                            <%--columna 8--%>
                            <asp:BoundField DataField="ParamActual" HeaderText="Parametrización Actual" />
                            <%--columna 9--%>
                            <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"  DataField="CODIGOADMINISTRATIVO" HeaderText="CODIGOADMINISTRATIVO" />
                            <%--columna 10--%>
                            <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"  DataField="SECTOR_ID" HeaderText="SECTOR_ID" />
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
    
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <style>
        .pictures {
            margin: 0;
            padding: 0;
            list-style: none;
            width:100%
        }

            .pictures > li {
                float: left;
                width: 50px;
                height: 50px;
                margin: 0 -1px -1px;
                border: 1px solid transparent;
                overflow: hidden;
            }

                .pictures > li > img {
                    width: 50px;
                    height: 50px;
                    cursor: -webkit-zoom-in;
                    cursor: zoom-in;
                }

        .viewer-download {
            color: #fff;
            font-family: FontAwesome;
            font-size: .75rem;
            line-height: 1.5rem;
            text-align: center;
        }

            .viewer-download::before {
                content: "\f019";
            }

        #FotoContainer {
            margin-top: 10px !important;
            display: inline-block;
        }
    </style>
    <style type="text/css">
        .tableFont {font-size:14px !important;}
        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            if (confirm("¿Está seguro de realizar esta acción?")) {
                    confirm_value.value = "Si";
            }
            else {
                    confirm_value.value = "No";
            }

            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>