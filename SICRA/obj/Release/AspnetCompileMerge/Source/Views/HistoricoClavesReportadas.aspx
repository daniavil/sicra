﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HistoricoClavesReportadas.aspx.cs" Inherits="SICRA.Views.HistoricoClavesReportadas" %>
<%@ Register Src="~/Modals/MsjModal.ascx" TagPrefix="uc1" TagName="MsjModal" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <a href="#"><span>SICRA</span></a> <%--agregar al menu que pertenece--%>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
    <a class="UrlActual" href='<%= ResolveUrl("HistoricoClavesReportadas.aspx") %>'>Histórico Claves Reportadas</a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<asp:ScriptManager runat="server"></asp:ScriptManager>

    <%--formulario flotante de actualizacion--%>
            <cc1:ModalPopupExtender ID="mpver" ClientIDMode="Static" runat="server" Enabled="true"
                CancelControlID="btnCancel" OkControlID="btnCancel" PopupControlID="pnver"
                TargetControlID="hfIdM" BackgroundCssClass="ModalPopupBG">
            </cc1:ModalPopupExtender>

            <div ID="pnver" runat="server" class="panel panel-info" style="width:80%; overflow-y:scroll;top:10px !important;height:-webkit-fill-available !important;">
                <div class="panel-heading">NIS: <span id="spNisrad" runat="server"></span> - CICLO: <span id="spCiclo" runat="server"></span></div>
                <div class="panel-body">
                    <input id="hfIdM" type="hidden" name="hfIdM" runat="server" />
                    <div class="form-inline">
                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                            <h2 style="width:100%"><span class="label label-primary">Reporte Inicial</span></h2>
                        </div>
                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                            <br />
                        </div>
                        <%------------------------------------------%>
                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                            <label for="txtUbicacion">Ubicación:</label>
                            <asp:Label runat="server" ID="txtUbicacion"></asp:Label>
                        </div>
                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                            <label for="txtRegion">Region:</label>
                            <asp:Label runat="server" ID="txtRegion"></asp:Label>
                        </div>
                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                            <label for="txtSector">Sector:</label>
                            <asp:Label runat="server" ID="txtSector"></asp:Label>
                        </div>
                        <%------------------------------------------%>
                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                            <label for="txtMedida">Medida:</label>
                            <asp:Label runat="server" ID="txtMedida"></asp:Label>
                        </div>
                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                            <label for="txtResuelto">Resuelto:</label>
                            <asp:Label runat="server" ID="txtResuelto"></asp:Label>
                        </div>
                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                            <label for="txtAnomalia">Anomalía:</label>
                            <asp:Label runat="server" ID="txtAnomalia"></asp:Label>
                        </div>
                        <%------------------------------------------%>
                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                            <label for="txtLecActiva">Lectura Activa:</label>
                            <asp:Label runat="server" ID="txtLecActiva"></asp:Label>
                        </div>
                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                            <label for="txtLecReactiva">Lectura Reactiva:</label>
                            <asp:Label runat="server" ID="txtLecReactiva"></asp:Label>
                        </div>
                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                            <label for="txtLecDemanda">Lectura Demanda:</label>
                            <asp:Label runat="server" ID="txtLecDemanda"></asp:Label>
                        </div>
                        <%------------------------------------------%>
                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                            <label for="txtCiclo">Ciclo:</label>
                            <asp:Label runat="server" ID="txtCiclo"></asp:Label>
                        </div>
                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                            <label for="txtDial">Dial Lectura:</label>
                            <asp:Label runat="server" ID="txtDial"></asp:Label>
                        </div>
                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                            <label for="txtRepo">Tipo:</label>
                            <asp:Label runat="server" ID="txtRepo"></asp:Label>
                        </div>
                        <%------------------------------------------%>
                        <div class="form-group col-sm-6 col-md-6 col-lg-6">
                            <label for="txtFechaRepo">Fecha Reporte:</label>
                            <asp:Label runat="server" ID="txtFechaRepo"></asp:Label>
                        </div>
                        <div class="form-group col-sm-6 col-md-6 col-lg-6">
                            <label for="txtFechaMaxResol">Fecha Máxima Resolución:</label>
                            <asp:Label runat="server" ID="txtFechaMaxResol"></asp:Label>
                        </div>
                        <%------------------------------------------%>
                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                            <label for="txtCategoria">Categoría Resolución:</label>
                            <asp:Label runat="server" ID="txtCategoria"></asp:Label>
                        </div>
                        <%------------------------------------------%>
                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                            <label for="txtCausa">Solicitud O Causa de Verificación:</label>
                            <asp:Label runat="server" ID="txtCausa"></asp:Label>
                        </div>
                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                            <br />
                        </div>
                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                            <h2 style="width:100%"><span class="label label-primary">Reportes / Resoluciones</span></h2>
                        </div>
                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <asp:GridView ID="gvRepoD" runat="server" AllowPaging="False" AutoGenerateColumns="False" CssClass="mydatagrid" 
                                    DataKeyNames="IdRow_D" Font-Size="Small" Width="100%" PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                        <HeaderStyle Font-Bold="True" ForeColor="White" />
                                        <EditRowStyle BackColor="#ffffcc" />
                                        <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                        <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                        <Columns>
                                    <asp:BoundField DataField="IdRow_D" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdRow_D" ReadOnly="True" Visible="true" />
                                    <asp:BoundField DataField="UsuarioResuelve" HeaderText="Usuario Reportó / Resolvió" ReadOnly="True" />
                                    <asp:BoundField DataField="LecturaActiva" HeaderText="Lectura Activa" ReadOnly="True" />
                                    <asp:BoundField DataField="LecturaReactiva" HeaderText="Lectura Reactiva" ReadOnly="True" />
                                    <asp:BoundField DataField="LecturaDemanda" HeaderText="Lectura Demanda" ReadOnly="True" />
                                    <asp:BoundField DataField="Resolucion" HeaderText="Reporte / Resolución" ReadOnly="True" />
                                    <asp:BoundField DataField="FechaResolucion" HeaderText="Fecha Reporte / Resolución" ReadOnly="True" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Realiza" ReadOnly="True" />
                                    <asp:TemplateField HeaderText="Tipo">
                                        <ItemTemplate><%# (Boolean.Parse(Eval("esRespTec").ToString())) ? "Respuesta" : "Reporte" %></ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="form-group col-sm-12 col-md-12 col-lg-12"><br /></div>
                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                            <div id="imgDiv">
                                <h2 style="width:100%"><span class="label label-primary">Imágenes</span></h2>
                                <div class="panel-body" id="sidebar">
                                    <%--GALERIA--%>
                                    <div id="FotoContainer" class="col-sm-12 col-md-12 col-lg-12">
                                        <div id="galley" runat="server" ClientIDMode="static">
                                            <asp:Literal ID="literalControl" runat="server" />
                                        </div>
                                        <asp:Label ID="lblmsjErrorImg" runat="server" Visible="False" Style="font-size: medium" CssClass="label label-danger"></asp:Label>
                                    </div>
                                    <script src="../GalleryRoot/js/viewer.js"></script>
                                    <script>
                                        window.addEventListener('DOMContentLoaded', function () {
                                            var galley = document.getElementById('galley');
                                            var viewer = new Viewer(galley, {
                                                toolbar: {
                                                    zoomIn: 4,
                                                    zoomOut: 4,
                                                    oneToOne: 4,
                                                    reset: 4,
                                                    prev: 4,
                                                    play: {
                                                        show: 0,
                                                        size: 'large',
                                                    },
                                                    next: 4,
                                                    rotateLeft: 4,
                                                    rotateRight: 4,
                                                    flipHorizontal: 4,
                                                    flipVertical: 4,
                                                },
                                            });
                                        });
                                    </script>
                                    <%--@*FIN GALERIA*@--%>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                            <asp:Button ID="btnCancel" Text="Cerrar" runat="server" CssClass="btn btn-warning active" />
                        </div>
                    </div>
                </div>
            </div>
        <%--</asp:Panel>--%>
        <%--FIN formulario flotante de actualizacion--%>


    <asp:Panel runat="server" ID="panelPrincipal">
                <div class="col-sm-12">
                    <h2 class="titulo">Histórico de Claves Reportadas</h2>
                    <div class="form-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Claves Reportadas</div>
                            <div class="panel-body">
                                <%--********************************formulario de filtros**************************--%>
                                <br />
                                <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#demo">FILTROS</button>
                                <div id="demo" class="collapse">
                                    <div class="panel panel-success">
                                        <div class="panel-body">
                                            <div class="form-horizontal">
                                                <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                                    <label for="txtNisrad">NIS:</label>
                                                    <asp:TextBox ID="txtClave" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="form-group col-sm-5 col-md-5 col-lg-5">
                                                    <label for="ddlRegion">Región:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-5 col-md-5 col-lg-5">
                                                    <label for="ddlSector">Sector:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlSector" OnSelectedIndexChanged="ddlSector_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                                    <label for="ddlMedida">Medida:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlMedida"></asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                                    <label for="ddlAnomalia">Anomalía:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlAnomalia"></asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                                    <label for="ddlPerfilReso">Perfil de Resolución:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlPerfilReso">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                                    <label for="ddlCiclo">Ciclo:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlCiclo"></asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                                    <label for="ddlDial">Dial:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlDial"></asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                                    <label for="ddlEstado">Resuelto:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlEstado">
                                                        <asp:ListItem Text="--Seleccionar--" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="ESPERA" Value="2"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                                    <asp:Button ID="btnFiltrar" runat="server" Text="Buscar" CssClass="btn btn-warning" OnClick="btnFiltrar_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--********************************FIN formulario de filtros**************************--%>
                                <br />
                                <br />
                                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                    <span >Conteo:</span>
                                    <span id="spnConteo" runat="server" style="font-weight: bold"></span>
                                </div>
                                <asp:GridView ID="gvRepoM" runat="server" AllowPaging="True" OnPageIndexChanging="gvRepoM_PageIndexChanging"
                                    AutoGenerateColumns="False" PageSize="20" CssClass="mydatagrid" DataKeyNames="IdRow_M"
                                    OnRowDataBound="gvRepoM_RowDataBound" Font-Size="Small" Width="100%" OnRowCommand="gvRepoM_RowCommand"
                                    PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#ffffcc" />
                                    <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                    <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="#">
                                            <ItemTemplate>
                                                <img alt="" class="imgPlusRow" style="cursor: pointer" src="../Imagenes/plus.png" />
                                                <asp:Panel ID="pnlRegistros" runat="server" Style="display: none">
                                                    <asp:GridView ID="gvRepoD" runat="server" CssClass="Grid" AutoGenerateColumns="false" Width="100%">
                                                        <Columns>
                                                            <asp:BoundField DataField="IdRow_D" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdRow_D" ReadOnly="True" Visible="true" />
                                                            <asp:BoundField DataField="UsuarioResuelve" HeaderText="Usuario Resolvio" ReadOnly="True" />
                                                            <asp:BoundField DataField="LecturaActiva" HeaderText="Lectura Activa" ReadOnly="True" />
                                                            <asp:BoundField DataField="LecturaReactiva" HeaderText="Lectura Reactiva" ReadOnly="True" />
                                                            <asp:BoundField DataField="LecturaDemanda" HeaderText="Lectura Demanda" ReadOnly="True" />
                                                            <asp:BoundField DataField="Resolucion" HeaderText="Resolución" ReadOnly="True" />
                                                            <asp:BoundField DataField="FechaResolucion" HeaderText="Fecha Resolución" ReadOnly="True" />
                                                            <asp:BoundField DataField="Tipo" HeaderText="Tipo" ReadOnly="True" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="IdRow_M" HeaderText="IdRow_M" />
                                        <asp:BoundField DataField="Nis_Rad" HeaderText="NIS" />
                                        <asp:BoundField DataField="Region" HeaderText="Región" />
                                        <asp:BoundField DataField="Sector" HeaderText="Sector" />
                                        <asp:BoundField DataField="Medida" HeaderText="Medida" />
                                        <asp:BoundField DataField="LecturaActiva" HeaderText="L. Activa" />
                                        <asp:BoundField DataField="LecturaReactiva" HeaderText="L. Reactiva" />
                                        <asp:BoundField DataField="LecturaDemanda" HeaderText="L. Demanda" />
                                        <asp:BoundField DataField="UsuarioFacturacion" HeaderText="Usuario Reporta" />
                                        <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha Reporte" />
                                        <asp:BoundField DataField="Anomalia" HeaderText="Anomalía" />
                                        <asp:BoundField DataField="Ciclo" HeaderText="Ciclo" />
                                        <asp:BoundField DataField="dial" HeaderText="Dial" />
                                        <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="Resuelto" HeaderText="Resuelto" />
                                        <asp:BoundField DataField="ResueltoStng" HeaderText="Resuelto" />
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Acción">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkver" runat="server" CommandArgument='<%# Eval("Nis_Rad").ToString() +","+ Eval("Ciclo").ToString()  %>' Text="VER" ForeColor="blue"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    <script src="../Scripts/GridCells.js"></script>
    
    <script type="text/javascript">
        $(document).on("click", "[class^=imgPlusRow]", function () {
            $(this).attr("class", "imgMinusRow");
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
            $(this).attr("src", "../Imagenes/minus.png");
        });
        $(document).on("click", "[class^=imgMinusRow]", function () {
            $(this).attr("class", "imgPlusRow");
            $(this).attr("src", "../Imagenes/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <link rel="stylesheet" href="../GalleryRoot/css/viewer.css" />
    <style>
        .file{
            margin-left:25%;
            margin-top:4%;
            margin-bottom:4%;
        }
        .titulo{
            margin-left: 35%;
            margin-bottom: 4%;
        }
        .total{
            margin-left:60%;
            margin-bottom: 1.5%;
            margin-top: 1.5%;
        }
        .btnCargar{
            margin-left: 65%;
            margin-top: 2%;
            margin-bottom: 2%;
        }
        .loading{
            position:fixed;
            top:0px;
            right:0px;
            width:100%;
            height:100%;
            background-color:#ffffff;
            background-repeat:no-repeat;
            background-position:center;
            z-index:10000000;
            opacity: 0.80;
            filter: alpha(opacity=0); 
        }
        .tableFont {font-size:14px !important;}
        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }
        .HellowWorldPopup
        {
            min-width:200px;
            min-height:150px;
            background:white;
        }
        .Controls {
            padding-top:10px;
            padding-bottom:10px;
        }
        .link
        {
         color: #428bca !important; 
         text-decoration: underline;
        }

        .pictures {
            margin: 0;
            padding: 0;
            list-style: none;
            width:100%
        }
        .pictures > li {
                float: left;
                width: 50px;
                height: 50px;
                margin: 0 -1px -1px;
                border: 1px solid transparent;
                overflow: hidden;
            }
        .pictures > li > img {
                    width: 50px;
                    height: 50px;
                    cursor: -webkit-zoom-in;
                    cursor: zoom-in;
                }
        .viewer-download {
            color: #fff;
            font-family: FontAwesome;
            font-size: .75rem;
            line-height: 1.5rem;
            text-align: center;
        }
        .viewer-download::before {
                content: "\f019";
            }
        #FotoContainer {
            margin-top: 10px !important;
            display: inline-block;
        }
        .tableFont {font-size:14px !important;}
        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }

    </style>
</asp:Content>