﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="AsignaMuestraFact.aspx.cs" Inherits="SICRA.Views.AsignaMuestraFact" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <a href="#"><span>SICRA</span></a>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>   
    <a class="UrlActual" href='<%= ResolveUrl("AsignaMuestraFact.aspx") %>'>Asignación de Muestra</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>

    <div class="col-sm-12">
        <h2 class="titulo">Asignación de Muestra de Crítica Facturación</h2>
        <div class="panel panel-primary">
            <div class="panel-body">
                <%--********************************formulario de filtros**************************--%>
                <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#demoFiltros">FILTROS</button>
                <div id="demoFiltros" class="collapse">
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <asp:HiddenField runat="server" ID="hfFiltra" Value="0" />
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <asp:RadioButton ID="ckbSesgada" runat="server" AutoPostBack="True" Text="Muestra Sesgada" OnCheckedChanged="ckbSesgada_CheckedChanged" />
                                        </div>
                                        <div class="form-group col-sm-10 col-md-10 col-lg-10">
                                            <asp:RadioButton ID="ckbTotal" runat="server" AutoPostBack="True" Text="Muestra Total" OnCheckedChanged="ckbTotal_CheckedChanged" />
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlCiclo">Ciclo:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlCiclo" Width="200px" OnSelectedIndexChanged="ddlCiclo_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlFecha">Fecha Lectura:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlFecha" Width="200px" OnSelectedIndexChanged="ddlFecha_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3" runat="server" id="divPorcentaje">
                                            <label for="txtPorcentaje">% de Muestras:</label>
                                            <asp:TextBox ID="txtPorcentaje" TextMode="Number" class="form-control" runat="server" min="1" max="100" step="1" Width="150px" Text="80"/>
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblMsj" runat="server" Visible="true" Style="font-size: medium" CssClass="label label-danger"></asp:Label>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3" runat="server" id="divUsuarios">
                                            <label for="cklUsuarios">Usuarios a Asignar:  ------  </label>
                                            <asp:CheckBox runat="server" ID="chlUsrs" Text="Seleccionar Todos" Checked="false" AutoPostBack="true" OnCheckedChanged="chlUsrs_CheckedChanged" />
                                            <div style="overflow-y: scroll; width: 98%; height: 300px; border: dashed 1px;">
                                                <asp:CheckBoxList ID="cklUsuarios" runat="server" Font-Size="Smaller"></asp:CheckBoxList>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3" runat="server" id="divAnomalia">
                                            <label for="ddlAnomalia">Anomalía  ------  </label>
                                            <asp:CheckBox runat="server" ID="ckTodosAnom" Text="Seleccionar Todos" Checked="false" AutoPostBack="true" OnCheckedChanged="ckTodosAnom_CheckedChanged" />
                                            <div style="overflow-y: scroll; width: 98%; height:300px; border: dashed 1px;">
                                                <asp:CheckBoxList ID="cklAnomalia" runat="server" Font-Size="Smaller"></asp:CheckBoxList>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3" runat="server" id="div1">
                                            <label for="cklObservaciones">Observaciones  ------  </label>
                                            <asp:CheckBox runat="server" ID="ckObserv" Text="Seleccionar Todos" Checked="false" AutoPostBack="true" OnCheckedChanged="ckObserv_CheckedChanged" />
                                            <div style="overflow-y: scroll; width: 98%; height:300px; border: dashed 1px;">
                                                <asp:CheckBoxList ID="cklObservaciones" runat="server" Font-Size="Smaller"></asp:CheckBoxList>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3" runat="server" id="div2">
                                            <label for="cklTodosUsrSoeeh">Usuarios Crítica  ------  </label>
                                            <asp:CheckBox runat="server" ID="cklTodosUsrSoeeh" Text="Seleccionar Todos" Checked="false" AutoPostBack="true" OnCheckedChanged="cklTodosUsrSoeeh_CheckedChanged" />
                                            <div style="overflow-y: scroll; width: 98%; height:300px; border: dashed 1px;">
                                                <asp:CheckBoxList ID="cklUsrCritica" runat="server" Font-Size="Smaller"></asp:CheckBoxList>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ckbSesgada" EventName="CheckedChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ckbTotal" EventName="CheckedChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <asp:Button ID="btnFiltrar" runat="server" Text="Buscar" CssClass="btn btn-warning" OnClick="btnFiltrar_Click" />
                    </div>
                </div>
                
                <%--********************************FIN formulario de filtros**************************--%>
                <br />
                <br />
                <br />
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <asp:Label ID="lblFiltros" runat="server" Style="font-size: small" CssClass="label label-default"></asp:Label>
                </div>

                <asp:Panel ID="pnlAsignado" runat="server" Visible="false">
                    <div class="form-group col-sm-2 col-md-2 col-lg-2">
                        <span style="background-color: lightcoral">Conteo Muestra:</span>
                        <span id="spnConteo" runat="server" style="font-weight: bold"></span>
                    </div>
                    <div class="form-group col-sm-2 col-md-2 col-lg-2">
                        <span style="background-color: lightblue">Fecha:</span>
                        <span id="spnFecha" runat="server" style="font-weight: bold"></span>
                    </div>
                    <div class="form-group col-sm-2 col-md-2 col-lg-2">
                        <span style="background-color: lightgreen">Tipo:</span>
                        <span id="spnTipo" runat="server" style="font-weight: bold"></span>
                    </div>
                    <div class="form-group col-sm-6 col-md-6 col-lg-6" style="text-align: left">
                        <asp:Button ID="btnAsignar" runat="server" CssClass="btn btn-primary" Text="Asignar" OnClick="btnAsignar_Click" OnClientClick="Confirm()" />
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <asp:Label ID="lblMsjSave" runat="server" Visible="False" Style="font-size:large" CssClass="label label-success"></asp:Label>
                    </div>

                    <asp:GridView ID="gvRepoM" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                        CssClass="mydatagrid" Width="100%" DataKeyNames="nisrad"
                        Font-Size="Small" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                        <HeaderStyle Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="Yellow" />
                        <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                        <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--col 1--%>
                            <asp:BoundField DataField="nisrad" HeaderText="NIS" />
                            <%--col 2--%>
                            <asp:BoundField DataField="dial" HeaderText="DIAL" />
                            <%--col 3--%>
                            <asp:BoundField DataField="codAnoCri" HeaderText="Cod. Anomalía Crítica" />
                            <%--col 4--%>
                            <asp:BoundField DataField="nomAnoCri" HeaderText="Nombre Anomalía Crítica" />
                            <%--col 5--%>
                            <asp:BoundField DataField="codAnoLec" HeaderText="Cod. Anomalía Lectura" />
                            <%--col 6--%>
                            <asp:BoundField DataField="nomAnoLec" HeaderText="Nombre Anomalía Lectura" />
                            <%--col 7--%>
                            <asp:BoundField DataField="codUsuario" HeaderText="Usuario Asignado" />
                            <%--col 8--%>
                            <asp:BoundField DataField="ciclo" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="ciclo" ReadOnly="True" Visible="true" />
                            <%--col 9--%>
                            <asp:BoundField DataField="LectActiva" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="LectActiva" ReadOnly="True" Visible="true" />
                            <%--col 10--%>
                            <asp:BoundField DataField="LectReactiva" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="LectReactiva" ReadOnly="True" Visible="true" />
                            <%--col 11--%>
                            <asp:BoundField DataField="LectDemanda" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="LectDemanda" ReadOnly="True" Visible="true" />
                            <%--col 12--%>
                            <asp:BoundField DataField="DescripObserv" HeaderText="Observación" />
                            <%--col 13--%>
                            <asp:BoundField DataField="UsrSoeeh" HeaderText="USR Crítica SOEEH" />
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <style>
        .pictures {
            margin: 0;
            padding: 0;
            list-style: none;
            width:100%
        }

            .pictures > li {
                float: left;
                width: 50px;
                height: 50px;
                margin: 0 -1px -1px;
                border: 1px solid transparent;
                overflow: hidden;
            }

                .pictures > li > img {
                    width: 50px;
                    height: 50px;
                    cursor: -webkit-zoom-in;
                    cursor: zoom-in;
                }

        .viewer-download {
            color: #fff;
            font-family: FontAwesome;
            font-size: .75rem;
            line-height: 1.5rem;
            text-align: center;
        }

            .viewer-download::before {
                content: "\f019";
            }

        #FotoContainer {
            margin-top: 10px !important;
            display: inline-block;
        }
    </style>
    <style type="text/css">
        .tableFont {font-size:14px !important;}
        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            if (confirm("¿Está seguro de realizar esta acción?")) {
                    confirm_value.value = "Si";
            }
            else {
                    confirm_value.value = "No";
            }

            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>