﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AutorizacionRectificacion.aspx.cs" Inherits="SICRA.Views.AutorizacionRectificacion" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <a href="#"><span>SICRA</span></a>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>   
    <a class="UrlActual" href='<%= ResolveUrl("AutorizacionRectificacion.aspx") %>'>Autorizacion de Rectificaciones</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>

    <%--formulario flotante de actualizacion--%>
    <cc1:ModalPopupExtender ID="mpResolver" ClientIDMode="Static" runat="server" Enabled="true"
        CancelControlID="btnCancel" OkControlID="btnCancel" PopupControlID="pnResolver"
        TargetControlID="hfIdM" BackgroundCssClass="ModalPopupBG">
    </cc1:ModalPopupExtender>

    <div id="pnResolver" runat="server" class="panel panel-success pnlresolv">
        <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left" style="padding-top: 7.5px;">Resolver Solicitud Refactura</h4>
            <div class="input-group" style="float: right">
                <asp:Button ID="btnCancel" Text="Cerrar" runat="server" CssClass="btn btn-danger active" />
            </div>
        </div>

        <input id="hfIdM" type="hidden" name="hddclick" runat="server" />
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Datos Generales</div>
                        <div class="panel-body">
                            <asp:HiddenField ID="hfIdAsigGes" runat="server" />
                            <asp:HiddenField ID="hfIdGestionRefa" runat="server" />
                            <asp:HiddenField ID="hfUrlFile" ClientIDMode="Static" runat="server" />
                            <asp:HiddenField ID="hfNomFile" runat="server" />

                            <div class="col-sm-3 b-r">
                                <label for="lblNisrad">NIS:</label>
                                <asp:Label ID="lblNisrad" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-3 b-r">
                                <label for="lblNisrad">Origen:</label>
                                <asp:Label ID="lblOrigen" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-3 b-r">
                                <label for="lblNisrad">Id Gestión:</label>
                                <asp:Label ID="lblCodGestion" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-3 b-r">
                                <label for="lblTipoRect">Tipo:</label>
                                <asp:Label ID="lblTipoRect" runat="server"></asp:Label>
                            </div>

                            <div class="col-sm-4 b-r">
                                <label for="lblMonto">Valor kWh:</label>
                                <asp:Label ID="lblKwh" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-4 b-r">
                                <label for="lblMonto">Monto LPS:</label>
                                <asp:Label ID="lblMonto" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-4 b-r">
                                <label for="lblDbcr">DBCR:</label>
                                <asp:Label ID="lblDbcr" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-12 b-r">
                                <label for="lblCiclos">Ciclo(s):</label>
                                <asp:Label ID="lblCiclos" runat="server"></asp:Label>
                            </div>

                            <div class="col-sm-4 b-r">
                                <label for="lblSector">Sector:</label>
                                <asp:Label ID="lblSector" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-4 b-r">
                                <label for="lblMercado">Mercado:</label>
                                <asp:Label ID="lblMercado" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-4 b-r">
                                <label for="lblFechaSoli">Fecha de Solicitud:</label>
                                <asp:Label ID="lblFechaSoli" runat="server"></asp:Label>
                            </div>

                            <div class="col-sm-12 b-r">
                                <label for="lblSolicita">Nombre Solicitante:</label>
                                <asp:Label ID="lblSolicita" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-12 b-r">
                                <label for="txtRazonSolicitud">Razón de Solicitud:</label>
                                <asp:TextBox runat="server" class="form-control" ID="txtCausa" ReadOnly="true" TextMode="MultiLine" Rows="2"></asp:TextBox>
                            </div>

                            <div class="col-sm-4 b-r">
                                <label for="lblEstado">Estado:</label>
                                <asp:Label ID="lblEstado" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-4 b-r">
                                <label for="lblFinalizado">Finalizado:</label>
                                <asp:Label ID="lblFinalizado" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-12 b-r">
                                <asp:Label ID="lblErrorDatos" runat="server" Visible="false"></asp:Label>
                            </div>
                            <asp:HiddenField ID="hfObj" runat="server" Value="" />
                        </div>
                    </div>
                </div>

                <%--GRID RESOLUCIONES Y DOCUMENTOS--%>
                <div class="col-sm-12 b-r">
                    <div class="btn btn-info glyphicon glyphicon-hand-right">
                        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Cadena de Resoluciones</button>
                    </div>
                    <div id="demo" class="collapse form-group col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">Resoluciones/Documentos</div>
                            <div class="panel-body" style="text-align: left">
                                <asp:GridView ID="gvResoluciones" runat="server" AllowSorting="False" AutoGenerateColumns="False" CssClass="mydatagrid"
                                    Width="100%" DataKeyNames="IdGestDet" Font-Size="Small" HeaderStyle-CssClass="header" RowStyle-CssClass="rows"
                                    OnRowDataBound="gvResoluciones_RowDataBound">
                                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="Yellow" />
                                    <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                    <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="#">
                                            <ItemTemplate>
                                                <img alt="" class="imgPlusRow" style="cursor: pointer" src="../Imagenes/plus.png" />
                                                <asp:Panel ID="pnlDocs" runat="server" Style="display: none">
                                                    <asp:GridView ID="gvDocs" runat="server" Width="100%" AutoGenerateColumns="false" AllowPaging="false" CssClass="Grid">
                                                        <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                                        <Columns>
                                                            <asp:BoundField DataField="IdDoc" HeaderText="IdDoc" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" Visible="true"></asp:BoundField>
                                                            <asp:BoundField DataField="NomDoc" HeaderText="Nombre Archivo" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="hlDownload" runat="server" CommandArgument='<%# Eval("Ruta") %>' OnCommand="hlDownload_Command" CommandName='<%# Eval("NomDoc") %>' Text="Descargar" Style="color: black !important;"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="IdGestDet" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="idAsigRefa" ReadOnly="True" Visible="true" />
                                        <asp:BoundField DataField="ComentarioResol" HeaderText="Resolución" />
                                        <asp:BoundField DataField="FechaHoraResuelto" HeaderText="F/H Resuelto" />
                                        <asp:BoundField DataField="NomResolvio" HeaderText="Resolvió" />
                                        <asp:BoundField DataField="Perfil" HeaderText="Perfil" />
                                        <asp:BoundField DataField="Estado" HeaderText="Estado" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <%--**************************************************************************************************************************************************************************************************************************************************************************--%>
                <div class="col-sm-12 b-r">
                    <br />
                </div>

                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Resolución</div>
                        <div class="panel-body" style="text-align: left">
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <label for="txtResolucion">Comentario de Resolución:</label>
                                <asp:TextBox runat="server" class="form-control" ID="txtResolucion" TextMode="MultiLine" Rows="2"></asp:TextBox>
                            </div>
                            <%--**************************************************************************************************************************************************************************************************************************************************************************--%>
                            <div class="col-sm-12 b-r">
                                <h3 class="m-t-none m-b">Archivo de Soporte:</h3>
                                <asp:Panel runat="server" ID="panelCargaImg" CssClass="panel panel-primary">
                                    <h5 style="color: red">ARCHIVO</h5>
                                    <asp:FileUpload ID="fUpload" runat="server" CssClass="form-control" AllowMultiple="true" />
                                </asp:Panel>
                            </div>

                            <div class="col-sm-12 b-r">
                                <br />
                            </div>

                            <div class="col-sm-4 b-r text-center">
                                <div class="btn btn-success glyphicon glyphicon-floppy-saved">
                                    <asp:Button ID="btnAceptar" Text="Aceptar Autorización" runat="server" CssClass="btn btn-success active" OnClick="btnAceptar_Click" OnClientClick="Confirm();" />
                                </div>
                            </div>

                            <div class="col-sm-4 b-r text-center">
                                <div class="btn btn-warning glyphicon">
                                    <asp:DropDownList runat="server" class="form-control" ID="ddlPerfilRetorna" OnSelectedIndexChanged="ddlPerfilRetorna_SelectedIndexChanged" onchange="Confirm();" AutoPostBack="true" Style="background-color: #eea236; color: white;"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-4 b-r text-center">
                                <div class="btn btn-danger glyphicon glyphicon-remove">
                                    <asp:Button ID="btnDenegar" Text="No Procede" runat="server" CssClass="btn btn-danger active" OnClick="btnDenegar_Click" OnClientClick="Confirm();" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--</asp:Panel>--%>
    <%--FIN formulario flotante de actualizacion--%>
    
    
    <div class="col-sm-12">
        <h2 class="titulo">Solicitudes de Rectificación</h2>
        <div class="panel panel-primary">
            <div class="panel-body">
                <br />
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <span style="background-color: lightcoral">Conteo de Registros:</span>
                    <span id="spnNoResueltas" runat="server" style="font-weight: bold"></span>
                </div>
                <asp:Panel ID="pnlAsignado" runat="server">
                    <asp:GridView ID="gvRepoM" runat="server" OnRowCommand="gvRepoM_RowCommand"
                    AllowSorting="False" AutoGenerateColumns="False" CssClass="mydatagrid" Width="100%"
                    DataKeyNames="IdGesRefDet" Font-Size="Small" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="Yellow" />
                    <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                    <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="IdGesRefDet" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdGesRefDet" ReadOnly="True" Visible="true" />
                        <asp:BoundField DataField="nisrad" HeaderText="NIS" />
                        <asp:BoundField DataField="ComentarioSolicitud" HeaderText="COMENTARIO" />
                        <asp:BoundField DataField="FechaCreacion" HeaderText="Fecha/Hora" />
                        <asp:BoundField DataField="NomPersona" HeaderText="Solicitante" />
                        <asp:BoundField DataField="IdGesRefMae" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdGesRefMae" ReadOnly="True" Visible="true" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Acción">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkResolver" runat="server" CommandName="resolver" 
                                    CommandArgument='<%# Eval("IdGesRefMae") %>' Text="RESOLVER" ForeColor="blue">
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </asp:Panel>

                <div class="form-horizontal col-sm-12 col-md-12 col-lg-12">
                    <asp:Label ID="lblError" runat="server" Visible="false"></asp:Label>
                </div>
            </div>
        </div>
    </div>

</asp:Content>


<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolderJS">
    <script src="../Scripts/GridCells.js"></script>        
    <script type="text/javascript">
        $(document).on("click", "[class^=imgPlusRow]", function () {
            $(this).attr("class", "imgMinusRow");
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
            $(this).attr("src", "../Imagenes/minus.png");
        });
        $(document).on("click", "[class^=imgMinusRow]", function () {
            $(this).attr("class", "imgPlusRow");
            $(this).attr("src", "../Imagenes/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>
    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            if (confirm("¿Está seguro de realizar esta acción?")) {
                confirm_value.value = "Si";
            }
            else {
                confirm_value.value = "No";
            }

            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="ContentPlaceHolderStyle">
    <style>
        .pnlresolv {
            max-width:-webkit-fill-available !important; 
            overflow-y:scroll;
            top:10px !important;
            height:-webkit-fill-available !important;
            left:30px !important;
            right:30px !important;
        }
    </style>
    <style type="text/css">
        .tableFont {font-size:14px !important;}
        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }
    </style>
</asp:Content>