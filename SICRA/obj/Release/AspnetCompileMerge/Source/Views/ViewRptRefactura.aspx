﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewRptRefactura.aspx.cs" Inherits="SICRA.Views.ViewRptRefactura" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <a href="#"><span>SICRA</span></a>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>   
    <a class="UrlActual" href='<%= ResolveUrl("ViewRptRefactura.aspx") %>'>Ver Crítica de Refacturación</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" />

    <div class="col-sm-12">
        <div class="panel panel-success">
            <div class="panel-heading">Filtros</div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="table-responsive col-sm-12 col-md-12 col-lg-12">
                        <table class="table table-striped table-bordered table-hover" id="tbFiltros" style="width: 100%; text-align: left">
                            <tr>
                                <td style="width: 10%">
                                    <span>Ciclo Muestras</span><br />
                                    <asp:DropDownList ID="ddlCiclo" ClientIDMode="Static" runat="server" CssClass="form-control" ToolTip="Seleccionar Ciclo de Facturación" AutoPostBack="true" OnSelectedIndexChanged="ddlCiclo_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                                <td style="width: 70%">
                                    <span>Dial de Refacturación</span><br />
                                    <asp:HiddenField runat="server" ID="hfDiales" ClientIDMode="Static" />
                                    <asp:DropDownList ID="ddlDial" runat="server" CssClass="form-control js-example-basic-multiple" ToolTip="Seleccionar Dial de Muestra"></asp:DropDownList>
                                    <asp:TextBox runat="server" ID="txtDiales" CssClass="form-control" Visible="false" ReadOnly="true" TextMode="MultiLine"></asp:TextBox>
                                </td>
                                <td style="width: 15%">
                                    <span>Típo de Reporte</span><br />
                                    <asp:RadioButtonList ID="rbRepo" runat="server">
                                        <asp:ListItem Text="Reporte General" Value="1" Selected="True" />
                                        <asp:ListItem Text="Reporte Detallado" Value="2" />
                                        <asp:ListItem Text="Reporte Control" Value="3" />
                                    </asp:RadioButtonList>
                                </td>
                                <td style="width: 5%">
                                    <asp:Button runat="server" ID="btnAceptar" CssClass="btn bg-primary" Text="Aplicar" OnClientClick="GetRepoData(); return false;"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="panel panel-info" id="pnlTables">
            <div class="panel-heading">Reportería Parametrizada</div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <%---------------------------------------------------------------------------------------------------%>
                    <div class="table-responsive" id="DivTbGeneral">
                        <table class="table table-striped table-bordered table-hover compact" style="width:100%" id="gvDatosGen">
                            <thead>
                                <tr>
                                    <th>Concepto</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <%---------------------------------------------------------------------------------------------------%>
                    <div class="table-responsive" id="DivTbDetalle">
                        <table class="table table-striped table-bordered table-hover compact" id="gvDatosDetalle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Analista</th>
                                    <th>Promedio % Muestra Evaluada</th>
                                    <th>Refactura Correcta</th>
                                    <th>Cantidad de Documentación Soporte Correcta</th>
                                    <th>Cantidad de Observación Cierre Correcta </th>
                                    <th>Cantidad de Proceso de Autorización Correcto </th>
                                    <th>Cantidad de Datos de Resolución Correctos </th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <td class="table-primary"><strong>Muestra Evaluada:</strong></td>
                                    <td class="table-primary"><label id="lblTotMuestra"></label></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <%---------------------------------------------------------------------------------------------------%>
                    <div class="table-responsive" id="DivTbControl">
                        <table class="table table-striped table-bordered table-hover compact" id="gvDatosControl" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Analista</th>
                                    <th>Total Asignados</th>
                                    <th>Cantidad No Resueltos</th>
                                    <th>Cantidad Resueltos</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <%---------------------------------------------------------------------------------------------------%>
                </div>
            </div>
        </div>
    </div>


    
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <link href="../Content/plugins/dataTables/datatables.min.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    <script src="../Scripts/plugins/select2/select2.full.min.js"></script>
    <link href="../Content/plugins/select2/select2.min.css" rel="stylesheet" />
    <script src="../Scripts/plugins/dataTables/datatables.min.js"></script>
    <script src="../Scripts/JsCriticaRefaDetalle.js"></script>
    <script type="text/javascript">
        $(function () {
            control = new RptCriRefaDetalle();
        });
    </script>

</asp:Content>