﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResolverClaveRepo.aspx.cs" Inherits="SICRA.Views.ResolverClaveRepo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <a href="#"><span>SICRA</span></a>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>   
    <a class="UrlActual" href='<%= ResolveUrl("ResolverClaveRepo.aspx") %>'>Resolver Clave Reportada</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>

    <%--formulario flotante de actualizacion--%>
    <cc1:ModalPopupExtender ID="mpResolver" ClientIDMode="Static" runat="server" Enabled="true"
        CancelControlID="btnCancel" OkControlID="btnCancel" PopupControlID="pnResolver"
        TargetControlID="hfIdM" BackgroundCssClass="ModalPopupBG">
    </cc1:ModalPopupExtender>


    <%--<asp:Panel ID="pnResolver" Style="display:none; overflow-x:scroll;overflow-y:scroll; max-width:800px; max-height:400px;" runat="server">--%>
    <%--<asp:Panel ID="pnResolver" runat="server">--%>
        <div ID="pnResolver" runat="server" class="panel panel-success" style="max-width:800px; overflow-y:scroll;top:10px !important;height:-webkit-fill-available !important;">
            <div class="panel-heading">Resolución # <span id="spNumResol" runat="server"></span> </div>
            <input id="hfIdM" type="hidden" name="hddclick" runat="server" />
            <div class="panel-body">
                <div class="form-horizontal">
                    <asp:HiddenField ID="hfCiclo" runat="server" />
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <span class="label label-success">Valores de Campo</span>
                    </div>
                    <%--**********************************************************************************************************--%>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <label for="txtNisrad">NIS:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtNisrad" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <label for="txtOs">OS Referencia: </label>
                        <asp:TextBox runat="server" class="form-control" ID="txtOs" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <label for="txtMulti">Tipo: </label>
                        <asp:TextBox runat="server" class="form-control" ID="txtTipoRepo" ReadOnly="true"></asp:TextBox>
                    </div>
                    <%--**********************************************************************************************************--%>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtMulti">Multiplicador:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtMulti" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtLecActCampo">Lectura Activa:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtLecActCampo" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtLecReaCampo">Lectura Reativa:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtLecReaCampo" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtLecDemCampo">Lectura Demanda:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtLecDemCampo" ReadOnly="true"></asp:TextBox>
                    </div>
                    <%--**********************************************************************************************************--%>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <input id="hfIdAnomalia" type="hidden" runat="server" />
                        <label for="txtAnomalia">Anomalía:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtAnomalia" ReadOnly="true" TextMode="MultiLine" Rows="1"></asp:TextBox>
                    </div>
                    <%--**********************************************************************************************************--%>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <label for="txtCausa">Solicitud Causa Verificacion:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtCausa" ReadOnly="true" TextMode="MultiLine" Rows="1"></asp:TextBox>
                    </div>
                    <%--**********************************************************************************************************--%>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <label for="txtRespOS">Comentario Creación OS:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtRespOS" ReadOnly="true" TextMode="MultiLine" Rows="2"></asp:TextBox>
                    </div>
                    <%--**********************************************************************************************************--%>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <label for="txtSolicitante">Solicitante:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtSolicitante" ReadOnly="true"></asp:TextBox>
                    </div>
                    <br />
                    <br />
                    <%--**********************************************************************************************************--%>
                    <div class="form-group col-sm-12  col-md-12 col-lg-12">
                        <span class="label label-success">Ingresar Nuevos Valores</span>
                    </div>
                    <div class="form-group col-sm-4  col-md-4 col-lg-4">
                        <label for="txtLecActiva">Lectura Activa:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtLecActiva" onkeypress="return ValidateNumeros(event)"></asp:TextBox>
                    </div>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <label for="txtLecReactiva">Lectura Reativa:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtLecReactiva" onkeypress="return ValidateNumeros(event)"></asp:TextBox>
                    </div>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <label for="txtLecDemanda">Lectura Demanda:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtLecDemanda" onkeypress="return validateDecimal(this.value,event)"></asp:TextBox>
                    </div>
                    <%--**********************************************************************************************************--%>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <label for="txtResolucion">Resolución:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtResolucion" TextMode="MultiLine" Rows="2"></asp:TextBox>
                    </div>
                    <%--**********************************************************************************************************--%>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <asp:Panel runat="server" ID="panelCargaImg" CssClass="panel panel-primary">
                            <div class="panel-heading">Carga de Imagenes y Archivos PDF</div>
                            <h5 style="color:red">SOLO ARCHIVOS DE IMAGEN y PDF SERÁN CARGADOS</h5>
                            <asp:FileUpload ID="fUpload" runat="server" CssClass="form-control" AllowMultiple="true" accept=".png,.jpg,.jpeg,.jfif,.pdf" />
                        </asp:Panel>
                    </div>
                    <%--**********************************************************************************************************--%>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    <asp:Button ID="btnAceptar" Text="Finalizar" runat="server" CssClass="btn btn-success active" OnClick="btnAceptar_Click" OnClientClick="Confirm()" />
                                </td>
                                <td>
                                    <asp:Button ID="btnEspera" Text="En Espera" runat="server" CssClass="btn btn-warning active" OnClick="btnEspera_Click" OnClientClick="Confirm()" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" Text="Cancelar" runat="server" CssClass="btn btn-danger active" />
                                </td>
                            </tr>
                        </table>
                    </div>  
                </div>
            </div>
        </div>
    <%--</asp:Panel>--%>
    <%--FIN formulario flotante de actualizacion--%>
    
    
    <div class="col-sm-12">
        <h2 class="titulo">Resolver Clave Reportada</h2>
        <div class="panel panel-primary">
            <div class="panel-body">
                <%--********************************formulario de filtros**************************--%>
                <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#demo">FILTROS</button>
                <div id="demo" class="collapse">
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <%--****************************************************************************--%>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group col-sm-5 col-md-5 col-lg-5">
                                            <label for="txtNisrad">NIS:</label>
                                            <asp:TextBox ID="txtClave" runat="server" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-5 col-md-5 col-lg-5">
                                            <label for="txtOsFiltro">OS Referencia:</label>
                                            <asp:TextBox ID="txtOsFiltro" runat="server" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="ddlEstado">¿Tiene OS?:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlTieneOS">
                                                <asp:ListItem Text="--Seleccionar--" Value=""></asp:ListItem>
                                                <asp:ListItem Text="SI" Value="SI"></asp:ListItem>
                                                <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlRegion">Región:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlSector">Sector:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlSector" OnSelectedIndexChanged="ddlSector_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlMedida">Medida:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlMedida"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlEstado">Estado:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlEstado">
                                                <asp:ListItem Text="--Seleccionar--" Value=""></asp:ListItem>
                                                <asp:ListItem Text="RESUELTO" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="NO RESUELTO" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ESPERA" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="ddlAnomalia">Anomalía:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlAnomalia"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlPerfilReso">Perfil de Resolución:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlPerfilReso">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="ddlDial">Dial:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlDial"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlVencido">Fecha Vencimiento :</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlVencido">
                                                <asp:ListItem Text="--SIN FILTRO--" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="FECHA VENCIDA" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="FECHA NO VENCIDA" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlfechasRegistro">Fechas de Registro:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlfechasRegistro" DataTextFormatString="{0:dd/MM/yyyy}">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlFechavencimiento">Fechas de vencimiento:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlFechavencimiento" DataTextFormatString="{0:dd/MM/yyyy}">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlUsuarioReporta">Usuario Reporta:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlUsuarioReporta">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlUsuarioResuelve">Usuario Resuelve:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlUsuarioResuelve">
                                            </asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlRegion" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlSector" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                    <asp:Button ID="btnFiltrar" runat="server" Text="Buscar" CssClass="btn btn-warning" OnClick="btnFiltrar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--********************************FIN formulario de filtros**************************--%>
                <br />
                <br />
                <br />

                <div class="form-group col-sm-2 col-md-2 col-lg-2">
                    <span style="background-color: lightcoral">No resueltas:</span>
                    <span id="spnNoResueltas" runat="server" style="font-weight: bold"></span>
                </div>
                <div class="form-group col-sm-2 col-md-2 col-lg-2">
                    <span style="background-color: lightgreen">Resueltas:</span>
                    <span id="spnResueltas" runat="server" style="font-weight: bold"></span>
                </div>
                <div class="form-group col-sm-2 col-md-2 col-lg-2">
                    <span style="background-color: lightyellow">Espera:</span>
                    <span id="spnEspera" runat="server" style="font-weight: bold"></span>
                </div>
                <div class="form-group col-sm-6 col-md-6 col-lg-6" style="text-align:right">
                    <%--<asp:Button id="btnExportar" runat="server" CssClass="btn btn-primary" Text="Exportar" OnClick="btnExportar_Click" />--%>
                </div>

                <div class="form-group col-sm-8 col-md-8 col-lg-8" style="text-align:right">
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                    <asp:HiddenField ID="hfError" runat="server" />
                </div>

                <asp:GridView ID="gvRepoM" runat="server" OnRowCommand="gvRepoM_RowCommand"
                    AllowSorting="False" AutoGenerateColumns="False" CssClass="mydatagrid" Width="100%"
                    DataKeyNames="IdRow_M" OnRowDataBound="gvRepoM_RowDataBound" Font-Size="Small"
                    HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="Yellow" />
                    <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                    <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <img alt="" class="imgPlusRow" style="cursor: pointer" src="../Imagenes/plus.png" />
                                <asp:Panel ID="pnlRegistros" runat="server" Style="display: none">
                                    <asp:GridView ID="gvRepoD" runat="server" CssClass="Grid" AutoGenerateColumns="false" Width="100%"
                                        OnRowDataBound="gvRepoD_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="IdRow_D" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdRow_D" ReadOnly="True" Visible="true" />
                                            <asp:BoundField DataField="UsuarioResuelve" HeaderText="Usuario Reportó / Resolvio" ReadOnly="True" />
                                            <asp:BoundField DataField="LecturaActiva" HeaderText="Lectura Activa" ReadOnly="True" />
                                            <asp:BoundField DataField="LecturaReactiva" HeaderText="Lectura Reactiva" ReadOnly="True" />
                                            <asp:BoundField DataField="LecturaDemanda" HeaderText="Lectura Demanda" ReadOnly="True" />
                                            <asp:BoundField DataField="Resolucion" HeaderText="Reporte / Resolución" ReadOnly="True" />
                                            <asp:BoundField DataField="FechaResolucion" HeaderText="Fecha Reporte / Resolución" ReadOnly="True" />
                                            <asp:BoundField DataField="esRespTec" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdRow_D" ReadOnly="True" Visible="true" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="IdRow_M" HeaderText="IdRow_M" />
                        <asp:BoundField DataField="Nis_Rad" HeaderText="NIS" />
                        <asp:BoundField DataField="Region" HeaderText="Región" />
                        <asp:BoundField DataField="Sector" HeaderText="Sector" />
                        <asp:BoundField DataField="Medida" HeaderText="Medida" />
                        <asp:BoundField DataField="LecturaActiva" HeaderText="L. Activa" />
                        <asp:BoundField DataField="LecturaReactiva" HeaderText="L. Reactiva" />
                        <asp:BoundField DataField="LecturaDemanda" HeaderText="L. Demanda" />
                        <asp:BoundField DataField="UsuarioFacturacion" HeaderText="Usuario Reporta" />
                        <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha Reporte" />
                        <asp:BoundField DataField="Anomalia" HeaderText="Anomalía" />
                        <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="Resuelto" HeaderText="Resuelto" />
                        <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="VisibleResolver" HeaderText="vencido" />
                        <asp:BoundField DataField="FechaMaxResol" HeaderText="Fecha Máxima" />
                        <asp:BoundField DataField="dial" HeaderText="Dial" />
                        <asp:BoundField DataField="TipoRepo" HeaderText="Tipo" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Acción">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkResolver" runat="server" CommandName="resolver" Visible='<%# Eval("VisibleResolver") %>'
                                    CommandArgument='<%# Eval("IdRow_M") %>' Text="Resolver" ForeColor="blue">
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    

</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <style type="text/css">
        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }
    </style>
</asp:Content>


<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    <script src="../Scripts/GridCells.js"></script>        
    <script type="text/javascript">
        $(document).on("click", "[class^=imgPlusRow]", function () {
            $(this).attr("class", "imgMinusRow");
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
            $(this).attr("src", "../Imagenes/minus.png");
        });
        $(document).on("click", "[class^=imgMinusRow]", function () {
            $(this).attr("class", "imgPlusRow");
            $(this).attr("src", "../Imagenes/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>

    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("¿Está seguro de realizar esta acción?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>