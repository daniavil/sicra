﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RepoSoporteRefacturacion.aspx.cs" Inherits="SICRA.Views.RepoSoporteRefacturacion" %>

<%@ Register Assembly="DevExpress.XtraReports.v21.1.Web.WebForms, Version=21.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<%@ Register Src="~/Modals/MsjModal.ascx" TagPrefix="uc1" TagName="MsjModal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <a href="#"><span>SICRA</span></a> <%--agregar al menu que pertenece--%>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
    <a class="UrlActual" href='<%= ResolveUrl("RepoSoporteRefacturacion.aspx") %>'>Reporte Soporte de Refacturación</a>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="form-horizontal" style="text-align: justify">
        <div class="form-group col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Impresión de Rectificación de Factura</div>
                <div class="panel-body">
                    <%--********************************formulario de filtros**************************--%>
                    <br />
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <div class="form-inline">
                                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                    <label for="txtNisrad">NIS:</label>
                                    <asp:TextBox ID="txtClave" runat="server" CssClass="form-control" Width="300px"></asp:TextBox>
                                    <asp:Button ID="btnFiltrar" runat="server" Text="Buscar" CssClass="btn btn-warning" OnClick="btnFiltrar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--********************************FIN formulario de filtros**************************--%>


                    <%--********soportes por ciclos********--%>
                    <div class="form-horizontal">
                        <div class="form-group-sm col-sm-12 col-md-12 col-lg-12" style="text-align: center">
                            <h2>Reporte Soporte de Refacturación</h2>
                        </div>
                        <div class="form-group-sm col-sm-12 col-md-12 col-lg-12" style="text-align:right">
                            <asp:CheckBox id="ckbAll" runat="server" Text="Seleccionar todos.." OnCheckedChanged="ckbAll_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
                        </div>
                        <div class="form-group-sm col-sm-12 col-md-12 col-lg-12" style="height:auto; max-height:40%;overflow-y:scroll;">
                            <asp:GridView ID="gvRectificaciones" runat="server" Width="100%" AutoGenerateColumns="false" AllowPaging="false"
                                OnPageIndexChanging="gvRectificaciones_PageIndexChanging" PagerStyle-CssClass="pager"
                                CssClass="mydatagrid" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="idSoporte" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="idSoporte" ReadOnly="True" Visible="true" />
                                    <asp:BoundField DataField="cicloFact" HeaderText="Ciclo Facturado" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    <asp:BoundField DataField="cicloRefact" HeaderText="Ciclo Refacturado" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    <asp:BoundField DataField="nomCliente" HeaderText="Cliente"></asp:BoundField>
                                    <asp:BoundField DataField="diferencia" HeaderText="Diferencia" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    <asp:BoundField DataField="valorDoc" HeaderText="Valor Documento" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    <asp:BoundField DataField="dbcr" HeaderText="DB/CR" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    <asp:BoundField DataField="observacion" HeaderText="Observación"></asp:BoundField>
                                    <asp:BoundField DataField="nomUsuario" HeaderText="Usuario"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Imprimir" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ckbImprimir" runat="server" value='<%# Eval("cicloFact") %>' OnCheckedChanged="ckbImprimir_CheckedChanged" AutoPostBack="true"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:HyperLinkField Text="Editar" ControlStyle-ForeColor="Black" DataNavigateUrlFields="idSoporte" DataNavigateUrlFormatString='<%# "~/SoporteRefacturacion.aspx?Id=" + eva  %>' HeaderText="Editar Observación" Target="_blank" />--%>

                                    <asp:TemplateField HeaderText="Editar observacion"> 
                                        <ItemTemplate> 
                                            <asp:HyperLink ControlStyle-ForeColor="Black" Text="Editar" runat="server" NavigateUrl='<%# "~/Views/SoporteRefacturacion.aspx?id=" + SICRA.Controllers.Fun.QueryStringEncode(Eval("idSoporte").ToString())  %>' Target="_blank" /> 
                                            <%--<asp:HyperLink ControlStyle-ForeColor="Black" Text="Editar" runat="server" NavigateUrl='<%# "~/Views/SoporteRefacturacion.aspx?id=" + Eval("idSoporte").ToString()  %>' Target="_blank" />--%> 
                                        </ItemTemplate> 
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="form-horizontal">
                            <div class="form-group-sm col-sm-4 col-md-4 col-lg-4" style="text-align: center">
                                <asp:Button ID="btnPrint" runat="server"  OnClick="btnPrint_Click" Text="Imprimir" CssClass="btn-primary"></asp:Button> 
                            </div>
                            <div class="form-group-sm col-sm-4 col-md-4 col-lg-4" style="text-align: center">
                                <asp:Button ID="btnExpoWord" runat="server"  OnClick="btnExpoWord_Click" Text="Exportar a WORD" CssClass="btn-success"></asp:Button> 
                            </div>
                            <div class="form-group-sm col-sm-4 col-md-4 col-lg-4" style="text-align: center">
                                <asp:Button ID="btnExportar" runat="server"  OnClick="btnExportar_Click" Text="Exportar a PDF" CssClass="btn-warning"></asp:Button> 
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("¿Está seguro de realizar esta acción?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>
