﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportaOperativa.aspx.cs" Inherits="SICRA.Views.ReportaOperativa" %>
<%@ Register Src="~/Modals/MsjModal.ascx" TagPrefix="uc1" TagName="MsjModal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <a href="#"><span>SICRA</span></a> <%--agregar al menu que pertenece--%>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
    <a class="UrlActual" href='<%= ResolveUrl("ReportaOperativa.aspx") %>'>Reportar - Operativa</a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updPanReportar">
        <ContentTemplate>
            <asp:UpdateProgress runat="server" ID="progress" DynamicLayout="true" AssociatedUpdatePanelID="updPanReportar">
                <ProgressTemplate>
                    <div style="text-align:center" class="loading">
                        <img src='<%= ResolveUrl("~/Imagenes/brickLoading.gif") %>'/>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Panel runat="server" ID="panelPrincipal">
                <div class="col-sm-12">
                    <h2 class="titulo">Reportar NIS - Operativa</h2>
                    <div class="form-group">
                        <asp:Panel runat="server" ID="panelContent">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Información NIS</div>
                                <div class="panel-body">
                                    <div class="form-horizontal col-sm-12 col-md-12 col-lg-12">

                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label style="background-color: lightpink;" for="txtClave">NIS:</label>
                                            <asp:TextBox ID="txtClave" runat="server" CssClass="form-control" Width="200px" MaxLength="7" TextMode="Number"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rftxtClave" runat="server" ErrorMessage="El campo NIS es obligatorio" ControlToValidate="txtClave" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label style="background-color: lightpink;" for="txtCicloRefa">Ciclo Refactura [AAMM]:</label>
                                            <asp:TextBox ID="txtCicloRefa" runat="server" CssClass="form-control" Width="200px" MinLength="4" MaxLength="4" TextMode="Number"></asp:TextBox>
                                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtCicloRefa" ID="txtCicloRefaValidacion" ValidationExpression="^[\s\S]{4,}$" runat="server" ForeColor="Red" ErrorMessage="Ciclo es de 4 dígitos."></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="rftxtCicloRefa" runat="server" ErrorMessage="El campo CICLO es obligatorio" ControlToValidate="txtCicloRefa" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <br />
                                            <asp:LinkButton ID="btnBuscar" runat="server" CssClass="btn btn-primary" OnClick="btnBuscar_Click">
                                                <span aria-hidden="true" class="glyphicon glyphicon-search"></span>
                                                <span>Buscar </span>
                                            </asp:LinkButton>
                                        </div>
                                    </div>

                                    <div class="form-horizontal col-sm-12 col-md-12 col-lg-12">

                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="txtOs">OS Referencia:</label>
                                            <asp:TextBox ID="txtOs" runat="server" CssClass="form-control" Width="200px" OnTextChanged="txtOs_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:Label ID="lblOs" runat="server" Style="font-size: small"></asp:Label>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="txtFechaRepo">Fecha Reporte:</label>
                                            <asp:TextBox ID="txtFechaRepo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-4 col-lg-3">
                                            <label for="txtFechaResol">Fecha Maxíma de Resolución:</label>
                                            <asp:TextBox ID="txtFechaResol" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblRepe" runat="server" Visible="False" Style="font-size: medium" CssClass="label label-danger"></asp:Label>
                                            <input id="hfIdM" type="hidden" value="0" runat="server" />
                                        </div>
                                    </div>
                                    <br />
                                    
                                    <div class="form-horizontal col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group col-sm-4">
                                            <label for="txtUbicacion">Ubicación:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtUbicacion" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="txtRegion">Region:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtRegion" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="txtSector">Sector:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtSector" ReadOnly="true"></asp:TextBox>
                                            <input id="hfIdSector" type="hidden" value="0" runat="server" />
                                        </div>
                                        <%------------------------------------------%>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="txtTarifa">Tarifa:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtTarifa" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="txtMulAnt">Multip. Anterior:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtMulAnt" TextMode="Number" ReadOnly="false"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="txtMulActual">Multip. Actual:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtMulActual" TextMode="Number" ReadOnly="false"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="txtMedAnt">Medidor Anterior:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtMedAnt" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="txtMedAct">Medidor Actual:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtMedAct" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <%------------------------------------------%>
                                        <div class="form-group col-sm-3">
                                            <label for="txtMedida">Medida:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtMedida" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="ddlAnomalia">Anomalía:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlAnomalia"></asp:DropDownList>
                                        </div> 
                                        <div class="input-group col-sm-3">
                                            <label for="txtFcambioMedidor">Fecha Cambio de Medidor:</label><br />
                                            <asp:TextBox ID="txtFcambioMedidor" runat="server" CssClass="form-control" Width="200px" TextMode="Date" AutoPostBack="true" />
                                        </div>

                                        <%------------------------------------------%>
                                        <div class="form-group col-sm-4">
                                            <label for="txtLecActiva">Lectura Activa:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtLecActiva" TextMode="Number" ReadOnly="false"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="txtLecActivaRet">Lectura Activa Retiro:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtLecActivaRet" TextMode="Number" ReadOnly="false"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="txtConsumoActivoFact">Consumo Activa Facturar:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtConsumoActivoFact" TextMode="Number" ReadOnly="false"></asp:TextBox>
                                        </div>
                                        <%------------------------------------------%>
                                        <div class="form-group col-sm-4">
                                            <label for="txtLecReactiva">Lectura Reactiva:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtLecReactiva" TextMode="Number" ReadOnly="false"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="txtLecActiva">Lectura Reactiva Retiro:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtLecReactivaRet" TextMode="Number" ReadOnly="false"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="txtLecActiva">Consumo Reactiva Facturar:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtConsumoReactivoFact" TextMode="Number" ReadOnly="false"></asp:TextBox>
                                        </div>
                                        <%------------------------------------------%>
                                        <div class="form-group col-sm-4">
                                            <label for="txtLecDemanda">Lectura Demanda:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtLecDemanda" TextMode="Number" ReadOnly="false"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="txtConsumoDemandaFact">Consumo Demanda Facturar:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtConsumoDemandaFact" TextMode="Number" ReadOnly="false"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="ddlDial">Dial Lectura:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlDial" OnSelectedIndexChanged="ddlDial_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <%------------------------------------------%>
                                        <div class="form-group col-sm-12">
                                            <label for="txtCausa">Solicitud O Causa de Verificación:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtCausa" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:Panel runat="server" ID="panelCargaImg" CssClass="panel panel-primary">
                                                <asp:HiddenField runat="server" ID="hfRutaArchivo"></asp:HiddenField>
                                                <div class="panel-heading">Carga de Imagen/PDF/Excel</div>
                                                <asp:FileUpload ID="fUpload" runat="server" CssClass="form-control" AllowMultiple="true" accept=".png,.jpg,.jpeg,.jfif,.pdf,.xlsx,.xls" />
                                                <div>
                                                    <asp:Label ID="lblIdGestion" runat="server" ForeColor="Blue" Font-Size="Smaller" />
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="form-horizontal col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:RequiredFieldValidator Display="Dynamic" runat="server" id="rdvtxtClave" controltovalidate="txtClave" ForeColor="Red" errormessage="Campo NIS Obligatorio.!" />
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:RequiredFieldValidator Display="Dynamic" runat="server" id="rdvtxtFcambioMedidor" controltovalidate="txtFcambioMedidor" ForeColor="Red" errormessage="Campo FECHA CAMBIO DE MEDIDOR Obligatorio.!" />
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:RequiredFieldValidator Display="Dynamic" runat="server" id="rdvtxtLecActiva" controltovalidate="txtLecActiva" ForeColor="Red" errormessage="Campo LECTURA ACTIVA Obligatorio.!" />
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:RequiredFieldValidator Display="Dynamic" runat="server" id="rdvtxtLecActivaRet" controltovalidate="txtLecActivaRet" ForeColor="Red" errormessage="Campo LECTURA ACTIVA RETIRO Obligatorio.!" />
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:RequiredFieldValidator Display="Dynamic" runat="server" id="rdvtxtConsumoActivoFact" controltovalidate="txtConsumoActivoFact" ForeColor="Red" errormessage="Campo CONSUMO ACTIVA FACTURAR Obligatorio.!" />
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:RequiredFieldValidator Display="Dynamic" runat="server" id="rdvddlDial" controltovalidate="ddlDial" ForeColor="Red" errormessage="Campo DIAL LECTURA Obligatorio.!" />
                                        </div>                                        
                                    </div>
                                        <div class="form-group col-sm-2">
                                            <asp:Button ID="btnReportar" runat="server" Text="Reportar NIS" CssClass="btn btn-info" OnClick="btnReportar_Click" OnClientClick="Confirm()" />
                                        </div>
                                        <div class="form-group col-sm-10">
                                            <asp:TextBox ID="lblMsj" runat="server" ReadOnly="true" Visible="False" Style="font-size: medium" Width="100%" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </asp:Panel>
            <uc1:MsjModal runat="server" ID="MsjModal"></uc1:MsjModal>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnReportar" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">

    <style>

        .file{
            margin-left:25%;
            margin-top:4%;
            margin-bottom:4%;
        }

        .titulo{
            margin-left: 35%;
            margin-bottom: 4%;
        }
        .total{
            margin-left:60%;
            margin-bottom: 1.5%;
            margin-top: 1.5%;
        }

        .btnCargar{
            margin-left: 65%;
            margin-top: 2%;
            margin-bottom: 2%;
        }

        .loading{
            position:fixed;
            top:0px;
            right:0px;
            width:100%;
            height:100%;
            background-color:#ffffff;
            background-repeat:no-repeat;
            background-position:center;
            z-index:10000000;
            opacity: 0.80;
            filter: alpha(opacity=0); 
        }

    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    <script>
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("¿Está seguro de realizar esta acción?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>