﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConsultaCriticaLectura.aspx.cs" Inherits="SICRA.Views.ConsultaCriticaLectura" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <a href="#"><span>SICRA</span></a>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>   
    <a class="UrlActual" href='<%= ResolveUrl("CriticaLectura.aspx") %>'>Crítica de Lecturas</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>

    <%--formulario flotante de actualizacion--%>
    <cc1:ModalPopupExtender ID="mpResolver" ClientIDMode="Static" runat="server" Enabled="true"
        CancelControlID="btnCancel" OkControlID="btnCancel" PopupControlID="pnResolver"
        TargetControlID="hfIdM" BackgroundCssClass="ModalPopupBG">
    </cc1:ModalPopupExtender>


    <%--<asp:Panel ID="pnResolver" Style="display:none; overflow-x:scroll;overflow-y:scroll; max-width:800px; max-height:400px;" runat="server">--%>
    <%--<asp:Panel ID="pnResolver" runat="server">--%>
    <div id="pnResolver" runat="server" class="panel panel-success" style="max-width:1200px; overflow-y:scroll;top:10px !important;height:-webkit-fill-available !important;">
        <div class="panel-heading">Cítica de Lectura</div>
        <input id="hfIdM" type="hidden" name="hddclick" runat="server" />
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Datos Generales</div>
                        <div class="panel-body">
                            <input id="hfEsTelemedido" type="hidden" name="hfEsTelemedido" runat="server" />
                            <div class="form-horizontal col-sm-2 col-md-2 col-lg-2">
                                <label for="lblNisrad">NIS:</label>
                                <asp:Label ID="lblNisrad" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-3 col-md-3 col-lg-3">
                                <label for="lblMedidor">Medidor:</label>
                                <asp:Label ID="lblMedidor" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-3 col-md-3 col-lg-3">
                                <label for="lblUbicacion">Ubicación:</label>
                                <asp:Label ID="lblUbicacion" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblTarifa">Tarifa:</label>
                                <asp:Label ID="lblTarifa" runat="server"></asp:Label>
                            </div>
                            <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                <label for="lblCliente">Cliente:</label>
                                <asp:Label ID="lblCliente" runat="server"></asp:Label>
                            </div>
                            <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                <label for="lblDireccion">Dirección:</label>
                                <asp:Label ID="lblDireccion" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblNisrad">Lectura Encontrada:</label>
                                <asp:Label ID="lblLecturaEncontrada" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblNisrad">Fecha Lectura:</label>
                                <asp:Label ID="lblFLectura" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblNisrad">Fecha Programada:</label>
                                <asp:Label ID="lblFProgramada" runat="server"></asp:Label>
                            </div>
                            <%--*******************************************************************--%>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblCodAnomalia">Cod. Anomalía:</label>
                                <asp:Label ID="lblCodAnomalia" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-8 col-md-8 col-lg-8">
                                <label for="lblNisrad">Anomalía:</label>
                                <asp:Label ID="lblAnomalia" runat="server"></asp:Label>
                            </div>
                            <%--*******************************************************************--%>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblDial">Dial:</label>
                                <asp:Label ID="lblDial" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-8 col-md-8 col-lg-8">
                                <label for="lblParam">Cambio Parametrización:</label>
                                <asp:Label ID="lblParam" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-12 col-md-12 col-lg-12">
                                <label for="lblParam">Observación Inspector:</label>
                                <asp:Label ID="lblObsInspector" runat="server"></asp:Label>
                            </div>
                            <asp:HiddenField ID="hfObj" runat="server" Value="" />
                        </div>
                    </div>
                </div>

                <div class="form-inline col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading">Info</div>
                        <div class="panel-body" style="text-align: left">

                            <div class="form-inline col-sm-6 col-md-6 col-lg-6">
                                <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Fotos</button>
                                <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo1">Historico Anomalia (12 Meses)</button>

                            </div>

                            <div class="form-inline col-sm-12 col-md-12 col-lg-12" style="text-align: left">
                                <div id="demo" class="collapse">
                                    <div class="panel-body" id="sidebar">
                                        <%--GALERIA--%>

                                        <div id="FotoContainer">
                                            <asp:Literal ID="literalControl" runat="server" />
                                        </div>
                                        <%--@*FIN GALERIA*@--%>
                                    </div>
                                </div>
                            </div>

                            <div class="form-inline col-sm-12 col-md-12 col-lg-12" style="text-align: left">
                                <div id="demo1" class="collapse">
                                    <div class="panel-body" id="sidebar1">
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:GridView ID="gvHistoAnomalia" runat="server" AllowPaging="False" AutoGenerateColumns="False" CssClass="mydatagrid"
                                                DataKeyNames="CLAVE" Font-Size="Small" Width="100%" PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                                <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                <EditRowStyle BackColor="#ffffcc" />
                                                <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                                <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                                <Columns>
                                                    <asp:BoundField DataField="CLAVE" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="CLAVE" ReadOnly="True" Visible="true" />
                                                    <asp:BoundField DataField="CICLO" HeaderText="Ciclo" ReadOnly="True" />
                                                    <asp:BoundField DataField="LECTURAACTIVA" HeaderText="Lectura" ReadOnly="True"/>
                                                    <asp:BoundField DataField="ANOMALIA" HeaderText="Anomalía" ReadOnly="True"/>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="form-inline col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Crítica</div>
                        <div class="panel-body" style="text-align:center">
                            <div class="form-inline col-sm-3">
                                <label for="ddlLectura">Lectura Correcta:</label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddlLectura" Width="100px" Enabled="false">
                                    <asp:listitem text="SI" value="1"></asp:listitem>
                                    <asp:listitem text="NO" value="0"></asp:listitem>
                                    <asp:listitem text="No se puede determinar" value="2"></asp:listitem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-inline col-sm-3">
                                <label for="ddlFoto">Foto Correcta:</label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddlFoto" Width="100px" Enabled="false">
                                    <asp:listitem text="SI" value="True"></asp:listitem>
                                    <asp:listitem text="NO" value="False"></asp:listitem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-inline col-sm-3">
                                <label for="ddlAnomalia">Anomalía Correcta:</label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddlAnomalia" Width="100px" Enabled="false">
                                    <asp:listitem text="SI" value="True"></asp:listitem>
                                    <asp:listitem text="NO" value="False"></asp:listitem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-inline col-sm-3">
                                <label for="ddlRendimiento">Rendimiento:</label>
                                <asp:Label ID="lblRendimiento" runat="server"></asp:Label>
                            </div>
                            <%--********************************************************************--%>
                            <div class="form-inline col-sm-4">
                                <asp:Panel ID="pnlLectDebioSer" runat="server">
                                    <label for="txtLecturaDebioSer">Lectura debió ser:</label>
                                    <asp:Label ID="lblLecturaDebioSer" runat="server"></asp:Label>
                                </asp:Panel>
                            </div>
                            <div class="form-inline col-sm-4">
                                <asp:Panel ID="pnlDdlFotoNo" runat="server">
                                    <label for="ddlFoto">Observación Foto:</label>
                                    <asp:Label ID="lblObsFoto" runat="server"></asp:Label>
                                </asp:Panel>
                            </div>
                            <div class="form-inline col-sm-4">
                                <asp:Panel ID="pnlDdlAnomaliaNo" runat="server">
                                    <label for="ddlAnomaliaNo">Anomalía debió Ser:</label>
                                    <asp:Label ID="lblAnomaliaNo" runat="server"></asp:Label>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                    <asp:Button ID="btnCancel" Text="Cerrar" runat="server" CssClass="btn btn-success active" OnClick="btnCancel_Click"/>
                </div>
            </div>
        </div>
    </div>
    <%--</asp:Panel>--%>
    <%--FIN formulario flotante de actualizacion--%>
    
    
    <div class="col-sm-12">
        <h2 class="titulo">Control de calidad de lecturas</h2>
        <div class="panel panel-primary">
            <div class="panel-body">
                <%--********************************formulario de filtros**************************--%>
                <div class="panel panel-success">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                <label for="ddlCiclo">Ciclo:</label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddlCiclo" OnSelectedIndexChanged="ddlCiclo_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                <label for="ddldial">Fecha Lectura Muestra:</label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddldial" OnSelectedIndexChanged="ddldial_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                <label for="ddlFecha">Típo Filtro:</label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddlTipoFiltro"></asp:DropDownList>
                            </div>
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <asp:Button ID="btnFiltrar" runat="server" Text="Buscar" CssClass="btn btn-warning" OnClick="btnFiltrar_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--********************************FIN formulario de filtros**************************--%>
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <asp:Label ID="lblMsjSave" runat="server" Style="font-size:large" CssClass="label label-success"></asp:Label>
                    </div>
                <br />
                <br />
                <div class="form-group col-sm-2 col-md-2 col-lg-2">
                    <span style="background-color: lightgreen">Resueltas:</span>
                    <span id="spnResueltas" runat="server" style="font-weight: bold"></span>
                </div>
                <asp:Panel ID="pnlAsignado" runat="server">
                    <asp:GridView ID="gvRepoM" runat="server" OnRowCommand="gvRepoM_RowCommand"
                    AllowSorting="False" AutoGenerateColumns="False" CssClass="mydatagrid" Width="100%"
                    DataKeyNames="IdRow" OnRowDataBound="gvRepoM_RowDataBound" Font-Size="Small"
                    HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="Yellow" />
                    <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                    <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="idRow" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdRow" ReadOnly="True" Visible="true" />
                        <asp:BoundField DataField="CLAVE" HeaderText="CLAVE" />
                        <asp:BoundField DataField="CODIGODIAL" HeaderText="DIAL" />
                        <asp:BoundField DataField="TipoFiltro" HeaderText="Típo Muestra" />
                        <asp:BoundField DataField="SUBCLASE" HeaderText="Código Anomalía" />
                        <asp:BoundField DataField="DESCRIPCION" HeaderText="Nombre Anomalía" />
                        <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="Resuelto" HeaderText="Resuelto" />
                        <asp:BoundField DataField="usuario" HeaderText="Usuario Generó" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Acción">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkResolver" runat="server" CommandName="resolver" 
                                    CommandArgument='<%# Eval("idRow") %>' Text='<%# (Boolean.Parse(Eval("Resuelto").ToString())) ? "VER" : "RESOLVER" %>' ForeColor="blue">
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <link rel="stylesheet" href="../GalleryRoot/css/viewer.css" />
    <style>
        .pictures {
            margin: 0;
            padding: 0;
            list-style: none;
            width:100%
        }

            .pictures > li {
                float: left;
                width: 50px;
                height: 50px;
                margin: 0 -1px -1px;
                border: 1px solid transparent;
                overflow: hidden;
            }

                .pictures > li > img {
                    width: 50px;
                    height: 50px;
                    cursor: -webkit-zoom-in;
                    cursor: zoom-in;
                }

        .viewer-download {
            color: #fff;
            font-family: FontAwesome;
            font-size: .75rem;
            line-height: 1.5rem;
            text-align: center;
        }

            .viewer-download::before {
                content: "\f019";
            }

        #FotoContainer {
            margin-top: 10px !important;
            display: inline-block;
        }
    </style>
    <style type="text/css">
        .tableFont {font-size:14px !important;}
        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
<script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            if (confirm("¿Está seguro de realizar esta acción?")) {
                    confirm_value.value = "Si";
            }
            else {
                    confirm_value.value = "No";
            }

            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>