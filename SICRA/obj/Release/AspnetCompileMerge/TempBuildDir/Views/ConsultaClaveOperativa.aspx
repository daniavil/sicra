﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConsultaClaveOperativa.aspx.cs" Inherits="SICRA.Views.ConsultaClaveOperativa" %>
<%@ Register Src="~/Modals/MsjModal.ascx" TagPrefix="uc1" TagName="MsjModal" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <a href="#"><span>SICRA</span></a> <%--agregar al menu que pertenece--%>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
    <a class="UrlActual" href='<%= ResolveUrl("ConsultaClavesReportadas.aspx") %>'>Consulta Claves Reportadas</a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <asp:ScriptManager runat="server"></asp:ScriptManager>
        <%--formulario flotante de actualizacion--%>
            <cc1:ModalPopupExtender ID="mpver" ClientIDMode="Static" runat="server" Enabled="true"
                CancelControlID="btnCancel" OkControlID="btnCancel" PopupControlID="pnver"
                TargetControlID="hfIdM" BackgroundCssClass="ModalPopupBG">
            </cc1:ModalPopupExtender>

            <div ID="pnver" runat="server" class="panel panel-info" style="width:80%; overflow-y:scroll;top:10px !important;height:-webkit-fill-available !important;">
                <div class="panel-heading">NIS: <span id="spNisrad" runat="server"></span></div>
                <div class="panel-body">
                    <input id="hfIdM" type="hidden" name="hfIdM" runat="server" />
                    <div class="form-inline">                        
                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                            <div id="imgDiv">
                                <h2 style="width:100%"><span class="label label-primary">Imágenes</span></h2>
                                <div class="panel-body" id="sidebar">
                                    <%--GALERIA--%>
                                    <div id="FotoContainer" class="col-sm-12 col-md-12 col-lg-12">
                                        <div id="galley" runat="server" ClientIDMode="static">
                                            <asp:Literal ID="literalControl" runat="server" />
                                        </div>
                                        <asp:Label ID="lblmsjErrorImg" runat="server" Visible="False" Style="font-size: medium" CssClass="label label-danger"></asp:Label>
                                    </div>
                                    <script src="../GalleryRoot/js/viewer.js"></script>
                                    <script>
                                        window.addEventListener('DOMContentLoaded', function () {
                                            var galley = document.getElementById('galley');
                                            var viewer = new Viewer(galley, {
                                                toolbar: {
                                                    zoomIn: 4,
                                                    zoomOut: 4,
                                                    oneToOne: 4,
                                                    reset: 4,
                                                    prev: 4,
                                                    play: {
                                                        show: 0,
                                                        size: 'large',
                                                    },
                                                    next: 4,
                                                    rotateLeft: 4,
                                                    rotateRight: 4,
                                                    flipHorizontal: 4,
                                                    flipVertical: 4,
                                                },
                                            });
                                        });
                                    </script>
                                    <%--@*FIN GALERIA*@--%>
                                </div>
                            </div>
                        </div>
                        <div class="form-group-sm col-sm-12 col-md-12 col-lg-12" style="height: auto; max-height: 40%; overflow-y: scroll;">
                            <asp:GridView ID="gvbitacora" runat="server" Width="100%" AutoGenerateColumns="false" AllowPaging="false" PagerStyle-CssClass="pager"
                                CssClass="mydatagrid" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="nombre" HeaderText="Nombre Archivo" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    <asp:BoundField DataField="fechaCarga" HeaderText="Fecha/Hora"></asp:BoundField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>Acción</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton id="hlDownload" runat="server" CommandArgument='<%# Eval("ruta") %>' OnCommand="hlDownload_Command" CommandName='<%# Eval("nombre") %>' Text="DESCARGAR" style="color: black !important;" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <asp:Label ID="lblerror" runat="server" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                            <asp:Button ID="btnCancel" Text="Cerrar" runat="server" CssClass="btn btn-warning active" />
                        </div>
                    </div>
                </div>
            </div>
        <%--</asp:Panel>--%>
        <%--FIN formulario flotante de actualizacion--%>


    <div class="col-sm-12">
        <h2 class="titulo">Detalle de Claves Reportadas</h2>
        <div class="form-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Claves Reportadas</div>
                <div class="panel-body">
                    <%--********************************formulario de filtros**************************--%>
                <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#demo">FILTROS</button>
                <div id="demo" class="collapse">
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <%--****************************************************************************--%>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="txtNisrad">NIS:</label>
                                            <asp:TextBox ID="txtClave" runat="server" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="txtOsFiltro">OS Referencia:</label>
                                            <asp:TextBox ID="txtOsFiltro" runat="server" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlEstado">¿Tiene OS?:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlTieneOS">
                                                <asp:ListItem Text="--Seleccionar--" Value=""></asp:ListItem>
                                                <asp:ListItem Text="SI" Value="SI"></asp:ListItem>
                                                <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlfechasRegistro">Ciclo Refactura:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlCiclo"></asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlRegion">Región:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlSector">Sector:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlSector" OnSelectedIndexChanged="ddlSector_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlMedida">Medida:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlMedida"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlEstado">Estado:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlEstado">
                                                <asp:ListItem Text="--Seleccionar--" Value=""></asp:ListItem>
                                                <asp:ListItem Text="RESUELTO" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="NO RESUELTO" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="ddlAnomalia">Anomalía:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlAnomalia"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlPerfilReso">Perfil de Reporte:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlPerfilReso">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="ddlDial">Dial:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlDial"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlVencido">Fecha Vencimiento :</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlVencido">
                                                <asp:ListItem Text="--SIN FILTRO--" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="FECHA VENCIDA" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="FECHA NO VENCIDA" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlfechasRegistro">Fechas de Registro:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlfechasRegistro" DataTextFormatString="{0:dd/MM/yyyy}">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlFechavencimiento">Fechas de vencimiento:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlFechavencimiento" DataTextFormatString="{0:dd/MM/yyyy}">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlUsuarioReporta">Usuario Reporta:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlUsuarioReporta">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlUsuarioResuelve">Usuario Resuelve:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlUsuarioResuelve">
                                            </asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlCicloRepo">Ciclo de Reporte:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlCicloRepo">
                                            </asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlRegion" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlSector" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                    <asp:Button ID="btnFiltrar" runat="server" Text="Buscar" CssClass="btn btn-warning" OnClick="btnFiltrar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--********************************FIN formulario de filtros**************************--%>
                    <br />
                    <br />
                    <br />
                    <div class="form-group col-sm-2 col-md-2 col-lg-2">
                        <span style="background-color: lightcoral">No resueltas:</span>
                        <span id="spnNoResueltas" runat="server" style="font-weight: bold"></span>
                    </div>
                    <div class="form-group col-sm-2 col-md-2 col-lg-2">
                        <span style="background-color: lightgreen">Resueltas:</span>
                        <span id="spnResueltas" runat="server" style="font-weight: bold"></span>
                    </div>
                    <div class="form-group-sm col-sm-6 col-md-6 col-lg-6" style="text-align: right">
                        <asp:HiddenField ID="hfClaves" runat="server" />
                        <asp:Button ID="btnExpoExcel" runat="server" OnClick="btnExpoExcel_Click" Text="Exportar Excel" CssClass="btn-success" ></asp:Button>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 ">
                        <div class="table-responsive">
                            <asp:GridView ID="gvRepoM" runat="server" AllowPaging="True" OnPageIndexChanging="gvRepoM_PageIndexChanging"
                                AutoGenerateColumns="False" PageSize="15" CssClass="mydatagrid" DataKeyNames="IdRow_M"
                                OnRowDataBound="gvRepoM_RowDataBound" Font-Size="Small" Width="100%" OnRowCommand="gvRepoM_RowCommand"
                                PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                <HeaderStyle Font-Bold="True" ForeColor="White" />
                                <EditRowStyle BackColor="#ffffcc" />
                                <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="#">
                                        <ItemTemplate>
                                            <img alt="" class="imgPlusRow" style="cursor: pointer" src="../Imagenes/plus.png" />
                                            <asp:Panel ID="pnlRegistros" runat="server" Style="display: none">
                                                <asp:GridView ID="gvRepoD" runat="server" CssClass="Grid" AutoGenerateColumns="false" Width="100%">
                                                    <Columns>
                                                        <asp:BoundField DataField="IdRow_D" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdRow_D" ReadOnly="True" Visible="true" />
                                                        <asp:BoundField DataField="UsuarioResuelve" HeaderText="Usuario Reporta/Resolve" ReadOnly="True" />
                                                        <asp:BoundField DataField="Estado" HeaderText="Estado" ReadOnly="True" />
                                                        <asp:BoundField DataField="Resolucion" HeaderText="Resolución" ReadOnly="True" />
                                                        <asp:BoundField DataField="FechaResolucion" HeaderText="Fecha Resolución" ReadOnly="True" DataFormatString = "{0:dd/MM/yyyy}"/>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="IdRow_M" HeaderText="IdRow_M" />
                                    <asp:BoundField DataField="Nis_Rad" HeaderText="NIS" />
                                    <asp:BoundField DataField="Region" HeaderText="Región" />
                                    <asp:BoundField DataField="Sector" HeaderText="Sector" />
                                    <asp:BoundField DataField="Medida" HeaderText="Medida" />
                                    <asp:BoundField DataField="LecturaActiva" HeaderText="L. Activa" />
                                    <asp:BoundField DataField="LecturaReactiva" HeaderText="L. Reactiva" />
                                    <asp:BoundField DataField="LecturaDemanda" HeaderText="L. Demanda" />
                                    <asp:BoundField DataField="FechaCambioMedidor" HeaderText="Fecha Cambio Medidor" DataFormatString = "{0:dd/MM/yyyy}"/>
                                    <asp:BoundField DataField="UsuarioFacturacion" HeaderText="Reporta" />
                                    <asp:BoundField DataField="PerfilAsignado" HeaderText="Perfil" />
                                    <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha Reporte" DataFormatString = "{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="Anomalia" HeaderText="Anomalía" />
                                    <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="Resuelto" HeaderText="ResueltoBool" />
                                    <asp:BoundField DataField="Ciclo" HeaderText="Ciclo" />
                                    <asp:BoundField DataField="dial" HeaderText="Dial" />
                                    <asp:BoundField DataField="OS" HeaderText="OS" />
                                    <asp:TemplateField HeaderText="Resuelto" SortExpression="Active">
                                        <ItemTemplate><%# (int.Parse(Eval("Resuelto").ToString())==1) ? "SI" : "NO" %></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Archivos">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkver" runat="server" CommandArgument='<%# Eval("IdRow_M").ToString()+","+ Eval("Nis_Rad").ToString() %>' ForeColor="blue">
                                                <asp:Label ID="lblTf" runat="server" Text='<%# Bind("Tf") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <link rel="stylesheet" href="../GalleryRoot/css/viewer.css" />
    <style>
        .file{
            margin-left:25%;
            margin-top:4%;
            margin-bottom:4%;
        }
        .titulo{
            margin-left: 35%;
            margin-bottom: 4%;
        }
        .total{
            margin-left:60%;
            margin-bottom: 1.5%;
            margin-top: 1.5%;
        }
        .btnCargar{
            margin-left: 65%;
            margin-top: 2%;
            margin-bottom: 2%;
        }
        .loading{
            position:fixed;
            top:0px;
            right:0px;
            width:100%;
            height:100%;
            background-color:#ffffff;
            background-repeat:no-repeat;
            background-position:center;
            z-index:10000000;
            opacity: 0.80;
            filter: alpha(opacity=0); 
        }
        .tableFont {font-size:14px !important;}
        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }
        .HellowWorldPopup
        {
            min-width:200px;
            min-height:150px;
            background:white;
        }
        .Controls {
            padding-top:10px;
            padding-bottom:10px;
        }
        .link
        {
         color: #428bca !important; 
         text-decoration: underline;
        }

        .pictures {
            margin: 0;
            padding: 0;
            list-style: none;
            width:100%
        }
        .pictures > li {
                float: left;
                width: 50px;
                height: 50px;
                margin: 0 -1px -1px;
                border: 1px solid transparent;
                overflow: hidden;
            }
        .pictures > li > img {
                    width: 50px;
                    height: 50px;
                    cursor: -webkit-zoom-in;
                    cursor: zoom-in;
                }
        .viewer-download {
            color: #fff;
            font-family: FontAwesome;
            font-size: .75rem;
            line-height: 1.5rem;
            text-align: center;
        }
        .viewer-download::before {
                content: "\f019";
            }
        #FotoContainer {
            margin-top: 10px !important;
            display: inline-block;
        }
        .tableFont {font-size:14px !important;}
        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }

    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    <script src="../Scripts/GridCells.js"></script>
    <script type="text/javascript">
        $(document).on("click", "[class^=imgPlusRow]", function () {
            $(this).attr("class", "imgMinusRow");
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
            $(this).attr("src", "../Imagenes/minus.png");
        });
        $(document).on("click", "[class^=imgMinusRow]", function () {
            $(this).attr("class", "imgPlusRow");
            $(this).attr("src", "../Imagenes/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>
</asp:Content>