﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SICRA._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="form-group col-sm-12 col-md-12 col-lg-12" style="text-align:center">
        <div class="form-group col-sm-6 col-md-6 col-lg-6" style="text-align: right">
            <h1 style="font-size:xx-large">Sistema Informático de Consultas, Reportes y Anomalías.</h1>
        </div>
    </div>


    <div class="form-group col-sm-12 col-md-12 col-lg-12" style="text-align:center">
        <div class="form-group col-sm-6 col-md-6 col-lg-6" style="text-align: right">
            <h1 style="font-size:xx-large"><span id="spnMsj" runat="server" class="label label-danger"></span></h1>
        </div>
    </div>

    

    <div class="form-group col-sm-12 col-md-12 col-lg-12">
        <div style="margin-left: 31%">
            <asp:Image AlternateText="EEH" Height="277" ID="IMG1" ImageUrl="~/Imagenes/eehlogo.png" runat="server" Width="403" />
        </div>
        <br />
        <div class="row" style="margin-top: 10%">
            <div class="col-sm-6">
                <div id="chart"></div>
            </div>
            <div class="col-sm-6"></div>
        </div>
    </div>
   
    
</asp:Content>
