﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MsjModal.ascx.cs" Inherits="SICRA.Modals.MsjModal" %>


<script type="text/javascript">
    $("#MsjModal").on('hidden', function () {
        $('body').off('click');
    });
</script>

<div class="modal" id="MsjModal" data-keyboard="false" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="MsjModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <asp:UpdatePanel runat="server" ID="upModal" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal-content">
                    <asp:Panel runat="server" ClientIDMode="Static" ID="hPanel">
                        <div class="modal-header" runat="server" clientidmode="static">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">
                                <asp:Label runat="server" ID="lblTitle"></asp:Label>
                            </h4>
                        </div>
                    </asp:Panel>
                    <div class="modal-body">
                        <asp:Label runat="server" ID="lblMensaje"></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <button style="background-color:transparent;border:none" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </ContentTemplate>
            
        </asp:UpdatePanel>
    </div>
</div>