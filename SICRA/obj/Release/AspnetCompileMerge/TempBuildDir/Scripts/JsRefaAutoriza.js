﻿function renderLocal() {


    var ciclos = $("#hfCiclos").val();

    var ddl = $(".js-example-basic-multiple").select2({
        placeholder: "Seleccionar Ciclo",
        allowClear: true,
        multiple: "multiple",
        closeOnSelect: false
    });

    ddl.val("");
    ddl.trigger("change");
    ddl.on('change', function (e) {
        $("#hfCiclos").val(ddl.val());
    });
}


function Confirm() {
    var confirm_value = document.createElement("INPUT");
    confirm_value.type = "hidden";
    confirm_value.name = "confirm_value";
    if (confirm("¿Está seguro de realizar esta acción?")) {
        confirm_value.value = "Si";
    } else {
        confirm_value.value = "No";
    }
    document.forms[0].appendChild(confirm_value);
}

function SoloDecimal(txt, event) {
    var charCode = (event.which) ? event.which : event.keyCode
    if (charCode == 46) {
        if (txt.value.indexOf(".") < 0)
            return true;
        else
            return false;
    }

    if (txt.value.indexOf(".") > 0) {
        var txtlen = txt.value.length;
        var dotpos = txt.value.indexOf(".");
        //Change the number here to allow more decimal points than 2
        if ((txtlen - dotpos) > 2)
            return false;
    }

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}


function CalcularTotalKwh() {
    try {
        var txtKwhCv = Number($("#txtKwhCv").val());
        var txtKwhRefa = Number($("#txtKwhRefa").val());
        var total = txtKwhCv + txtKwhRefa;
        $("#txtKwhTotal").val(total);
    } catch (e) {
        $("#txtKwhTotal").val('0');
    }
}

function CalcularTotalLps() {
    try {
        var txtLpsCv = Number($("#txtLpsCv").val());
        var txtLpsRefa = Number($("#txtLpsRefa").val());
        var total = txtLpsCv + txtLpsRefa;
        $("#txtLpsTotal").val(total);
    } catch (e) {
        $("#txtLpsTotal").val('0');
    }
}