﻿function HideLabel(labelId, e, txtId) {
  //  alert('labelId:'+labelId+' txtId:'+txtId);
    document.getElementById(labelId).style.display = "none";
    ShowTextBox(txtId, e, labelId);
}

//show label
function ShowLabel(labelId, e, txtId) {
    document.getElementById(labelId).style.display = "";
}

//show textbox
function ShowTextBox(txtId, e, labelId) {
    var txt = $("#" + txtId);  //document.getElementById(txtId);
    var lbl = $("#" + labelId); //document.getElementById(labelId);
   // alert('txt:'+txt+' lbl:'+lbl);
    txt.css('display', '');
   // HideLabel(labelId, e, txtId);
   // txt.focus();
    
}

//save textbox data by pressing Enter
//or cancel on Escape
function SaveDataOnEnter(txtId, e, labelId, btnId) {
    if (window.event) {
        e = window.event;
    }
   
    var txt = $("#" + txtId);
    var charCode = (e.which) ? e.which : event.keyCode;

    switch (e.keyCode) {
        case 13:	//Enter - save
            document.getElementById(btnId).click();
            break;
        case 27:	//Escape - hide
            document.getElementById(txtId).style.display = "none";
            ShowLabel(labelId, e, txtId);
            break;
    }

   /* //validar que solo se ingresen numeros, punto y comas

    if (event.keyCode === 46 || event.keyCode === 44) {
        return true;
    } else if ((event.keyCode < 48 || event.keyCode > 57)) {
        return event.returnValue = false;
    } else {
        return true;
    }*/    
    return validateDecimal(txt.val(), e);
}

function validateDecimal(valor, event) {
   // alert('value:'+valor+' event:'+event);
    var value = String.fromCharCode(event.keyCode);
    var RE = /^\d*\.?\d{0,2}$/;
    if (RE.test(valor + value)) {
        return true;
    } else {
        return false;
    }
}

//save textbox data by lost focus
function SaveDataOnLostFocus(txtId, btnId, lblId) {
    document.getElementById(txtId).style.display = "none";
    document.getElementById(btnId).click(); //alert("txtId:"+txtId+" btnId:"+btnId+" lblId:"+lblId);
    if ($(btnId).val() === "" || $(btnId).val() === null) {
        $(lblId).val = $(txtId).val;
        //$(txtId).val = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp";
    }
    return false;
}

function ValidateNumeros(e) {
    
    if (e.keyCode >= 48 && e.keyCode <= 57)
        return true;
    else
        return false;
}