﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdmonUser.aspx.cs" Inherits="SICRA.Views.AdmonUser" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <a href="#"><span>SICRA</span></a>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>   
    <a class="UrlActual" href='<%= ResolveUrl("AdmonUser.aspx") %>'>Admon de Usuarios</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>
    <asp:UpdatePanel runat="server" ID="updPanReportar">
        <ContentTemplate>
            <asp:UpdateProgress runat="server" ID="progress" DynamicLayout="true" AssociatedUpdatePanelID="updPanReportar">
                <ProgressTemplate>
                    <div style="text-align: center" class="loading">
                        <img src='<%= ResolveUrl("~/Imagenes/brickLoading.gif") %>' />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Panel runat="server" ID="panelPrincipal">
                <div class="col-sm-12">
                    <h2 class="titulo">Admon De Usuarios</h2>
                    <div class="form-group">
                        <asp:Panel runat="server" ID="panelContent">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Información de Usuario</div>
                                <div class="panel-body">
                                    <div class="form-horizontal">
                                        
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <input id="hfIdUsr" type="hidden" value="0" runat="server" />
                                            <label for="txtUsuario">Usuario SOEEH:</label>
                                            <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" Width="200px" onkeypress="EnterEvent(event);"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-9 col-md-9 col-lg-9">
                                            <label for="txtPersona">Nombre Persona:</label>
                                            <asp:TextBox ID="txtPersona" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="txtCorreo">Correo EEH:</label>
                                            <asp:TextBox ID="txtCorreo" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="ddlperfil">Perfil Autorizaciones:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlperfil"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="ddlPerfilReportes">Perfil Reportes (*No Obligatorio):</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlPerfilReportes"></asp:DropDownList>
                                        </div>

                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="chkAutoriza">Autoriza Rectificaciones:</label>
                                            <asp:CheckBox ID="chkAutoriza" runat="server" class="form-control"/>
                                        </div>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="chkAdmin">Es Administrador:</label>
                                            <asp:CheckBox ID="chkAdmin" runat="server" class="form-control"/>
                                        </div>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="chkActivo">Estado Activo:</label>
                                            <asp:CheckBox ID="chkActivo" runat="server" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-6 col-lg-6" runat="server">
                                        <label for="cklSectores">Asignar Sectores:</label>
                                        <div style="overflow-y: scroll; width: 98%; height: auto; border: dashed 1px;">
                                            <asp:CheckBoxList ID="cklSectores" runat="server"></asp:CheckBoxList>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-6 col-lg-6" runat="server">
                                        <label for="cklSectores">Asignar Mercados:</label>
                                        <div style="overflow-y: scroll; width: 98%; height: auto; border: dashed 1px;">
                                            <asp:CheckBoxList ID="cklMercados" runat="server"></asp:CheckBoxList>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="form-group col-sm-12">
                                        <asp:Button ID="btnGuardar" Visible="false" runat="server" Text="Guardar Nuevo Usuario" CssClass="btn btn-success" OnClick="btnGuardar_Click" OnClientClick="Confirm()" />
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <asp:Button ID="BtnActualizar" Visible="false" runat="server" Text="Actualizar Usuario" CssClass="btn btn-warning" OnClick="BtnActualizar_Click" OnClientClick="Confirm()" />
                                    </div>
                                    <br />
                                    <div class="form-group col-sm-12">
                                        <asp:Label ID="lblMsj" runat="server" Visible="False" Style="font-size: large" CssClass="label label-primary"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="btnReportar" />--%>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolderJS">
    <script>
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("¿Está seguro de realizar esta acción?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }

        function EnterEvent(e) {
            if (e.keyCode == 13) {
                __doPostBack('<%=txtUsuario.ClientID%>', "");
            }
        }
    </script>
</asp:Content>