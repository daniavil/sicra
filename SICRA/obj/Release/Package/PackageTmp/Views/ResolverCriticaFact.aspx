﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResolverCriticaFact.aspx.cs" Inherits="SICRA.Views.ResolverCriticaFact" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <a href="#"><span>SICRA</span></a>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>   
    <a class="UrlActual" href='<%= ResolveUrl("ResolverCriticaFact.aspx") %>'>Crítica de Facturación</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>

    <%--formulario flotante de actualizacion--%>
    <cc1:ModalPopupExtender ID="mpResolver" ClientIDMode="Static" runat="server" Enabled="true"
        CancelControlID="btnCancel" OkControlID="btnCancel" PopupControlID="pnResolver"
        TargetControlID="hfIdM" BackgroundCssClass="ModalPopupBG">
    </cc1:ModalPopupExtender>


    <%--<asp:Panel ID="pnResolver" Style="display:none; overflow-x:scroll;overflow-y:scroll; max-width:800px; max-height:400px;" runat="server">--%>
    <%--<asp:Panel ID="pnResolver" runat="server">--%>
    <div id="pnResolver" runat="server" class="panel panel-success" style="max-width:1200px; overflow-y:scroll;top:10px !important;height:-webkit-fill-available !important;">
        <div class="panel-heading">Crítica</div>
        <input id="hfIdM" type="hidden" name="hddclick" runat="server" />
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Datos Generales</div>
                        <div class="panel-body">
                            <div class="form-horizontal col-sm-2 col-md-2 col-lg-2">
                                <label for="lblNisrad">NIS:</label>
                                <asp:Label ID="lblNisrad" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-3 col-md-3 col-lg-3">
                                <label for="lblMedidor">Medidor:</label>
                                <asp:Label ID="lblMedidor" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-3 col-md-3 col-lg-3">
                                <label for="lblUbicacion">Ubicación:</label>
                                <asp:Label ID="lblUbicacion" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblTarifa">Tarifa:</label>
                                <asp:Label ID="lblTarifa" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-6 col-md-6 col-lg-6">
                                <label for="lblCliente">Cliente:</label>
                                <asp:Label ID="lblCliente" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-6 col-md-6 col-lg-6">
                                <label for="lblDireccion">Dirección:</label>
                                <asp:Label ID="lblDireccion" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblNisrad">Lectura Encontrada:</label>
                                <asp:Label ID="lblLecturaEncontrada" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblNisrad">Fecha Lectura:</label>
                                <asp:Label ID="lblFLectura" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblNisrad">Fecha Programada:</label>
                                <asp:Label ID="lblFProgramada" runat="server"></asp:Label>
                            </div>
                            <%--Lectura--%>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblNisrad">Cod. Anomalía Lect:</label>
                                <asp:Label ID="lblCodAnomaliaLect" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-5 col-md-5 col-lg-5">
                                <label for="lblNisrad">Anomalía Lectura:</label>
                                <asp:Label ID="lblAnomaliaLect" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-3 col-md-3 col-lg-3">
                                <label for="lblNisrad">Dial:</label>
                                <asp:Label ID="lblDial" runat="server"></asp:Label>
                            </div>
                            <%--critica facturacion--%>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblCodAnoCri">Cod. Anomalía Crítica:</label>
                                <asp:Label ID="lblCodAnoCri" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-5 col-md-5 col-lg-5">
                                <label for="lblAnoCritica">Anomalía Crítica:</label>
                                <asp:Label ID="lblAnoCritica" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-3 col-md-3 col-lg-3">
                                <label for="lblRepSicra">Reportada en SICRA:</label>
                                <asp:Label ID="lblRepSicra" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblCsmoCri">CSMO Crítica :</label>
                                <asp:Label ID="lblCsmoCri" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblCsmoFact">CSMO Facturación:</label>
                                <asp:Label ID="lblCsmoFact" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblAccion">Acción :</label>
                                <asp:Label ID="lblAccion" runat="server"></asp:Label>
                            </div>
                            <%--otros--%>
                            <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                <label for="lblTipoLect">Tipo Lectura :</label>
                                <asp:Label ID="lblTipoLect" runat="server"></asp:Label>
                            </div>
                            <div class="form-horizontal col-sm-8 col-md-8 col-lg-8">
                                <label for="lblObsCri">Observacion Crítica:</label>
                                <asp:Label ID="lblObsCri" runat="server"></asp:Label>
                            </div>
                            <asp:Panel runat="server" ID="pnlUsrCri" class="form-horizontal col-sm-8 col-md-8 col-lg-8" Visible="false">
                                <table style="width:100%">
                                    <tr>
                                        <td><label for="lblUsrCri">Usuario Crítica INCMS:</label></td>
                                        <td><asp:Label ID="lblUsrCri" runat="server"></asp:Label></td>
                                        <td><asp:Label ID="lblNomUsr" runat="server"></asp:Label></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Label ID="lblMsjError" runat="server" Visible="False" Style="font-size: medium" CssClass="label label-danger"></asp:Label>
                            <asp:HiddenField ID="hfObj" runat="server" Value="" />
                        </div>
                    </div>
                </div>

                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Fotos</button>
                    <div id="demo" class="collapse">
                        <div class="panel-body" id="sidebar">
                            <%--GALERIA--%>

                            <div id="FotoContainer">
                                <asp:Literal ID="literalControl" runat="server" />
                            </div>

                            <script src="../GalleryRoot/js/viewer.js"></script>
                            <script>
                                window.addEventListener('DOMContentLoaded', function () {
                                    var galley = document.getElementById('galley');
                                    var viewer = new Viewer(galley, {
                                        toolbar: {
                                            zoomIn: 4,
                                            zoomOut: 4,
                                            oneToOne: 4,
                                            reset: 4,
                                            prev: 4,
                                            play: {
                                                show: 0,
                                                size: 'large',
                                            },
                                            next: 4,
                                            rotateLeft: 4,
                                            rotateRight: 4,
                                            flipHorizontal: 4,
                                            flipVertical: 4,
                                        },
                                    });
                                });
                            </script>

                            <%--@*FIN GALERIA*@--%>
                        </div>
                    </div>
                </div>
               
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" Visible="true">
                    <ContentTemplate>
                        <%--Formulario de critica--%>
                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Crítica</div>
                                <div class="panel-body" style="text-align: center">
                                    <div class="form-inline col-sm-12" style="background-color: lightgray; text-align: left; margin: 10px,0,10px,0">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <label for="ddlP1accionCorrecta">Accion Correcta:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlP1accionCorrecta" AutoPostBack="true" OnSelectedIndexChanged="ddlP1accionCorrecta_SelectedIndexChanged">
                                                        <asp:ListItem Text="SI" Value="SI"></asp:ListItem>
                                                        <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Panel runat="server" ID="pnlP1" Visible="false" class="form-inline" Style="text-align: left">
                                                        <label for="ddlP1DebioDe">Debió de:</label>
                                                        <asp:DropDownList runat="server" class="form-control" ID="ddlP1DebioDe" Width="100px">
                                                            <asp:ListItem Text="MODIFICAR" Value="MODIFICAR"></asp:ListItem>
                                                            <asp:ListItem Text="CONFIRMAR" Value="CONFIRMAR"></asp:ListItem>
                                                            <asp:ListItem Text="PROMEDIAR" Value="PROMERDIAR"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="form-inline col-sm-12" style="background-color: aliceblue; text-align: left; margin: 10px,0,10px,0">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <label for="ddlP2ObservacionCorrecta">Observación Correcta:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlP2ObservacionCorrecta" AutoPostBack="true" OnSelectedIndexChanged="ddlP2ObservacionCorrecta_SelectedIndexChanged">
                                                        <asp:ListItem Text="SI" Value="SI"></asp:ListItem>
                                                        <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Panel runat="server" ID="pnlP2" Visible="false" class="form-inline" Style="text-align: left">
                                                        <label for="ddlP2DescipcionObs">Observación Predefinida:</label>
                                                        <asp:DropDownList runat="server" class="form-control" ID="ddlP2DescipcionObs" Width="100px">
                                                        </asp:DropDownList>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="form-inline col-sm-12" style="background-color: lightgray; text-align: left; margin: 10px,0,10px,0">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <label for="ddlP3EnSicra">¿Reportada en SICRA? :</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlP3EnSicra" AutoPostBack="true" OnSelectedIndexChanged="ddlP3EnSicra_SelectedIndexChanged">
                                                        <asp:ListItem Text="SI" Value="SI"></asp:ListItem>
                                                        <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--<asp:RequiredFieldValidator id="RFVddlP3EnSicra" ControlToValidate="ddlP3EnSicra" InitialValue="" Text="Campo Requerido" ForeColor="Red" runat="server"/>--%>
                                                </td>
                                                <td>
                                                    <asp:Panel runat="server" ID="pnlp31" Visible="false" class="form-inline" Style="text-align: left">
                                                        <label for="ddlP3HechoCorrecto">¿Ejecutada correctamente? :</label>
                                                        <asp:DropDownList runat="server" class="form-control" ID="ddlP3HechoCorrecto">
                                                            <asp:ListItem Text="SI" Value="SI"></asp:ListItem>
                                                            <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </asp:Panel>
                                                    <asp:Panel runat="server" ID="pnlp32" class="form-inline" Style="text-align: left">
                                                        <label for="chblP3Hacer">Debió de:</label>
                                                        <asp:CheckBoxList ID="chblP3Hacer" runat="server"></asp:CheckBoxList>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="form-inline col-sm-12" style="background-color: aliceblue; text-align: left; margin: 10px,0,10px,0">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width:45%">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <label for="ddlP4DebioRepoSicra">¿Debió reportarse en SICRA? :</label>
                                                                <asp:DropDownList runat="server" class="form-control" ID="ddlP4DebioRepoSicra" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlP4DebioRepoSicra_SelectedIndexChanged">
                                                                    <asp:ListItem Text="NO" Value="NO" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Text="SI" Value="SI"></asp:ListItem>
                                                                    <asp:ListItem Text="SI (Seguimiento Refactura)" Value="SISeg"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="width:55%">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Panel runat="server" ID="PnSeguimientoFact" Visible="false" class="form-inline" Style="text-align: left; width: 100%">
                                                                    <table style="width: 100%">
                                                                        <tr>
                                                                            <td>
                                                                                <label asp-for="lblNis" class="label-control">Meses</label>
                                                                                <div class="controls">
                                                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlMesRefaCri">
                                                                                        <asp:ListItem Value="1"></asp:ListItem>
                                                                                        <asp:ListItem Value="2"></asp:ListItem>
                                                                                        <asp:ListItem Value="3"></asp:ListItem>
                                                                                        <asp:ListItem Value="4"></asp:ListItem>
                                                                                        <asp:ListItem Value="5"></asp:ListItem>
                                                                                        <asp:ListItem Value="6"></asp:ListItem>
                                                                                        <asp:ListItem Value="7"></asp:ListItem>
                                                                                        <asp:ListItem Value="8"></asp:ListItem>
                                                                                        <asp:ListItem Value="9"></asp:ListItem>
                                                                                        <asp:ListItem Value="10"></asp:ListItem>
                                                                                        <asp:ListItem Value="11"></asp:ListItem>
                                                                                        <asp:ListItem Value="12"></asp:ListItem>
                                                                                        <asp:ListItem Value="13"></asp:ListItem>
                                                                                        <asp:ListItem Value="14"></asp:ListItem>
                                                                                        <asp:ListItem Value="15"></asp:ListItem>
                                                                                        <asp:ListItem Value="16"></asp:ListItem>
                                                                                        <asp:ListItem Value="17"></asp:ListItem>
                                                                                        <asp:ListItem Value="18"></asp:ListItem>
                                                                                        <asp:ListItem Value="19"></asp:ListItem>
                                                                                        <asp:ListItem Value="20"></asp:ListItem>
                                                                                        <asp:ListItem Value="21"></asp:ListItem>
                                                                                        <asp:ListItem Value="22"></asp:ListItem>
                                                                                        <asp:ListItem Value="23"></asp:ListItem>
                                                                                        <asp:ListItem Value="24"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <label asp-for="lblFLec" class="label-control">Tipo Cambio</label>
                                                                                <div class="controls">
                                                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlTipoCambio">
                                                                                        <asp:ListItem Text="Refactura" Value="1"></asp:ListItem>
                                                                                        <asp:ListItem Text="Distribución" Value="2"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <label asp-for="lblFLec" class="label-control">Comentario</label>
                                                                                <div class="controls">
                                                                                    <asp:TextBox runat="server" ID="txtComentSeg" TextMode="MultiLine" Rows="3" Width="100%"></asp:TextBox>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--Fin Formulario de critica--%>
                    </ContentTemplate>
                    <Triggers>
                        <%--<asp:AsyncPostBackTrigger ControlID="ddlFecha" EventName="SelectedIndexChanged" />--%>
                        <%--<asp:AsyncPostBackTrigger ControlID="ddlP1accionCorrecta" EventName="SelectedIndexChanged" />--%>
                    </Triggers>
                </asp:UpdatePanel>
                
                
                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                    <asp:Button ID="btnAceptar" Text="Aceptar" runat="server" CssClass="btn btn-primary active" OnClick="btnAceptar_Click" OnClientClick="Confirm();" />
                </div>
                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                    <asp:Button ID="btnCancel" Text="Cancelar" runat="server" CssClass="btn btn-warning active" OnClick="btnCancel_Click"/>
                </div>
            </div>
        </div>
    </div>
    <%--</asp:Panel>--%>
    <%--FIN formulario flotante de actualizacion--%>
    
    
    <div class="col-sm-12">
        <h2 class="titulo">Control de calidad de lecturas</h2>
        <div class="panel panel-primary">
            <div class="panel-body">
                <%--********************************formulario de filtros**************************--%>
                <div class="panel panel-success">
                    <div class="panel-heading">Filtros</div>
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                <asp:HiddenField ID="hfFilCiclo" runat="server" />
                                <label for="ddlCiclo">Ciclo:</label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddlCiclo" AutoPostBack="true" OnSelectedIndexChanged="ddlCiclo_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                            <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                <asp:HiddenField ID="hfFilDial" runat="server" />
                                <label for="ddldial">Fecha Lectura:</label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddldial"></asp:DropDownList>
                            </div>
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <asp:Button ID="btnFiltrar" runat="server" Text="Buscar" CssClass="btn btn-warning" OnClick="btnFiltrar_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--********************************FIN formulario de filtros**************************--%>
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <asp:Label ID="lblMsjSave" runat="server" Style="font-size:large" CssClass="label label-success"></asp:Label>
                    </div>
                <br />
                <br />
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <span style="background-color: lightcoral">Conteo de Registros:</span>
                    <span id="spnNoResueltas" runat="server" style="font-weight: bold"></span>
                </div>
                <asp:Panel ID="pnlAsignado" runat="server">
                    <asp:GridView ID="gvRepoM" runat="server" OnRowCommand="gvRepoM_RowCommand"
                    AllowSorting="False" AutoGenerateColumns="False" CssClass="mydatagrid" Width="100%"
                    DataKeyNames="IdRow" OnRowDataBound="gvRepoM_RowDataBound" Font-Size="Small"
                    HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="Yellow" />
                    <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                    <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="idRow" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdRow" ReadOnly="True" Visible="true" />
                        <asp:BoundField DataField="nisrad" HeaderText="CLAVE" />
                        <asp:BoundField DataField="dial" HeaderText="DIAL" />
                        <asp:TemplateField HeaderText="Tipo Muestra" SortExpression="Active">
                            <ItemTemplate><%# (Boolean.Parse(Eval("EsTotal").ToString())) ? "TOTAL" : "SESGADA" %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CodAnomaliaCritica" HeaderText="Código Anomalía" />
                        <asp:BoundField DataField="AnomaliaCritica" HeaderText="Nombre Anomalía" />
                        <asp:BoundField DataField="usuarioGenera" HeaderText="Usuario Generó" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Acción">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkResolver" runat="server" CommandName="resolver" 
                                    CommandArgument='<%# Eval("idRow") %>' Text="RESOLVER" ForeColor="blue">
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <style>
        .pictures {
            margin: 0;
            padding: 0;
            list-style: none;
            width:100%
        }

            .pictures > li {
                float: left;
                width: 50px;
                height: 50px;
                margin: 0 -1px -1px;
                border: 1px solid transparent;
                overflow: hidden;
            }

                .pictures > li > img {
                    width: 50px;
                    height: 50px;
                    cursor: -webkit-zoom-in;
                    cursor: zoom-in;
                }

        .viewer-download {
            color: #fff;
            font-family: FontAwesome;
            font-size: .75rem;
            line-height: 1.5rem;
            text-align: center;
        }

            .viewer-download::before {
                content: "\f019";
            }

        #FotoContainer {
            margin-top: 10px !important;
            display: inline-block;
        }
    </style>
    <style type="text/css">
        .tableFont {font-size:14px !important;}
        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            if (confirm("¿Está seguro de realizar esta acción?")) {
                    confirm_value.value = "Si";
            }
            else {
                    confirm_value.value = "No";
            }

            document.forms[0].appendChild(confirm_value);
        }
    </script>
    <link rel="stylesheet" href="../GalleryRoot/css/viewer.css" />
</asp:Content>