﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SolicitudGestionRefactura.aspx.cs" Inherits="SICRA.Views.SolicitudGestionRefactura" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <a href="#"><span>SICRA</span></a> <%--agregar al menu que pertenece--%>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
    <a class="UrlActual" href='<%= ResolveUrl("SolicitudGestionRefactura.aspx") %>'>Creación de Solicitud de Rectificación</a>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <style type="text/css">
        .bootRBL input {
            display: inline;
            margin-right: 0.25em;
        }

        .bootRBL label {
            display: inline;
            margin-right: 3em;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    
    <script src="../Scripts/plugins/select2/select2.full.min.js"></script>
    <link href="../Content/plugins/select2/select2.min.css" rel="stylesheet" />
    <script src="../Scripts/JsRefaAutoriza.js"></script>
    
    <script type="text/javascript">        
        $(function () {
            control = new renderLocal();
        });
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h1><u>Datos Generales</u></h1>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <%--**************************************************************************************************************************************************************************************************************************************************************************--%>
                            <div class="col-sm-4 b-r">
                                <h3 class="m-t-none m-b">Origen de Gestión</h3>
                                <asp:DropDownList ID="ddlOrigenGestion" runat="server" class="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvOrigen" ControlToValidate="ddlOrigenGestion" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato no correcto!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                            </div>
                            <div class="col-sm-4 b-r">
                                <h3 class="m-t-none m-b">Id Gestión</h3>
                                <asp:TextBox ID="txtIdGestion" runat="server" class="form-control" OnTextChanged="txtIdGestion_TextChanged" AutoPostBack="true"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvGestion" ControlToValidate="txtIdGestion" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Id Gestión No existente.!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                            </div>
                            <div class="col-sm-4 b-r">
                                <h3 class="m-t-none m-b">NIS</h3>
                                <asp:TextBox ID="txtNis" runat="server" class="form-control" OnTextChanged="txtNis_TextChanged" AutoPostBack="true"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvNis" ControlToValidate="txtNis" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato no correcto!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                            </div>
                            <%--**************************************************************************************************************************************************************************************************************************************************************************--%>
                            <div class="col-sm-12">
                                <h3 class="m-t-none m-b">Ciclos de Rectificación (####,####,####...)</h3>
                                <asp:HiddenField runat="server" ID="hfCiclos" ClientIDMode="Static" />
                                <asp:DropDownList ID="ddlCiclos" runat="server" CssClass="form-control js-example-basic-multiple" ToolTip="Seleccionar Ciclo de refacturación"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvCiclos" ControlToValidate="ddlCiclos" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato de ciclos no validos" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                            </div>
                            <%--**************************************************************************************************************************************************************************************************************************************************************************--%>
                            <div class="col-sm-12"><br /></div>
                            <%--**************************************************************************************************************************************************************************************************************************************************************************--%>
                            <div class="col-sm-4 b-r">
                                <h3 class="m-t-none m-b">Tipo de Rectificación</h3>
                                <asp:DropDownList ID="ddlTipoRect" runat="server" class="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddlTipoRect" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato no correcto!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                            </div>
                            <div class="col-sm-4 b-r">
                                <h3 class="m-t-none m-b">Sector Cliente</h3>
                                <asp:TextBox ID="txtSector" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="hfIdSector" ClientIDMode="Static" />
                            </div>
                            <div class="col-sm-4 b-r">
                                <h3 class="m-t-none m-b">Mercado Cliente</h3>
                                <asp:TextBox ID="txtMercado" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="hfIdMercado" ClientIDMode="Static" />
                            </div>
                            <%--**************************************************************************************************************************************************************************************************************************************************************************--%>
                            <div class="col-sm-12"><br /></div>
                            <div class="col-sm-12"><br /></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox panel-primary">
                    <div class="ibox-title">
                        <h1><u>Datos de Rectificación</u></h1>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <%--**************************************************************************************************************************************************************************************************************************************************************************--%>

                            <div class="col-sm-6 b-r table-bordered ibox-title">
                                <h6><b>VALORES POR RECTIFICACIÓN POR CARGO VARIO</b></h6>
                                <div class="col-sm-6 b-r">
                                    <h3 class="m-t-none m-b">Valor kWh</h3>
                                    <asp:TextBox ID="txtKwhCv" runat="server" class="form-control" ClientIDMode="Static" onkeypress="return SoloDecimal(this,event);" onchange="javascript: CalcularTotalKwh();" Text="0" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtKwhCv" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato no correcto!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                                </div>
                                <div class="col-sm-6 b-r">
                                    <h3 class="m-t-none m-b">Valor LPS.</h3>
                                    <asp:TextBox ID="txtLpsCv" runat="server" class="form-control" ClientIDMode="Static" onkeypress="return SoloDecimal(this,event);" onchange="javascript: CalcularTotalLps();" Text="0" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtLpsCv" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato no correcto!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                                </div>
                            </div>
                            <div class="col-sm-6 b-r table-bordered ibox-title">
                                <h6><b>VALORES POR RECTIFICACIÓN POR REFACTURA</b></h6>
                                <div class="col-sm-6 b-r">
                                    <h3 class="m-t-none m-b">Valor kWh</h3>
                                    <asp:TextBox ID="txtKwhRefa" runat="server" class="form-control" ClientIDMode="Static" onkeypress="return SoloDecimal(this,event);" onchange="javascript: CalcularTotalKwh();" Text="0" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtKwhRefa" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato no correcto!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                                </div>
                                <div class="col-sm-6 b-r">
                                    <h3 class="m-t-none m-b">Valor LPS.</h3>
                                    <asp:TextBox ID="txtLpsRefa" runat="server" class="form-control" ClientIDMode="Static" onkeypress="return SoloDecimal(this,event);" onchange="javascript: CalcularTotalLps();" Text="0" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtLpsRefa" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato no correcto!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                                </div>
                            </div>
                            <div class="col-sm-12 b-r table-bordered ibox-title">
                                <h6><b>TOTAL RECTIFICACIÓN</b></h6>
                                <div class="col-sm-4 b-r">
                                    <h3 class="m-t-none m-b">Total kWh</h3>
                                    <asp:TextBox ID="txtKwhTotal" ClientIDMode="Static" runat="server" class="form-control" ReadOnly="true" onkeypress="return SoloDecimal(this,event);" Text="0" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtKwhTotal" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato no correcto!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                                </div>
                                <div class="col-sm-4 b-r">
                                    <h3 class="m-t-none m-b">Total LPS.</h3>
                                    <asp:TextBox ID="txtLpsTotal" runat="server" ClientIDMode="Static" class="form-control" ReadOnly="true" onkeypress="return SoloDecimal(this,event);" Text="0" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="txtLpsTotal" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato no correcto!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                                </div>
                                <div class="col-sm-4 b-r">
                                    <h3 class="m-t-none m-b">DB/CR</h3>
                                    <asp:RadioButtonList ID="rbDbcr" runat="server" RepeatDirection="Horizontal" CssClass="form-inline bootRBL">
                                        <asp:ListItem Text="Débito" Value="db" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Crédito" Value="cr"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="rbDbcr" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato no correcto!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                                </div>
                            </div>
                            <%--**************************************************************************************************************************************************************************************************************************************************************************--%>
                            <div class="col-sm-12 b-r">
                                <h3 class="m-t-none m-b">Razón / Comentario Refactura:</h3>
                                <asp:TextBox runat="server" class="form-control" ID="txtRazonRefa" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtRazonRefa" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato no correcto!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                            </div>
                            <%--**************************************************************************************************************************************************************************************************************************************************************************--%>
                            <div class="col-sm-12 b-r">
                                <h3 class="m-t-none m-b">Archivo de Soporte de refactura</h3>
                                <asp:Panel runat="server" ID="panelCargaImg" CssClass="panel panel-primary">
                                    <h5 style="color: red">ADJUNTAR ARCHIVOS</h5>
                                    <asp:FileUpload ID="fUpload" runat="server" CssClass="form-control" AllowMultiple="true" />
                                </asp:Panel>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="fUpload" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Dato no correcto!" ForeColor="Red" runat="server" ValidationGroup="DatosRequeridos" />
                            </div>
                            <%--**************************************************************************************************************************************************************************************************************************************************************************--%>
                            <div class="col-sm-12 b-r">
                                <asp:Button ID="btnCrear" runat="server" Text="Crear Gestión" CssClass="btn btn-info" OnClick="btnCrear_Click" OnClientClick="Confirm()" ValidationGroup="DatosRequeridos" />
                            </div>
                            <div class="col-sm-12 b-r">
                                <asp:Label ID="lblErrorUsr" runat="server" CssClass="label label-danger" Font-Size="X-Large"></asp:Label>
                            </div>
                            <%--**************************************************************************************************************************************************************************************************************************************************************************--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12 b-r">
                                <asp:Label ID="lblError" runat="server" Text="" Font-Size="Medium" ></asp:Label>
                            </div>
                            <div class="col-sm-12"><br /></div>
                            <div class="col-sm-12"><br /></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
