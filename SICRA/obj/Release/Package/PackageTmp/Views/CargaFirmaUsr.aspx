﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CargaFirmaUsr.aspx.cs" Inherits="SICRA.Views.CargaFirmaUsr" %>

<%@ Register Assembly="DevExpress.XtraReports.v21.1.Web.WebForms, Version=21.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<%@ Register Src="~/Modals/MsjModal.ascx" TagPrefix="uc1" TagName="MsjModal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <a href="#"><span>SICRA</span></a> <%--agregar al menu que pertenece--%>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
    <a class="UrlActual" href='<%= ResolveUrl("CargaFirmaUsr.aspx") %>'>Carga de Firma Personal</a>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="form-horizontal" style="text-align: justify">
        <div class="form-group col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Carga de  soporte de Rectificación</div>
                <div class="panel-body">
                    <%--********************************formulario de filtros**************************--%>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <h1>Usuario: <span id="lblUser" runat="server"></span></h1>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <asp:Panel runat="server" ID="panelCargaImg" CssClass="panel panel-primary">
                            <div class="panel-heading">Carga de Imagenes</div>
                            <h5 style="color:red">SOLO ARCHIVOS DE IMÁGENES CON FORMATO  .PNG SERÁ CARGADO</h5>
                            <asp:FileUpload ID="fUpload" runat="server" CssClass="form-control" AllowMultiple="false" accept=".png" 
                                onchange="ImagePreview(this);"/>
                        </asp:Panel>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <asp:Image ID="imgFirma" runat="server" Height="400px" ImageAlign="Middle" Width="600px" />
                    </div>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <asp:Button ID="btnAceptar" Text="Aceptar" runat="server" CssClass="btn btn-primary active" OnClick="btnAceptar_Click" OnClientClick="Confirm()" />
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <asp:Label ID="lblmsj" runat="server" Font-Size="X-Large"></asp:Label>
                    </div>
                    <%--********************************FIN formulario de filtros**************************--%>
                </div>
            </div>
        </div>
    </div>

    <script>
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("¿Está seguro de realizar esta acción?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>

    <script type="text/javascript">
          function ImagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imgFirma.ClientID%>').prop('src', e.target.result)
                        .width(600)
                        .height(400);
                };
                reader.readAsDataURL(input.files[0]);
                }
            }

    </script>


    <style>
        .hlDownload {
            color:black !important;
        }
    </style>
</asp:Content>
