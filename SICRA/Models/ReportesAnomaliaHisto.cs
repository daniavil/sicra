﻿using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Models
{
    public class ReportesAnomaliaHisto
    {
        public REPOANOMALIA_M_HISTORICO repoMHisto { get; set; }
        public List<REPOANOMALIA_D_HISTORICO> repoDHisto { get; set; }
        public List<ImagenReporteAnomalia> imgsRepoHisto { get; set; }
    }
}