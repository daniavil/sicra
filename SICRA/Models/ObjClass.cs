﻿using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SICRA.Models
{
    public class RespMsj
    {
        public string estado { get; set; }
        public string mensaje { get; set; }
    }
    public class ReporteClaveDTO
    {
        public long IdRow_M { get; set; }
        public decimal? Nis_Rad { get; set; }
        public string IdAnomalia { get; set; }
        public string Anomalia { get; set; }
        public long? LecturaActiva { get; set; }
        public long? LecturaReactiva { get; set; }
        public decimal? LecturaDemanda { get; set; }
        public string multiplicador { get; set; }
        public string Causa { get; set; }
        public string numResolucion { get; set; }
        public int ciclo { get; set; }
        public string Solicitante { get; set; }
        public int? IntTipoRepo { get; set; }
        public string TipoRepo { get; set; }
        public long? OS { get; set; }
        public string MsjOs { get; set; }
        public bool EnEspera { get; set; }
    }
    public class RepoOperativaDTO
    {
        public long IdRow_M { get; set; }
        public decimal? Nis_Rad { get; set; }
        public string Region { get; set; }
        public string Sector { get; set; }
        public string Ubicacion { get; set; }
        public string Tarifa { get; set; }
        public long? LecturaActiva { get; set; }
        public long? ConsumoActivaFact { get; set; }
        public long? LecturaActivaRetira { get; set; }
        public long? LecturaReactiva { get; set; }
        public long? ConsumoReactivaFact { get; set; }
        public long? LecturaReactivaRetira { get; set; }
        public decimal? LecturaDemanda { get; set; }
        public decimal? ConsumoDemandaFact { get; set; }
        public decimal? MultiplicadorAnt { get; set; }
        public decimal? MultiplicadorAct { get; set; }
        public string MedidorAnt { get; set; }
        public string MedidorAct { get; set; }
        public string Medida { get; set; }
        public string Anomalia { get; set; }
        public string SolcitudCausaVerificacion { get; set; }
        public int? dial { get; set; }
        public DateTime? FechaCambioMedidor { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaMaxResol { get; set; }
        public int? Ciclo { get; set; }
        public bool Resuelto { get; set; }
        public string UsuarioReporta { get; set; }
        public Rep_Operativa_D rep_Operativa_D { get; set; }
        public string Solicitante { get; set; }
        public string PerfilUsuario { get; set; }
        public long? OS { get; set; }
    }
    public class RepoAnomaliaMaestroDto
    {
        [Key]
        public long IdRow_M { get; set; }

        [StringLength(256)]
        public string UsuarioFacturacion { get; set; }

        [StringLength(50)]
        public string Region { get; set; }

        [StringLength(50)]
        public string Sector { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Nis_Rad { get; set; }

        [StringLength(9)]
        public string Ubicacion { get; set; }

        public string Anomalia { get; set; }

        public string SolcitudCausaVerificacion { get; set; }

        public decimal? Multiplicador { get; set; }

        [StringLength(50)]
        public string Medida { get; set; }

        public long? LecturaActiva { get; set; }

        public long? LecturaReactiva { get; set; }

        public decimal? LecturaDemanda { get; set; }
        public DateTime? FechaCambioMedidor { get; set; }

        public DateTime? FechaRegistro { get; set; }

        public int? dial { get; set; }

        public int? Ciclo { get; set; }

        public int Resuelto { get; set; }

        public string ResueltoStng { get; set; }

        public bool VisibleResolver { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaMaxResol { get; set; }
        public string tipo { get; set; }

        public string categoriaResol { get; set; }

        public List<RepoAnomaliaDetalleDto> repoAnomaliaDetalleDto { get; set; }

        public List<ImagenReporteAnomalia> imagenReporteAnomalias { get; set; }

        public string ImagenesInnerText { get; set; }
        /// <summary>
        /// tiene foto
        /// </summary>
        public string Tf { get; set; }
        /// <summary>
        /// perfil asignado para resolver
        /// </summary>
        public string PerfilAsignado { get; set; }
        public int? IdPerfilResoRepo { get; set; }
        public long? OS { get; set; }
        public string MsjOs { get; set; }
        /// <summary>
        /// 0 -> Refactura
        /// 1 -> Crítica
        /// 2 -> PostFacturación
        /// </summary>
        public int esCritica { get; set; }
        public string TipoRepo { get; set; }

    }
    public class RepoAnomaliaDetalleDto
    {
        public long IdRow_D { get; set; }

        public long IdRow_M { get; set; }

        public string UsuarioResuelve { get; set; }

        public long? LecturaActiva { get; set; }

        public long? LecturaReactiva { get; set; }

        public decimal? LecturaDemanda { get; set; }

        public string Resolucion { get; set; }

        public DateTime? FechaResolucion { get; set; }

        public string tipo { get; set; }

        public int Ciclo { get; set; }

        public bool esRespTec { get; set; }

        public string esRespTecStng { get; set; }
        public string PerfilUsuario { get; set; }

    }
    public class RepoExport
    {
        public string NIS { get; set; }
        public string Region { get; set; }
        public string Sector { get; set; }
        public string LecturaActiva { get; set; }
        public string LecturaReactiva { get; set; }
        public string LecturaDemanda { get; set; }
        public string Usuario_Reporta { get; set; }
        public string Fecha_Reporte { get; set; }
        public string Anomalia { get; set; }
        public string dial { get; set; }
    }
    public class RepoAnomaliaExport
    {
        public string UsuarioReporta { get; set; }
        public string Region { get; set; }
        public string Sector { get; set; }
        public decimal Nis_Rad { get; set; }
        public string Ubicacion { get; set; }
        public string Anomalia { get; set; }
        public string SolcitudCausaVerificacion { get; set; }
        public decimal Multiplicador { get; set; }
        public string Medida { get; set; }
        public double LecturaActivaReporte { get; set; }
        public double LecturaReactivaReporte { get; set; }
        public decimal LecturaDemandaReporte { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int dial { get; set; }
        public int Ciclo { get; set; }
        public string resuelto { get; set; }
        public DateTime FechaMaxResol { get; set; }
        public string UsuarioResuelve { get; set; }
        public double LecturaActivaResp { get; set; }
        public double LecturaReactivaResp { get; set; }
        public decimal LecturaDemandaResp { get; set; }
        public string Resolucion { get; set; }
        public DateTime FechaResolucion { get; set; }
        public string tipo { get; set; }
        public bool VisibleResolver { get; set; }
    }
    public class ObjIdValor
    {
        public string id { get; set; }
        public string valor { get; set; }
    }
}