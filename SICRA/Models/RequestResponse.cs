﻿using RestSharp;
using SICRA.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Models
{
    public class RequestResponse
    {

        public string Solicitud { get; set; }
        public IRestResponse Respuesta { get; set; }
        public DateTime FechaHoraMinSeg { get; set; }
    }
}