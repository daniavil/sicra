﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Models
{
    public class ParSoporteRefacturacion
    {
        public string nis_rad { get; set; }
        public string periodo { get; set; }
    }
    public class ObjSoporteRefacturacion
    {
        public decimal nis_rad { get; set; }
        public string nombre_cliente { get; set; }
        public string ubicacion { get; set; }
        public string nom_tarifa { get; set; }
        public int? sec_rec { get; set; }
        public int? sec_nis { get; set; }
        public DateTime f_fact { get; set; }
        public DateTime f_fact_anul { get; set; }
        public string sec_rec_anul { get; set; }
        public string imp_tot_rec { get; set; }
        public string csmo_fact { get; set; }
        public string csmo_react { get; set; }
        public string pot_fact { get; set; }
        public string pot_leida { get; set; }
        public int level { get; set; }
        public string est_act { get; set; }
        public DateTime f_fact_ant { get; set; }
        public int cicloFact { get; set; }
        public List<SoporteRefacturacion_lecturas> soporteRefacturacion_Lecturas { get; set; }
        public List<SoporteRefacturacion_importes> soporteRefacturacion_Importes { get; set; }
    }
    public class SoporteRefacturacion_lecturas
    {
        public string nis_rad { get; set; }
        public string tip_csmo { get; set; }
        public string lect { get; set; }
        public string f_lect { get; set; }
        public string csmo { get; set; }
        public string cte { get; set; }
        public string tip_lect { get; set; }
        public string f_fact { get; set; }
        public string sec_rec { get; set; }
        public string num_rue { get; set; }
    }
    public class SoporteRefacturacion_importes
    {
        public string nis_rad { get; set; }
        public string sec_rec { get; set; }
        public string sec_nis { get; set; }
        public string f_fact { get; set; }
        public string co_concepto { get; set; }
        public string sec_concepto { get; set; }
        public string csmo_fact { get; set; }
        public decimal imp_concepto { get; set; }
        public string desc_cod { get; set; }
    }

    public class ImportesFact
    {
        public List<SoporteRefacturacion_importes> ImpFact { get; set; }
        public string JsonImp { get; set; }
        public string JsonFiltro { get; set; }
    }
    public class ListadoImportes
    {
        public string Importe { get; set; }
        public decimal Corregido { get; set; }
        public decimal Facturado { get; set; }
        public decimal Diferencia { get; set; }
    }
    public partial class SoporteRefacturaBd
    {
        public int idSoporte { get; set; }
        public decimal nisrad { get; set; }
        public int cicloFact { get; set; }
        public string nomUsuario { get; set; }
        public string pqr { get; set; }
        public string observacion { get; set; }
        public DateTime FechaRegistro { get; set; }
        public List<ObjSoporteRefacturacion> Soportes { get; set; }
        
    }
}