namespace SICRA.Models.EEHLogistics
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class LogisticsModel : DbContext
    {
        public LogisticsModel()
            : base("name=LogisticsModel")
        {
        }

        public virtual DbSet<tipoLecturasAnomalia> tipoLecturasAnomalia { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tipoLecturasAnomalia>()
                .Property(e => e.descripcion)
                .IsUnicode(false);
        }
    }
}
