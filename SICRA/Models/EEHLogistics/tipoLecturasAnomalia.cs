namespace SICRA.Models.EEHLogistics
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tipoLecturasAnomalia")]
    public partial class tipoLecturasAnomalia
    {
        [Key]
        public int idLecturaAnomalia { get; set; }

        [Column(TypeName = "text")]
        public string descripcion { get; set; }
    }
}
