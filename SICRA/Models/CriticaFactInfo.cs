﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Models
{
    public class AnomaliasFacturacionInfo
    {
        public string id { get; set; }
        public string nombre { get; set; }
    }

    public class UsuariosFacturacionInfo
    {
        public string codUsr { get; set; }
        public string nombre { get; set; }
    }

    public class DatosCriticaInfo
    {
        public string nisrad { get; set; }
        public string dial { get; set; }
        public string ciclo { get; set; }
        public string codAnoCri { get; set; }
        public string nomAnoCri { get; set; }
        public string codAnoLec { get; set; }
        public string nomAnoLec { get; set; }
        public string codUsuario { get; set; }
        public string LectActiva { get; set; }
        public string LectReactiva { get; set; }
        public string LectDemanda { get; set; }
        public string DescripObserv { get; set; }
        public string UsrSoeeh { get; set; }
    }

    public class CriticaFactInfo
    {
        public string CodAnomalia { get; set; }
        public string DescripcionAnomalia { get; set; }
        public string repoSicra { get; set; }
        public string csmocri { get; set; }
        public string csmofact { get; set; }
        public string accion { get; set; }
        /// <summary>
        /// Ind_rest 1:real, 2:promedio
        /// </summary>
        public string esprom { get; set; }
        /// <summary>
        /// usuario que hizo critica
        /// </summary>
        public string usrcri { get; set; }
        public string ObsCri { get; set; }


    }
}
