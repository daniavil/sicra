﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Models
{
    public class ClienteRoot
    {
        public Cliente cliente { get; set; }
        public List<CliFotos> cliFotos { get; set; }
        public Respuesta respuesta { get; set; }
        public List<HistoricoLectura> ListaHistoricoLectura { get; set; }
        public List<HistoricoConsumo> ListaHistoricoConsumo { get; set; }
        public string ImagenesInnerText { get; set; }
    }

    public class Cliente
    {
        public string clave { get; set; }
        public string nis_rad { get; set; }
        public string num_medidor { get; set; }
        public string num_medidor_ant { get; set; }
        public string ubicacion { get; set; }
        public string region { get; set; }
        public string area { get; set; }
        public string nom_cli { get; set; }
        public string direccion { get; set; }
        public string cod_tarifa { get; set; }
        public string nom_tarifa { get; set; }
        public string num_agujas { get; set; }
        public string cod_lectura_actual { get; set; }
        public string consumo_activo_actual { get; set; }
        public string consumo_activo_anterior { get; set; }
        public string lectura_activa_actual { get; set; }
        public string lectura_activa_anterior { get; set; }
        public string consumo_reactivo_actual { get; set; }
        public string consumo_reactivo_anterior { get; set; }
        public string lectura_reactiva_actual { get; set; }
        public string lectura_reactiva_anterior { get; set; }
        public string multiplicador { get; set; }
        public string num_dias_fact { get; set; }
        public string fecha_hora_lectura { get; set; }
        public string cod_lectura_campo { get; set; }
        public string consumo_activo_campo { get; set; }
        public string lectura_activa_campo { get; set; }
        public string consumo_reactivo_campo { get; set; }
        public string lectura_reactiva_campo { get; set; }
        public string lectura_demanda_actual { get; set; }
        public string lectura_demanda_anterior { get; set; }
        public string entidad_lector { get; set; }
        public string anomalia { get; set; }
        public string dial { get; set; }
        public string cicloLectura { get; set; }
        public string cicloActual { get; set; }
        //-----------------------------------------------
        public string codmercado { get; set; }
        public string mercado { get; set; }
        public string codarea { get; set; }


    }

    public class CliFotos
    {
        public CliFotos()
        {
        }

        public string clave { get; set; }
        public string imagen { get; set; }
        public string fecha { get; set; }
        public string origen { get; set; }
    }

    public class Respuesta
    {
        public Respuesta()
        {
        }

        public string resultado { get; set; }
        public bool error { get; set; }
        public string mensaje { get; set; }
    }

    public class HistoricoLectura
    {
        public string fecha { get; set; }
        public string tipoconsumo { get; set; }
        public string consumo { get; set; }
        public string lectura { get; set; }
        public string multiplicador { get; set; }
        public string tipolectura { get; set; }

    }

    public class HistoricoConsumo
    {
        public string FECHA { get; set; }
        public string CODIGOLECTURA { get; set; }
        public string LECTURAACTIVA { get; set; }
        public string CONSUMOACTIVA { get; set; }
        public string LECTURAREACTIVA { get; set; }
        public string CONSUMOREACTIVA { get; set; }
        public string ACTIVAREAL { get; set; }
        public string REACTIVAREAL { get; set; }
        public string MULTIPLICADOR { get; set; }
        public string ANOMALIA { get; set; }
        public string OBSERVACION { get; set; }
        public string OBSERVACIONCRITICA { get; set; }

    }
}