﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Models
{
    public class ObjList
    {
        public string id { get; set; }
        public string nombre { get; set; }
    }

    public class FechaLecturaObj
    {
        public string FECHA_LECTURA { get; set; }
        public string esHoy { get; set; }
    }

    public class ParCriticaLecturaInfo
    {
        public string clave { get; set; }
        public string CodSector { get; set; }
        public string codAnomalias { get; set; }
        public string fecha { get; set; }
        public int esTotal { get; set; }
        public int periodo { get; set; }
    }
    public class CriticaLecturaInfo
    {
        public string CODIGODIAL { get; set; }
        public string PERIODO { get; set; }
        public string SECTOR_ID { get; set; }
        public string CLAVE { get; set; }
        public string LECTURAENCONTRADA { get; set; }
        public string FECHALECTURA { get; set; }
        public string FECHA_PROGRAMADA { get; set; }
        public string CODIGOANOMALIA { get; set; }
        public string SUBCLASE { get; set; }
        public string DESCRIPCION { get; set; }
        public string CODIGOADMINISTRATIVO { get; set; }
        public string CODIGOGRUPOTRABAJO { get; set; }
        public string NomLector { get; set; }
        public string tipolect { get; set; }
        public string ParamAnterior { get; set; }
        public string ParamActual { get; set; }
        public int EsTelemedido { get; set; }

        public string ObsInspector { get; set; }
    }
    public class respuestaWsCriticaLecturaInfo
    {
        public string RespWs { get; set; }
        public List<CriticaLecturaInfo> ListaCriticaLecturas { get; set; }
    }
}
