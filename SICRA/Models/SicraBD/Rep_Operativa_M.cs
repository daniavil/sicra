namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Rep_Operativa_M
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Rep_Operativa_M()
        {
            ImagenReporteOperativa = new HashSet<ImagenReporteOperativa>();
            Rep_Operativa_D = new HashSet<Rep_Operativa_D>();
        }

        [Key]
        public long IdRow_M { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Nis_Rad { get; set; }

        public int? idSector { get; set; }

        [StringLength(9)]
        public string Ubicacion { get; set; }

        [StringLength(50)]
        public string Tarifa { get; set; }

        public long? LecturaActiva { get; set; }

        public long? ConsumoActivaFact { get; set; }

        public long? LecturaActivaRetira { get; set; }

        public long? LecturaReactiva { get; set; }

        public long? ConsumoReactivaFact { get; set; }

        public long? LecturaReactivaRetira { get; set; }

        public decimal? LecturaDemanda { get; set; }

        public decimal? ConsumoDemandaFact { get; set; }

        public decimal? MultiplicadorAnt { get; set; }

        public decimal? MultiplicadorAct { get; set; }

        [StringLength(50)]
        public string MedidorAnt { get; set; }

        [StringLength(50)]
        public string MedidorAct { get; set; }

        [StringLength(50)]
        public string Medida { get; set; }

        public int? Anomalia { get; set; }

        public string SolcitudCausaVerificacion { get; set; }

        public int? dial { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaCambioMedidor { get; set; }

        public DateTime? FechaRegistro { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaMaxResol { get; set; }

        public int? Ciclo { get; set; }

        public bool Resuelto { get; set; }

        [StringLength(256)]
        public string UsuarioReporta { get; set; }

        public long? OS { get; set; }

        public int? IdPerfilResoRepo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ImagenReporteOperativa> ImagenReporteOperativa { get; set; }

        public virtual PerfilResolucionRepo PerfilResolucionRepo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rep_Operativa_D> Rep_Operativa_D { get; set; }

        public virtual Sectores Sectores { get; set; }
    }
}
