namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CicloRefactura")]
    public partial class CicloRefactura
    {
        [Key]
        public int idCiclo { get; set; }

        public int ciclo { get; set; }

        public int IdGesRefMae { get; set; }

        public virtual GestionRefacturaMae GestionRefacturaMae { get; set; }
    }
}
