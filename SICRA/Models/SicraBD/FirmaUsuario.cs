namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FirmaUsuario")]
    public partial class FirmaUsuario
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int idRow { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idUsuario { get; set; }

        [Required]
        public string nombreImg { get; set; }

        [Required]
        public string firma { get; set; }

        public DateTime? FechaCarga { get; set; }

        public virtual Usuarios Usuarios { get; set; }
    }
}
