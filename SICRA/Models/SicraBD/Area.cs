namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Area")]
    public partial class Area
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Area()
        {
            GestionRefacturaMae = new HashSet<GestionRefacturaMae>();
            PerfilesAreas = new HashSet<PerfilesAreas>();
        }

        [Key]
        public int IdArea { get; set; }

        [Required]
        [StringLength(50)]
        public string NombreArea { get; set; }

        public int IdTipoArea { get; set; }

        public virtual TipoArea TipoArea { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GestionRefacturaMae> GestionRefacturaMae { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PerfilesAreas> PerfilesAreas { get; set; }
    }
}
