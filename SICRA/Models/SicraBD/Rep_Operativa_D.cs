namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Rep_Operativa_D
    {
        [Key]
        public long IdRow_D { get; set; }

        public long? IdRow_M { get; set; }

        [StringLength(20)]
        public string Estado { get; set; }

        public string Resolucion { get; set; }

        public DateTime? FechaResolucion { get; set; }

        [StringLength(256)]
        public string UsuarioResuelve { get; set; }

        public virtual Rep_Operativa_M Rep_Operativa_M { get; set; }
    }
}
