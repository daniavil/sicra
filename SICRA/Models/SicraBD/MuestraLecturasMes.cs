namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MuestraLecturasMes
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int idRow { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CLAVE { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool EsTotal { get; set; }

        public int? CODIGODIAL { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PERIODO { get; set; }

        public int? SECTOR_ID { get; set; }

        public int? LECTURAENCONTRADA { get; set; }

        [Column(TypeName = "date")]
        public DateTime FECHALECTURA { get; set; }

        [StringLength(50)]
        public string FECHA_PROGRAMADA { get; set; }

        [StringLength(20)]
        public string CODIGOANOMALIA { get; set; }

        [StringLength(20)]
        public string SUBCLASE { get; set; }

        [StringLength(2000)]
        public string DESCRIPCION { get; set; }

        public double? CODIGOADMINISTRATIVO { get; set; }

        public double? CODIGOGRUPOTRABAJO { get; set; }

        [StringLength(500)]
        public string CambioParametrizacion { get; set; }

        [StringLength(50)]
        public string TipoFiltro { get; set; }

        public int? lecturaCorrecta { get; set; }

        public int? LecturaDebioSer { get; set; }

        public bool fotoCorrecta { get; set; }

        public bool anomaliaCorrecta { get; set; }

        public string anomaliaDebioSer { get; set; }

        [StringLength(50)]
        public string usuario { get; set; }

        public DateTime? fechaRegistro { get; set; }

        public DateTime? fechaResolucion { get; set; }

        public bool Resuelto { get; set; }

        public int? IdObFoto { get; set; }

        public int? UsrResuelve { get; set; }

        public int? EsTelemedido { get; set; }
        public string Rendimiento { get; set; }

        public virtual ObsFotoMuestraLectura ObsFotoMuestraLectura { get; set; }
    }
}
