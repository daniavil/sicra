namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ImagenReporteAnomalia_Historico
    {
        [Key]
        [Column(Order = 0)]
        public Guid idRow { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(200)]
        public string nombre { get; set; }

        [Key]
        [Column(Order = 2)]
        public string ruta { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long IdRow_M { get; set; }

        [Key]
        [Column(Order = 4)]
        public DateTime fechaCarga { get; set; }

        public int? ciclo { get; set; }
    }
}
