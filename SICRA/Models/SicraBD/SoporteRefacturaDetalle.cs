namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SoporteRefacturaDetalle")]
    public partial class SoporteRefacturaDetalle
    {
        [Key]
        public int idRow { get; set; }

        public int? idSoporte { get; set; }

        [StringLength(200)]
        public string importe { get; set; }

        public decimal? corregido { get; set; }

        public decimal? facturado { get; set; }

        public decimal? diferencia { get; set; }

        public virtual SoporteRefactura SoporteRefactura { get; set; }
    }
}
