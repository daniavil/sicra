namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Sectores
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Sectores()
        {
            GestionRefacturaMae = new HashSet<GestionRefacturaMae>();
            Rep_Operativa_M = new HashSet<Rep_Operativa_M>();
            Usuario_Sector = new HashSet<Usuario_Sector>();
        }

        [Key]
        public int idSector { get; set; }

        [Required]
        [StringLength(50)]
        public string Sector { get; set; }

        public int? CodSector { get; set; }

        public int? CodAreaInCms { get; set; }

        public int idRegion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GestionRefacturaMae> GestionRefacturaMae { get; set; }

        public virtual Regiones Regiones { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rep_Operativa_M> Rep_Operativa_M { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Usuario_Sector> Usuario_Sector { get; set; }
    }
}
