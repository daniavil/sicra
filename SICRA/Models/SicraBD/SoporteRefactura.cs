namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SoporteRefactura")]
    public partial class SoporteRefactura
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SoporteRefactura()
        {
            SoporteRefacturaDetalle = new HashSet<SoporteRefacturaDetalle>();
        }

        [Key]
        public int idSoporte { get; set; }

        [Column(TypeName = "numeric")]
        public decimal nisrad { get; set; }

        public int? SecNis { get; set; }

        public int? SecRec { get; set; }

        public int? SecRecAnul { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FFact { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FFactAnul { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FFactAnt { get; set; }

        public int cicloFact { get; set; }

        public int? cicloRefact { get; set; }

        [Required]
        [StringLength(200)]
        public string nomUsuario { get; set; }

        [Required]
        [StringLength(200)]
        public string nomCliente { get; set; }

        [StringLength(9)]
        public string ubicacion { get; set; }

        [StringLength(50)]
        public string pqr { get; set; }

        [StringLength(50)]
        public string tarifa { get; set; }

        public int? ConsumoFacturado { get; set; }

        public int? ConsumoRefacturado { get; set; }

        public int? LectActivaFact { get; set; }

        public int? LectReactivaFact { get; set; }

        public decimal? LectDemandaFact { get; set; }

        public int? LectActivaRefact { get; set; }

        public int? LectReactivaRefact { get; set; }

        public decimal? LectDemandaRefact { get; set; }

        public decimal? diferencia { get; set; }

        public decimal? valorDoc { get; set; }

        [StringLength(2)]
        public string dbcr { get; set; }

        [Required]
        [StringLength(2000)]
        public string observacion { get; set; }

        [Column(TypeName = "date")]
        public DateTime FechaRegistro { get; set; }

        public int? CodAgrupacion { get; set; }

        public int? Orden { get; set; }

        public int? idUsuario { get; set; }

        public int? IdGestionRefa { get; set; }

        public int? idTipoRect { get; set; }

        public virtual GestionRefacturaMae GestionRefacturaMae { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SoporteRefacturaDetalle> SoporteRefacturaDetalle { get; set; }
    }
}
