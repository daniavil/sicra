namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GestionRefacturaMae")]
    public partial class GestionRefacturaMae
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GestionRefacturaMae()
        {
            CicloRefactura = new HashSet<CicloRefactura>();
            GestionRefacturaDet = new HashSet<GestionRefacturaDet>();
            SoporteRefactura = new HashSet<SoporteRefactura>();
        }

        [Key]
        public int IdGesRefMae { get; set; }

        [Required]
        public string codGestion { get; set; }

        public int idTipoOrigen { get; set; }

        [Column(TypeName = "numeric")]
        public decimal nisrad { get; set; }

        [Required]
        public string ciclo { get; set; }

        public int kWhCv { get; set; }

        public decimal LpsCv { get; set; }

        public int kWhRef { get; set; }

        public decimal LpsRef { get; set; }

        public int kWhTotal { get; set; }

        public decimal LpsTotal { get; set; }

        [Required]
        [StringLength(2)]
        public string dbcr { get; set; }

        public string ComentarioSolicitud { get; set; }

        public int IdSector { get; set; }

        public int IdMercado { get; set; }

        public int idUsuario { get; set; }

        public bool GesCom { get; set; }

        public int? IdPerfilCrea { get; set; }

        public int? IdAreaCrea { get; set; }

        public int? IdPerfilActualAsignado { get; set; }

        public int? IdArea { get; set; }

        public int? IdPerfilFinalAutoriza { get; set; }

        public int idTipoRect { get; set; }

        public DateTime FechaCreacion { get; set; }

        public DateTime? FechaFinalizada { get; set; }

        public bool Denegada { get; set; }

        public bool Finalizada { get; set; }

        public int? IdAreaCE { get; set; }

        public int? IdPerfilMinAutorizaCE { get; set; }

        public int? IdPerfilActualAsignadoCE { get; set; }

        public int? IdPerfilFinalAutorizaCE { get; set; }

        public virtual Area Area { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CicloRefactura> CicloRefactura { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GestionRefacturaDet> GestionRefacturaDet { get; set; }

        public virtual Mercado Mercado { get; set; }

        public virtual Sectores Sectores { get; set; }

        public virtual TipoOrigen TipoOrigen { get; set; }

        public virtual TipoRectificacion TipoRectificacion { get; set; }

        public virtual Usuarios Usuarios { get; set; }

        public virtual PerfilAutorizacion PerfilAutorizacion { get; set; }

        public virtual PerfilAutorizacion PerfilAutorizacion1 { get; set; }

        public virtual PerfilAutorizacion PerfilAutorizacion2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SoporteRefactura> SoporteRefactura { get; set; }
    }
}
