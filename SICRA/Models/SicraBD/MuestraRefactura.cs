namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MuestraRefactura")]
    public partial class MuestraRefactura
    {
        [Key]
        public Guid idRow { get; set; }

        public int nisrad { get; set; }

        public bool EsTotal { get; set; }

        public int? dial { get; set; }

        public int Ciclo { get; set; }

        [Required]
        [StringLength(20)]
        public string CodObservacion { get; set; }

        [StringLength(500)]
        public string Observacion { get; set; }

        public int? lecturaActiva { get; set; }

        public int? lecturaReactiva { get; set; }

        public decimal? lecturaDemanda { get; set; }

        public int? CodOrigen { get; set; }

        public string Origen { get; set; }

        [Required]
        [StringLength(50)]
        public string usuarioGenera { get; set; }

        [StringLength(50)]
        public string usuarioResuelve { get; set; }

        [StringLength(50)]
        public string usuarioResuelveRefa { get; set; }

        public DateTime fechaRegistro { get; set; }

        public DateTime? fechaResolucion { get; set; }

        public bool Resuelto { get; set; }

        [StringLength(5)]
        public string P1RefacturaCorrecta { get; set; }

        [StringLength(5)]
        public string P2DocSubida { get; set; }

        [StringLength(5)]
        public string P3ProcesoAutorizacionCorrecto { get; set; }

        [StringLength(5)]
        public string P4CorreccionProcedente { get; set; }

        [StringLength(5)]
        public string P5DatosResoCorrecto { get; set; }

        [StringLength(5)]
        public string P6DocCorrecta { get; set; }

        [StringLength(5)]
        public string P7ObsCierreCorrecto { get; set; }

        [StringLength(5)]
        public string P8RepoSicra { get; set; }

        [StringLength(5)]
        public string P9OSRevGen { get; set; }

        public int? CaliRefaCorrecta { get; set; }

        public int? CaliDocSoporteCorrecta { get; set; }

        public int? CaliObsCierreCorrecta { get; set; }

        public int? CaliProcAutCorrecta { get; set; }

        public int? CaliDatosResolCorrecta { get; set; }

        public int? Calificacion { get; set; }
    }
}
