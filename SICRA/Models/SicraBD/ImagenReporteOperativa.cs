namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ImagenReporteOperativa")]
    public partial class ImagenReporteOperativa
    {
        [Key]
        public Guid idRow { get; set; }

        [Required]
        [StringLength(200)]
        public string nombre { get; set; }

        [Required]
        public string ruta { get; set; }

        public long IdRow_M { get; set; }

        public DateTime fechaCarga { get; set; }

        public bool esImg { get; set; }

        public virtual Rep_Operativa_M Rep_Operativa_M { get; set; }
    }
}
