namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ObsFotoMuestraLectura")]
    public partial class ObsFotoMuestraLectura
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ObsFotoMuestraLectura()
        {
            MuestraLecturasMes = new HashSet<MuestraLecturasMes>();
        }

        [Key]
        public int IdObFoto { get; set; }

        [Required]
        [StringLength(200)]
        public string Observacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MuestraLecturasMes> MuestraLecturasMes { get; set; }
    }
}
