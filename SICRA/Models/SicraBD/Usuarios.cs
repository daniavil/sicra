namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Usuarios
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuarios()
        {
            GestionRefacturaDet = new HashSet<GestionRefacturaDet>();
            GestionRefacturaMae = new HashSet<GestionRefacturaMae>();
            Usuario_Mercado = new HashSet<Usuario_Mercado>();
            Usuario_Sector = new HashSet<Usuario_Sector>();
        }

        [Key]
        public int idUsuario { get; set; }

        [Required]
        [StringLength(15)]
        public string Usuario { get; set; }

        [StringLength(200)]
        public string NomPersona { get; set; }

        public bool AdminUser { get; set; }

        public string correo { get; set; }

        public bool Aut { get; set; }

        public int? idPerfil { get; set; }

        public int? IdPerfilResoRepo { get; set; }

        public bool Activo { get; set; }

        public virtual FirmaUsuario FirmaUsuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GestionRefacturaDet> GestionRefacturaDet { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GestionRefacturaMae> GestionRefacturaMae { get; set; }

        public virtual PerfilAutorizacion PerfilAutorizacion { get; set; }

        public virtual PerfilResolucionRepo PerfilResolucionRepo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Usuario_Mercado> Usuario_Mercado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Usuario_Sector> Usuario_Sector { get; set; }
    }
}
