namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Usuario_Mercado
    {
        [Key]
        public int idRow { get; set; }

        public int? idUsuario { get; set; }

        public int? IdMercado { get; set; }

        public virtual Mercado Mercado { get; set; }

        public virtual Usuarios Usuarios { get; set; }
    }
}
