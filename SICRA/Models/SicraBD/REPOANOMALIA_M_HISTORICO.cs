namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class REPOANOMALIA_M_HISTORICO
    {
        [Key]
        public long idrow { get; set; }

        public long IdRow_M { get; set; }

        [StringLength(256)]
        public string UsuarioFacturacion { get; set; }

        [StringLength(50)]
        public string Region { get; set; }

        [StringLength(50)]
        public string Sector { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Nis_Rad { get; set; }

        [StringLength(9)]
        public string Ubicacion { get; set; }

        public string Anomalia { get; set; }

        public string SolcitudCausaVerificacion { get; set; }

        public decimal? Multiplicador { get; set; }

        [StringLength(50)]
        public string Medida { get; set; }

        public long? LecturaActiva { get; set; }

        public long? LecturaReactiva { get; set; }

        public decimal? LecturaDemanda { get; set; }

        public DateTime? FechaRegistro { get; set; }

        public int? dial { get; set; }

        public int Ciclo { get; set; }

        public int Resuelto { get; set; }

        public DateTime? FechaMaxResol { get; set; }

        public int? esCritica { get; set; }

        public int? idCategoria { get; set; }

        public int? IdPerfilResoRepo { get; set; }

        public long? OS { get; set; }

        public string MsjOS { get; set; }

        public bool? EnEspera { get; set; }
    }
}
