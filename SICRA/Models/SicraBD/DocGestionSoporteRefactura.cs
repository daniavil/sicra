namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DocGestionSoporteRefactura")]
    public partial class DocGestionSoporteRefactura
    {
        [Key]
        public int IdDoc { get; set; }

        [Required]
        [StringLength(300)]
        public string NomDoc { get; set; }

        [Required]
        public string Ruta { get; set; }

        public DateTime FechaHora { get; set; }

        public int IdGesRefDet { get; set; }

        public virtual GestionRefacturaDet GestionRefacturaDet { get; set; }
    }
}
