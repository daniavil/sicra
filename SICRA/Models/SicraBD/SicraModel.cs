using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace SICRA.Models.SicraBD
{
    public partial class SicraModel : DbContext
    {
        public SicraModel()
            : base("name=SicraModel")
        {
        }

        public virtual DbSet<Area> Area { get; set; }
        public virtual DbSet<BitacoraCargaSoporte> BitacoraCargaSoporte { get; set; }
        public virtual DbSet<CargosRefactura> CargosRefactura { get; set; }
        public virtual DbSet<CategoriaResolucion> CategoriaResolucion { get; set; }
        public virtual DbSet<CicloFacturacion> CicloFacturacion { get; set; }
        public virtual DbSet<CicloRefactura> CicloRefactura { get; set; }
        public virtual DbSet<DocGestionSoporteRefactura> DocGestionSoporteRefactura { get; set; }
        public virtual DbSet<FirmaUsuario> FirmaUsuario { get; set; }
        public virtual DbSet<GestionRefacturaDet> GestionRefacturaDet { get; set; }
        public virtual DbSet<GestionRefacturaMae> GestionRefacturaMae { get; set; }
        public virtual DbSet<ImagenReporteAnomalia> ImagenReporteAnomalia { get; set; }
        public virtual DbSet<ImagenReporteOperativa> ImagenReporteOperativa { get; set; }
        public virtual DbSet<Mercado> Mercado { get; set; }
        public virtual DbSet<Modulo> Modulo { get; set; }
        public virtual DbSet<MuestraCritica> MuestraCritica { get; set; }
        public virtual DbSet<MuestraLecturasMes> MuestraLecturasMes { get; set; }
        public virtual DbSet<MuestraRefactura> MuestraRefactura { get; set; }
        public virtual DbSet<ObsFotoMuestraLectura> ObsFotoMuestraLectura { get; set; }
        public virtual DbSet<PerfilAutorizacion> PerfilAutorizacion { get; set; }
        public virtual DbSet<PerfilesAreas> PerfilesAreas { get; set; }
        public virtual DbSet<PerfilModulo> PerfilModulo { get; set; }
        public virtual DbSet<PerfilResolucionRepo> PerfilResolucionRepo { get; set; }
        public virtual DbSet<RangoPerfilArea> RangoPerfilArea { get; set; }
        public virtual DbSet<Regiones> Regiones { get; set; }
        public virtual DbSet<Rep_Operativa_D> Rep_Operativa_D { get; set; }
        public virtual DbSet<Rep_Operativa_M> Rep_Operativa_M { get; set; }
        public virtual DbSet<REPOANOMALIA_D> REPOANOMALIA_D { get; set; }
        public virtual DbSet<REPOANOMALIA_D_HISTORICO> REPOANOMALIA_D_HISTORICO { get; set; }
        public virtual DbSet<REPOANOMALIA_M> REPOANOMALIA_M { get; set; }
        public virtual DbSet<REPOANOMALIA_M_HISTORICO> REPOANOMALIA_M_HISTORICO { get; set; }
        public virtual DbSet<Sectores> Sectores { get; set; }
        public virtual DbSet<SoporteRefactura> SoporteRefactura { get; set; }
        public virtual DbSet<SoporteRefacturaDetalle> SoporteRefacturaDetalle { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<TipoArea> TipoArea { get; set; }
        public virtual DbSet<TipoOrigen> TipoOrigen { get; set; }
        public virtual DbSet<TipoRectificacion> TipoRectificacion { get; set; }
        public virtual DbSet<Usuario_Mercado> Usuario_Mercado { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }
        public virtual DbSet<ImagenReporteAnomalia_Historico> ImagenReporteAnomalia_Historico { get; set; }
        public virtual DbSet<Usuario_Sector> Usuario_Sector { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Area>()
                .HasMany(e => e.PerfilesAreas)
                .WithRequired(e => e.Area)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BitacoraCargaSoporte>()
                .Property(e => e.nisrad)
                .HasPrecision(7, 0);

            modelBuilder.Entity<CargosRefactura>()
                .Property(e => e.CodCargo)
                .IsFixedLength();

            modelBuilder.Entity<GestionRefacturaDet>()
                .HasMany(e => e.DocGestionSoporteRefactura)
                .WithRequired(e => e.GestionRefacturaDet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GestionRefacturaMae>()
                .Property(e => e.nisrad)
                .HasPrecision(7, 0);

            modelBuilder.Entity<GestionRefacturaMae>()
                .Property(e => e.dbcr)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<GestionRefacturaMae>()
                .HasMany(e => e.CicloRefactura)
                .WithRequired(e => e.GestionRefacturaMae)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GestionRefacturaMae>()
                .HasMany(e => e.GestionRefacturaDet)
                .WithRequired(e => e.GestionRefacturaMae)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GestionRefacturaMae>()
                .HasMany(e => e.SoporteRefactura)
                .WithOptional(e => e.GestionRefacturaMae)
                .HasForeignKey(e => e.IdGestionRefa);

            modelBuilder.Entity<Mercado>()
                .HasMany(e => e.GestionRefacturaMae)
                .WithRequired(e => e.Mercado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PerfilAutorizacion>()
                .HasMany(e => e.GestionRefacturaDet)
                .WithOptional(e => e.PerfilAutorizacion)
                .HasForeignKey(e => e.idPerfilAsignado);

            modelBuilder.Entity<PerfilAutorizacion>()
                .HasMany(e => e.GestionRefacturaDet1)
                .WithOptional(e => e.PerfilAutorizacion1)
                .HasForeignKey(e => e.idPerfilAutoriza);

            modelBuilder.Entity<PerfilAutorizacion>()
                .HasMany(e => e.GestionRefacturaMae)
                .WithOptional(e => e.PerfilAutorizacion)
                .HasForeignKey(e => e.IdPerfilActualAsignado);

            modelBuilder.Entity<PerfilAutorizacion>()
                .HasMany(e => e.GestionRefacturaMae1)
                .WithOptional(e => e.PerfilAutorizacion1)
                .HasForeignKey(e => e.IdPerfilFinalAutoriza);

            modelBuilder.Entity<PerfilAutorizacion>()
                .HasMany(e => e.GestionRefacturaMae2)
                .WithOptional(e => e.PerfilAutorizacion2)
                .HasForeignKey(e => e.IdPerfilCrea);

            modelBuilder.Entity<PerfilAutorizacion>()
                .HasMany(e => e.PerfilAutorizacion1)
                .WithOptional(e => e.PerfilAutorizacion2)
                .HasForeignKey(e => e.IdPerfilSuperior);

            modelBuilder.Entity<PerfilAutorizacion>()
                .HasMany(e => e.PerfilesAreas)
                .WithRequired(e => e.PerfilAutorizacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PerfilesAreas>()
                .HasMany(e => e.RangoPerfilArea)
                .WithRequired(e => e.PerfilesAreas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Regiones>()
                .HasMany(e => e.Sectores)
                .WithRequired(e => e.Regiones)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rep_Operativa_M>()
                .Property(e => e.Nis_Rad)
                .HasPrecision(8, 0);

            modelBuilder.Entity<Rep_Operativa_M>()
                .Property(e => e.LecturaDemanda)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Rep_Operativa_M>()
                .Property(e => e.ConsumoDemandaFact)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Rep_Operativa_M>()
                .Property(e => e.MultiplicadorAnt)
                .HasPrecision(9, 2);

            modelBuilder.Entity<Rep_Operativa_M>()
                .Property(e => e.MultiplicadorAct)
                .HasPrecision(9, 2);

            modelBuilder.Entity<Rep_Operativa_M>()
                .HasMany(e => e.ImagenReporteOperativa)
                .WithRequired(e => e.Rep_Operativa_M)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<REPOANOMALIA_D>()
                .Property(e => e.LecturaDemanda)
                .HasPrecision(18, 3);

            modelBuilder.Entity<REPOANOMALIA_D_HISTORICO>()
                .Property(e => e.LecturaDemanda)
                .HasPrecision(18, 3);

            modelBuilder.Entity<REPOANOMALIA_M>()
                .Property(e => e.Nis_Rad)
                .HasPrecision(8, 0);

            modelBuilder.Entity<REPOANOMALIA_M>()
                .Property(e => e.Multiplicador)
                .HasPrecision(9, 2);

            modelBuilder.Entity<REPOANOMALIA_M>()
                .Property(e => e.LecturaDemanda)
                .HasPrecision(18, 3);

            modelBuilder.Entity<REPOANOMALIA_M>()
                .HasMany(e => e.ImagenReporteAnomalia)
                .WithRequired(e => e.REPOANOMALIA_M)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<REPOANOMALIA_M>()
                .HasMany(e => e.REPOANOMALIA_D)
                .WithRequired(e => e.REPOANOMALIA_M)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<REPOANOMALIA_M_HISTORICO>()
                .Property(e => e.Nis_Rad)
                .HasPrecision(8, 0);

            modelBuilder.Entity<REPOANOMALIA_M_HISTORICO>()
                .Property(e => e.Multiplicador)
                .HasPrecision(9, 2);

            modelBuilder.Entity<REPOANOMALIA_M_HISTORICO>()
                .Property(e => e.LecturaDemanda)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Sectores>()
                .HasMany(e => e.GestionRefacturaMae)
                .WithRequired(e => e.Sectores)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sectores>()
                .HasMany(e => e.Usuario_Sector)
                .WithRequired(e => e.Sectores)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SoporteRefactura>()
                .Property(e => e.nisrad)
                .HasPrecision(7, 0);

            modelBuilder.Entity<SoporteRefactura>()
                .Property(e => e.ubicacion)
                .IsUnicode(false);

            modelBuilder.Entity<SoporteRefactura>()
                .Property(e => e.LectDemandaFact)
                .HasPrecision(18, 3);

            modelBuilder.Entity<SoporteRefactura>()
                .Property(e => e.LectDemandaRefact)
                .HasPrecision(18, 3);

            modelBuilder.Entity<SoporteRefactura>()
                .Property(e => e.dbcr)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TipoArea>()
                .HasMany(e => e.Area)
                .WithRequired(e => e.TipoArea)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoOrigen>()
                .HasMany(e => e.GestionRefacturaMae)
                .WithRequired(e => e.TipoOrigen)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoRectificacion>()
                .HasMany(e => e.GestionRefacturaMae)
                .WithRequired(e => e.TipoRectificacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuarios>()
                .HasOptional(e => e.FirmaUsuario)
                .WithRequired(e => e.Usuarios);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.GestionRefacturaDet)
                .WithOptional(e => e.Usuarios)
                .HasForeignKey(e => e.IdUsuarioAutoriza);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.GestionRefacturaMae)
                .WithRequired(e => e.Usuarios)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.Usuario_Sector)
                .WithRequired(e => e.Usuarios)
                .WillCascadeOnDelete(false);
        }
    }
}
