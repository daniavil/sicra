namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CargosRefactura")]
    public partial class CargosRefactura
    {
        [Key]
        [StringLength(10)]
        public string CodCargo { get; set; }
    }
}
