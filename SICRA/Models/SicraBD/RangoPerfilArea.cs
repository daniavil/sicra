namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RangoPerfilArea")]
    public partial class RangoPerfilArea
    {
        [Key]
        public int IdRangoPerfil { get; set; }

        public int ValorIni { get; set; }

        public int? ValorFin { get; set; }

        public int IdPerfilArea { get; set; }

        public virtual PerfilesAreas PerfilesAreas { get; set; }
    }
}
