namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PerfilModulo")]
    public partial class PerfilModulo
    {
        [Key]
        public int IdRow { get; set; }

        public int? idModulo { get; set; }

        public int? IdPerfilResoRepo { get; set; }

        public virtual Modulo Modulo { get; set; }

        public virtual PerfilResolucionRepo PerfilResolucionRepo { get; set; }
    }
}
