namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class REPOANOMALIA_D
    {
        public long IdRow_M { get; set; }

        [Key]
        public long IdRow_D { get; set; }

        [StringLength(256)]
        public string UsuarioResuelve { get; set; }

        public long? LecturaActiva { get; set; }

        public long? LecturaReactiva { get; set; }

        public decimal? LecturaDemanda { get; set; }

        public string Resolucion { get; set; }

        public DateTime? FechaResolucion { get; set; }

        [StringLength(15)]
        public string Tipo { get; set; }

        public int Ciclo { get; set; }

        public bool esRespTec { get; set; }

        public virtual REPOANOMALIA_M REPOANOMALIA_M { get; set; }
    }
}
