namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GestionRefacturaDet")]
    public partial class GestionRefacturaDet
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GestionRefacturaDet()
        {
            DocGestionSoporteRefactura = new HashSet<DocGestionSoporteRefactura>();
        }

        [Key]
        public int IdGesRefDet { get; set; }

        public int IdGesRefMae { get; set; }

        public int? idPerfilAsignado { get; set; }

        public int? IdUsuarioAutoriza { get; set; }

        public int? idPerfilAutoriza { get; set; }

        public string ComentarioResuelve { get; set; }

        public DateTime FechaCreacion { get; set; }

        public DateTime? FechaHoraResuelto { get; set; }

        public bool? Denego { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DocGestionSoporteRefactura> DocGestionSoporteRefactura { get; set; }

        public virtual GestionRefacturaMae GestionRefacturaMae { get; set; }

        public virtual PerfilAutorizacion PerfilAutorizacion { get; set; }

        public virtual Usuarios Usuarios { get; set; }

        public virtual PerfilAutorizacion PerfilAutorizacion1 { get; set; }
    }
}
