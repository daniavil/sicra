namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PerfilResolucionRepo")]
    public partial class PerfilResolucionRepo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PerfilResolucionRepo()
        {
            PerfilModulo = new HashSet<PerfilModulo>();
            Rep_Operativa_M = new HashSet<Rep_Operativa_M>();
            REPOANOMALIA_M = new HashSet<REPOANOMALIA_M>();
            Usuarios = new HashSet<Usuarios>();
        }

        [Key]
        public int IdPerfilResoRepo { get; set; }

        [Required]
        [StringLength(20)]
        public string NombrePerfil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PerfilModulo> PerfilModulo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rep_Operativa_M> Rep_Operativa_M { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<REPOANOMALIA_M> REPOANOMALIA_M { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Usuarios> Usuarios { get; set; }
    }
}
