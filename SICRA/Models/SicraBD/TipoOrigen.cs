namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoOrigen")]
    public partial class TipoOrigen
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoOrigen()
        {
            GestionRefacturaMae = new HashSet<GestionRefacturaMae>();
        }

        [Key]
        public int idTipoOrigen { get; set; }

        [Required]
        public string nombre { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GestionRefacturaMae> GestionRefacturaMae { get; set; }
    }
}
