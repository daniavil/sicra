namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MuestraCritica")]
    public partial class MuestraCritica
    {
        public Guid idRow { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int nisrad { get; set; }

        public bool EsTotal { get; set; }

        public int? dial { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Ciclo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaLectura { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(20)]
        public string CodAnomaliaCritica { get; set; }

        [StringLength(500)]
        public string AnomaliaCritica { get; set; }

        [StringLength(20)]
        public string CodAnomaliaLectura { get; set; }

        [StringLength(500)]
        public string AnomaliaLectura { get; set; }

        public int? lecturaActiva { get; set; }

        public int? lecturaReactiva { get; set; }

        public decimal? lecturaDemanda { get; set; }

        [Required]
        [StringLength(50)]
        public string usuarioGenera { get; set; }

        [StringLength(50)]
        public string usuarioResuelve { get; set; }

        [StringLength(50)]
        public string usuarioResIncms { get; set; }

        public DateTime fechaRegistro { get; set; }

        public DateTime? fechaResolucion { get; set; }

        public bool Resuelto { get; set; }

        [StringLength(5)]
        public string P1AccionCorrecta { get; set; }

        public string P1DebioDe { get; set; }

        [StringLength(5)]
        public string P2ObservacionCorrecta { get; set; }

        public string P2DescipcionObs { get; set; }

        [StringLength(5)]
        public string P3EnSicra { get; set; }

        [StringLength(5)]
        public string P3HechoCorrecto { get; set; }

        public string P3Hacer { get; set; }

        [StringLength(5)]
        public string P4DebioRepoSicra { get; set; }

        public int? CaliAccion { get; set; }

        public int? CaliObservacion { get; set; }

        public int? CaliSicra { get; set; }

        public int? CaliRepo { get; set; }

        public int? Calificacion { get; set; }
    }
}
