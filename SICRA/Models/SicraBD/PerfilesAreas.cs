namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PerfilesAreas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PerfilesAreas()
        {
            RangoPerfilArea = new HashSet<RangoPerfilArea>();
        }

        [Key]
        public int IdPerfilArea { get; set; }

        public int IdArea { get; set; }

        public int idPerfil { get; set; }

        public virtual Area Area { get; set; }

        public virtual PerfilAutorizacion PerfilAutorizacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RangoPerfilArea> RangoPerfilArea { get; set; }
    }
}
