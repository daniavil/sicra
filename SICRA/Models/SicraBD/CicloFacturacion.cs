namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CicloFacturacion")]
    public partial class CicloFacturacion
    {
        [Key]
        public int idCiclo { get; set; }

        public int Ciclo { get; set; }

        public bool Actual { get; set; }
    }
}
