namespace SICRA.Models.SicraBD
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BitacoraCargaSoporte")]
    public partial class BitacoraCargaSoporte
    {
        public int Id { get; set; }

        public decimal nisrad { get; set; }

        [Required]
        [StringLength(250)]
        public string archivo { get; set; }

        [Required]
        public string ruta { get; set; }

        public DateTime fechaCarga { get; set; }

        [Required]
        [StringLength(20)]
        public string usuario { get; set; }
    }
}
