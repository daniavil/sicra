﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Models
{
    public class Parametros
    {
        public string UserName;
        public string tipoEntidad;
        public string IdPar;
    }

    public class ParamData
    {
        public string LectActAnt;
        public string LectReaAnt;
        public string Multi;
        public string NumAgu;
        public string LectActActual;
        public string LectReaActual;
        public string consumoAct;
        public string consumoRea;
    }
}