﻿using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
//using Epplus;

namespace SICRA.Views
{
    public partial class ConsultaDatosCliente : System.Web.UI.Page
    {
        private static readonly string TokenSys = ConfigurationManager.AppSettings["TokenSys"];
        private static readonly string wsData = ConfigurationManager.AppSettings["wsData"];
        private AdminLogin _usrLog = new AdminLogin();

        protected void Page_Load(object sender, EventArgs e)
        {
            //_usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            //_usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
            }
            else if (Request.Form["__EVENTTARGET"] == "MainContent_txtClave")
            {
                CargarDatosCli();
            }
        }
        protected void btnConsultaClave_Click(object sender, EventArgs e)
        {
            CargarDatosCli();
        }
        private void CargarDatosCli()
        {
            ClienteRoot cliente = CargarInfoCliente(txtClave.Text.Trim());

            if (cliente != null)
            {
                pnlDatos.Visible = true;

                //datos del suscriptor
                lblNisrad.Text = cliente.cliente.nis_rad;
                lblMedidor.Text = cliente.cliente.num_medidor;
                lblUbicacion.Text = cliente.cliente.ubicacion;
                lblTarifa.Text = cliente.cliente.nom_tarifa;
                lblCliente.Text = cliente.cliente.nom_cli;
                lblDireccion.Text = cliente.cliente.direccion;

                //informacion de facturacion. #1 - Datos de campo
                lblLeidoPor.Text = cliente.cliente.entidad_lector;
                lblFechaLect.Text = Convert.ToDateTime(cliente.cliente.fecha_hora_lectura).ToShortDateString();
                lblCodLectCampo.Text = cliente.cliente.cod_lectura_campo;
                lblAnomalia.Text = cliente.cliente.anomalia;
                lblConActCampo.Text = cliente.cliente.consumo_activo_campo;
                lblLectActCampo.Text = cliente.cliente.lectura_activa_campo;
                lblConReaCampo.Text = cliente.cliente.consumo_reactivo_campo;
                lblLectReaCampo.Text = cliente.cliente.lectura_reactiva_campo;
                //informacion de facturacion. #2 - Datos Actuales
                lblDiasFact.Text = cliente.cliente.consumo_activo_actual;
                lblCodLectActual.Text = cliente.cliente.cod_lectura_actual;
                lblConActActual.Text = cliente.cliente.consumo_activo_actual;
                lblLectActActual.Text = cliente.cliente.lectura_activa_actual;
                lblConReaActual.Text = cliente.cliente.consumo_reactivo_actual;
                lblLectReaActual.Text = cliente.cliente.lectura_reactiva_actual;
                //informacion de facturacion. #3 - Datos Anteriores
                lblConActAnt.Text = cliente.cliente.consumo_activo_anterior;
                lblLectActAnt.Text = cliente.cliente.lectura_activa_anterior;
                lblConReaAnt.Text = cliente.cliente.consumo_reactivo_anterior;
                lblLectReaAnt.Text = cliente.cliente.lectura_reactiva_anterior;

                //pestaña historico de lecturas
                gvLecturas.DataSource = null;
                gvLecturas.DataSource = cliente.ListaHistoricoLectura;
                gvLecturas.DataBind();

                //pestaña historico de consumos
                gvConsumos.DataSource = null;
                gvConsumos.DataSource = cliente.ListaHistoricoConsumo;
                gvConsumos.DataBind();

                //pestaña Calculo
                lblLectActAntCal.Text = cliente.cliente.lectura_activa_actual;
                lblLectReaAntCal.Text = cliente.cliente.lectura_reactiva_actual;
                lblMultiCal.Text = cliente.cliente.multiplicador;
                lblNumAgu.Text = cliente.cliente.num_agujas;

                //Div Imagenes
                literalControl.Text = cliente.ImagenesInnerText;
            }
            else
                pnlDatos.Visible = false;
        }
        private ClienteRoot CargarInfoCliente(string clave)
        {
            ClienteRoot clienteRoot = new ClienteRoot { cliente = new Cliente(), cliFotos = new List<CliFotos>(), respuesta = new Respuesta() };
            try
            {
                Parametros par = new Parametros();
                string JSONString = string.Empty;
                string encodebase64 = string.Empty;

                par.IdPar = clave;
                par.UserName = null;
                par.tipoEntidad = null;

                JSONString = JsonConvert.SerializeObject(par);
                encodebase64 = HttpServerUtility.UrlTokenEncode(Encoding.UTF8.GetBytes(Fun.encrypt(JSONString, TokenSys)));

                string URLWebAPI = wsData;
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URLWebAPI);
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync($"api/Consulta/GetData1/{encodebase64}").Result;

                if (response.IsSuccessStatusCode)
                {
                    string decodebase64 = "";

                    decodebase64 = JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result);
                    JSONString = Fun.decrypt(Encoding.UTF8.GetString(HttpServerUtility.UrlTokenDecode(decodebase64)), TokenSys);

                    clienteRoot.cliente = JsonConvert.DeserializeObject<Cliente>(JSONString);
                    clienteRoot.respuesta.error = clienteRoot.cliente == null ? true : false;
                    clienteRoot.respuesta.mensaje = clienteRoot.cliente == null ? "No se encontró ningún resultado asociado a la clave ingresada" : "ok";

                    if (!clienteRoot.respuesta.error)
                    {
                        //List<string> ImgFiles = new List<string>();
                        //List<string> ImgFilesDate = new List<string>();
                        List<imagePreview> imageUrlList = new List<imagePreview>();
                        List<PrintConfigPreviewViewModel> imageList = new List<PrintConfigPreviewViewModel>();

                        //BUSCAR POR CLAVE
                        if (!clienteRoot.cliente.clave.ToString().Equals(clienteRoot.cliente.nis_rad.ToString()))
                        {
                            response = client.GetAsync($"api/Consulta/GetData2/{clienteRoot.cliente.clave.ToString()}").Result;
                        }
                        else
                        {
                            //BUSCAR POR NIS_RAD
                            response = client.GetAsync($"api/Consulta/GetData2/{clienteRoot.cliente.nis_rad.ToString()}").Result;
                        }

                        if (response.IsSuccessStatusCode)
                        {
                            JSONString = JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result);
                            clienteRoot.cliFotos = JsonConvert.DeserializeObject<List<CliFotos>>(JSONString);

                            foreach (var (item, index) in clienteRoot.cliFotos.Select((v, i) => (v, i)))
                            {
                                try
                                {
                                    if (item.origen == "Logistics")
                                    {
                                        string path = Request.PhysicalApplicationPath;
                                        byte[] bytes = Convert.FromBase64String(item.imagen);

                                        MemoryStream ms = new MemoryStream(bytes);
                                        ms.Position = 0;
                                        imageList.Add(new PrintConfigPreviewViewModel() { Name = item.fecha, Picture = bytes, clave = item.clave });
                                    }
                                    else
                                    {
                                        byte[] img1 = System.IO.File.ReadAllBytes(item.imagen);
                                        imageList.Add(new PrintConfigPreviewViewModel() { Name = item.fecha, Picture = img1, clave = item.clave });
                                    }
                                }
                                catch (Exception ex)
                                {
                                    // comentado porque poniendo estos valores en null daba error
                                    //imageUrlList = null;
                                    //imageList = null;
                                }
                            }
                            imageUrlList = GuardarCopiaImagenes(imageList, clave);
                        }
                        else
                        {
                            clienteRoot.cliFotos = null;
                        }

                        //Consultar Lecturas
                        try
                        {
                            response = client.GetAsync($"api/Consulta/GetData3/{clienteRoot.cliente.nis_rad.ToString()}").Result;
                            if (response.IsSuccessStatusCode)
                            {
                                JSONString = JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result);
                                clienteRoot.ListaHistoricoLectura = JsonConvert.DeserializeObject<List<HistoricoLectura>>(JSONString);
                            }
                        }
                        catch (Exception)
                        {
                            clienteRoot.ListaHistoricoLectura = null;
                        }

                        //Consultar Consumos
                        try
                        {
                            response = client.GetAsync($"api/Consulta/GetData4/{clienteRoot.cliente.clave.ToString()}").Result;
                            if (response.IsSuccessStatusCode)
                            {
                                JSONString = JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result);
                                clienteRoot.ListaHistoricoConsumo = JsonConvert.DeserializeObject<List<HistoricoConsumo>>(JSONString);
                            }
                        }
                        catch (Exception)
                        {
                            clienteRoot.ListaHistoricoConsumo = null;
                        }

                        // retorna datos
                        //ViewData["myInnerHtml"] = GetImagenesMosaico(imageUrlList).Text;
                        clienteRoot.ImagenesInnerText = GetImagenesMosaico(imageUrlList).Text;
                        return clienteRoot;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public class PrintConfigPreviewViewModel
        {
            public string Name { get; set; }
            public byte[] Picture { get; set; }
            public string clave { get; set; }
        }
        public class imagePreview
        {
            public string Name { get; set; }
            public string url { get; set; }
        }
        public List<imagePreview> GuardarCopiaImagenes(List<PrintConfigPreviewViewModel> imagenes, string clave)
        {
            try
            {
                List<imagePreview> ImgFiles = new List<imagePreview>();
                //foreach (var item in imagenes)
                foreach (var (item, index) in imagenes.Select((v, i) => (v, i)))
                {
                    string path = Request.PhysicalApplicationPath;
                    string nombreArchivo = $"{path}ImgCli\\img{index}_{item.Name}_C{item.clave}.Jpeg";

                    //string resolvedUrl = System.Web.HttpContext.Current.Request.Url.Authority.ToString();//VirtualPathUtility.ToAbsolute("~/");

                    ImgFiles.Add(new imagePreview() { url = $"img{index}_{item.Name}_C{item.clave}.Jpeg", Name = $"img{index}_{item.Name}_C{item.clave}" });

                    Bitmap objBitMap = new Bitmap(byteArrayToImage(item.Picture));
                    objBitMap.Save(nombreArchivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                return ImgFiles;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
                ms.Position = 0;
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public LiteralControl GetImagenesMosaico(List<imagePreview> images)
        {
            LiteralControl html = new LiteralControl();

            string folderProject = Request.Url.AbsolutePath.ToString().Trim();

            string content = @"<div id=""galley"" runat=""server"" ClientIDMode=""static""><ul class='pictures'>";

            try
            {
                int Dividendo = images.Count(), divisor = 6, q, r, totalFilas = 0;
                q = (int)(Dividendo / divisor);
                r = Dividendo % divisor;
                totalFilas = q + (r > 0 ? 1 : 0);

                string imageneshtml = "";

                foreach (var (item, index) in images.Select((v, i) => (v, i)))
                {
                    if (item.url != "")
                    {
                        string path = HttpContext.Current.Request.Url.Authority.ToString();
                        //imageneshtml += $@"<li><img data-original='{item.Name}' src ='{folderProject}/ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                        imageneshtml += $@"<li><img data-original='{item.Name}' src ='../ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                    }
                }
                content += imageneshtml;
                content += " </ul></div>";
            }
            catch (Exception)
            {
                content = "<H3>SIN IMÁGENES</H3>";
            }
            
            html.Text = content;
            return html;
        }
        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            var data = new ParamData
            {
                LectActAnt= lblLectActAntCal.Text,
                LectReaAnt= lblLectReaAntCal.Text,
                Multi= lblMultiCal.Text,
                NumAgu= lblNumAgu.Text,
                LectActActual=txtLecActCal.Text,
                LectReaActual=txtLecReaCal.Text
            };
            var calculos = CalculaConsumo(data);
            lblConsumoActCal.Text = calculos.consumoAct;
            lblConsumoReaCal.Text = calculos.consumoRea;

            //tab1.Attributes["class"] = tab1.Attributes["class"].Replace("active", "").Trim();
            //tab4.Attributes["class"] = "active";

            //div4b.Attributes["class"] = "active";
        }
        public ParamData CalculaConsumo(ParamData parametro)
        {
            try
            {
                parametro.consumoAct = Calculo(int.Parse(parametro.NumAgu), decimal.Parse(parametro.Multi), long.Parse(parametro.LectActAnt), long.Parse(parametro.LectActActual)).ToString();
            }
            catch (Exception)
            {
                parametro.consumoAct = "N/A";
            }
            try
            {
                parametro.consumoRea = Calculo(int.Parse(parametro.NumAgu), decimal.Parse(parametro.Multi), long.Parse(parametro.LectReaAnt), long.Parse(parametro.LectReaActual)).ToString();
            }
            catch (Exception)
            {
                parametro.consumoRea = "N/A";
            }
            return parametro;
        }
        private decimal Calculo(int numAgu, decimal multi, long LectAnt, long LectActual)
        {
            long difLect;
            decimal consumo;

            difLect = LectActual - LectAnt;
            consumo = (difLect) * (multi);

            if (numAgu == 4)
            {
                if (difLect < 0)
                {
                    difLect = (10000 + difLect);
                    consumo = (difLect * multi);
                }
                else if (difLect > 10000)
                {
                    consumo = (difLect * multi);
                }

                if (difLect < 0)
                {
                    difLect = (90000 + difLect);
                    consumo = (difLect * multi);
                }
            }
            else
            {
                if (difLect < 0)
                {
                    difLect = (100000 + difLect);
                    consumo = (difLect * multi);
                }
                else if (difLect > 100000)
                {
                    consumo = (difLect * multi);
                }
                if (difLect < 0)
                {
                    difLect = (900000 + difLect);
                    consumo = (difLect * multi);
                }
            }
            return consumo;
        }
    }
}