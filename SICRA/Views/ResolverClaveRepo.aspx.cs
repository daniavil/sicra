﻿using ClosedXML.Excel;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Epplus;

namespace SICRA.Views
{
    public partial class ResolverClaveRepo : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private RepoAnomaliaController _rCtrl = new RepoAnomaliaController();
        private RegionesController _regCtrl = new RegionesController();
        private UsuarioController _uCtrl = new UsuarioController();
        public readonly string pathImgRepOpe = ConfigurationManager.AppSettings["pathImgRepOpe"].ToString();
        public readonly int repoDiaEspera = int.Parse(ConfigurationManager.AppSettings["repoDiaEspera"].ToString());
        private string _nombreArchivo = "";

        /**********************************************Eventos*************************************************/
        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                CargarDdl();

                string nis = string.Empty;
                string ciclo = string.Empty;

                nis = Request.QueryString.Get("n");

                if (!string.IsNullOrEmpty(nis))
                {
                    txtClave.Text = nis;
                    CargarReportes();
                }
            }
        }
        protected void gvRepoM_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string IdM = gvRepoM.DataKeys[e.Row.RowIndex].Value.ToString();
                GridView gvReDetails = e.Row.FindControl("gvRepoD") as GridView;

                var listaDetalle = _rCtrl.RepoDetailAnomaliaByIdM(long.Parse(IdM.Trim()));

                foreach (var item in listaDetalle)
                {
                    var usr = _uCtrl.GetUsuarioByUser(item.UsuarioResuelve);
                    item.UsuarioResuelve = usr != null ? usr.NomPersona : item.UsuarioResuelve;
                }

                gvReDetails.DataSource = listaDetalle;
                gvReDetails.DataBind();


                TableCell cellResuelto = e.Row.Cells[12];

                if (cellResuelto.Text == "1")
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].BackColor = Color.LightGreen;
                    }
                }
                else if (cellResuelto.Text == "0")
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].BackColor = Color.LightCoral;
                    }
                }
                else if (cellResuelto.Text == "2")
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#F4FA58");
                    }
                }
            }
        }
        protected void gvRepoM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToUpper() == "RESOLVER")
            {
                long id = long.Parse(e.CommandArgument.ToString());

                ReporteClaveDTO repoMaster = _rCtrl.GetRepoMaestroAnomaliaDTOById(id);

                hfIdM.Value = e.CommandArgument.ToString();
                txtNisrad.Text = repoMaster.Nis_Rad.ToString();
                txtLecActCampo.Text = repoMaster.LecturaActiva.ToString();
                txtLecReaCampo.Text = repoMaster.LecturaReactiva.ToString();
                txtLecDemCampo.Text = repoMaster.LecturaDemanda.ToString();
                txtLecActiva.Text = repoMaster.LecturaActiva.ToString();
                txtLecReactiva.Text = repoMaster.LecturaReactiva.ToString();
                txtLecDemanda.Text = repoMaster.LecturaDemanda.ToString();
                hfIdAnomalia.Value = repoMaster.IdAnomalia.ToString();
                txtAnomalia.Text = repoMaster.Anomalia.ToString();
                txtCausa.Text = repoMaster.Causa.ToString();
                txtMulti.Text = repoMaster.multiplicador;
                txtResolucion.Text = string.Empty;
                txtSolicitante.Text = repoMaster.Solicitante;
                spNumResol.InnerText = repoMaster.numResolucion;
                hfCiclo.Value = repoMaster.ciclo.ToString();
                txtTipoRepo.Text = repoMaster.TipoRepo;
                txtOs.Text = repoMaster.OS != null ? repoMaster.OS.ToString() : string.Empty;
                txtRespOS.Text = repoMaster.MsjOs;
                btnEspera.Visible = !repoMaster.EnEspera;
                mpResolver.Show();
            }
        }
        protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlRegion.SelectedIndex = 0;
        }
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSector.SelectedIndex = 0;
        }
        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            CargarReportes();
        }
        protected void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                FiltroObj filtros = null;

                if (txtClave.Text.Trim() == "" && ddlRegion.SelectedValue == ""
                    && ddlSector.SelectedValue == "" && ddlMedida.SelectedValue == ""
                    && ddlEstado.SelectedValue == "" && ddlAnomalia.SelectedValue == ""
                    && ddlDial.SelectedValue == "")
                {
                    filtros = null;
                }
                else
                {
                    filtros = new FiltroObj
                    {
                        Nisrad = txtClave.Text.Trim(),
                        Region = ddlRegion.SelectedValue,
                        Sector = ddlSector.SelectedValue,
                        Medida = ddlMedida.SelectedValue,
                        Resuelto = ddlEstado.SelectedValue,
                        idAnomalia = ddlAnomalia.SelectedValue,
                        Dial = ddlDial.SelectedValue
                    };
                }

                List<RepoExport> listaReportadas = _rCtrl.listaRepoMaestroAnomaliaReporte(_usrLog.usuSicra, filtros);


                string fileName = $"REP_{DateTime.Now.ToString("ddMMyyyy")}_CICLO_{_rCtrl.GetCicloFactura()}.xls";

                //ClosedXML method.
                //1. convert the list to datatable 
                DataTable dt = ToDataTable<RepoExport>(listaReportadas);
                //2. using the following code to export datatable to excel.
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", $"attachment;filename={fileName}");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            GuardarRegistro(1);
        }
        protected void btnEspera_Click(object sender, EventArgs e)
        {
            GuardarRegistro(2);
        }
        protected void gvRepoD_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TableCell cellResuelto = e.Row.Cells[7];

                if (cellResuelto.Text == "True")
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#81F7F3");
                    }
                }
                else
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].BackColor = System.Drawing.ColorTranslator.FromHtml("#F4FA58");
                    }
                }
            }
        }

        /***********************************************METDOS**********************************************/
        private void CargarDdl()
        {
            ddlRegion.DataSource = null;
            ddlRegion.DataSource = _regCtrl.GetRegionesByUsuario(_usrLog.user);
            ddlRegion.DataTextField = "Region";
            ddlRegion.DataValueField = "Region";
            ddlRegion.DataBind();
            ddlRegion.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlSector.DataSource = null;
            ddlSector.DataSource = _regCtrl.GetSectoresByUsuario(_usrLog.user);
            ddlSector.DataTextField = "Sector";
            ddlSector.DataValueField = "Sector";
            ddlSector.DataBind();
            ddlSector.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlMedida.DataSource = null;
            ddlMedida.DataSource = _regCtrl.GetMedidaByUsuario(_usrLog.user);
            ddlMedida.DataBind();
            ddlMedida.Items.Insert(0, new ListItem("--SELECCIONAR--",""));

            ddlPerfilReso.DataSource = null;
            ddlPerfilReso.DataSource = _usrLog.usuSicra.AdminUser ? _uCtrl.GetListaPerfilRepo() : _uCtrl.GetListaPerfilRepoById(_usrLog.usuSicra.IdPerfilResoRepo);
            ddlPerfilReso.DataTextField = "NombrePerfil";
            ddlPerfilReso.DataValueField = "IdPerfilResoRepo";
            ddlPerfilReso.DataBind();
            if (_usrLog.usuSicra.AdminUser)
            {
                ddlPerfilReso.Items.Insert(0, new ListItem("--TODOS--", ""));
            }
            ddlPerfilReso.SelectedIndex = 0;

            ddlDial.DataSource = null;
            ddlDial.Items.Clear();
            for (int i = 1; i <= 23; i++)
            {
                ListItem li = new ListItem(i.ToString(), i.ToString());
                ddlDial.Items.Add(li);
            }
            ddlDial.DataBind();
            ddlDial.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlAnomalia.DataSource = null;
            ddlAnomalia.DataSource = _rCtrl.GetAnomalias();
            ddlAnomalia.DataTextField = "descripcion";
            ddlAnomalia.DataValueField = "idLecturaAnomalia";
            ddlAnomalia.DataBind();
            ddlAnomalia.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlfechasRegistro.DataSource = null;
            ddlfechasRegistro.DataSource = _rCtrl.GetFechasRegistro("a");
            ddlfechasRegistro.DataBind();
            ddlfechasRegistro.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlFechavencimiento.DataSource = null;
            ddlFechavencimiento.DataSource = _rCtrl.GetFechasVencimiento("a");
            ddlFechavencimiento.DataBind();
            ddlFechavencimiento.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlUsuarioReporta.DataSource = null;
            ddlUsuarioReporta.DataSource = _rCtrl.GetUsuariosReporte();
            ddlUsuarioReporta.DataTextField = "NomPersona";
            ddlUsuarioReporta.DataValueField = "Usuario";
            ddlUsuarioReporta.DataBind();
            ddlUsuarioReporta.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlUsuarioResuelve.DataSource = null;
            ddlUsuarioResuelve.DataSource = _rCtrl.GetUsuariosRespuesta();
            ddlUsuarioResuelve.DataTextField = "NomPersona";
            ddlUsuarioResuelve.DataValueField = "Usuario";
            ddlUsuarioResuelve.DataBind();
            ddlUsuarioResuelve.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
        }
        private void CargarReportes()
        {
            try
            {
                FiltroObj filtros = null;

                if (txtClave.Text.Trim() == "" && ddlRegion.SelectedValue == ""
                    && ddlSector.SelectedValue == "" && ddlMedida.SelectedValue == ""
                    && ddlEstado.SelectedValue == "" && ddlAnomalia.SelectedValue == ""
                    && ddlDial.SelectedValue == "" && ddlPerfilReso.SelectedValue == ""
                    && ddlfechasRegistro.SelectedValue == "" && ddlFechavencimiento.SelectedValue == ""
                    && ddlUsuarioReporta.SelectedValue == "" && ddlUsuarioResuelve.SelectedValue == ""
                    && txtOsFiltro.Text.Trim() == "" && ddlTieneOS.SelectedValue == ""
                    && ddlVencido.SelectedValue == "")
                {
                    filtros = null;
                }
                else
                {
                    filtros = new FiltroObj
                    {
                        Nisrad = txtClave.Text.Trim(),
                        Region = ddlRegion.SelectedValue,
                        Sector = ddlSector.SelectedValue,
                        Medida = ddlMedida.SelectedValue,
                        Resuelto = ddlEstado.SelectedValue,
                        idAnomalia = ddlAnomalia.SelectedValue,
                        Dial = ddlDial.SelectedValue,
                        idPerfilReso = ddlPerfilReso.SelectedValue,
                        FechaRegistro = ddlfechasRegistro.SelectedValue,
                        FechaMaxResol = ddlFechavencimiento.SelectedValue,
                        UsuarioFacturacion = ddlUsuarioReporta.SelectedValue,
                        UsuarioResuelve = ddlUsuarioResuelve.SelectedValue,
                        OS = txtOsFiltro.Text.Trim(),
                        TieneOS = ddlTieneOS.SelectedValue,
                        Vencido = Convert.ToInt32(ddlVencido.SelectedValue)
                    };
                }

                List<RepoAnomaliaMaestroDto> listaReportadas = _rCtrl.listaRepoMaestroAnomalia(_usrLog.usuSicra, filtros);

                gvRepoM.DataSource = null;
                gvRepoM.DataSource = listaReportadas;
                gvRepoM.DataBind();

                spnNoResueltas.InnerText = listaReportadas.Where(r => r.Resuelto==0).Count().ToString();
                spnResueltas.InnerText = listaReportadas.Where(r => r.Resuelto==1).Count().ToString();
                spnEspera.InnerText = listaReportadas.Where(r => r.Resuelto == 2).Count().ToString();
            }
            catch (Exception ex)
            {
            }
        }
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
        private ImagenReporteAnomalia GuardarBitacoraSoporte(HttpPostedFile file, string indexClave,bool esImg)
        {
            try
            {
                _nombreArchivo = indexClave + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(file.FileName);

                string filePath = $"{pathImgRepOpe}\\{_nombreArchivo}";
                file.SaveAs(filePath);

                var imgRepoOpe = new ImagenReporteAnomalia
                {
                    nombre = _nombreArchivo,
                    ruta = filePath,
                    fechaCarga = DateTime.Now,
                    idRow = Guid.NewGuid(),
                    ciclo = int.Parse(hfCiclo.Value),
                    esImg = esImg
                };

                return imgRepoOpe;
            }
            catch (Exception ex)
            {
                hfError.Value = ex.Message + Environment.NewLine + hfError.Value;
                lblError.Text = hfError.Value;
                return null;
            }
        }
        void GuardarRegistro(int estado)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                try
                {
                    var repoMae = _rCtrl.GetRepoMaestroAnomaliaById(long.Parse(hfIdM.Value.ToString()));

                    repoMae.REPOANOMALIA_D.Add(new REPOANOMALIA_D
                    {
                        UsuarioResuelve = _usrLog.user,
                        LecturaActiva = string.IsNullOrEmpty(txtLecActiva.Text.Trim()) ? 0 : long.Parse(txtLecActiva.Text.Trim()),
                        LecturaReactiva = string.IsNullOrEmpty(txtLecReactiva.Text.Trim()) ? 0 : long.Parse(txtLecReactiva.Text.Trim()),
                        LecturaDemanda = string.IsNullOrEmpty(txtLecDemanda.Text.Trim()) ? 0 : decimal.Parse(txtLecDemanda.Text.Trim()),
                        Resolucion = txtResolucion.Text.Trim(),
                        FechaResolucion = DateTime.Now,//estado==1? DateTime.Now : ,
                        Tipo = txtTipoRepo.Text.Trim(),
                        IdRow_M = repoMae.IdRow_M,
                        Ciclo = repoMae.Ciclo,
                        esRespTec = true
                    });
                    repoMae.Resuelto = estado; //------------> nuevo estado para espera o finalizar

                    //cuando se pone en espera, se agregan N dias a la fecha maxima de resolucion.
                    if (estado == 2)
                    {
                        repoMae.FechaMaxResol = DateTime.Now.AddDays(repoDiaEspera);
                        repoMae.EnEspera = true;
                    }

                    //imagenes a reporte
                    var listaImg = new List<ImagenReporteAnomalia>();
                    try
                    {
                        foreach (var (item, index) in fUpload.PostedFiles.Select((v, i) => (v, i)))
                        {
                            string ext = Path.GetExtension(item.FileName).ToLower();

                            if (ext == ".jfif" || ext == ".png" || ext == ".jpg" || ext == ".jpeg" || ext == ".pdf")
                            {
                                string nom = ext == ".pdf" ? "File_" + index + "_" + txtNisrad.Text : "img" + index + "_" + txtNisrad.Text;
                                bool esImg = ext == ".pdf" ? false : true;

                                var guardaImg = GuardarBitacoraSoporte(item, nom, esImg);
                                guardaImg.IdRow_M = repoMae.IdRow_M;
                                listaImg.Add(guardaImg);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        hfError.Value = ex.Message + Environment.NewLine + hfError.Value;
                        lblError.Text = hfError.Value;
                    }

                    repoMae.ImagenReporteAnomalia = listaImg;

                    RespMsj resp = _rCtrl.PutReporteAnomalia(repoMae);
                    CargarReportes();
                }
                catch (Exception ex)
                {
                    hfError.Value = ex.Message + Environment.NewLine + hfError.Value;
                    lblError.Text = hfError.Value;
                }
            }
        }

    }
}