﻿using ClosedXML.Excel;
using DevExpress.XtraPrinting;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Epplus;

namespace SICRA.Views
{
    public partial class ConsultaClavesReportadas : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private ClienteController _cCtrl = new ClienteController();
        private LogisticsController _lCtrl = new LogisticsController();
        private RepoAnomaliaController _rCtrl = new RepoAnomaliaController();
        private RegionesController _regCtrl = new RegionesController();
        private UsuarioController _uCtrl = new UsuarioController();

        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                CargarDdl();

                string nis = string.Empty;
                string ciclo = string.Empty;

                nis = Request.QueryString.Get("n");

                if (!string.IsNullOrEmpty(nis))
                {
                    txtClave.Text = nis;
                    CargarReportes();
                }
            }
        }

        private void CargarDdl()
        {
            ddlRegion.DataSource = null;
            ddlRegion.DataSource = _regCtrl.GetRegionesByUsuario(_usrLog.user);
            ddlRegion.DataTextField = "Region";
            ddlRegion.DataValueField = "Region";
            ddlRegion.DataBind();
            ddlRegion.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlSector.DataSource = null;
            ddlSector.DataSource = _regCtrl.GetSectoresByUsuario(_usrLog.user);
            ddlSector.DataTextField = "Sector";
            ddlSector.DataValueField = "Sector";
            ddlSector.DataBind();
            ddlSector.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlMedida.DataSource = null;
            ddlMedida.DataSource = _regCtrl.GetMedidaByUsuario(_usrLog.user);
            ddlMedida.DataBind();
            ddlMedida.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlPerfilReso.DataSource = null;
            ddlPerfilReso.DataSource = _uCtrl.GetListaPerfilRepo();
            ddlPerfilReso.DataTextField = "NombrePerfil";
            ddlPerfilReso.DataValueField = "IdPerfilResoRepo";
            ddlPerfilReso.DataBind();
            ddlPerfilReso.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
            ddlPerfilReso.SelectedIndex = 0;

            ddlDial.DataSource = null;
            ddlDial.Items.Clear();
            for (int i = 1; i <= 23; i++)
            {
                ListItem li = new ListItem(i.ToString(), i.ToString());
                ddlDial.Items.Add(li);
            }
            ddlDial.DataBind();
            ddlDial.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlAnomalia.DataSource = null;
            ddlAnomalia.DataSource = _rCtrl.GetAnomalias();
            ddlAnomalia.DataTextField = "descripcion";
            ddlAnomalia.DataValueField = "idLecturaAnomalia";
            ddlAnomalia.DataBind();
            ddlAnomalia.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlfechasRegistro.DataSource = null;
            ddlfechasRegistro.DataSource = _rCtrl.GetFechasRegistro("a");
            //ddlfechasRegistro.DataTextFormatString = "{0:dd-MM-yyyy}";
            ddlfechasRegistro.DataBind();
            ddlfechasRegistro.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlFechavencimiento.DataSource = null;
            ddlFechavencimiento.DataSource = _rCtrl.GetFechasVencimiento("a");
            //ddlFechavencimiento.DataTextFormatString = "{0:dd-MM-yyyy}";
            ddlFechavencimiento.DataBind();
            ddlFechavencimiento.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlUsuarioReporta.DataSource = null;
            ddlUsuarioReporta.DataSource = _rCtrl.GetUsuariosReporte();
            ddlUsuarioReporta.DataTextField = "NomPersona";
            ddlUsuarioReporta.DataValueField = "Usuario";
            ddlUsuarioReporta.DataBind();
            ddlUsuarioReporta.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlUsuarioResuelve.DataSource = null;
            ddlUsuarioResuelve.DataSource = _rCtrl.GetUsuariosRespuesta();
            ddlUsuarioResuelve.DataTextField = "NomPersona";
            ddlUsuarioResuelve.DataValueField = "Usuario";
            ddlUsuarioResuelve.DataBind();
            ddlUsuarioResuelve.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
        }

        private void CargarReportes()
        {
            try
            {
                FiltroObj filtros = null;

                if (txtClave.Text.Trim() == "" && ddlRegion.SelectedValue == ""
                    && ddlSector.SelectedValue == "" && ddlMedida.SelectedValue == "" 
                    && ddlEstado.SelectedValue == "" && ddlAnomalia.SelectedValue == ""
                    && ddlDial.SelectedValue == "" && ddlPerfilReso.SelectedValue == ""
                    && ddlfechasRegistro.SelectedValue == "" && ddlFechavencimiento.SelectedValue == ""
                    && ddlUsuarioReporta.SelectedValue == "" && ddlUsuarioResuelve.SelectedValue == ""
                    && txtOsFiltro.Text.Trim() == "" && ddlTieneOS.SelectedValue == ""
                    && ddlVencido.SelectedValue == "")
                {
                    filtros = null;
                }
                else
                {
                    filtros = new FiltroObj
                    {
                        Nisrad = txtClave.Text.Trim(),
                        Region = ddlRegion.SelectedValue,
                        Sector = ddlSector.SelectedValue,
                        Medida = ddlMedida.SelectedValue,
                        Resuelto = ddlEstado.SelectedValue,
                        idAnomalia = ddlAnomalia.SelectedValue,
                        Dial = ddlDial.SelectedValue,
                        idPerfilReso = ddlPerfilReso.SelectedValue,
                        FechaRegistro = ddlfechasRegistro.SelectedValue,
                        FechaMaxResol = ddlFechavencimiento.SelectedValue,
                        UsuarioFacturacion = ddlUsuarioReporta.SelectedValue,
                        UsuarioResuelve = ddlUsuarioResuelve.SelectedValue,
                        OS = txtOsFiltro.Text.Trim(),
                        TieneOS = ddlTieneOS.SelectedValue,
                        Vencido = Convert.ToInt32(ddlVencido.SelectedValue)
                    };
                }

                List<RepoAnomaliaMaestroDto> listaReportadas = _rCtrl.listaRepoMaestroAnomalia(_usrLog.usuSicra, filtros);

                hfClaves.Value= string.Join(", ", listaReportadas.Select(x=>x.Nis_Rad));

                gvRepoM.DataSource = null;
                gvRepoM.DataSource = listaReportadas;
                gvRepoM.DataBind();

                spnNoResueltas.InnerText = listaReportadas.Where(r => r.Resuelto==0).Count().ToString();
                spnResueltas.InnerText = listaReportadas.Where(r => r.Resuelto==1).Count().ToString();
                spnEspera.InnerText = listaReportadas.Where(r => r.Resuelto == 2).Count().ToString();
            }
            catch (Exception ex)
            {
            }
        }

        protected void gvRepoM_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //string IdM = gvRepoM.DataKeys[e.Row.RowIndex].Value.ToString();
                    //GridView gvReDetails = e.Row.FindControl("gvRepoD") as GridView;

                    //var listaDetalle = _rCtrl.RepoDetailAnomaliaByIdM(long.Parse(IdM.Trim()));

                    //gvReDetails.DataSource = listaDetalle;
                    //gvReDetails.DataBind();

                    TableCell cellResuelto = e.Row.Cells[12];
                    TableCell cellResueltoString = e.Row.Cells[14];
                    if (cellResuelto.Text == "1")
                    {
                        cellResueltoString.BackColor = Color.LightGreen;
                    }
                    else if (cellResuelto.Text == "0")
                    {
                        cellResueltoString.BackColor = Color.LightCoral;
                    }
                    else if (cellResuelto.Text == "2")
                    {
                        cellResueltoString.BackColor = System.Drawing.ColorTranslator.FromHtml("#F4FA58");
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void gvRepoM_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CargarReportes();
            gvRepoM.PageIndex = e.NewPageIndex;
            gvRepoM.DataBind();
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            CargarReportes();
        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSector.SelectedIndex = 0;
        }

        protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlRegion.SelectedIndex = 0;
        }

        protected void btnExpoExcel_Click(object sender, EventArgs e)
        {
            try
            {
                FiltroObj filtros = null;
                if (txtClave.Text.Trim() == "" && ddlRegion.SelectedValue == ""
                    && ddlSector.SelectedValue == "" && ddlMedida.SelectedValue == ""
                    && ddlEstado.SelectedValue == "" && ddlAnomalia.SelectedValue == ""
                    && ddlDial.SelectedValue == "" && ddlPerfilReso.SelectedValue == ""
                    && ddlfechasRegistro.SelectedValue == "" && ddlFechavencimiento.SelectedValue == ""
                    && ddlUsuarioReporta.SelectedValue == "" && ddlUsuarioResuelve.SelectedValue == ""
                    && txtOsFiltro.Text.Trim() == "" && ddlTieneOS.SelectedValue == ""
                    && ddlVencido.SelectedValue == "")
                {
                    filtros = null;
                }
                else
                {
                    filtros = new FiltroObj
                    {
                        Nisrad = txtClave.Text.Trim(),
                        Region = ddlRegion.SelectedValue,
                        Sector = ddlSector.SelectedValue,
                        Medida = ddlMedida.SelectedValue,
                        Resuelto = ddlEstado.SelectedValue,
                        idAnomalia = ddlAnomalia.SelectedValue,
                        Dial = ddlDial.SelectedValue,
                        idPerfilReso = ddlPerfilReso.SelectedValue,
                        FechaRegistro = ddlfechasRegistro.SelectedValue,
                        FechaMaxResol = ddlFechavencimiento.SelectedValue,
                        UsuarioFacturacion = ddlUsuarioReporta.SelectedValue,
                        UsuarioResuelve = ddlUsuarioResuelve.SelectedValue,
                        OS = txtOsFiltro.Text.Trim(),
                        TieneOS = ddlTieneOS.SelectedValue,
                        Vencido = Convert.ToInt32(ddlVencido.SelectedValue)
                    };
                }

                var ds = _rCtrl.ListaRepoAnomaliaExport(_usrLog.usuSicra, filtros);
                exportToExcel(ds);
            }
            catch (Exception ex)
            {
            }
        }
        private void exportToExcel(DataSet ds)
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(ds);
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=ReporteAnomalia.xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);

                    Response.Flush();
                    Response.End();
                }
            }
        }
        void WriteDocumentToResponse(byte[] documentData, string format, bool isInline, string fileName)
        {
            string contentType;
            string disposition = (isInline) ? "inline" : "attachment";

            switch (format.ToLower())
            {
                case "xls":
                    contentType = "application/vnd.ms-excel";
                    break;
                case "xlsx":
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;
                case "mht":
                    contentType = "message/rfc822";
                    break;
                case "html":
                    contentType = "text/html";
                    break;
                case "txt":
                case "csv":
                    contentType = "text/plain";
                    break;
                case "png":
                    contentType = "image/png";
                    break;
                default:
                    contentType = String.Format("application/{0}", format);
                    break;
            }

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename={1}", disposition, fileName));
            Response.BinaryWrite(documentData);
            Response.End();
        }

        public class imagePreview
        {
            public string Name { get; set; }
            public string url { get; set; }
        }

        public class PrintConfigPreviewViewModel
        {
            public string Name { get; set; }
            public byte[] Picture { get; set; }
            public string clave { get; set; }
            public string fecha { get; set; }
        }

        protected void gvRepoM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            long id = long.Parse(e.CommandArgument.ToString());
            var r = _rCtrl.GetRepoAnomaliaDTO(id);

            if (r!=null)
            {
                //maestro
                spNisrad.InnerText = r.Nis_Rad.ToString();
                spCiclo.InnerText = r.Ciclo.ToString();
                txtUbicacion.Text = r.Ubicacion;
                txtRegion.Text = r.Region;
                txtSector.Text = r.Sector;
                txtMedida.Text = r.Medida;
                txtAnomalia.Text = r.Anomalia;
                txtResuelto.Text = r.ResueltoStng;
                txtLecActiva.Text = r.LecturaActiva.ToString();
                txtLecReactiva.Text=r.LecturaReactiva.ToString();
                txtLecDemanda.Text=r.LecturaDemanda.ToString();
                txtCiclo.Text=r.Ciclo.ToString();
                txtFechaRepo.Text=r.FechaRegistro.ToString();
                txtFechaMaxResol.Text=r.FechaMaxResol.ToString();
                txtCategoria.Text = r.categoriaResol;
                txtRepo.Text = r.tipo;
                txtDial.Text = r.dial.ToString();
                txtCausa.Text = r.SolcitudCausaVerificacion;
                txtOs.Text = r.OS != null ? r.OS.ToString() : string.Empty;
                txtRespOS.Text = r.MsjOs;

                //detalle
                gvRepoD.DataSource = r.repoAnomaliaDetalleDto; //listaDetalle;
                gvRepoD.DataBind();

                /**********************************IMAGENES**********************************/
                List<imagePreview> imageUrlList = new List<imagePreview>();
                List<PrintConfigPreviewViewModel> imageList = new List<PrintConfigPreviewViewModel>();

                foreach (var (item, index) in r.imagenReporteAnomalias.Where(x=>x.esImg==true).Select((v, i) => (v, i)))
                {
                    try
                    {
                        byte[] img1 = File.ReadAllBytes(item.ruta);
                        imageList.Add(new PrintConfigPreviewViewModel() { Name = item.nombre, Picture = img1, clave = r.Nis_Rad.ToString(),fecha=item.fechaCarga.ToString("ddMMyyyy") });
                    }
                    catch (Exception)
                    {
                        imageUrlList = null;
                        imageList = null;

                    }
                }

                if (imageList != null && imageUrlList != null)
                {
                    imageUrlList = GuardarCopiaImagenes(imageList);
                    literalControl.Text = GetImagenesMosaico(imageUrlList).Text;
                    lblmsjErrorImg.Visible = false;
                }
                else
                {
                    lblmsjErrorImg.Visible = true;
                    lblmsjErrorImg.Text = "Atención: Ocurrió un error al intentar cargar imágenes.";
                }
                /********************************************************************/

                var ListaArchivos = new List<ImagenReporteAnomalia>();
                /**********************************Grid Archivos**********************************/
                foreach (var (item, index) in r.imagenReporteAnomalias.Where(x => x.esImg == false).Select((v, i) => (v, i)))
                {
                    ListaArchivos.Add(item);
                }

                gvbitacora.DataSource = null;
                gvbitacora.DataSource = ListaArchivos;
                gvbitacora.DataBind();

                /********************************************************************/
                mpver.Show();
            }
        }

        public LiteralControl GetImagenesMosaico(List<imagePreview> images)
        {
            LiteralControl html = new LiteralControl();

            string folderProject = Request.Url.AbsolutePath.ToString().Trim();

            //string content = @"<div id=""galley"" runat=""server"" ClientIDMode=""static""><ul class='pictures'>";
            string content = @"<ul class='pictures'>";

            try
            {
                int Dividendo = images.Count(), divisor = 6, q, r, totalFilas = 0;
                q = (int)(Dividendo / divisor);
                r = Dividendo % divisor;
                totalFilas = q + (r > 0 ? 1 : 0);

                string imageneshtml = "";

                foreach (var (item, index) in images.Select((v, i) => (v, i)))
                {
                    if (item.url != "")
                    {
                        string path = HttpContext.Current.Request.Url.Authority.ToString();
                        //imageneshtml += $@"<li><img data-original='{item.Name}' src ='{folderProject}/ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                        imageneshtml += $@"<li><img data-original='{item.Name}' src ='../ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                    }
                }
                content += imageneshtml;
                content += " </ul>";
            }
            catch (Exception)
            {
                content = "<H3>SIN IMÁGENES</H3>";
            }
            
            html.Text = content;
            return html;
        }

        public List<imagePreview> GuardarCopiaImagenes(List<PrintConfigPreviewViewModel> imagenes)
        {
            try
            {
                List<imagePreview> ImgFiles = new List<imagePreview>();
                //foreach (var item in imagenes)
                foreach (var (item, index) in imagenes.Select((v, i) => (v, i)))
                {
                    string path = Request.PhysicalApplicationPath;
                    string nombreArchivo = $"{path}ImgCli\\img{index}_ope{item.clave}.Jpeg";

                    //string resolvedUrl = System.Web.HttpContext.Current.Request.Url.Authority.ToString();//VirtualPathUtility.ToAbsolute("~/");

                    ImgFiles.Add(new imagePreview() { url = $"img{index}_ope{item.clave}.Jpeg", Name = $"img{index}_ope{item.clave}_{item.fecha}" });

                    Bitmap objBitMap = new Bitmap(byteArrayToImage(item.Picture));
                    objBitMap.Save(nombreArchivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                return ImgFiles;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
                ms.Position = 0;
                System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
                return returnImage;
            }
            catch (Exception)
            {
                return null;
            }
        }

        protected void hlDownload_Command(object sender, CommandEventArgs e)
        {
            ReadPDFFile(e.CommandArgument.ToString(), e.CommandName.ToString());
        }

        public void ReadPDFFile(string path, string archivo)
        {
            try
            {
                WebClient client = new WebClient();
                Byte[] buffer = client.DownloadData(path);

                if (buffer != null)
                {
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-length", buffer.Length.ToString());
                    Response.AddHeader("content-disposition", $"attachment;filename={archivo}");
                    Response.BinaryWrite(buffer);
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "window.open('application/pdf','_newtab');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Error Message", "alert('" + ex.Message.ToString() + "')", true);
            }
        }
    }
}