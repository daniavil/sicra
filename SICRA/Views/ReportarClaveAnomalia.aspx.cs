﻿using SICRA.Controllers;
using SICRA.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SICRA.Models.SicraBD;
using System.Drawing;
using SICRA.Dto;
//using Epplus;

namespace SICRA.Views
{
    public partial class ReportarClaveAnomalia : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        int valMultiMedida = int.Parse(ConfigurationManager.AppSettings["Multi_Medida"].ToString());
        private ClienteController _cCtrl = new ClienteController();
        private LogisticsController _lCtrl = new LogisticsController();
        private RepoAnomaliaController _rCtrl = new RepoAnomaliaController();
        private UsuarioController _uCtrl = new UsuarioController();
        private SoeehController _sCtrl = new SoeehController();
        public readonly int diales12Cri = int.Parse(ConfigurationManager.AppSettings["dialiniCritica"]);
        public readonly int dialesrestaCri = int.Parse(ConfigurationManager.AppSettings["dialResCritica"]);
        public readonly int diales12Refa = int.Parse(ConfigurationManager.AppSettings["dialiniRefa"]);
        public readonly int dialesrestaRefa = int.Parse(ConfigurationManager.AppSettings["dialResRefa"]);
        public readonly int dialiniPostFact = int.Parse(ConfigurationManager.AppSettings["dialiniPostFact"]);
        public readonly int dialResPostFact = int.Parse(ConfigurationManager.AppSettings["dialResPostFact"]);
        public readonly int repoDiaEspera = int.Parse(ConfigurationManager.AppSettings["repoDiaEspera"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                ddlTipoRepo.DataSource = null;
                ddlTipoRepo.Items.Clear();
                ddlTipoRepo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
                ddlTipoRepo.Items.Insert(1, new ListItem("Crítica", "1"));
                ddlTipoRepo.Items.Insert(2, new ListItem("PostFacturación", "2"));
                ddlTipoRepo.Items.Insert(3, new ListItem("Refactura", "0"));
                ddlTipoRepo.SelectedIndex = 0;
            }
        }

        private void CargarDatosCli()
        {
            try
            {
                if (Convert.ToInt32(txtClave.Text.Trim()) > 0)
                {
                    ClienteRoot cliente = _cCtrl.GetInfoCliente(txtClave.Text.Trim());
                    string idTipoRepo = ddlTipoRepo.SelectedValue.Trim();

                    if (cliente != null)
                    {
                        var reporExiste = cliente.cliente.cicloActual != null ? _rCtrl.GetReporteAnomaliaByClave(decimal.Parse(cliente.cliente.nis_rad), int.Parse(cliente.cliente.cicloActual), int.Parse(ddlTipoRepo.SelectedValue)) : null;

                        Fun.ClearControls(this);
                        txtClave.Text = cliente.cliente.nis_rad;
                        ddlTipoRepo.SelectedValue = idTipoRepo;
                        btnReportar.Enabled = true;
                        //txtCicloFact.Text = cliente.cliente.cicloLectura;

                        hfClave390.Value = cliente.cliente.clave;
                        ddlAnomalia.DataSource = null;
                        ddlAnomalia.DataSource = _lCtrl.GetListaAnomalias();
                        ddlAnomalia.DataTextField = "descripcion";
                        ddlAnomalia.DataValueField = "idLecturaAnomalia";
                        ddlAnomalia.DataBind();
                        //ddlAnomalia.Items.Insert(0, new ListItem("", ""));

                        ddlDial.DataSource = null;
                        ddlDial.Items.Clear();
                        for (int i = 1; i <= 23; i++)
                        {
                            ListItem li = new ListItem(i.ToString(), i.ToString());
                            ddlDial.Items.Add(li);
                        }
                        ddlDial.DataBind();
                        ddlDial.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

                        ddlPerfilReso.DataSource = null;
                        ddlPerfilReso.DataSource = _uCtrl.GetListaPerfilRepo();
                        ddlPerfilReso.DataTextField = "NombrePerfil";
                        ddlPerfilReso.DataValueField = "IdPerfilResoRepo";
                        ddlPerfilReso.DataBind();
                        ddlPerfilReso.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
                        ddlPerfilReso.SelectedIndex = 0;


                        if (!string.IsNullOrEmpty(cliente.cliente.dial))
                        {
                            ddlDial.SelectedValue = cliente.cliente.dial;
                        }
                        GetCategoriaResolucion();

                        if (reporExiste != null)
                        {
                            lblRepe.Visible = true;
                            lblRepe.Text = "ATENCIÓN: Esta Clave ya ha sido reportada anteriormente.!!";
                            lblRepe.CssClass = "label label-danger";

                            ddlAnomalia.SelectedValue = reporExiste.Anomalia;
                            ddlDial.SelectedValue = reporExiste.dial.ToString();
                            hfIdM.Value = reporExiste.IdRow_M.ToString();
                            //txtCausa.Text = reporExiste.SolcitudCausaVerificacion;
                            txtUbicacion.Text = reporExiste.Ubicacion;
                            txtRegion.Text = reporExiste.Region;
                            txtSector.Text = reporExiste.Sector;
                            txtMedida.Text = reporExiste.Medida;
                            txtMultiplicador.Text = reporExiste.Multiplicador.ToString();
                            txtLecActiva.Text = reporExiste.LecturaActiva.ToString();
                            txtLecReactiva.Text = reporExiste.LecturaReactiva.ToString();
                            txtLecDemanda.Text = reporExiste.LecturaDemanda.ToString();
                            txtCicloFact.Text = reporExiste.Ciclo.ToString();
                            //ddlTipoRepo.SelectedValue = reporExiste.esCritica.ToString();
                            ddlCategoria.SelectedValue = reporExiste.idCategoria.ToString();
                            txtFechaRepo.Text = reporExiste.FechaRegistro.Value.ToShortDateString();
                            txtFechaResol.Text = reporExiste.FechaMaxResol.Value.ToString();// .ToShortDateString();
                            txtOs.Text = reporExiste.OS != null ? reporExiste.OS.ToString() : string.Empty;
                            hfMsjOs.Value = reporExiste.MsjOS;
                            CargarRespuestas(reporExiste.IdRow_M);
                            //ddlTipoRepo.SelectedIndex = 0;
                            ChkGenOs.Checked = false;

                            if (reporExiste.OS == null)
                            {
                                pnlGenOS.Visible = true;
                            }
                            else
                            {
                                pnlGenOS.Visible = false;
                            }
                            return;
                        }
                        else
                        {
                            lblRepe.Text = "Clave válida.";
                            lblRepe.CssClass = "label label-success";
                            gvRepoD.DataSource = null;
                            gvRepoD.DataBind();

                            txtOs.Text = string.Empty;
                            hfMsjOs.Value = null;
                            pnlGenOS.Visible = true;
                            hfIdM.Value = "0";
                            txtUbicacion.Text = cliente.cliente.ubicacion;
                            txtRegion.Text = cliente.cliente.region;
                            txtSector.Text = cliente.cliente.area;
                            txtMedida.Text = decimal.Parse(cliente.cliente.multiplicador) >= valMultiMedida ? "Medida Especial" : "Medida Directa";
                            txtMultiplicador.Text = cliente.cliente.multiplicador;
                            txtLecActiva.Text = cliente.cliente.lectura_activa_campo = !string.IsNullOrEmpty(cliente.cliente.lectura_activa_campo) ? cliente.cliente.lectura_activa_campo : "0";
                            txtLecReactiva.Text = cliente.cliente.lectura_reactiva_campo = !string.IsNullOrEmpty(cliente.cliente.lectura_reactiva_campo) ? cliente.cliente.lectura_reactiva_campo : "0";
                            txtLecDemanda.Text = cliente.cliente.lectura_demanda_actual = !string.IsNullOrEmpty(cliente.cliente.lectura_demanda_actual) ? cliente.cliente.lectura_demanda_actual : "0.000";
                        }

                        if (int.Parse(cliente.cliente.cicloLectura) == 9999)
                        {
                            lblRepe.Visible = true;
                            lblRepe.Text = lblRepe.Text + Environment.NewLine + $"ATENCIÓN: No existe historico de lectura de la clave ingresada.";
                            txtLecActiva.Text = "0";
                            txtLecReactiva.Text = "0";
                            txtLecDemanda.Text = "0.000";
                            txtCicloFact.Text = $"{DateTime.Now.ToString("yy")}{DateTime.Now.Month.ToString("00")}";
                            lblRepe.CssClass = "label label-danger";
                        }
                        else if (int.Parse(cliente.cliente.cicloLectura) < int.Parse(cliente.cliente.cicloActual))
                        {
                            lblRepe.Visible = true;
                            lblRepe.Text = lblRepe.Text + Environment.NewLine + $"ATENCIÓN: No existe lectura para el ciclo actual ({cliente.cliente.cicloActual})";
                            txtLecActiva.Text = "0";
                            txtLecReactiva.Text = "0";
                            txtLecDemanda.Text = "0.000";
                            txtCicloFact.Text = $"{cliente.cliente.cicloActual}";
                            lblRepe.CssClass = "label label-danger";
                        }
                        else
                        {
                            txtCicloFact.Text = cliente.cliente.cicloActual;
                            lblRepe.Text = "Clave válida.";
                            lblRepe.CssClass = "label label-success";
                        }
                        CalculoFechaByTipoRepo();


                        //SOLICITUD GLPI 27828 - DARWIN GARCÍA -------------------------------------
                        if (decimal.Parse(cliente.cliente.multiplicador) >= valMultiMedida)
                        {
                            pnlGenOS.Visible = false;
                        }
                        else
                            pnlGenOS.Visible = true;
                        //FIN SOLICITUD GLPI 27828 - DARWIN GARCÍA -------------------------------------
                    }
                    else
                    {
                        Fun.ClearControls(this);
                        lblRepe.Visible = true;
                        lblRepe.Text = "ATENCIÓN: Esta Clave NO existe.!";
                        lblRepe.CssClass = "label label-danger";
                        Fun.ClearControls(this);
                    }
                }
                else
                {
                    lblRepe.Visible = true;
                    lblRepe.Text = "Error en entrada de clave";
                    lblRepe.CssClass = "label label-danger";
                    Fun.ClearControls(this);
                }
            }
            catch (Exception ex)
            {
                lblRepe.Visible = true;
                lblRepe.Text = ex.Message;
                lblRepe.CssClass = "label label-danger";
                Fun.ClearControls(this);
            }
        }

        protected void btnReportar_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                GuardarReporteAnomalia();
            }
        }

        protected void ddlDial_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string tipoRepo = ddlTipoRepo.SelectedValue.ToString();

                txtFechaRepo.Text = DateTime.Now.ToShortDateString();

                //diales iniciales (1 y 2)
                if (int.Parse(ddlDial.SelectedValue) < 3)
                {
                    //txtFechaResol.Text = DateTime.Now.AddDays(diales12).ToShortDateString();
                    //selected refactura
                    if (tipoRepo == "0")
                    {
                        txtFechaResol.Text = _rCtrl.GetFechaResolucion(0,diales12Refa).ToShortDateString();
                    }
                    //selected critica
                    else if (tipoRepo == "1")
                    {
                        txtFechaResol.Text = _rCtrl.GetFechaResolucion(1,diales12Cri).ToShortDateString();
                    }
                    //selected Post Facturacion
                    else if (tipoRepo == "2")
                    {
                        txtFechaResol.Text = _rCtrl.GetFechaResolucion(2,dialiniPostFact).ToShortDateString();
                    }
                }
                //diales restantes (>3)
                else
                {
                    //txtFechaResol.Text = DateTime.Now.AddDays(dialesrestantes).ToShortDateString();
                    //selected refactura
                    if (tipoRepo == "0")
                    {
                        txtFechaResol.Text = _rCtrl.GetFechaResolucion(0,dialesrestaRefa).ToShortDateString();
                    }
                    //selected critica
                    else if (tipoRepo == "1")
                    {
                        txtFechaResol.Text = _rCtrl.GetFechaResolucion(1,dialesrestaCri).ToShortDateString();
                    }
                    //selected Post Facturacion
                    else if (tipoRepo == "2")
                    {
                        txtFechaResol.Text = _rCtrl.GetFechaResolucion(2,dialResPostFact).ToShortDateString();
                    }
                }
            }
            catch (Exception)
            {
                txtFechaRepo.Text = "";
                txtFechaResol.Text = "";
            }
        }

        private void CargarRespuestas(long idM)
        {
            var listaDetalle = _rCtrl.RepoDetailAnomaliaByIdM(idM);
            gvRepoD.DataSource = listaDetalle;
            gvRepoD.DataBind();
        }

        private void GetCategoriaResolucion()
        {
            var listaCategoria = _rCtrl.GetListaCateResolucion();
            ddlCategoria.DataSource = null;
            ddlCategoria.DataSource = listaCategoria;
            ddlCategoria.DataTextField = "descripcion";
            ddlCategoria.DataValueField = "idCategoria";
            ddlCategoria.DataBind();
        }

        protected void ddlTipoRepo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //CalculoFechaByTipoRepo();
            CargarDatosCli();
        }

        private void CalculoFechaByTipoRepo()
        {
            try
            {
                var ddltipoRepo2 = ddlTipoRepo;

                txtFechaRepo.Text = DateTime.Now.ToShortDateString();

                //diales iniciales (1 y 2)
                if (int.Parse(ddlDial.SelectedValue) < 3)
                {
                    //txtFechaResol.Text = DateTime.Now.AddDays(diales12).ToShortDateString();
                    //selected refactura
                    if (ddltipoRepo2.SelectedValue.ToString() == "0")
                    {
                        txtFechaResol.Text = _rCtrl.GetFechaResolucion(0,diales12Refa).ToShortDateString();
                    }
                    //selected critica
                    else if (ddltipoRepo2.SelectedValue.ToString() == "1")
                    {
                        txtFechaResol.Text = _rCtrl.GetFechaResolucion(1,diales12Cri).ToShortDateString();
                    }
                    //selected Post Facturacion
                    else if (ddltipoRepo2.SelectedValue.ToString() == "2")
                    {
                        txtFechaResol.Text = _rCtrl.GetFechaResolucion(2,dialiniPostFact).ToShortDateString();
                    }
                }
                //diales restantes (>3)
                else
                {
                    //txtFechaResol.Text = DateTime.Now.AddDays(dialesrestantes).ToShortDateString();
                    //selected refactura
                    if (ddltipoRepo2.SelectedValue.ToString() == "0")
                    {
                        txtFechaResol.Text = _rCtrl.GetFechaResolucion(0,dialesrestaRefa).ToShortDateString();
                    }
                    //selected critica
                    else if (ddltipoRepo2.SelectedValue.ToString() == "1")
                    {
                        txtFechaResol.Text = _rCtrl.GetFechaResolucion(1,dialesrestaCri).ToShortDateString();
                    }
                    //selected Post Facturacion
                    else if (ddltipoRepo2.SelectedValue.ToString() == "2")
                    {
                        txtFechaResol.Text = _rCtrl.GetFechaResolucion(2,dialResPostFact).ToShortDateString();
                    }
                }
            }
            catch (Exception ex)
            {
                txtFechaRepo.Text = "";
                txtFechaResol.Text = "";
            }
        }


        //protected void txtOs_TextChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (txtOs.Text.Trim() != "")
        //        {
        //            var Os = _sCtrl.GetExisteOS(int.Parse(txtOs.Text.Trim()));

        //            if (Os.Texto1 == "0")
        //            {
        //                lblOs.ForeColor = Color.Red;
        //            }
        //            else
        //            {
        //                lblOs.ForeColor = Color.Green;
        //            }
        //            lblOs.Text = Os.Texto2;
        //        }
        //        else
        //        {
        //            lblOs.ForeColor = Color.Orange;
        //            lblOs.Text = "Sin OS";
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        lblOs.ForeColor = Color.Red;
        //        lblOs.Text = "OS no valida";
        //    }
        //}

        private void GuardarReporteAnomalia()
        {
            try
            {
                RespMsj resp = null;

                var Os = GuardarOS();

                if (Os.CreaOs && !Os.Correcto)
                {
                    lblMsj.Text = "Ocurrió un problema al crear la OS, por lo cual no se creará el reporte en SICRA. Por favor vuelva a intentar crear el reporte. " 
                        + Environment.NewLine + $"Error: {Os.Resultadotext}";
                    lblMsj.Visible = true;
                    lblMsj.Attributes.Add("class", "label label-danger");
                }
                else
                {
                    var reporExiste = _rCtrl.GetReporteAnomaliaByClave(int.Parse(txtClave.Text.Trim()), int.Parse(txtCicloFact.Text.Trim()), int.Parse(ddlTipoRepo.SelectedValue));

                    if (reporExiste != null)
                    {
                        reporExiste.UsuarioFacturacion = _usrLog.user;
                        reporExiste.Anomalia = ddlAnomalia.SelectedValue.ToString();
                        reporExiste.SolcitudCausaVerificacion = reporExiste.SolcitudCausaVerificacion;// + Environment.NewLine + txtCausa.Text.Trim();
                        reporExiste.dial = int.Parse(ddlDial.SelectedValue.ToString());
                        reporExiste.Resuelto = 0;
                        reporExiste.FechaMaxResol = DateTime.Now.AddDays(repoDiaEspera); //DateTime.Now.AddDays(2); //DateTime.Parse(txtFechaResol.Text).AddHours(23).AddMinutes(59);---> aca
                        reporExiste.idCategoria = int.Parse(ddlCategoria.SelectedValue.ToString());
                        reporExiste.esCritica = int.Parse(ddlTipoRepo.SelectedValue.ToString());
                        reporExiste.IdPerfilResoRepo = int.Parse(ddlPerfilReso.SelectedValue.ToString());
                        reporExiste.OS = !Os.CreaOs ? (long?)null : (Os.Correcto ? Os.IdOS : (long?)null);
                        reporExiste.MsjOS = !Os.CreaOs ? null : Os.Resultadotext;
                        reporExiste.REPOANOMALIA_D = new List<REPOANOMALIA_D>{
                                new REPOANOMALIA_D
                                {
                                    IdRow_M=reporExiste.IdRow_M,
                                    UsuarioResuelve =_usrLog.user,
                                    LecturaActiva= !string.IsNullOrEmpty(txtLecActiva.Text) ? long.Parse(txtLecActiva.Text.Trim()) : (long?)null,
                                    LecturaReactiva=!string.IsNullOrEmpty(txtLecActiva.Text) ? long.Parse(txtLecReactiva.Text.Trim()) : (long?)null,
                                    LecturaDemanda = decimal.Parse(txtLecDemanda.Text.Trim()),
                                    Resolucion=txtCausa.Text.Trim(),
                                    FechaResolucion=DateTime.Now,
                                    Tipo=ddlTipoRepo.SelectedItem.Text,
                                    Ciclo =_rCtrl.GetCicloFactura(),
                                    esRespTec=false
                                }
                            };

                        resp = _rCtrl.PutReporteAnomalia(reporExiste);
                        CargarRespuestas(reporExiste.IdRow_M);
                    }
                    else
                    {
                        var ReporteClave = new REPOANOMALIA_M
                        {
                            IdRow_M = long.Parse(hfIdM.Value),
                            UsuarioFacturacion = _usrLog.user,
                            Region = txtRegion.Text.Trim(),
                            Sector = txtSector.Text.Trim(),
                            Nis_Rad = decimal.Parse(txtClave.Text.Trim()),
                            Ubicacion = txtUbicacion.Text.Trim(),
                            Anomalia = ddlAnomalia.SelectedValue.ToString(),
                            SolcitudCausaVerificacion = txtCausa.Text.Trim(),
                            Multiplicador = decimal.Parse(txtMultiplicador.Text.Trim()),
                            Medida = txtMedida.Text.Trim(),
                            LecturaActiva = !string.IsNullOrEmpty(txtLecActiva.Text) ? long.Parse(txtLecActiva.Text.Trim()) : (long?)null,
                            LecturaReactiva = !string.IsNullOrEmpty(txtLecActiva.Text) ? long.Parse(txtLecReactiva.Text.Trim()) : (long?)null,
                            LecturaDemanda = decimal.Parse(txtLecDemanda.Text.Trim()),
                            FechaRegistro = DateTime.Now,
                            dial = int.Parse(ddlDial.SelectedValue.ToString()),
                            Ciclo = _rCtrl.GetCicloFactura(),
                            Resuelto = 0,
                            FechaMaxResol = DateTime.Now.AddDays(repoDiaEspera), //.AddHours(23).AddMinutes(59),
                            esCritica = int.Parse(ddlTipoRepo.SelectedValue.ToString()),
                            idCategoria = int.Parse(ddlCategoria.SelectedValue.ToString()),
                            IdPerfilResoRepo = int.Parse(ddlPerfilReso.SelectedValue.ToString()),
                            OS = !Os.CreaOs ? (long?)null : (Os.Correcto ? Os.IdOS : (long?)null),
                            MsjOS = !Os.CreaOs ? null : Os.Resultadotext,
                            EnEspera = false,
                            REPOANOMALIA_D = new List<REPOANOMALIA_D>
                        {
                            new REPOANOMALIA_D
                            {
                                UsuarioResuelve =_usrLog.user,
                                LecturaActiva= !string.IsNullOrEmpty(txtLecActiva.Text) ? long.Parse(txtLecActiva.Text.Trim()) : (long?)null,
                                LecturaReactiva=!string.IsNullOrEmpty(txtLecActiva.Text) ? long.Parse(txtLecReactiva.Text.Trim()) : (long?)null,
                                LecturaDemanda = decimal.Parse(txtLecDemanda.Text.Trim()),
                                Resolucion=txtCausa.Text.Trim(),
                                FechaResolucion=DateTime.Now,
                                Tipo=ddlTipoRepo.SelectedItem.Text,
                                Ciclo=_rCtrl.GetCicloFactura(),
                                esRespTec=false
                            }
                        }
                        };
                        resp = _rCtrl.PutReporteAnomalia(ReporteClave);
                        CargarRespuestas(long.Parse(hfIdM.Value));
                    }

                    lblMsj.Visible = true;
                    lblMsj.Text = resp.mensaje + " - " + Os.Resultadotext;
                    if (resp.estado == "ok")
                    {
                        if (ChkGenOs.Checked)
                        {
                            if (Os.Correcto)
                            {
                                lblMsj.Attributes.Add("class", "label label-success");
                            }
                            else
                            {
                                lblMsj.Attributes.Add("class", "label label-warning");
                            }
                        }
                        else
                        {
                            lblMsj.Attributes.Add("class", "label label-success");
                        }
                        btnReportar.Visible = false;
                    }
                    else
                    {
                        lblMsj.Attributes.Add("class", "label label-danger");
                    }

                    txtOs.Text = Os.IdOS.ToString();
                    hfMsjOs.Value = Os.Resultadotext;
                }
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                lblMsj.Visible = true;
                lblMsj.Text = msj;
                lblMsj.Attributes.Add("class", "label label-danger");
            }
        }

        private OS_Obj GuardarOS()
        {
            var osCrea = new OS_Obj
            {
                Clave = int.Parse(hfClave390.Value),
                Comentario = txtCausa.Text.Trim(),
                UsuarioCrea = _usrLog.user,
                IdOS = string.IsNullOrWhiteSpace(txtOs.Text.Trim()) ? (int?)null : int.Parse(txtOs.Text.Trim()),
                Resultadotext = string.IsNullOrWhiteSpace(txtOs.Text.Trim()) ? null : hfMsjOs.Value.Trim(),
                Correcto = string.IsNullOrWhiteSpace(txtOs.Text.Trim()) ? false : true
            };

            if (ChkGenOs.Checked)
            {
                var os = _sCtrl.PutOSSOEEH(osCrea);
                os.CreaOs = true;
                return os;
            }
            else
            {
                osCrea.CreaOs = false;
                return osCrea;
            }
        }

        
    }
}