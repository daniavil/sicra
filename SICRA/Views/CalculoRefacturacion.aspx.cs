﻿using SICRA.Controllers;
using SICRA.Models;
using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SICRA.Models.SicraBD;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json;
using SICRA.Views.Reportes;
using DevExpress.XtraPrinting;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Drawing;
using Image = System.Drawing.Image;
using SICRA.Dto;
using System.Globalization;

namespace SICRA.Views
{
    public partial class CalculoRefacturacion : System.Web.UI.Page
    {
        #region ***************************Variables*************************************************
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private RefacturaController _rCtrl = new RefacturaController();
        #endregion

        #region ***************************Eventos*************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //SetInitialRow();
            }
        }

        protected void txtClave_TextChanged(object sender, EventArgs e)
        {
            //Fun.ClearControls(this);
            CargarCiclosFacturados();
        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            CargarDDLLecturas();
            Reproceso();
            //FillGridCorregido();
        }

        protected void gvEnergia_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var r = e.Row;
            CalculaColumDias(r);
            tbInfoMesAnt.Visible = true;
        }

        protected void gvEnergia_DataBound(object sender, EventArgs e)
        {
            //CalculosPieGrid(sender);
        }

        protected void ddlLecAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlLecAct.SelectedValue == "manual")
            {
                hfMetodo.Value = "M";
            }
            else
            {
                hfMetodo.Value = "A";
            }
            SetEntradasLecturas(hfMetodo.Value);
        }

        protected void btnCalActivaGrid_Click(object sender, EventArgs e)
        {
            hfMetodo.Value = "M";
            SetEntradasLecturas(hfMetodo.Value);
        }

        protected void gvFacturado_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var imp = FillGridImportes(e.Row);
                GridView gvFacturadoDet = e.Row.FindControl("gvFacturadoDet") as GridView;

                if (imp != null)
                {
                    e.Row.Cells[9].Text = imp.JsonImp;
                    gvFacturadoDet.DataSource = null;
                    gvFacturadoDet.DataSource = imp.ImpFact;
                    gvFacturadoDet.DataBind();
                }
            }
        }

        protected void gvCorrecion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var imp = CalculaRowCorreccionDetalle(e.Row);
                if (imp != null)
                {
                    GridView gvCorrecionDet = e.Row.FindControl("gvCorrecionDet") as GridView;
                    gvCorrecionDet.DataSource = null;
                    gvCorrecionDet.DataSource = imp;
                    gvCorrecionDet.DataBind();
                    e.Row.Cells[6].Text = gvCorrecionDet.Rows.Cast<GridViewRow>().Sum(r => double.Parse(r.Cells[2].Text)).ToString();
                }
            }
        }

        protected void lnkNewCargo_Click(object sender, EventArgs e)
        {
            var listaImportes = _rCtrl.GetImportesRefa();
            ddlNewImporte.DataSource = null;
            ddlNewImporte.DataSource = listaImportes;
            ddlNewImporte.DataTextField = "desc_cod";
            ddlNewImporte.DataValueField = "co_concepto";
            ddlNewImporte.DataBind();

            mpUpdCargo.Show();
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "goCorregido", "$('#third_tab').trigger('click');", true);
        }

        protected void lbAddNewCrgo_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
            mpUpdCargo.Show();
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(hfMetodo.Value))
            {
                Reproceso();
            }
            else
            {
                SetEntradasLecturas(hfMetodo.Value);
            }
            //FillGridCorregido();
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "goCorregido", "$('#third_tab').trigger('click');", true);
        }

        protected void gvCorrecionDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    TableCell cellEsNuevo = e.Row.Cells[3];
                    if (cellEsNuevo.Text == "1")
                    {
                        for (int i = 0; i < e.Row.Cells.Count; i++)
                        {
                            e.Row.Cells[i].BackColor = Color.LightCoral;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        #region ***************************Métodos*************************************************
        private void CargarCiclosFacturados()
        {
            try
            {
                var obj = _rCtrl.GetCiclosFacturadosYMultiByNis(int.Parse(txtClave.Text.Trim()));

                if (obj.periodo.Count > 0)
                {
                    ddlCiclos.DataSource = null;
                    ddlCiclos.Items.Clear();
                    ddlCiclos.DataSource = obj.periodo;
                    ddlCiclos.DataBind();
                    lblMultip.Text = obj.multiplicador.ToString();
                }
            }
            catch (Exception ex)
            {
                lblMsjError.Visible = true;
                lblMsjError.Text = "Error: " + ex.Message;
            }
        }

        protected void CargarDDLLecturas()
        {
            try
            {
                int PeriodoAnterior = _rCtrl.GetCicloAnterior(int.Parse(hfCicloIni.Value));
                var filtros = new FiltrosLectPeriodo
                {
                    Nisrad = int.Parse(txtClave.Text),
                    Ciclo = PeriodoAnterior
                };
                var infoCiclo = _rCtrl.GetLecturaByPeriodo(filtros);

                //LLenar DDL Lect Activa
                ddlLecAct.DataSource = null;
                var lA = infoCiclo.Where(x => x.Tip_csmo.Equals("CO011")).ToList();
                ddlLecAct.DataSource = lA;
                ddlLecAct.DataTextField = "LectTipo";
                ddlLecAct.DataValueField = "Lect";
                ddlLecAct.DataBind();
                if (lA.Count > 1)
                {
                    ddlLecAct.BackColor = Color.LightCoral;
                }
                ddlLecAct.Items.Insert(0, new ListItem("--SELECCIONAR--"));
                ddlLecAct.Items.Insert((ddlLecAct.Items.Count - 1), new ListItem("Valor Manual", "manual"));

                //LLenar DDL Lect Reactiva
                ddlLecRea.DataSource = null;
                var lR = infoCiclo.Where(x => x.Tip_csmo.Equals("CO015")).ToList();
                ddlLecRea.DataSource = lR;
                ddlLecRea.DataTextField = "LectTipo";
                ddlLecRea.DataValueField = "Lect";
                ddlLecRea.DataBind();
                if (lR.Count > 1)
                {
                    ddlLecRea.BackColor = Color.LightCoral;
                }
                ddlLecRea.Items.Insert(0, new ListItem("--SELECCIONAR--"));
                ddlLecRea.Items.Insert((ddlLecRea.Items.Count - 1), new ListItem("Valor Manual", "manual"));
            }
            catch (Exception ex)
            {
                lblMsjError.Visible = true;
                lblMsjError.Text = ex.Message;
            }



        }

        private void GetLecturasMesAnterior()
        {
            try
            {
                int PeriodoAnterior = _rCtrl.GetCicloAnterior(int.Parse(hfCicloIni.Value));
                var datosGrid = _rCtrl.GetDatosRefactura(int.Parse(txtClave.Text.Trim()), PeriodoAnterior.ToString());
                var PrimerFila = datosGrid.Take(1).FirstOrDefault();

                lblFechaAnterior.Text = string.Concat(PrimerFila.ffactura.Day, "/", PrimerFila.ffactura.Month, "/", PrimerFila.ffactura.Year);
                lblConActAnt.Text = PrimerFila.consumoactivafact.ToString();
                lblConReaAnt.Text = PrimerFila.consumoreactivafact.ToString();
            }
            catch (Exception ex)
            {
                lblMsjError.Visible = true;
                lblMsjError.Text = "Error en GetLecturasMesAnterior" + ex.Message;
            }
        }

        private void Calcularvalores()
        {
            try
            {
                char[] delimiters = new char[] { ',' };
                string[] ciclos = hfCiclos.Value.Trim().Split(delimiters);
                var periodos = "'" + string.Join("','", ciclos.Select(x => x.ToString()).ToArray()) + "'";

                if (cblCiclos.Items.Count < 1)
                {
                    foreach (var c in ciclos)
                    {
                        ListItem item = new ListItem();
                        item.Text = c;
                        item.Value = c;
                        cblCiclos.Items.Add(item);
                    }
                    cblCiclos.DataBind();
                }

                txtCiclos.Text = hfCiclos.Value.Trim();
                txtCiclos.Visible = true;
                ddlCiclos.Visible = false;
                lblCicloIni.Text = hfCicloIni.Value;
                lblCicloFin.Text = hfCicloFin.Value;
                txtClave.ReadOnly = true;

                //valores activa
                int lectActAct = int.Parse(txtLecActFinCdr.Text.Trim());
                int lectActAnt = int.Parse(txtLecActIniCdr.Text.Trim());
                int res = 0;
                res = (lectActAct - lectActAnt) * int.Parse(Math.Round(decimal.Parse(lblMultip.Text.Trim())).ToString());
                lblDifLectAct.Text = res.ToString();
                hfCdr.Value = (Convert.ToDecimal(res / decimal.Parse(hfCantDiasCdr.Value.Trim()))).ToString();
                lblCdr.Text = Math.Round(Convert.ToDecimal(res / decimal.Parse(hfCantDiasCdr.Value.Trim())), 2).ToString();

                try
                {
                    //valores Reactiva
                    int lectReacAct = int.Parse(txtLecReaFinCdr.Text.Trim());
                    int lectReacAnt = int.Parse(txtLecReaIniCdr.Text.Trim());
                    int resReac = 0;
                    resReac = (lectReacAct - lectReacAnt) * int.Parse(Math.Round(decimal.Parse(lblMultip.Text.Trim())).ToString());
                    lblDifLectRea.Text = resReac.ToString();
                    hfCdrReact.Value = (Convert.ToDecimal(resReac / decimal.Parse(hfCantDiasCdr.Value.Trim()))).ToString();
                    lblCdrReac.Text = Math.Round(Convert.ToDecimal(resReac / decimal.Parse(hfCantDiasCdr.Value.Trim())), 2).ToString();
                }
                catch (Exception)
                {
                }
                var datosGrid = _rCtrl.GetDatosRefactura(int.Parse(txtClave.Text.Trim()), periodos);
                FillGrids(datosGrid);
                lblMsjError.Visible = false;
            }
            catch (Exception ex)
            {
                lblMsjError.Visible = true;
                lblMsjError.Text = "Error: " + ex.Message;
            }
        }

        private void CalculaColumDias(GridViewRow r)
        {
            if (r.RowType == DataControlRowType.DataRow)
            {
                DateTime firstDateValue = DateTime.Parse(r.Cells[22].Text);
                DateTime secondDateValue = DateTime.Parse(r.Cells[21].Text);
                double dias = (secondDateValue.Date - firstDateValue.Date).TotalDays;
                r.Cells[4].Text = dias.ToString();
            }
        }

        private void CalculaCdrSI()
        {
            foreach (GridViewRow r in gvEnergia.Rows)
            {
                //tbInfoMesAnt.Visible = false;
                if (r.RowType == DataControlRowType.DataRow)
                {
                    decimal dias = Convert.ToInt32(r.Cells[4].Text);

                    ///------------------------------------Activa
                    //**********************Calcula Consumo Reconstruido************************/
                    decimal cdr = decimal.Parse(lblDifLectAct.Text) / decimal.Parse(hfCantDiasCdr.Value);
                    int diasXCdr = Convert.ToInt32(dias * cdr);
                    r.Cells[6].Text = diasXCdr.ToString();
                    //**********************************Calcula Diferencia***************************/
                    int difConsumo = int.Parse(r.Cells[6].Text) - int.Parse(r.Cells[5].Text);
                    r.Cells[7].Text = difConsumo.ToString();
                    ///------------------------------------Reactiva
                    //**********************Calcula Consumo Reconstruido************************/
                    cdr = decimal.Parse(lblDifLectRea.Text) / decimal.Parse(hfCantDiasCdr.Value);
                    diasXCdr = Convert.ToInt32(dias * cdr);
                    r.Cells[13].Text = diasXCdr.ToString();
                    //**********************************Calcula Diferencia***************************/
                    difConsumo = int.Parse(r.Cells[13].Text) - int.Parse(r.Cells[12].Text);
                    r.Cells[14].Text = difConsumo.ToString();
                    //**************Set Valores Lecturas Activa y reactiva no se calculan*************/
                    r.Cells[8].Text = "N/A";
                    r.Cells[15].Text = "N/A";
                }
            }
        }

        private void CalculaConsumosCdrNO()
        {
            int sumTotalDias = 0;
            for (int i = 0; i < gvEnergia.Rows.Count; i++)
            {
                sumTotalDias += int.Parse(gvEnergia.Rows[i].Cells[4].Text);
            }

            foreach (GridViewRow r in gvEnergia.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    r.Cells[8].Text = "*";
                    r.Cells[15].Text = "*";
                    decimal dias = Convert.ToInt32(r.Cells[4].Text);
                    //valores activa
                    decimal ActivaPorDia = (Convert.ToDecimal(lblDifLectAct.Text.Trim()) / Convert.ToDecimal(hfCantDiasCdr.Value) / Convert.ToDecimal(lblMultip.Text));
                    decimal ActivaPorDia_PorTotalDias = Math.Round(ActivaPorDia * sumTotalDias, 0);
                    decimal Unidades = Math.Round(((dias * ActivaPorDia_PorTotalDias) / sumTotalDias));
                    int consumoPrimerFila = Convert.ToInt32(Unidades * Convert.ToDecimal(lblMultip.Text));
                    r.Cells[6].Text = consumoPrimerFila.ToString();
                    int difConsumo = consumoPrimerFila - int.Parse(r.Cells[5].Text);
                    r.Cells[7].Text = difConsumo.ToString();
                    r.Cells[9].Text = Unidades.ToString();
                    r.Cells[10].Text = ActivaPorDia.ToString();

                    //valores reactiva
                    decimal ReactivaPorDia = (Convert.ToDecimal(lblDifLectRea.Text.Trim()) / Convert.ToDecimal(hfCantDiasCdr.Value) / Convert.ToDecimal(lblMultip.Text));
                    decimal ReactivaPorDia_PorTotalDias = Math.Round(ReactivaPorDia * sumTotalDias, 0);
                    decimal UnidadesReac = Math.Round(((dias * ReactivaPorDia_PorTotalDias) / sumTotalDias));
                    int consumoPrimerFilaReac = Convert.ToInt32(UnidadesReac * Convert.ToDecimal(lblMultip.Text));
                    r.Cells[13].Text = consumoPrimerFilaReac.ToString();
                    int difConsumoReact = consumoPrimerFilaReac - int.Parse(r.Cells[12].Text);
                    r.Cells[14].Text = difConsumoReact.ToString();
                    r.Cells[16].Text = UnidadesReac.ToString();
                    r.Cells[17].Text = ReactivaPorDia.ToString();
                }
            }
        }

        private void CalcularLecturasActivas(int lectAnt)
        {
            int LectAnterior = 0;
            foreach (GridViewRow r in gvEnergia.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    CalculaColumDias(r);
                    decimal Unidades = decimal.Parse(r.Cells[9].Text);

                    if (r.RowIndex == 0)
                    {
                        r.Cells[11].Text = (lectAnt + Unidades).ToString();
                    }
                    else
                    {
                        r.Cells[11].Text = (LectAnterior + Unidades).ToString();
                    }
                    LectAnterior = int.Parse(r.Cells[11].Text);
                    r.Cells[8].Text = r.Cells[11].Text;
                }
            }
        }

        private void CalcularLecturasReactivas(int lectAnt)
        {
            int LectAnterior = 0;
            foreach (GridViewRow r in gvEnergia.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    decimal Unidades = decimal.Parse(r.Cells[16].Text);

                    if (r.RowIndex == 0)
                    {
                        r.Cells[18].Text = (lectAnt + Unidades).ToString();
                    }
                    else
                    {
                        r.Cells[18].Text = (LectAnterior + Unidades).ToString();
                    }
                    LectAnterior = int.Parse(r.Cells[18].Text);
                    r.Cells[15].Text = r.Cells[18].Text;
                }
            }
        }

        private void CalculosPieGrid()
        {
            if (chkCalculaAlumbrado.Checked)
            {
                pnlCalculaAlumbrado.Visible = true;
            }
            else
            {
                pnlCalculaAlumbrado.Visible = false;
            }

            //***************************SUMAS Totales************************
            int sumTotalDias = 0, SumTotalConsumoFact = 0, SumTotalConsumoFactRec = 0, SumTotalDiferencia = 0,
                SumTotalConsumoFactReact = 0, SumTotalConsumoFactRecReact = 0, SumTotalDiferenciaReact = 0;
            for (int i = 0; i < gvEnergia.Rows.Count; i++)
            {
                //Activa
                sumTotalDias += int.Parse(gvEnergia.Rows[i].Cells[4].Text);
                SumTotalConsumoFact += int.Parse(gvEnergia.Rows[i].Cells[5].Text);
                SumTotalConsumoFactRec += int.Parse(gvEnergia.Rows[i].Cells[6].Text);
                SumTotalDiferencia += int.Parse(gvEnergia.Rows[i].Cells[7].Text);
                //reactiva
                SumTotalConsumoFactReact += int.Parse(gvEnergia.Rows[i].Cells[12].Text);
                SumTotalConsumoFactRecReact += int.Parse(gvEnergia.Rows[i].Cells[13].Text);
                SumTotalDiferenciaReact += int.Parse(gvEnergia.Rows[i].Cells[14].Text);
            }

            gvEnergia.FooterRow.Cells[0].Text = "0";
            gvEnergia.FooterRow.Cells[1].Text = "1";
            //gvEnergia.FooterRow.Cells[2].Text = "TOTALES: ";
            gvEnergia.FooterRow.Cells[2].Visible = false;

            gvEnergia.FooterRow.Cells[3].Text = "TOTALES: ";
            gvEnergia.FooterRow.Cells[3].Font.Bold = true;
            gvEnergia.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Center;
            //***************************Total días************************
            gvEnergia.FooterRow.Cells[4].Text = sumTotalDias.ToString();
            gvEnergia.FooterRow.Cells[4].Font.Bold = true;
            gvEnergia.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Center;
            //***************************Total Consumo Facturado************************
            gvEnergia.FooterRow.Cells[5].Text = SumTotalConsumoFact.ToString();
            gvEnergia.FooterRow.Cells[5].Font.Bold = true;
            gvEnergia.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Center;
            //***************************Total Consumo Reconstruido************************
            gvEnergia.FooterRow.Cells[6].Text = SumTotalConsumoFactRec.ToString();
            gvEnergia.FooterRow.Cells[6].Font.Bold = true;
            gvEnergia.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Center;
            //***************************Total Diferencia Consumo************************
            gvEnergia.FooterRow.Cells[7].Text = SumTotalDiferencia.ToString();
            gvEnergia.FooterRow.Cells[7].Font.Bold = true;
            gvEnergia.FooterRow.Cells[7].HorizontalAlign = HorizontalAlign.Center;

            gvEnergia.FooterRow.Cells[8].Text = "8";
            gvEnergia.FooterRow.Cells[9].Text = "9";
            gvEnergia.FooterRow.Cells[10].Text = "10";
            gvEnergia.FooterRow.Cells[11].Text = "11";
            gvEnergia.FooterRow.Cells[12].Text = SumTotalConsumoFactReact.ToString();
            gvEnergia.FooterRow.Cells[12].Font.Bold = true;
            gvEnergia.FooterRow.Cells[12].HorizontalAlign = HorizontalAlign.Center;
            gvEnergia.FooterRow.Cells[13].Text = SumTotalConsumoFactRecReact.ToString();
            gvEnergia.FooterRow.Cells[13].Font.Bold = true;
            gvEnergia.FooterRow.Cells[13].HorizontalAlign = HorizontalAlign.Center;
            gvEnergia.FooterRow.Cells[14].Text = SumTotalDiferenciaReact.ToString();
            gvEnergia.FooterRow.Cells[14].Font.Bold = true;
            gvEnergia.FooterRow.Cells[14].HorizontalAlign = HorizontalAlign.Center;
            gvEnergia.FooterRow.Cells[15].Text = "15";
            gvEnergia.FooterRow.Cells[16].Text = "16";
            gvEnergia.FooterRow.Cells[17].Text = "17";
            gvEnergia.FooterRow.Cells[18].Text = "18";

            if (chkCalculaAlumbrado.Checked)
            {
                sumTotalDias = 0;
                SumTotalConsumoFact = 0;
                SumTotalConsumoFactRec = 0;
                SumTotalDiferencia = 0;

                for (int i = 0; i < gvEnergia.Rows.Count - 1; i++)
                {
                    sumTotalDias += int.Parse(gvEnergia.Rows[i].Cells[4].Text);
                    SumTotalConsumoFact += int.Parse(gvEnergia.Rows[i].Cells[5].Text);
                    SumTotalConsumoFactRec += int.Parse(gvEnergia.Rows[i].Cells[6].Text);
                    SumTotalDiferencia += int.Parse(gvEnergia.Rows[i].Cells[7].Text);
                }
                lblTotDias.Text = sumTotalDias.ToString();
                lblFacturadoReal.Text = SumTotalConsumoFact.ToString();
                lblReconstruidoReal.Text = SumTotalConsumoFactRec.ToString();
            }
        }

        private void CalculoUltimaFila()
        {
            int NumFilasGrid = gvEnergia.Rows.Count;
            int UltimaFila = gvEnergia.Rows.Count - 1;
            int SumaKwhReconsMenosUltima = 0;
            int SumaKwhReconsMenosUltimaReact = 0;
            int sumTotalDias = 0;

            foreach (GridViewRow r in gvEnergia.Rows)
            {
                sumTotalDias += int.Parse(r.Cells[4].Text);
                if (chkCalculaAlumbrado.Checked)
                {
                    //primera fila
                    if (r.RowIndex == 0)
                    {
                    }
                    //última fila
                    else if (r.RowIndex == UltimaFila)
                    {
                        //Activa
                        r.Cells[6].Text = r.Cells[5].Text;
                        int difConsumo = int.Parse(r.Cells[6].Text) - int.Parse(r.Cells[5].Text);
                        r.Cells[7].Text = difConsumo.ToString();
                        //Reactiva
                        r.Cells[13].Text = r.Cells[12].Text;
                        difConsumo = int.Parse(r.Cells[13].Text) - int.Parse(r.Cells[12].Text);
                        r.Cells[14].Text = difConsumo.ToString();
                    }
                    //filas intermedias
                    else
                    {
                    }
                }
                else
                {

                    if (r.RowIndex == UltimaFila)
                    {
                        //Activa
                        decimal TotalKwhFactReconstruido = Math.Round((decimal.Parse(hfCdr.Value) * sumTotalDias), 0);
                        r.Cells[6].Text = (TotalKwhFactReconstruido - SumaKwhReconsMenosUltima).ToString();
                        r.Cells[7].Text = (int.Parse(r.Cells[6].Text) - int.Parse(r.Cells[5].Text)).ToString();

                        //Reactiva
                        TotalKwhFactReconstruido = Math.Round((decimal.Parse(hfCdrReact.Value) * sumTotalDias), 0);
                        r.Cells[13].Text = (TotalKwhFactReconstruido - SumaKwhReconsMenosUltimaReact).ToString();
                        r.Cells[14].Text = (int.Parse(r.Cells[13].Text) - int.Parse(r.Cells[12].Text)).ToString();
                    }
                    else
                    {
                        //Activa
                        r.Cells[7].Text = (int.Parse(r.Cells[6].Text) - int.Parse(r.Cells[5].Text)).ToString();
                        SumaKwhReconsMenosUltima += int.Parse(r.Cells[6].Text);
                        //Reactiva
                        r.Cells[14].Text = (int.Parse(r.Cells[13].Text) - int.Parse(r.Cells[12].Text)).ToString();
                        SumaKwhReconsMenosUltimaReact += int.Parse(r.Cells[13].Text);
                    }
                }
            }
        }

        private void FillGrids(List<DatosRefactura> lista)
        {
            gvEnergia.DataSource = null;
            gvEnergia.DataSource = lista;
            gvEnergia.DataBind();

            gvFacturado.DataSource = null;
            gvFacturado.DataSource = lista;
            gvFacturado.DataBind();
        }

        //private void FillGridCorregido()
        //{
        //    try
        //    {
        //        var listaM = new List<DatosRefactura>();
        //        foreach (GridViewRow r in gvEnergia.Rows)
        //        {
        //            if (r.RowType == DataControlRowType.DataRow)
        //            {
        //                listaM.Add(new DatosRefactura
        //                {
        //                    nisrad = txtClave.Text.Trim(),
        //                    periodofacturado = r.Cells[1].Text.ToString(),
        //                    ffactura = DateTime.Parse(r.Cells[21].Text.ToString()),
        //                    nombretarifa = r.Cells[20].Text.ToString(),
        //                    consumoactivafact = r.Cells[6].Text.ToString(),
        //                    consumoreactivafact = r.Cells[13].Text.ToString(),
        //                    totalimporte = "----RECALCULADA----",
        //                    codtarifa = r.Cells[19].Text.ToString()
        //                });
        //            }
        //        }

        //        gvCorrecion.DataSource = null;
        //        gvCorrecion.DataSource = listaM;
        //        gvCorrecion.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        lblMsjError.Visible = true;
        //        lblMsjError.Text = "Error en FillGridCorregido" + ex.ToString();
        //    }
        //}

        private ImportesFact FillGridImportes(GridViewRow r)
        {
            var filtro = new FiltrosImportes
            {
                Nisrad = int.Parse(r.Cells[1].Text.ToString()),
                Ciclo = int.Parse(r.Cells[2].Text.ToString()),
                FechaFact = int.Parse(r.Cells[10].Text.ToString())
            };

            var listImp = _rCtrl.GetImportes(filtro);


            var imp = new ImportesFact
            {
                ImpFact = listImp,
                JsonImp = JsonConvert.SerializeObject(listImp),
                JsonFiltro = JsonConvert.SerializeObject(filtro)
            };
            return imp;
        }

        private List<DtoCalculoCorregido> CalculaRowCorreccionDetalle(GridViewRow r)
        {
            try
            {
                var filtros = new FiltroCalculoConceptos();

                int periodo = int.Parse(r.Cells[1].Text);

                filtros.Nisrad = int.Parse(txtClave.Text.Trim());
                filtros.CodTarifa = r.Cells[7].Text;
                filtros.CsmoAct = int.Parse(r.Cells[4].Text);
                filtros.PeriodoAct = periodo;
                filtros.FechaFactInt = int.Parse(DateTime.Parse(r.Cells[2].Text).ToString("yyyyMMdd"));
                List<DtoCalculoCorregido> c;
                if (r.RowIndex == 0)
                {
                    int periodoAnt = _rCtrl.GetCicloAnterior(int.Parse(hfCicloIni.Value));
                    filtros.CicloAnt = int.Parse(periodoAnt.ToString().Substring(2, 2) + periodoAnt.ToString().Substring(4, 2));
                    filtros.CsmoAnt = int.Parse(lblConActAnt.Text);
                    c = _rCtrl.GetCalculoCorregidoMes(filtros);
                }
                else
                {
                    int periodoAnt = _rCtrl.GetCicloAnterior(int.Parse(r.Cells[1].Text));
                    filtros.CicloAnt = int.Parse(periodoAnt.ToString().Substring(2, 2) + periodoAnt.ToString().Substring(4, 2));
                    filtros.CsmoAnt = int.Parse(gvEnergia.Rows[r.RowIndex - 1].Cells[6].Text);
                    c = _rCtrl.GetCalculoCorregidoMes(filtros);
                }


                foreach (GridViewRow row in gvFacturado.Rows)
                {
                    if (r.RowType == DataControlRowType.DataRow)
                    {
                        int periodoFact = int.Parse(row.Cells[2].Text);

                        if (filtros.PeriodoAct == periodoFact)
                        {
                            var dRefact = JsonConvert.DeserializeObject<List<SoporteRefacturacion_importes>>(row.Cells[9].Text);

                            foreach (var item in dRefact)
                            {
                                if (item.co_concepto.ToUpper() == "CC470" || item.co_concepto.ToUpper() == "VA127")
                                {
                                    var imp2 = new DtoCalculoCorregido
                                    {
                                        codconcepto = item.co_concepto.ToUpper(),
                                        DescripcionImporte = item.desc_cod.ToUpper(),
                                        ValorCobrar = item.imp_concepto
                                    };
                                    c.Add(imp2);
                                }
                            }
                            break;
                        }
                    }
                }


                if (c.Count > 1 && gvNewCargo.Rows.Count > 0)
                {
                    AgregarImportesTable(c, periodo.ToString());
                }
                return c;
            }
            catch (Exception)
            {
                return null;
            }
        }

        protected List<DtoCalculoCorregido> AgregarImportesTable(List<DtoCalculoCorregido> c, string periodo)
        {
            foreach (GridViewRow fila in gvNewCargo.Rows)
            {
                char[] delimiters = new char[] { ',' };
                TextBox box1 = (TextBox)fila.Cells[1].FindControl("txt0");
                string[] ListaPeriodos = box1.Text.Trim().Split(delimiters);

                foreach (var p in ListaPeriodos)
                {
                    if (p == periodo)
                    {
                        TextBox boxCod = (TextBox)fila.Cells[1].FindControl("txt1");
                        TextBox boxNom = (TextBox)fila.Cells[2].FindControl("txt2");
                        TextBox boxVal = (TextBox)fila.Cells[3].FindControl("txt3");

                        var ImporteNuevo = new DtoCalculoCorregido
                        {
                            codconcepto = boxCod.Text.ToUpper(),
                            DescripcionImporte = boxNom.Text.ToUpper(),
                            ValorCobrar = decimal.Parse(boxVal.Text),
                            IsNew = 1
                        };
                        c.Add(ImporteNuevo);
                    }
                }
            }

            return c;
        }

        protected void Reproceso()
        {
            Calcularvalores();
            GetLecturasMesAnterior();

            ddlLecRea.Visible = true;
            ddlLecAct.Visible = true;
            if (chkCdr.Checked)
            {
                hfMetodo.Value = string.Empty;

                CalculaCdrSI();
                CalculoUltimaFila();
                CalculosPieGrid();
                ddlLecRea.Visible = false;
                ddlLecAct.Visible = false;
            }
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox box0 = (TextBox)gvNewCargo.Rows[rowIndex].Cells[1].FindControl("txt0");
                        TextBox box1 = (TextBox)gvNewCargo.Rows[rowIndex].Cells[2].FindControl("txt1");
                        TextBox box2 = (TextBox)gvNewCargo.Rows[rowIndex].Cells[3].FindControl("txt2");
                        TextBox box3 = (TextBox)gvNewCargo.Rows[rowIndex].Cells[4].FindControl("txt3");
                        box0.Text = dt.Rows[i]["Column1"].ToString();
                        box1.Text = dt.Rows[i]["Column2"].ToString();
                        box2.Text = dt.Rows[i]["Column3"].ToString();
                        box3.Text = dt.Rows[i]["Column4"].ToString();
                        rowIndex++;
                    }
                }
            }
        }

        private void AddNewRowToGrid()
        {
            if (gvNewCargo.Rows.Count > 0)
            {
                int rowIndex = 1;
                if (ViewState["CurrentTable"] != null)
                {
                    DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                    DataRow drCurrentRow = null;
                    if (dtCurrentTable.Rows.Count > 0)
                    {
                        string selectedPeriodos = string.Join(",", cblCiclos.Items.OfType<ListItem>().Where(r => r.Selected).Select(r => r.Text));

                        for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                        {
                            if (dtCurrentTable.Rows.Count == rowIndex)
                            {
                                string CodImporte = ddlNewImporte.SelectedValue.ToString();
                                string NomImporte = ddlNewImporte.SelectedItem.Text;
                                string Valor = txtValImporte.Text.ToString();
                                drCurrentRow = dtCurrentTable.NewRow();
                                int index = i + 1;

                                drCurrentRow["RowNumber"] = index;
                                dtCurrentTable.Rows[i - 1]["Column1"] = selectedPeriodos;
                                dtCurrentTable.Rows[i - 1]["Column2"] = CodImporte;
                                dtCurrentTable.Rows[i - 1]["Column3"] = NomImporte;
                                dtCurrentTable.Rows[i - 1]["Column4"] = Valor;
                                drCurrentRow["IsNew"] = 1;
                            }
                            rowIndex++;
                        }
                        dtCurrentTable.Rows.Add(drCurrentRow);
                        ViewState["CurrentTable"] = dtCurrentTable;
                        gvNewCargo.DataSource = dtCurrentTable;
                        gvNewCargo.DataBind();
                    }
                }
                else
                {
                    Response.Write("ViewState is null");
                }
                //Set Previous Data on Postbacks  
                SetPreviousData();
            }
            else
            {
                DataTable dt = new DataTable();
                DataRow dr = null;
                dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dt.Columns.Add(new DataColumn("Column1", typeof(string)));
                dt.Columns.Add(new DataColumn("Column2", typeof(string)));
                dt.Columns.Add(new DataColumn("Column3", typeof(string)));
                dt.Columns.Add(new DataColumn("Column4", typeof(string)));
                dt.Columns.Add(new DataColumn("IsNew", typeof(string)));

                dr = dt.NewRow();
                dr["RowNumber"] = 1;
                dr["Column1"] = string.Empty;
                dr["Column2"] = string.Empty;
                dr["Column3"] = string.Empty;
                dr["Column4"] = string.Empty;
                dr["IsNew"] = "1";
                dt.Rows.Add(dr);
                //Store the DataTable in ViewState  
                ViewState["CurrentTable"] = dt;
                gvNewCargo.DataSource = dt;
                gvNewCargo.DataBind();

                AddNewRowToGrid();
            }
        }

        protected void SetEntradasLecturas(string Metodo)
        {
            try
            {
                Reproceso();
                if (Metodo == "M")
                {
                    txtLecAct.Visible = true;
                    txtLecRea.Visible = true;
                    btnCalActivaGrid.Visible = true;
                    CalculaConsumosCdrNO();
                    CalcularLecturasActivas(int.Parse(txtLecAct.Text.Trim()));
                    CalcularLecturasReactivas(int.Parse(txtLecRea.Text.Trim()));
                    CalculoUltimaFila();
                    CalculosPieGrid();

                    //FillGridCorregido();

                    if (chkCdr.Checked)
                    {
                        ddlLecAct.Enabled = false;
                        ddlLecRea.Enabled = false;
                    }
                    else
                    {
                        ddlLecAct.Enabled = true;
                        ddlLecRea.Enabled = true;
                    }
                }
                else if (Metodo == "A")
                {
                    txtLecAct.Visible = false;
                    txtLecRea.Visible = false;
                    btnCalActivaGrid.Visible = false;
                    CalculaConsumosCdrNO();
                    CalcularLecturasActivas(int.Parse(ddlLecAct.SelectedValue));
                    CalcularLecturasReactivas(int.Parse(ddlLecRea.SelectedValue)); // DDL Reactiva
                    CalculoUltimaFila();
                    CalculosPieGrid();
                    //FillGridCorregido();
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
    }
}