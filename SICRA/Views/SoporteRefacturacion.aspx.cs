﻿using SICRA.Controllers;
using SICRA.Models;
using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls;
using SICRA.Models.SicraBD;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Web;

namespace SICRA.Views
{
    public partial class SoporteRefacturacion : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private SoportesController _sCtrl = new SoportesController();

        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.Params["id"]))
                    {
                        int idSoporte = int.Parse(Fun.QueryStringDecode(Request.Params["id"]));
                        pnlFiltro.Visible = false;

                        CargarSopoorteClienteByPar(idSoporte);
                    }
                }
                catch (Exception ex)
                {
                    pnlFiltro.Visible = false;
                }
            }
        }
        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                //int ciclo = int.Parse(txtPeriodo.Text.Trim().Substring(txtPeriodo.Text.Trim().Length - 4));
                //var existe = _sCtrl.VerificarExisteSoporte(decimal.Parse(txtClave.Text.Trim()), ciclo);
                //if (existe)
                //{
                //    CargarSoportesClienteBd();
                //    pnlInfo.Visible = true;
                //    lblMsj.Visible = true;
                //    lblMsj.Text = $"El Ciclo de Facturacion {ciclo} ya ha sido guardado anteriormente.!";
                //}
                //else
                //{
                //    CargarSoportesCliente();
                //}


                CargarSoportesCliente();
            }
            catch (Exception ex)
            {
                pnlInfo.Visible = false;
                lblMsj.Visible = true;
                lblMsj.Text = $"Ocurrió una Excepción: {ex.Message}";
            }
        }

        private void CargarSopoorteClienteByPar(int idSoporte)
        {
            try
            {
                SoporteRefactura soporteBd = new SoporteRefactura();

                idSopSet.Value = idSoporte.ToString();

                soporteBd = _sCtrl.GetSoporteFacturacionBd(idSoporte);

                txtUsuario.Text = _usrLog.usuSicra.Usuario;
                lblCliente.Text = soporteBd.nomCliente;
                lblClave.Text = soporteBd.nisrad.ToString();
                lblUbicacion.Text = soporteBd.ubicacion;
                lblTarifa.Text = soporteBd.tarifa;
                lblFechaFact.Text = soporteBd.FechaRegistro.ToShortDateString();
                txtObsAdi.Text = soporteBd.observacion;

                hfNisrad.Value = soporteBd.nisrad.ToString();
                hfPeriodo.Value = txtPeriodo.Text.Trim();

                lblCicloFact.Text = soporteBd.cicloFact.ToString();
                lblConsumoAnt.Text = soporteBd.ConsumoFacturado?.ToString();
                lblLectActivaAnt.Text = soporteBd.LectActivaFact?.ToString();
                lblLectReactivaAnt.Text = soporteBd.LectReactivaFact?.ToString();
                lblLectDemandaAnt.Text = soporteBd.LectDemandaFact?.ToString();

                lblCicloRefact.Text = soporteBd.cicloRefact.ToString();
                lblConsumoAct.Text = soporteBd.ConsumoRefacturado?.ToString();
                lblLectActivaAct.Text = soporteBd.LectActivaRefact?.ToString();
                lblLectReactivaAct.Text = soporteBd.LectReactivaRefact?.ToString();
                lblLectDemandaAct.Text = soporteBd.LectDemandaRefact?.ToString();
                lblDiferencia.Text = soporteBd.diferencia?.ToString();
                hfSecNis.Value = soporteBd.SecNis.ToString();
                hfSecRec.Value = soporteBd.SecRec.ToString();
                hfSecRecAnul.Value = soporteBd.SecRecAnul.ToString();
                hfFfactAnul.Value = soporteBd.FFactAnul.ToString();
                hfFfactAnt.Value = soporteBd.FFactAnt.ToString();
                hfOrden.Value = soporteBd.Orden.ToString();
                txtPqr.Text = soporteBd.pqr;

                DataSet ds = _sCtrl.GetImportesBySoporte(soporteBd);
                gvRectificaciones.DataSource = ds;
                gvRectificaciones.DataBind();

                DataRow ultimaRow = ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1];
                decimal difTot = decimal.Parse(ultimaRow["dato4"].ToString());
                lbldbcr.InnerHtml = $"DB - CR: <u><b>{soporteBd.dbcr}</b></u>";

                lblValDoc.Text = difTot.ToString();
                lblDebitoCredito.Text = soporteBd.dbcr;

                btnGuardar.Enabled = false;
                btnGuardar.Visible = false;
                pnlInfo.Visible = true;
                btnActualizar.Visible = true;
            }
            catch (Exception ex)
            {
                pnlInfo.Visible = false;
                lblMsj.Visible = true;
                btnActualizar.Visible = false;
                btnGuardar.Enabled = false;
                btnGuardar.Visible = false;
                lblMsj.Text = $"Ocurrió una Excepción: {ex.Message}";
            }               
        }

        /// <summary>
        /// Cargar refacturas de claves
        /// </summary>
        private void CargarSoportesCliente()
        {
            try
            {
                //Ejemplo
                //string clave = "1331018";
                //string perido = "201906";

                /******************************REFACTURA DE FACTURA**********************************/

                List<ObjSoporteRefacturacion> soportes = new List<ObjSoporteRefacturacion>();
                soportes = _sCtrl.GetSoportesFacturacionWs(txtClave.Text.Trim(), txtPeriodo.Text.Trim(),true);

                if (soportes.Count == 2)
                {
                    soportes = _sCtrl.GetSoportesFacturacionWs(txtClave.Text.Trim(), txtPeriodo.Text.Trim(),false);

                    pnlInfo.Visible = true;
                    lblMsj.Text = string.Empty;
                    lblMsj.Visible = false;
                    txtObsAdi.Text = string.Empty;
                    hfNisrad.Value = string.Empty;
                    hfPeriodo.Value = string.Empty;

                    txtUsuario.Text = _usrLog.usuSicra.Usuario;
                    lblCliente.Text = soportes[0].nombre_cliente;
                    lblClave.Text = soportes[0].nis_rad.ToString();
                    lblUbicacion.Text = soportes[0].ubicacion;
                    lblTarifa.Text = soportes[0].nom_tarifa;
                    lblFechaFact.Text = soportes[0].f_fact.ToShortDateString(); //DateTime.Now.ToShortDateString();

                    hfNisrad.Value = soportes[0].nis_rad.ToString();
                    hfPeriodo.Value = txtPeriodo.Text.Trim();

                    hfSecNis.Value= soportes[0].sec_nis?.ToString();
                    hfSecRec.Value = soportes[0].sec_rec?.ToString();
                    hfSecRecAnul.Value = soportes[0].sec_rec_anul;
                    hfFfactAnul.Value = soportes[0].f_fact_anul.ToShortDateString();
                    hfFfactAnt.Value= soportes[0].f_fact_ant.ToShortDateString();
                    hfOrden.Value = soportes[0].level.ToString();

                    var soporteFact = soportes
                        .Where(s => s.level == (soportes.Min(x => x.level)))
                        .FirstOrDefault();

                    lblCicloFact.Text = txtPeriodo.Text.Substring(txtPeriodo.Text.Trim().Length - 4, 4); //soporteFact.cicloFact;
                    lblConsumoAnt.Text = soporteFact.csmo_fact;
                    lblLectActivaAnt.Text = soporteFact.soporteRefacturacion_Lecturas?.Where(l => l.tip_csmo.Equals("CO011")).FirstOrDefault()?.lect;
                    lblLectReactivaAnt.Text = soporteFact.soporteRefacturacion_Lecturas?.Where(l => l.tip_csmo.Equals("CO015")).FirstOrDefault()?.lect;
                    lblLectDemandaAnt.Text = soporteFact.pot_fact;

                    var soporteRefact = soportes
                        .Where(s => s.level == (soportes.Max(x => x.level)))
                        .FirstOrDefault();

                    lblCicloRefact.Text = soporteRefact.cicloFact.ToString();
                    lblConsumoAct.Text = soporteRefact.csmo_fact;
                    lblLectActivaAct.Text = soporteRefact.soporteRefacturacion_Lecturas?.Where(l => l.tip_csmo.Equals("CO011")).FirstOrDefault()?.lect;
                    lblLectReactivaAct.Text = soporteRefact.soporteRefacturacion_Lecturas?.Where(l => l.tip_csmo.Equals("CO015")).FirstOrDefault()?.lect;
                    lblLectDemandaAct.Text = soporteRefact.pot_fact;

                    lblDiferencia.Text = (int.Parse(lblConsumoAct.Text) - int.Parse(lblConsumoAnt.Text)).ToString();

                    DataSet ds = _sCtrl.GetImportesBySoporte(soportes);
                    gvRectificaciones.DataSource = ds;
                    gvRectificaciones.DataBind();

                    DataRow ultimaRow = ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1];
                    decimal difTot = decimal.Parse(ultimaRow["dato4"].ToString());
                    lbldbcr.InnerHtml = "DB - CR: <u><b>" + (difTot < 0 ? "CR" : "DB") + "</b></u>";

                    lblValDoc.Text = difTot.ToString();
                    lblDebitoCredito.Text = difTot < 0 ? "CR" : "DB";

                    btnGuardar.Enabled = true;
                    btnGuardar.Visible = true;

                    int ciclo = int.Parse(txtPeriodo.Text.Trim().Substring(txtPeriodo.Text.Trim().Length - 4));
                    var existe = _sCtrl.VerificarExisteSoporte(decimal.Parse(txtClave.Text.Trim()), ciclo);
                    if (existe)
                    {
                        CargarSoportesClienteBd();
                        pnlInfo.Visible = true;
                        lblMsj.Visible = true;
                        lblMsj.Text = $"El Ciclo de Facturacion {ciclo} ya ha sido guardado anteriormente.!";
                    }
                }
                /******************************REFACTURA DE REFACTURA**********************************/
                else if (soportes.Count > 2)
                {
                    bool encontroSoporte = false;

                    //foreach (var soporte in soportes)
                    foreach (var (soporte, index) in soportes.Select((v, i) => (v, i)))
                    {
                        if (soporte.level>1)
                        {
                            int cicloFactA = soporte.cicloFact;
                            soporte.cicloFact = soportes[0].cicloFact;

                            //verificar si soporte ya existe en bd
                            bool existeSoporte = _sCtrl.VerificarExisteSoporte(soporte);

                            if (!existeSoporte)
                            {
                                soporte.cicloFact = cicloFactA;
                                //si no existe, agregarlo
                                pnlInfo.Visible = true;
                                lblMsj.Text = string.Empty;
                                lblMsj.Visible = false;
                                txtObsAdi.Text = string.Empty;
                                hfNisrad.Value = string.Empty;
                                hfPeriodo.Value = string.Empty;

                                txtUsuario.Text = _usrLog.usuSicra.Usuario;
                                lblCliente.Text = soporte.nombre_cliente;
                                lblClave.Text = soporte.nis_rad.ToString();
                                lblUbicacion.Text = soporte.ubicacion;
                                lblTarifa.Text = soporte.nom_tarifa;
                                lblFechaFact.Text = soporte.f_fact.ToShortDateString(); //DateTime.Now.ToShortDateString();



                                //var soporteFact = soportes
                                //    .Where(s => s.level == (soportes.Min(x => x.level)))
                                //    .FirstOrDefault();

                                ///REFACTURA EN ORDEN ASCENDENTE LEVEL 2-N (Anterior)
                                lblCicloFact.Text = txtPeriodo.Text.Substring(txtPeriodo.Text.Trim().Length - 4, 4); //soporteFact.cicloFact;
                                lblConsumoAnt.Text = soportes[index - 1].csmo_fact;
                                lblLectActivaAnt.Text = soportes[index - 1].soporteRefacturacion_Lecturas?.Where(l => l.tip_csmo.Equals("CO011")).FirstOrDefault()?.lect;
                                lblLectReactivaAnt.Text = soportes[index - 1].soporteRefacturacion_Lecturas?.Where(l => l.tip_csmo.Equals("CO015")).FirstOrDefault()?.lect;
                                lblLectDemandaAnt.Text = soportes[index - 1].pot_fact;

                                //var soporteRefact = soportes
                                //    .Where(s => s.level == (soportes.Max(x => x.level)))
                                //    .FirstOrDefault();

                                ///REFACTURA EN ORDEN ASCENDENTE LEVEL 2+N
                                hfNisrad.Value = soporte.nis_rad.ToString();
                                hfPeriodo.Value = txtPeriodo.Text.Trim();
                                hfSecNis.Value = soporte.sec_nis?.ToString();
                                hfSecRec.Value = soporte.sec_rec?.ToString();
                                hfSecRecAnul.Value = soporte.sec_rec_anul;
                                hfFfactAnul.Value = soporte.f_fact_anul.ToShortDateString();
                                hfFfactAnt.Value = soporte.f_fact_ant.ToShortDateString();
                                hfOrden.Value = soporte.level.ToString();

                                lblCicloRefact.Text = soporte.cicloFact.ToString();
                                lblConsumoAct.Text = soporte.csmo_fact;
                                lblLectActivaAct.Text = soporte.soporteRefacturacion_Lecturas?.Where(l => l.tip_csmo.Equals("CO011")).FirstOrDefault()?.lect;
                                lblLectReactivaAct.Text = soporte.soporteRefacturacion_Lecturas?.Where(l => l.tip_csmo.Equals("CO015")).FirstOrDefault()?.lect;
                                lblLectDemandaAct.Text = soporte.pot_fact;

                                lblDiferencia.Text = (int.Parse(lblConsumoAct.Text) - int.Parse(lblConsumoAnt.Text)).ToString();

                                DataSet ds = _sCtrl.GetImportesBySoporte(soportes, soporte.level, soportes[index - 1].level);
                                gvRectificaciones.DataSource = ds;
                                gvRectificaciones.DataBind();

                                DataRow ultimaRow = ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1];
                                decimal difTot = decimal.Parse(ultimaRow["dato4"].ToString());
                                lbldbcr.InnerHtml = "DB - CR: <u><b>" + (difTot < 0 ? "CR" : "DB") + "</b></u>";

                                lblValDoc.Text = difTot.ToString();
                                lblDebitoCredito.Text = difTot < 0 ? "CR" : "DB";

                                btnGuardar.Enabled = true;
                                btnGuardar.Visible = true;
                                encontroSoporte = true;
                                break;
                            }
                        }
                    }

                    if (!encontroSoporte)
                    {
                        pnlInfo.Visible = false;
                        lblMsj.Visible = true;
                        lblMsj.Text = "La NIS no cuenta con Refacturas.!";
                    }





                }
                else
                {
                    pnlInfo.Visible = false;
                    lblMsj.Visible = true;
                    lblMsj.Text = "La NIS no cuenta con Refacturas.!";
                }
            }
            catch (Exception ex)
            {
                pnlInfo.Visible = false;
                lblMsj.Visible = true;
                lblMsj.Text = $"Ocurrió una Excepción: {ex.Message}";
            }
        }
        private void CargarSoportesClienteBd()
        {
            try
            {
                int ciclo = int.Parse(txtPeriodo.Text.Trim().Substring(txtPeriodo.Text.Trim().Length - 4));
                var soporteBd = _sCtrl.GetSoporteFacturacionBd(decimal.Parse(txtClave.Text.Trim()), ciclo);

                txtUsuario.Text = _usrLog.usuSicra.Usuario;
                lblCliente.Text = soporteBd.nomCliente;
                lblClave.Text = soporteBd.nisrad.ToString();
                lblUbicacion.Text = soporteBd.ubicacion;
                lblTarifa.Text = soporteBd.tarifa;
                lblFechaFact.Text = soporteBd.FechaRegistro.ToShortDateString();
                txtObsAdi.Text = soporteBd.observacion;

                hfNisrad.Value = soporteBd.nisrad.ToString();
                hfPeriodo.Value = txtPeriodo.Text.Trim();

                lblCicloFact.Text = soporteBd.cicloFact.ToString();
                lblConsumoAnt.Text = soporteBd.ConsumoFacturado?.ToString();
                lblLectActivaAnt.Text = soporteBd.LectActivaFact?.ToString();
                lblLectReactivaAnt.Text = soporteBd.LectReactivaFact?.ToString();
                lblLectDemandaAnt.Text = soporteBd.LectDemandaFact?.ToString();

                lblCicloRefact.Text = soporteBd.cicloRefact.ToString();
                lblConsumoAct.Text = soporteBd.ConsumoRefacturado?.ToString();
                lblLectActivaAct.Text = soporteBd.LectActivaRefact?.ToString();
                lblLectReactivaAct.Text = soporteBd.LectReactivaRefact?.ToString();
                lblLectDemandaAct.Text = soporteBd.LectDemandaRefact?.ToString();
                lblDiferencia.Text = soporteBd.diferencia?.ToString();
                hfSecNis.Value = soporteBd.SecNis.ToString();
                hfSecRec.Value = soporteBd.SecRec.ToString();
                hfSecRecAnul.Value = soporteBd.SecRecAnul.ToString();
                hfFfactAnul.Value= soporteBd.FFactAnul.ToString();
                hfFfactAnt.Value = soporteBd.FFactAnt.ToString();
                hfOrden.Value = soporteBd.Orden.ToString();

                DataSet ds = _sCtrl.GetImportesBySoporte(soporteBd);
                gvRectificaciones.DataSource = ds;
                gvRectificaciones.DataBind();

                DataRow ultimaRow = ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1];
                decimal difTot = decimal.Parse(ultimaRow["dato4"].ToString());
                lbldbcr.InnerHtml = $"DB - CR: <u><b>{soporteBd.dbcr}</b></u>";

                lblValDoc.Text = difTot.ToString();
                lblDebitoCredito.Text = soporteBd.dbcr;

                btnGuardar.Enabled = false;
                btnGuardar.Visible = false;
            }
            catch (Exception ex)
            {
                pnlInfo.Visible = false;
                lblMsj.Visible = true;
                lblMsj.Text = $"Ocurrió una Excepción: {ex.Message}";
            }
        }
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                var ListaSoporteDetalle = new List<SoporteRefacturaDetalle>();
                foreach(GridViewRow row in gvRectificaciones.Rows)
                {
                    string importe = HttpUtility.HtmlDecode(row.Cells[0].Text);
                    decimal corregido = decimal.Parse(row.Cells[1].Text);
                    decimal? facturado = decimal.Parse(row.Cells[2].Text);
                    decimal diferencia = decimal.Parse(row.Cells[3].Text);

                    if (importe.Trim().ToUpper() != "TOTAL")
                    {
                        ListaSoporteDetalle.Add(new SoporteRefacturaDetalle
                        {
                            importe = importe,
                            corregido = corregido,
                            facturado = facturado,
                            diferencia = diferencia
                        });
                    }
                }

                SoporteRefactura s = new SoporteRefactura
                {
                    nomUsuario = _usrLog.usuSicra.Usuario,
                    pqr = txtPqr.Text.Trim(),
                    nomCliente = lblCliente.Text,
                    nisrad = decimal.Parse(lblClave.Text),
                    ubicacion = lblUbicacion.Text,
                    FechaRegistro = Convert.ToDateTime(lblFechaFact.Text), //DateTime.Now,
                    cicloFact = int.Parse(lblCicloFact.Text),
                    cicloRefact = int.Parse(lblCicloRefact.Text),
                    tarifa = lblTarifa.Text,
                    ConsumoFacturado = string.IsNullOrEmpty(lblConsumoAnt.Text)?(int?)null:int.Parse(lblConsumoAnt.Text),
                    ConsumoRefacturado = string.IsNullOrEmpty(lblConsumoAct.Text) ? (int?)null : int.Parse(lblConsumoAct.Text),
                    LectActivaFact = string.IsNullOrEmpty(lblLectActivaAnt.Text) ? (int?)null : int.Parse(lblLectActivaAnt.Text),
                    LectActivaRefact = string.IsNullOrEmpty(lblLectActivaAct.Text) ? (int?)null : int.Parse(lblLectActivaAct.Text),
                    LectReactivaFact = string.IsNullOrEmpty(lblLectReactivaAnt.Text) ? (int?)null : int.Parse(lblLectReactivaAnt.Text),
                    LectReactivaRefact = string.IsNullOrEmpty(lblLectReactivaAct.Text) ? (int?)null : int.Parse(lblLectReactivaAct.Text),
                    LectDemandaFact = string.IsNullOrEmpty(lblLectDemandaAnt.Text) ? (decimal?)null : decimal.Parse(lblLectDemandaAnt.Text),
                    LectDemandaRefact = string.IsNullOrEmpty(lblLectDemandaAct.Text) ? (decimal?)null : decimal.Parse(lblLectDemandaAct.Text),
                    diferencia = string.IsNullOrEmpty(lblDiferencia.Text) ? (decimal?)null : decimal.Parse(lblDiferencia.Text),
                    valorDoc = string.IsNullOrEmpty(lblValDoc.Text) ? (decimal?)null : decimal.Parse(lblValDoc.Text),
                    dbcr = lblDebitoCredito.Text,
                    SoporteRefacturaDetalle = ListaSoporteDetalle,
                    observacion = txtObsAdi.Text.Trim(),
                    SecNis= int.Parse(hfSecNis.Value),
                    SecRec= int.Parse(hfSecRec.Value),
                    SecRecAnul= int.Parse(hfSecRecAnul.Value),
                    FFact = Convert.ToDateTime(lblFechaFact.Text), //DateTime.Now,
                    FFactAnul = Convert.ToDateTime(hfFfactAnul.Value), //DateTime.Now,
                    FFactAnt = Convert.ToDateTime(hfFfactAnt.Value), //DateTime.Now,
                    Orden= int.Parse(hfOrden.Value),
                    idUsuario=_usrLog.usuSicra.idUsuario
                };

                var resp = _sCtrl.PutSoporteRefactura(s);

                Fun.ClearControls(this);

                pnlInfo.Visible = false;
                lblMsj.Visible = true;
                lblMsj.Text = resp.mensaje;
            }
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                SoporteRefactura s = _sCtrl.GetSoporteFacturacionBd(int.Parse(idSopSet.Value.Trim()));

                s.pqr = txtPqr.Text.Trim();
                s.observacion = txtObsAdi.Text.Trim();

                var resp = _sCtrl.PutSoporteRefactura(s);

                //Fun.ClearControls(this);

                pnlInfo.Visible = false;
                lblMsj.Visible = true;
                lblMsj.Text = resp.mensaje;
            }
        }
    }
}