﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
//using Epplus;

namespace SICRA.Views
{
    public partial class AsignaMuestraRefactura : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private CriticaRefacturaController _cCtrl = new CriticaRefacturaController();

        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                CargarChkListUsuarios();
            }
        }
        private void CargarChkListUsuarios()
        {
            try
            {
                var usuarios = _cCtrl.GetUsuariosFacturacionRefactura();

                foreach (var usuario in usuarios)
                {
                    ListItem item = new ListItem();
                    item.Text = usuario.nombre;
                    item.Value = usuario.codUsr;
                    cklUsuarios.Items.Add(item);
                }
                cklUsuarios.DataBind();
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;

                lblMsjSave.Text = $"Ocurrió una excepción: {msj}";
            }
        }
        private void CargarChkListorigen()
        {
            try
            {
                var l = _cCtrl.GetOrigenRefactura();

                foreach (var o in l)
                {
                    ListItem item = new ListItem();
                    item.Text = o.valor;
                    item.Value = o.id;
                    cklOrigen.Items.Add(item);
                }
                cklOrigen.DataBind();
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;

                lblMsjSave.Text = $"Ocurrió una excepción: {msj}";
            }
        }
        private void CargarChkListUsrSoeeh()
        {
            try
            {
                var usrs = _cCtrl.GetListaUsuarioSoeehRefa(ddlCiclo.SelectedValue, ddlDial.SelectedValue);
                cklUsrRefac.Items.Clear();
                foreach (var observ in usrs)
                {
                    ListItem item = new ListItem();
                    item.Text = observ.valor;
                    item.Value = observ.id;
                    cklUsrRefac.Items.Add(item);
                }
                cklUsrRefac.DataBind();
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                lblMsjSave.Text = $"Ocurrió una excepción: {msj}";
            }
        }
        private void CargarChkListObservaciones()
        {
            try
            {
                var usrs = _cCtrl.GetListaObservaciones(ddlCiclo.SelectedValue, ddlDial.SelectedValue);
                cklObservaciones.Items.Clear();
                foreach (var observ in usrs)
                {
                    ListItem item = new ListItem();
                    item.Text = observ.valor;
                    item.Value = observ.id;
                    cklObservaciones.Items.Add(item);
                }
                cklObservaciones.DataBind();
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                lblMsjSave.Text = $"Ocurrió una excepción: {msj}";
            }
        }
        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                pnlAsignado.Visible = true;
                lblFiltros.Text = string.Empty;
                if (ddlDial.SelectedIndex > 0)
                {
                    if (ExisteMuestraFecha())
                    {
                        lblMsj.Text = "Ya existen registros de este tipo de muestra para la fecha seleccionada.!";
                        lblMsj.Visible = true;
                        gvRepoM.DataSource = null;
                        gvRepoM.DataBind();
                    }
                    else
                    {
                        //Sectores sector = _regCtrl.GetFirstSectorByUser(_usrLog.usuSicra.idUsuario);
                        List<string> codOrigen = new List<string>();
                        foreach (ListItem li in cklOrigen.Items)
                        {
                            if (li.Selected == true)
                            {
                                codOrigen.Add("'" + li.Value + "'");
                            }
                        }
                        string origen = "";
                        origen = string.Join(",", codOrigen);

                        List<string> codObserv = new List<string>();
                        foreach (ListItem li in cklObservaciones.Items)
                        {
                            if (li.Selected == true)
                            {
                                codObserv.Add("'" + li.Value + "'");
                            }
                        }
                        string Observ = "";
                        Observ = string.Join(",", codObserv);


                        List<string> codUsrefa = new List<string>();
                        foreach (ListItem li in cklUsrRefac.Items)
                        {
                            if (li.Selected == true)
                            {
                                codUsrefa.Add("'" + li.Value + "'");
                            }
                        }
                        string UsuRefa = "";
                        UsuRefa = string.Join(",", codUsrefa);

                        CargarMuestra(origen, Observ, UsuRefa);

                        btnAsignar.Enabled = true;
                        lblMsjSave.Visible = false;
                        lblMsjSave.Text = string.Empty;
                    }
                }
                else
                {
                    lblMsjSave.Text = "ERROR EN PARÁMETROS.";
                    lblMsjSave.Attributes["CssClass"] = "label label - danger";
                    lblMsjSave.Visible = true;
                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblFiltros.Text = $"Excepción: {ex.Message}";
                //lblMsjSave.Text = "ERROR EN PARÁMETROS.";
                //lblMsjSave.Attributes["CssClass"] = "label label - danger";
                //lblMsjSave.Visible = true;
            }
            
        }
        private void CargarMuestra(string origen, string observ, string usuariosRefa)
        {
            List<string> listUsuarios = new List<string>();
            listUsuarios = cklUsuarios.Items.Cast<ListItem>().Where(li => li.Selected).Select(y => y.Value).ToList();
            int porcentaje = int.Parse(txtPorcentaje.Text);

            var muestra = _cCtrl.GetMuestraBd(int.Parse(ddlCiclo.SelectedValue.Trim()), int.Parse(ddlDial.SelectedValue.Trim()), 
                origen, observ, usuariosRefa, int.Parse(txtPorcentaje.Text.Trim()) ,listUsuarios);

            gvRepoM.DataSource = null;
            gvRepoM.DataSource = muestra;
            gvRepoM.DataBind();

            //lblFiltros.Text = muestra.RespWs;

            if (muestra != null)
            {
                if (muestra.Count > 0)
                {
                    spnConteo.InnerText = muestra.Count.ToString();
                    spnFecha.InnerText = ddlDial.SelectedValue;
                    spnTipo.InnerText = ckbSesgada.Checked ? "MUESTRA SESGADA" : "MUESTRA TOTAL";
                    pnlAsignado.Visible = true;
                    lblFiltros.Visible = false;
                }
                else
                {
                    lblFiltros.Visible = true;
                    pnlAsignado.Visible = false;
                    lblFiltros.Text = $"Atención, no se produjeron datos.";
                }
            }
            else
            {
                lblFiltros.Visible = true;
                pnlAsignado.Visible = false;
                lblFiltros.Text = $"Atención, no se produjeron datos.";
            }            
        }
        protected void btnAsignar_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                try
                {
                    List<MuestraRefactura> muestras = new List<MuestraRefactura>();

                    foreach (GridViewRow row in gvRepoM.Rows)
                    {
                        try
                        {
                            muestras.Add(new MuestraRefactura
                            {
                                idRow = Guid.NewGuid(),
                                nisrad = int.Parse(HttpUtility.HtmlDecode(row.Cells[1].Text)),
                                EsTotal = ckbTotal.Checked ? true : false,
                                dial = int.Parse(HttpUtility.HtmlDecode(row.Cells[2].Text)),
                                Ciclo = int.Parse(HttpUtility.HtmlDecode(row.Cells[7].Text)),
                                CodObservacion = HttpUtility.HtmlDecode(row.Cells[11].Text),
                                Observacion = HttpUtility.HtmlDecode(row.Cells[5].Text),
                                lecturaActiva = HttpUtility.HtmlDecode(row.Cells[8].Text).Trim() == "" ? (int?)null : int.Parse(HttpUtility.HtmlDecode(row.Cells[8].Text)),
                                lecturaReactiva = HttpUtility.HtmlDecode(row.Cells[9].Text).Trim() == "" ? (int?)null : int.Parse(HttpUtility.HtmlDecode(row.Cells[9].Text)),
                                lecturaDemanda = HttpUtility.HtmlDecode(row.Cells[10].Text).Trim() == "" ? (decimal?)null : decimal.Parse(HttpUtility.HtmlDecode(row.Cells[10].Text)),
                                CodOrigen = int.Parse(HttpUtility.HtmlDecode(row.Cells[12].Text)),
                                Origen = HttpUtility.HtmlDecode(row.Cells[4].Text),
                                usuarioGenera = _usrLog.user,
                                usuarioResuelve = HttpUtility.HtmlDecode(row.Cells[3].Text),
                                usuarioResuelveRefa = HttpUtility.HtmlDecode(row.Cells[6].Text),
                                fechaRegistro = DateTime.Now,
                                Resuelto = false
                            });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    RespMsj resp = GuardarMuestra(muestras);
                    btnAsignar.Enabled = false;
                    lblMsjSave.Visible = true;
                    lblMsjSave.Text = resp.mensaje;
                    lblMsjSave.Attributes["CssClass"] = "label label - success";

                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                }
                catch (Exception ex)
                {
                }
            }
        }
        private RespMsj GuardarMuestra(List<MuestraRefactura> muestras)
        {
            var resp = _cCtrl.PutAsingarMuestras(muestras);
            return resp;
        }
        protected void ddlCiclo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDial.DataSource = null;
            ddlDial.Items.Clear();
            for (int i = 1; i <= 23; i++)
            {
                ListItem li = new ListItem(i.ToString(), i.ToString());
                ddlDial.Items.Add(li);
            }
            ddlDial.DataBind();
            ddlDial.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
        }
        protected void ddlDial_SelectedIndexChanged(object sender, EventArgs e)
        {
            string dial = ""; //DateTime.Parse(ddlFecha.SelectedValue.ToString()).ToShortDateString();
            if (ddlDial.SelectedIndex > 0)
            {
                dial = ddlDial.SelectedValue.Trim();
                if (ExisteMuestraFecha())
                {
                    lblMsj.Text = "Ya existen registros de este tipo de muestra para la fecha seleccionada.!";
                    lblMsj.Visible = true;
                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                }
                else
                {
                    CargarChkListUsrSoeeh();
                    CargarChkListObservaciones();

                    lblMsj.Text = string.Empty;
                    lblMsj.Visible = false;
                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                    pnlAsignado.Visible = false;
                }
            }
        }
        private bool ExisteMuestraFecha()
        {
            bool tipo = ckbTotal.Checked ? true : false;

            var existe = _cCtrl.GetCountMuestrasbyFecha(int.Parse(ddlDial.SelectedValue.ToString()), tipo);
            return existe;
        }
        protected void ckbSesgada_CheckedChanged(object sender, EventArgs e)
        {
            gvRepoM.DataSource = null;
            gvRepoM.DataBind();
            var ckb = (CheckBox)sender;
            if (ckb.Checked)
            {
                CargarCiclos();
                CargarChkListorigen();

                hfFiltra.Value = "0";

                ckbTotal.Checked = false;

                divAnomalia.Visible = true;
                div1.Visible = true;
                div2.Visible = true;
                divPorcentaje.Visible = true;

                txtPorcentaje.Text = "80";
            }
        }
        protected void ckbTotal_CheckedChanged(object sender, EventArgs e)
        {
            gvRepoM.DataSource = null;
            gvRepoM.DataBind();
            var ckb = (CheckBox)sender;
            if (ckb.Checked)
            {
                CargarCiclos();
                lblMsj.Text = string.Empty;
                hfFiltra.Value = "1";

                ckbSesgada.Checked = false;

                divAnomalia.Visible = false;
                div1.Visible = false;
                div2.Visible = false;
                //divPorcentaje.Visible = false;

                cklOrigen.Items.Clear();
                cklOrigen.DataSource = null;
                cklOrigen.DataBind();

                cklObservaciones.Items.Clear();
                cklObservaciones.DataSource = null;
                cklObservaciones.DataBind();

                cklUsrRefac.Items.Clear();
                cklUsrRefac.DataSource = null;
                cklUsrRefac.DataBind();

                gvRepoM.DataSource = null;
                gvRepoM.DataBind();

                txtPorcentaje.Text = "100";
            }
        }
        protected void ckObserv_CheckedChanged(object sender, EventArgs e)
        {
            var ckb = (CheckBox)sender;
            if (ckb.Checked)
            {
                foreach (ListItem li in cklObservaciones.Items)
                {
                    li.Selected = true;
                }
            }
            else
            {
                foreach (ListItem li in cklObservaciones.Items)
                {
                    li.Selected = false;
                }
            }
        }
        protected void CargarCiclos()
        {
            ddlCiclo.DataSource = null;
            ddlCiclo.DataSource = _cCtrl.GetPeriodosRefactura();
            ddlCiclo.DataBind();
            ddlCiclo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlDial.DataSource = null;
            ddlDial.Items.Clear();
            ddlDial.DataBind();
        }
        protected void chlUsrs_CheckedChanged(object sender, EventArgs e)
        {
            var ckb = (CheckBox)sender;
            if (ckb.Checked)
            {
                foreach (ListItem li in cklUsuarios.Items)
                {
                    li.Selected = true;
                }
            }
            else
            {
                foreach (ListItem li in cklUsuarios.Items)
                {
                    li.Selected = false;
                }
            }
        }
        protected void cklTodosUsrRefac_CheckedChanged(object sender, EventArgs e)
        {
            var ckb = (CheckBox)sender;
            if (ckb.Checked)
            {
                foreach (ListItem li in cklUsrRefac.Items)
                {
                    li.Selected = true;
                }
            }
            else
            {
                foreach (ListItem li in cklUsrRefac.Items)
                {
                    li.Selected = false;
                }
            }
        }
    }
}