﻿using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SICRA.Models.SicraBD;
//using Epplus;

namespace SICRA.Views
{
    public partial class HistoricoClaveOperativa : System.Web.UI.Page
    {
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private ClienteController _cCtrl = new ClienteController();
        private LogisticsController _lCtrl = new LogisticsController();
        private ReporteController _rCtrl = new ReporteController();
        private RegionesController _regCtrl = new RegionesController();
        private AdminLogin _usrLog = new AdminLogin();

        protected void Page_Load(object sender, EventArgs e)
        {
            //_usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            //_usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                CargarDdl();
            }
        }

        private void CargarDdl()
        {
            ddlRegion.DataSource = null;
            ddlRegion.DataSource = _regCtrl.GetRegionesByUsuario("");
            ddlRegion.DataTextField = "Region";
            ddlRegion.DataValueField = "Region";
            ddlRegion.DataBind();
            ddlRegion.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlSector.DataSource = null;
            ddlSector.DataSource = _regCtrl.GetSectoresByUsuario("");
            ddlSector.DataTextField = "Sector";
            ddlSector.DataValueField = "Sector";
            ddlSector.DataBind();
            ddlSector.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlMedida.DataSource = null;
            ddlMedida.DataSource = _regCtrl.GetMedidaByUsuario("");
            ddlMedida.DataBind();
            ddlMedida.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlDial.DataSource = null;
            ddlDial.Items.Clear();
            for (int i = 1; i <= 23; i++)
            {
                ListItem li = new ListItem(i.ToString(), i.ToString());
                ddlDial.Items.Add(li);
            }
            ddlDial.DataBind();
            ddlDial.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlAnomalia.DataSource = null;
            ddlAnomalia.DataSource = _rCtrl.GetAnomalias();
            ddlAnomalia.DataTextField = "descripcion";
            ddlAnomalia.DataValueField = "idLecturaAnomalia";
            ddlAnomalia.DataBind();
            ddlAnomalia.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlCiclo.DataSource = null;
            ddlCiclo.DataSource = _rCtrl.GetCiclosAnomaliaHistorico();
            ddlCiclo.DataBind();
            ddlCiclo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
        }

        private void CargarReportes()
        {
            try
            {
                FiltroObj filtros = null;
                List<RepoAnomaliaMaestroDto> listaReportadas = null;

                if (txtClave.Text.Trim() == "" && ddlRegion.SelectedValue == ""
                    && ddlSector.SelectedValue == "" && ddlMedida.SelectedValue == "" 
                    && ddlEstado.SelectedValue == "" && ddlAnomalia.SelectedValue == ""
                    && ddlCiclo.SelectedValue == "" && ddlDial.SelectedValue == "")
                {
                    filtros = null;
                    spnConteo.InnerText = " 0 - Debe de Aplicar Filtros.";
                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                }
                else
                {
                    filtros = new FiltroObj
                    {
                        Nisrad = txtClave.Text.Trim(),
                        Region = ddlRegion.SelectedValue,
                        Sector = ddlSector.SelectedValue,
                        Medida = ddlMedida.SelectedValue,
                        Resuelto = ddlEstado.SelectedValue,
                        idAnomalia = ddlAnomalia.SelectedValue,
                        CicloFact = ddlCiclo.SelectedValue,
                        Dial = ddlDial.SelectedValue
                    };

                    listaReportadas = _rCtrl.listaRepoMaestroAnomaliaHistorico(filtros);

                    gvRepoM.DataSource = null;
                    gvRepoM.DataSource = listaReportadas;
                    gvRepoM.DataBind();
                    spnConteo.InnerText = listaReportadas.Count().ToString();
                }
            }
            catch (Exception ex)
            {
                spnConteo.InnerText = " 0 - Debe de Aplicar Filtros.";
            }
        }

        protected void gvRepoM_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    string IdM = gvRepoM.DataKeys[e.Row.RowIndex].Value.ToString();
                //    GridView gvReDetails = e.Row.FindControl("gvRepoD") as GridView;

                //    var listaDetalle = _rCtrl.RepoDetailAnomaliaByIdMHistorico(long.Parse(IdM.Trim()));

                //    gvReDetails.DataSource = listaDetalle;
                //    gvReDetails.DataBind();

                //    TableCell cellResuelto = e.Row.Cells[14];
                //    TableCell cellResueltoString = e.Row.Cells[15];
                //    if (cellResuelto.Text == "1")
                //    {
                //        cellResueltoString.BackColor = Color.LightGreen;
                //    }
                //    else
                //    {
                //        cellResueltoString.BackColor = Color.LightCoral;
                //    }
                //}
            }
            catch (Exception ex)
            {
            }
        }
        protected void gvRepoM_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CargarReportes();
            gvRepoM.PageIndex = e.NewPageIndex;
            gvRepoM.DataBind();
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            CargarReportes();
        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSector.SelectedIndex = 0;
        }

        protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlRegion.SelectedIndex = 0;
        }
    }
}