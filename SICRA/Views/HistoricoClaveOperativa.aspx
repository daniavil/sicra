﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HistoricoClaveOperativa.aspx.cs" Inherits="SICRA.Views.HistoricoClaveOperativa" %>
<%--<%@ Register Src="~/Modals/MsjModal.ascx" TagPrefix="uc1" TagName="MsjModal" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <a href="#"><span>SICRA</span></a> <%--agregar al menu que pertenece--%>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
    <a class="UrlActual" href='<%= ResolveUrl("HistoricoClavesReportadas.aspx") %>'>Histórico Claves Reportadas</a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updPanReportar">
        <ContentTemplate>
            <asp:UpdateProgress runat="server" ID="progress" DynamicLayout="true" AssociatedUpdatePanelID="updPanReportar">
                <ProgressTemplate>
                    <div style="text-align:center" class="loading">
                        <img src='<%= ResolveUrl("~/Imagenes/brickLoading.gif") %>'/>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Panel runat="server" ID="panelPrincipal">
                <div class="col-sm-12">
                    <h2 class="titulo">Histórico de Claves Reportadas</h2>
                    <div class="form-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Claves Reportadas</div
                            <div class="panel-body">
                                <%--********************************formulario de filtros**************************--%>
                                <br />
                                <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#demo">FILTROS</button>
                                <div id="demo" class="collapse">
                                    <div class="panel panel-success">
                                        <div class="panel-body">
                                            <div class="form-horizontal">
                                                <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                                    <label for="txtNisrad">NIS:</label>
                                                    <asp:TextBox ID="txtClave" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="form-group col-sm-5 col-md-5 col-lg-5">
                                                    <label for="ddlRegion">Región:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-5 col-md-5 col-lg-5">
                                                    <label for="ddlSector">Sector:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlSector" OnSelectedIndexChanged="ddlSector_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                                    <label for="ddlMedida">Medida:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlMedida"></asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-8 col-md-8 col-lg-8">
                                                    <label for="ddlAnomalia">Anomalía:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlAnomalia"></asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                                    <label for="ddlCiclo">Ciclo:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlCiclo"></asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                                    <label for="ddlDial">Dial:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlDial"></asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                                    <label for="ddlEstado">Resuelto:</label>
                                                    <asp:DropDownList runat="server" class="form-control" ID="ddlEstado">
                                                        <asp:ListItem Text="--Seleccionar--" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                                    <asp:Button ID="btnFiltrar" runat="server" Text="Buscar" CssClass="btn btn-warning" OnClick="btnFiltrar_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--********************************FIN formulario de filtros**************************--%>
                                <br />
                                <br />
                                <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                    <span >Conteo:</span>
                                    <span id="spnConteo" runat="server" style="font-weight: bold"></span>
                                </div>
                                <asp:GridView ID="gvRepoM" runat="server" AllowPaging="True" OnPageIndexChanging="gvRepoM_PageIndexChanging"
                                    AutoGenerateColumns="False" PageSize="50" CssClass="mydatagrid" DataKeyNames="IdRow_M"
                                    OnRowDataBound="gvRepoM_RowDataBound" Font-Size="Small" Width="100%"
                                    PagerStyle-CssClass="pager" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#ffffcc" />
                                    <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                    <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="#">
                                            <ItemTemplate>
                                                <img alt="" class="imgPlusRow" style="cursor: pointer" src="../Imagenes/plus.png" />
                                                <asp:Panel ID="pnlRegistros" runat="server" Style="display: none">
                                                    <asp:GridView ID="gvRepoD" runat="server" CssClass="Grid" AutoGenerateColumns="false" Width="100%">
                                                        <Columns>
                                                            <asp:BoundField DataField="IdRow_D" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdRow_D" ReadOnly="True" Visible="true" />
                                                            <asp:BoundField DataField="UsuarioResuelve" HeaderText="Usuario Resolvio" ReadOnly="True" />
                                                            <asp:BoundField DataField="LecturaActiva" HeaderText="Lectura Activa" ReadOnly="True" />
                                                            <asp:BoundField DataField="LecturaReactiva" HeaderText="Lectura Reactiva" ReadOnly="True" />
                                                            <asp:BoundField DataField="LecturaDemanda" HeaderText="Lectura Demanda" ReadOnly="True" />
                                                            <asp:BoundField DataField="Resolucion" HeaderText="Resolución" ReadOnly="True" />
                                                            <asp:BoundField DataField="FechaResolucion" HeaderText="Fecha Resolución" ReadOnly="True" />
                                                            <asp:BoundField DataField="Tipo" HeaderText="Tipo" ReadOnly="True" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="IdRow_M" HeaderText="IdRow_M" />
                                        <asp:BoundField DataField="Nis_Rad" HeaderText="NIS" />
                                        <asp:BoundField DataField="Region" HeaderText="Región" />
                                        <asp:BoundField DataField="Sector" HeaderText="Sector" />
                                        <asp:BoundField DataField="Medida" HeaderText="Medida" />
                                        <asp:BoundField DataField="LecturaActiva" HeaderText="L. Activa" />
                                        <asp:BoundField DataField="LecturaReactiva" HeaderText="L. Reactiva" />
                                        <asp:BoundField DataField="LecturaDemanda" HeaderText="L. Demanda" />
                                        <asp:BoundField DataField="UsuarioFacturacion" HeaderText="Usuario Reporta" />
                                        <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha Reporte" />
                                        <asp:BoundField DataField="Anomalia" HeaderText="Anomalía" />
                                        <asp:BoundField DataField="Ciclo" HeaderText="Ciclo" />
                                        <asp:BoundField DataField="dial" HeaderText="Dial" />
                                        <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="Resuelto" HeaderText="Resuelto" />
                                        <asp:TemplateField HeaderText="Resuelto" SortExpression="Active">
                                            <ItemTemplate><%# (int.Parse(Eval("Resuelto").ToString())==1) ? "SI" : "NO" %></ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
            </asp:Panel>
            <%--<uc1:MsjModal runat="server" ID="MsjModal"></uc1:MsjModal>--%>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlRegion" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlSector" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <style>

        .file{
            margin-left:25%;
            margin-top:4%;
            margin-bottom:4%;
        }

        .titulo{
            margin-left: 35%;
            margin-bottom: 4%;
        }
        .total{
            margin-left:60%;
            margin-bottom: 1.5%;
            margin-top: 1.5%;
        }

        .btnCargar{
            margin-left: 65%;
            margin-top: 2%;
            margin-bottom: 2%;
        }

        .loading{
            position:fixed;
            top:0px;
            right:0px;
            width:100%;
            height:100%;
            background-color:#ffffff;
            background-repeat:no-repeat;
            background-position:center;
            z-index:10000000;
            opacity: 0.80;
            filter: alpha(opacity=0); 
        }

    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    <script src="../Scripts/GridCells.js"></script>
    <script type="text/javascript">
        $(document).on("click", "[class^=imgPlusRow]", function () {
            $(this).attr("class", "imgMinusRow");
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
            $(this).attr("src", "../Imagenes/minus.png");
        });
        $(document).on("click", "[class^=imgMinusRow]", function () {
            $(this).attr("class", "imgPlusRow");
            $(this).attr("src", "../Imagenes/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>
    <script>
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("¿Está seguro de realizar esta acción?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>