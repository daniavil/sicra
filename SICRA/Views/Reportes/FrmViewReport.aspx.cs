﻿using ClosedXML.Excel;
using DevExpress.XtraReports.Web;
using DevExpress.XtraReports.UI;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using SICRA.Views.Reportes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Epplus;

namespace SICRA.Views.Reportes
{
    public partial class FrmViewReport : System.Web.UI.Page
    {
        private RefacturaController _rCtrl = new RefacturaController();
        protected void Page_Load(object sender, EventArgs e)
        {
            var report = new rptAutorizaRefa();
            report.DataSource = _rCtrl.GetRptAutorizacion(1);

            //var cachedReportSource = new CachedReportSourceWeb(report);
            ASPxWebDocumentViewer1.ReportSourceId = "SICRA.Views.Reportes.rptAutorizaRefa";
            ASPxWebDocumentViewer1.OpenReport(new CachedReportSourceWeb(report));
            ASPxWebDocumentViewer1.DataBind();
        }

    }
}