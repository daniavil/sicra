﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Windows.Forms;
using DevExpress.DataAccess.Sql;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraPrinting;
//using System.Web.UI.WebControls;

namespace SICRA.Views.Reportes
{
    public partial class rptSoporteRefactura : DevExpress.XtraReports.UI.XtraReport
    {
        public rptSoporteRefactura()
        {
            InitializeComponent();
        }

        private void pbPreparadoPor_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string firmaRoot = GetCurrentColumnValue("firma").ToString(); //GetFirma();

            if (!string.IsNullOrEmpty(firmaRoot.Trim()))
            {
                // Create an XRPictureBox object.
                //XRPictureBox xrPictureBox = new XRPictureBox();
                // Set its image.
                pbPreparadoPor.ImageSource = new ImageSource(new Bitmap($@"{firmaRoot}"));
                // Uncomment these lines to get images from a data source.
                // ExpressionBinding expressionBinding = new ExpressionBinding("BeforePrint", "ImageSource", "[Picture]");
                // xrPictureBox.ExpressionBindings.Add(expressionBinding);
                // Set its location.
                //xrPictureBox.LocationF = new PointF(150F, 25F);
                // Set its size.
                pbPreparadoPor.SizeF = new SizeF(140F, 60F);
                // Set its size mode.
                pbPreparadoPor.Sizing = ImageSizeMode.StretchImage;
            }
            else
            {
                pbPreparadoPor.ImageSource = null;
            }
        }

        private string GetFirma()
        {
            SqlDataSource source = this.DataSource as SqlDataSource;
            DevExpress.DataAccess.Sql.DataApi.ITable table = source.Result["SoporteRefactura"];

            //string path = Request.PhysicalApplicationPath;
            string rutafirma = "";
            //string rutafirma = table.GetColumn(24).GetValue<int>(0).ToString();  //row["ProductName"].ToString();

            return rutafirma;
        }

    }
}
