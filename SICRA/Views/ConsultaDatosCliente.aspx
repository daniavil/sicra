﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConsultaDatosCliente.aspx.cs" Inherits="SICRA.Views.ConsultaDatosCliente" %>
<%@ Register Src="~/Modals/MsjModal.ascx" TagPrefix="uc1" TagName="MsjModal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <a href="#"><span>SICRA</span></a> <%--agregar al menu que pertenece--%>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
<%--    <a class="UrlActual" href='<%= ResolveUrl("PagosMasivosExcel.aspx") %>'>Pagos Masivos Excel.</a>--%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../Scripts/GridCells.js"></script>
    <style>

        .file{
            margin-left:25%;
            margin-top:4%;
            margin-bottom:4%;
        }

        .titulo{
            margin-left: 35%;
            margin-bottom: 4%;
        }
        .total{
            margin-left:60%;
            margin-bottom: 1.5%;
            margin-top: 1.5%;
        }

        .btnCargar{
            margin-left: 65%;
            margin-top: 2%;
            margin-bottom: 2%;
        }

        .loading{
            position:fixed;
            top:0px;
            right:0px;
            width:100%;
            height:100%;
            background-color:#ffffff;
            background-repeat:no-repeat;
            background-position:center;
            z-index:10000000;
            opacity: 0.80;
            filter: alpha(opacity=0); 
        }

    </style>

    <asp:Panel runat="server" ID="panelPrincipal">
        <div class="col-sm-12">
            <h2 class="titulo">Consulta de Cliente</h2>
            <div class="form-group">
                <%--Panel de clave a buscar--%>
                <asp:Panel runat="server" ID="panelContent">
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <div class="form-inline" style="margin-left: 0px">
                                <div class="form-group col-sm-12">
                                    <label for="txtClave">NIS:</label>
                                    <asp:TextBox ID="txtClave" runat="server" CssClass="form-control" Width="200px" onkeypress="EnterEvent(event);return ValidateNumeros(event)"></asp:TextBox>
                                    <asp:Button ID="btnConsultaClave" runat="server" Text="Consultar NIS" CssClass="btn btn-info" OnClick="btnConsultaClave_Click" />
                                </div>
                                <br />
                                <div class="form-group col-sm-12">
                                    <asp:Label ID="lblMsjConsulta" runat="server" CssClass="label label-danger"></asp:Label>
                                </div>
                            </div>
                            <br />
                        </div>
                    </div>
                </asp:Panel>

                <%--panel de informacion--%>
                <asp:Panel runat="server" ID="pnlDatos" Visible="false">
                    <div class="row row-offcanvas row-offcanvas-right">

                        <!--Panel Lateral Izquierdo-->
                        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Datos Generales</div>
                                <div class="panel-body">
                                    <div class="form-horizontal col-sm-2 col-md-2 col-lg-2">
                                        <label for="lblNisrad">NIS:</label>
                                        <asp:Label ID="lblNisrad" runat="server"></asp:Label>
                                    </div>
                                    <div class="form-horizontal col-sm-3 col-md-3 col-lg-3">
                                        <label for="lblMedidor">Medidor:</label>
                                        <asp:Label ID="lblMedidor" runat="server"></asp:Label>
                                    </div>
                                    <div class="form-horizontal col-sm-3 col-md-3 col-lg-3">
                                        <label for="lblUbicacion">Ubicación:</label>
                                        <asp:Label ID="lblUbicacion" runat="server"></asp:Label>
                                    </div>
                                    <div class="form-horizontal col-sm-4 col-md-4 col-lg-4">
                                        <label for="lblTarifa">Tarifa:</label>
                                        <asp:Label ID="lblTarifa" runat="server"></asp:Label>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                        <label for="lblCliente">Cliente:</label>
                                        <asp:Label ID="lblCliente" runat="server"></asp:Label>
                                    </div>
                                    <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                        <label for="lblDireccion">Dirección:</label>
                                        <asp:Label ID="lblDireccion" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>

                            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Fotos</button>
                            <div id="demo" class="collapse">
                                <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 sidebar-offcanvas" id="sidebar">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading" id="handle">Galería</div>
                                        <div class="panel-body">
                                            <%--GALERIA--%>

                                            <div id="FotoContainer">
                                                <asp:Literal ID="literalControl" runat="server" />
                                            </div>

                                            <script src="../GalleryRoot/js/viewer.js"></script>
                                            <script>
                                                window.addEventListener('DOMContentLoaded', function () {
                                                    var galley = document.getElementById('galley');
                                                    var viewer = new Viewer(galley, {
                                                        toolbar: {
                                                            zoomIn: 4,
                                                            zoomOut: 4,
                                                            oneToOne: 4,
                                                            reset: 4,
                                                            prev: 4,
                                                            play: {
                                                                show: 0,
                                                                size: 'large',
                                                            },
                                                            next: 4,
                                                            rotateLeft: 4,
                                                            rotateRight: 4,
                                                            flipHorizontal: 4,
                                                            flipVertical: 4,
                                                        },
                                                    });
                                                });
                                            </script>

                                            <%--@*FIN GALERIA*@--%>
                                        </div>
                                    </div>
                                </div>
                                <!--/.sidebar-offcanvas-->
                            </div>

                            <br />
                            <br />
                            <div id="exTab3" class="container" style="height: auto; width:98%">
                                <ul class="nav nav-pills" id="myTabs">
                                    <li id="tab1" class="active">
                                        <a href="#div1b" data-toggle="tab">Cálculos</a>
                                    </li>
                                    <li id="tab2">
                                        <a href="#div2b" data-toggle="tab">Facturación</a>
                                    </li>
                                    <li id="tab3">
                                        <a href="#div3b" data-toggle="tab">Historico Lecturas</a>
                                    </li>
                                    <li id="tab4">
                                        <a href="#div4b" data-toggle="tab">Historico Consumos</a>
                                    </li>
                                </ul>

                                <div class="tab-content clearfix">
                                    <%--Primera pestaña--%>
                                    <div class="tab-pane active" id="div1b">
                                        <div class="panel panel-primary">
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <%--Calculo Consumo--%>
                                                    <fieldset>
                                                        <form class="form-inline">
                                                            <%--datos     --%>
                                                            <div class="panel panel-success">
                                                                <div class="panel-heading">Datos</div>
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                            <label for="lblLectActAntCal">Lectura Activa Anterior:</label>
                                                                            <asp:Label ID="lblLectActAntCal" runat="server"></asp:Label>
                                                                        </div>
                                                                        <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                            <label for="lblLectReaAntCal">Lectura Reactiva Anterior:</label>
                                                                            <asp:Label ID="lblLectReaAntCal" runat="server"></asp:Label>
                                                                        </div>
                                                                        <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                            <label for="lblMultiCal">Multiplicador:</label>
                                                                            <asp:Label ID="lblMultiCal" runat="server"></asp:Label>
                                                                        </div>
                                                                        <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                            <label for="lblNumAgu">Número de Agujas:</label>
                                                                            <asp:Label ID="lblNumAgu" runat="server"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="panel panel-success">
                                                                <div class="panel-heading">Cálculo</div>
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <div class="form-inline col-sm-6 col-md-6 col-lg-6">
                                                                            <label for="txtLecActCal">Lectura Activa:</label>
                                                                            <asp:TextBox ID="txtLecActCal" runat="server"></asp:TextBox>
                                                                        </div>
                                                                        <div class="form-inline col-sm-6 col-md-6 col-lg-6">
                                                                            <label for="txtLecReaCal">Lectura Reactiva:</label>
                                                                            <asp:TextBox ID="txtLecReaCal" runat="server"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="form-inline col-sm-6 col-md-6 col-lg-6">
                                                                            <label for="lblConsumoActCal">Consumo Activo Calculado:</label>
                                                                            <asp:Label ID="lblConsumoActCal" runat="server"></asp:Label>
                                                                        </div>
                                                                        <div class="form-inline col-sm-6 col-md-6 col-lg-6">
                                                                            <label for="lblConsumoReaCal">Consumo Reactivo Calculado:</label>
                                                                            <asp:Label ID="lblConsumoReaCal" runat="server"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="form-inline col-sm-12 col-md-12 col-lg-12">
                                                                            <asp:Button ID="btnCalcular" runat="server" Text="Calcular" CssClass="btn btn-success" OnClick="btnCalcular_Click" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--segunda pestaña--%>
                                    <div class="tab-pane" id="div2b">
                                        <div class="panel panel-primary">
                                            <div class="panel-body" style="font-size: small">
                                                <%--datos de campo--%>
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">Datos De Campo</div>
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblLeidoPor">Leído Por:</label>
                                                                <asp:Label ID="lblLeidoPor" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblFechaLect">Fecha de Lectura:</label>
                                                                <asp:Label ID="lblFechaLect" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblCodLect">Código de Lectura:</label>
                                                                <asp:Label ID="lblCodLectCampo" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblAnomalia">Anomalía:</label>
                                                                <asp:Label ID="lblAnomalia" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="form-group">
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblConActCampo">Consumo Activa:</label>
                                                                <asp:Label ID="lblConActCampo" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblLectActCampo">Lectura Activa:</label>
                                                                <asp:Label ID="lblLectActCampo" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblConReaCampo">Consumo Reactivo:</label>
                                                                <asp:Label ID="lblConReaCampo" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblLectReaCampo">Lectura Reactiva:</label>
                                                                <asp:Label ID="lblLectReaCampo" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <%--datos Actuales--%>
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">Datos Actuales</div>
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <div class="form-inline col-sm-6 col-md-6 col-lg-6">
                                                                <label for="lblDiasFact">Días Facturados:</label>
                                                                <asp:Label ID="lblDiasFact" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-6 col-md-6 col-lg-6">
                                                                <label for="lblCodLectActual">Código de Lectura:</label>
                                                                <asp:Label ID="lblCodLectActual" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="form-group">
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblConActActual">Consumo Activa:</label>
                                                                <asp:Label ID="lblConActActual" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblLectActActual">Lectura Activa:</label>
                                                                <asp:Label ID="lblLectActActual" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblConReaActual">Consumo Reactivo:</label>
                                                                <asp:Label ID="lblConReaActual" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblLectReaActual">Lectura Reactiva:</label>
                                                                <asp:Label ID="lblLectReaActual" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <%--datos Anterior--%>
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">Datos Anteriores</div>
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblConActActual">Consumo Activa:</label>
                                                                <asp:Label ID="lblConActAnt" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblLectActActual">Lectura Activa:</label>
                                                                <asp:Label ID="lblLectActAnt" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblConReaActual">Consumo Reactivo:</label>
                                                                <asp:Label ID="lblConReaAnt" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="form-inline col-sm-3 col-md-3 col-lg-3">
                                                                <label for="lblLectReaActual">Lectura Reactiva:</label>
                                                                <asp:Label ID="lblLectReaAnt" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--Tercera pestaña--%>
                                    <div class="tab-pane" id="div3b">
                                        <div class="panel panel-primary">
                                            <div class="panel-body">
                                                <!--Table-->
                                                <asp:GridView runat="server" ID="gvLecturas" AutoGenerateColumns="false" AllowPaging="false"
                                                    CssClass="table table-bordered table-hover" Font-Size="Smaller">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Fecha" DataField="fecha" />
                                                        <asp:BoundField HeaderText="Multiplicador" DataField="multiplicador" />
                                                        <asp:BoundField HeaderText="Tipo Consumo" DataField="tipoconsumo" />
                                                        <asp:BoundField HeaderText="Consumo" DataField="consumo" />
                                                        <asp:BoundField HeaderText="Tipo Lectura" DataField="tipolectura" />
                                                        <asp:BoundField HeaderText="Lectura" DataField="lectura" />
                                                    </Columns>
                                                    <EmptyDataTemplate>No Hay informacíon en el grupo.</EmptyDataTemplate>
                                                    <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                                                </asp:GridView>
                                                <!--Table-->
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                    <%--Cuarta pestaña--%>
                                    <div class="tab-pane" id="div4b">
                                        <div class="panel panel-primary">
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <!--Table-->
                                                    <asp:GridView runat="server" ID="gvConsumos" AutoGenerateColumns="false" AllowPaging="false"
                                                        CssClass="table table-bordered table-hover" Font-Size="Smaller">
                                                        <Columns>
                                                            <asp:BoundField HeaderText="Fecha" DataField="FECHA" />
                                                            <asp:BoundField HeaderText="Código Lectura" DataField="CODIGOLECTURA" />
                                                            <asp:BoundField HeaderText="Lectura Activa" DataField="LECTURAACTIVA" />
                                                            <asp:BoundField HeaderText="Consumo Activo" DataField="CONSUMOACTIVA" />
                                                            <asp:BoundField HeaderText="Lectura Reactiva" DataField="LECTURAREACTIVA" />
                                                            <asp:BoundField HeaderText="Consumo Reactivo" DataField="CONSUMOREACTIVA" />
                                                            <asp:BoundField HeaderText="Activa Real" DataField="ACTIVAREAL" />
                                                            <asp:BoundField HeaderText="Reactiva Real" DataField="REACTIVAREAL" />
                                                            <asp:BoundField HeaderText="Multiplicador" DataField="MULTIPLICADOR" />
                                                            <asp:BoundField HeaderText="Anomalía" DataField="ANOMALIA" />
                                                            <asp:BoundField HeaderText="Observación" DataField="OBSERVACION" />
                                                            <asp:BoundField HeaderText="Observación Crítica" DataField="OBSERVACIONCRITICA" />
                                                        </Columns>
                                                        <EmptyDataTemplate>No Hay informacíon en el grupo.</EmptyDataTemplate>
                                                        <PagerStyle HorizontalAlign="Right" CssClass="pagination-ys" />
                                                    </asp:GridView>
                                                    <!--Table-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                </asp:Panel>
            </div>
        </div>
    </asp:Panel>

    <link rel="stylesheet" href="../GalleryRoot/css/viewer.css" />
    <style>
        .pictures {
            margin: 0;
            padding: 0;
            list-style: none;
            max-width: 30rem;
        }

            .pictures > li {
                float: left;
                width: 50px;
                height: 50px;
                margin: 0 -1px -1px 0;
                border: 1px solid transparent;
                overflow: hidden;
            }

                .pictures > li > img {
                    width: 50px;
                    height: 50px;
                    cursor: -webkit-zoom-in;
                    cursor: zoom-in;
                }

        .viewer-download {
            color: #fff;
            font-family: FontAwesome;
            font-size: .75rem;
            line-height: 1.5rem;
            text-align: center;
        }

            .viewer-download::before {
                content: "\f019";
            }

        #FotoContainer {
            margin-top: 10px !important;
            display: inline-block;
        }

        #sidebar {
            /* para mantener visible un elemento en todo momento aunque se haga scroll en la página */
            /*position: fixed;
            margin-right:100px;
            overflow: hidden;
            right: 0;
            z-index: 9999;*/
            /*margin-top:237px;*/
        }
    </style>

    <script type="text/javascript" src="../Scripts/jquery-3.3.1.js"></script>
    <script>
        function EnterEvent(e) {
            if (e.keyCode == 13) {
                __doPostBack('<%=txtClave.ClientID%>', "");
            }        
        }
    </script>
</asp:Content>
