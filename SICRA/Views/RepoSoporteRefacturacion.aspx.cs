﻿using SICRA.Controllers;
using SICRA.Models;
using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SICRA.Models.SicraBD;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json;
using SICRA.Views.Reportes;
using DevExpress.XtraPrinting;
using System.IO;

namespace SICRA.Views
{
    public partial class RepoSoporteRefacturacion : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private SoportesController _sCtrl = new SoportesController();

        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarSopotesGrid();
            }
            catch (Exception ex)
            {
                gvRectificaciones.DataSource = null;
                gvRectificaciones.DataBind();
            }
        }

        private void CargarSopotesGrid()
        {
            gvRectificaciones.DataSource = _sCtrl.GetSoportesFacturacionBd(decimal.Parse(txtClave.Text.Trim()),_usrLog.usuSicra.Usuario);
            gvRectificaciones.DataBind();
        }

        protected void gvRectificaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CargarSopotesGrid();
            gvRectificaciones.PageIndex = e.NewPageIndex;
            gvRectificaciones.DataBind();
        }

        protected void ckbImprimir_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void ckbAll_CheckedChanged(object sender, EventArgs e)
        {
            if (gvRectificaciones.Rows.Count > 0)
            {
                if (ckbAll.Checked)
                {
                    foreach (GridViewRow rw in gvRectificaciones.Rows)
                    {
                        CheckBox chk = (CheckBox)rw.Cells[0].FindControl("ckbImprimir");
                        chk.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow rw in gvRectificaciones.Rows)
                    {
                        CheckBox chk = (CheckBox)rw.Cells[0].FindControl("ckbImprimir");
                        chk.Checked = false;
                    }
                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> arrayIdSoporte = new List<int>();

                if (gvRectificaciones.Rows.Count > 0)
                {
                    foreach (GridViewRow rw in gvRectificaciones.Rows)
                    {
                        CheckBox chk = (CheckBox)rw.Cells[0].FindControl("ckbImprimir");
                        if (chk.Checked == true)
                        {
                            int IdSoporte = int.Parse(gvRectificaciones.Rows[rw.RowIndex].Cells[0].Text.ToString());
                            arrayIdSoporte.Add(IdSoporte);
                        }
                    }

                    rptSoporteRefactura report = new rptSoporteRefactura();
                    string ids = string.Join(",", arrayIdSoporte.ToArray()) + ",";
                    report.Parameters["sIdSoportes"].Value = ids;
                    report.Parameters["nis"].Value = txtClave.Text.Trim();


                    using (MemoryStream ms = new MemoryStream())
                    {
                        report.ExportToPdf(ms, new PdfExportOptions() { ShowPrintDialogOnOpen = true });
                        WriteDocumentToResponse(ms.ToArray(), "pdf", true, $"Soporte_{txtClave.Text}.pdf");
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        void WriteDocumentToResponse(byte[] documentData, string format, bool isInline, string fileName)
        {
            string contentType;
            string disposition = (isInline) ? "inline" : "attachment";

            switch (format.ToLower())
            {
                case "xls":
                    contentType = "application/vnd.ms-excel";
                    break;
                case "xlsx":
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;
                case "mht":
                    contentType = "message/rfc822";
                    break;
                case "html":
                    contentType = "text/html";
                    break;
                case "txt":
                case "csv":
                    contentType = "text/plain";
                    break;
                case "png":
                    contentType = "image/png";
                    break;
                default:
                    contentType = String.Format("application/{0}", format);
                    break;
            }

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename={1}", disposition, fileName));
            Response.BinaryWrite(documentData);
            Response.End();
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> arrayIdSoporte = new List<int>();

                if (gvRectificaciones.Rows.Count > 0)
                {
                    foreach (GridViewRow rw in gvRectificaciones.Rows)
                    {
                        CheckBox chk = (CheckBox)rw.Cells[0].FindControl("ckbImprimir");
                        if (chk.Checked == true)
                        {
                            int IdSoporte = int.Parse(gvRectificaciones.Rows[rw.RowIndex].Cells[0].Text.ToString());
                            arrayIdSoporte.Add(IdSoporte);
                        }
                    }

                    rptSoporteRefactura report = new rptSoporteRefactura();
                    string ids = string.Join(",", arrayIdSoporte.ToArray()) + ",";
                    report.Parameters["sIdSoportes"].Value = ids;
                    report.Parameters["nis"].Value = txtClave.Text.Trim();


                    using (MemoryStream ms = new MemoryStream())
                    {
                        report.ExportToPdf(ms);
                        Response.Clear();
                        Response.ContentType = "Application/pdf";
                        Response.AddHeader("Content-Disposition", "attachment; filename=Soporte_" + txtClave.Text + ".pdf");
                        Response.BinaryWrite(ms.ToArray());
                        Response.Flush();
                        Response.Close();
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnExpoWord_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> arrayIdSoporte = new List<int>();

                if (gvRectificaciones.Rows.Count > 0)
                {
                    foreach (GridViewRow rw in gvRectificaciones.Rows)
                    {
                        CheckBox chk = (CheckBox)rw.Cells[0].FindControl("ckbImprimir");
                        if (chk.Checked == true)
                        {
                            int IdSoporte = int.Parse(gvRectificaciones.Rows[rw.RowIndex].Cells[0].Text.ToString());
                            arrayIdSoporte.Add(IdSoporte);
                        }
                    }

                    rptSoporteRefactura report = new rptSoporteRefactura();
                    string ids = string.Join(",", arrayIdSoporte.ToArray()) + ",";
                    report.Parameters["sIdSoportes"].Value = ids;
                    report.Parameters["nis"].Value = txtClave.Text.Trim();

                    using (MemoryStream ms = new MemoryStream())
                    {
                        report.ExportToDocx(ms);
                        Response.Clear();
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                        Response.AddHeader("Content-Disposition", "attachment; filename=Soporte_" + txtClave.Text + ".docx");
                        Response.BinaryWrite(ms.ToArray());
                        Response.Flush();
                        Response.Close();
                        Response.End();
                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }
}