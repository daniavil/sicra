﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CargarSoporteRefa.aspx.cs" Inherits="SICRA.Views.CargarSoporteRefa" %>

<%@ Register Assembly="DevExpress.XtraReports.v21.1.Web.WebForms, Version=21.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<%@ Register Src="~/Modals/MsjModal.ascx" TagPrefix="uc1" TagName="MsjModal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <a href="#"><span>SICRA</span></a> <%--agregar al menu que pertenece--%>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
    <a class="UrlActual" href='<%= ResolveUrl("CargarSoporteRefa.aspx") %>'>Carga de Soporte de Refacturación</a>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="form-horizontal" style="text-align: justify">
        <div class="form-group col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Carga de  soporte de Rectificación</div>
                <div class="panel-body">
                    <%--********************************formulario de filtros**************************--%>
                    <br />
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <div class="form-inline">
                                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                    <label for="txtNisrad">NIS:</label>
                                    <asp:TextBox ID="txtClave" runat="server" CssClass="form-control" Width="300px"></asp:TextBox>
                                    <asp:Button ID="btnFiltrar" runat="server" Text="Buscar" CssClass="btn btn-warning" OnClick="btnFiltrar_Click" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <asp:Label ID="lblMsj" runat="server" ForeColor="Red" />
                            </div>
                        </div>
                    </div>
                    <%--********************************FIN formulario de filtros**************************--%>

                    <%--********soportes por ciclos********--%>

                    <asp:Panel runat="server" ID="pnlCarga" Visible="false">
                        <div class="form-inline">
                            <asp:Panel runat="server" ID="panelLoadExcel">
                                <asp:HiddenField runat="server" ID="hfRutaArchivo"></asp:HiddenField>
                                <br />
                                <div class="form-group">
                                    <asp:FileUpload ID="fUpload" runat="server" CssClass="form-control" />
                                </div>
                                <div class="form-group">
                                    <asp:Button runat="server" ID="btncargar" CssClass="btn btn-success" Text="Subir Soporte" OnClick="btncargar_Click" OnClientClick="Confirm()" />
                                </div>
                                <div>
                                    <asp:Label ID="lblIdGestion" runat="server" ForeColor="Blue" Font-Size="Smaller" />
                                </div>
                            </asp:Panel>
                        </div>

                        <br />

                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                            <span style="background-color: lightgreen">Cantidad Soportes Subidos:</span>
                            <span id="spnconteo" runat="server" style="font-weight: bold"></span>
                        </div>
                        <div class="form-group-sm col-sm-12 col-md-12 col-lg-12" style="height: auto; max-height: 40%; overflow-y: scroll;">
                            <asp:GridView ID="gvbitacora" runat="server" Width="100%" AutoGenerateColumns="false" AllowPaging="false" PagerStyle-CssClass="pager"
                                CssClass="mydatagrid" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                                <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="nisrad" HeaderText="NIS" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    <asp:BoundField DataField="archivo" HeaderText="Nombre Archivo" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    <asp:BoundField DataField="fechaCarga" HeaderText="Fecha/Hora"></asp:BoundField>
                                    <asp:BoundField DataField="usuario" HeaderText="Usuario" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%--<asp:hyperlink id="hlDownload" runat="server" NavigateUrl='<%# Eval("ruta") %>' Text="VER" Target='<%# "_blank" %>' style="color: black !important;"></asp:hyperlink>--%>
                                            <asp:LinkButton id="hlDownload" runat="server" CommandArgument='<%# Eval("ruta") %>' OnCommand="hlDownload_Command" CommandName='<%# Eval("archivo") %>' Text="VER" style="color: black !important;" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <asp:Label ID="lblerror" runat="server" ForeColor="Red" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>

    <script>
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("¿Está seguro de realizar esta acción?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>

    <style>
        .hlDownload {
            color:black !important;
        }
    </style>
</asp:Content>
