﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
//using Epplus;

namespace SICRA.Views
{
    public partial class ResolverCriticaRefactura : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private CriticaRefacturaController _cCtrl = new CriticaRefacturaController();
        private CriticaLecturaController _clCtrl = new CriticaLecturaController();
        private UsuarioController _uCtrl = new UsuarioController();

        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                CargarddlCiclos();
                //CargarMuestrasBd();
            }
        }
        private void CargarddlCiclos()
        {
            try
            {
                ddlCiclo.DataSource = null;
                ddlCiclo.DataSource = _cCtrl.GetCiclosByUser(_usrLog.user);
                ddlCiclo.DataBind();
                ddlCiclo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
            }
            catch (Exception)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }

        }
        private void CargarddlDiales(int ciclo)
        {
            try
            {
                ddldial.DataSource = null;
                ddldial.DataSource = _cCtrl.GetDialesByUser(_usrLog.user, ciclo);
                ddldial.DataBind();
                ddldial.Items.Insert(0, new ListItem("--SELECCIONAR--", "0"));
            }
            catch (Exception ex)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }

        }
        private void CargarMuestrasBd(int ciclo, int dial)
        {
            var muestras = _cCtrl.GetAsignacionByUser(_usrLog.user, ciclo, dial);
            gvRepoM.DataSource = null;
            gvRepoM.DataSource = muestras;
            gvRepoM.DataBind();

            spnNoResueltas.InnerText = muestras.Count.ToString();
        }
        protected void gvRepoM_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    string IdM = gvRepoM.DataKeys[e.Row.RowIndex].Value.ToString();

            //    TableCell cellResuelto = e.Row.Cells[7];

            //    if (bool.Parse(cellResuelto.Text) == true)
            //    {
            //        for (int i = 0; i < e.Row.Cells.Count; i++)
            //        {
            //            e.Row.Cells[i].BackColor = Color.LightGreen;
            //        }
            //    }
            //    else
            //    {
            //        for (int i = 0; i < e.Row.Cells.Count; i++)
            //        {
            //            e.Row.Cells[i].BackColor = Color.LightCoral;
            //        }
            //    }
            //}
        }
        private void cargarDatos(string commandargument)
        {
            string idrow = commandargument;
            hfIdM.Value = string.Empty;
            var muestraBd = _cCtrl.GetAsignacionById(idrow);
            hfIdM.Value = idrow;
            int clave = muestraBd.nisrad;

            hfFilCiclo.Value = ddlCiclo.SelectedValue;
            hfFilDial.Value = ddldial.SelectedValue;

            ClienteRoot cliente = GetCliente(clave.ToString());
            CriticaRefacturaDtoInfo infoCritica = null;
            SetClearValues();

            if (cliente != null)
            {
                literalControl.Text = cliente.ImagenesInnerText;
                lblNisrad.Text = cliente.cliente.nis_rad;
                lblMedidor.Text = cliente.cliente.num_medidor;
                lblUbicacion.Text = cliente.cliente.ubicacion;
                lblTarifa.Text = cliente.cliente.nom_tarifa;
                lblCliente.Text = cliente.cliente.nom_cli;
                lblDireccion.Text = cliente.cliente.direccion;

                int cicloIncms = int.Parse("20" + muestraBd.Ciclo);
                var info = _clCtrl.GetInfoMuestraWs(cliente.cliente.clave.ToString(), cicloIncms);
                infoCritica = _cCtrl.GetCriticaFactInfo(cliente.cliente.nis_rad.ToString(), muestraBd.Ciclo);

                if (infoCritica != null)
                {
                    UpdatePanel1.Visible = true;
                    btnAceptar.Visible = true;
                    lblMsjError.Visible = false;
                    lblMsjError.Text = string.Empty;
                    lblOrigenCritica.Text = infoCritica.Origen;
                    lblUsrCri.Text = infoCritica.codUsuarioRef;
                    lblObsCri.Text = infoCritica.Observacion;
                    var usrCri = _uCtrl.GetUsuarioByUser(infoCritica.codUsuarioRef);
                    lblNomUsr.Text = usrCri == null ? "Usuario No existe en SICRA." : usrCri.NomPersona;

                    if (info.CriticaLecturaInfo != null)
                    {
                        lblLecturaEncontrada.Text = info.CriticaLecturaInfo.LECTURAENCONTRADA;
                        lblFLectura.Text = info.CriticaLecturaInfo.FECHALECTURA;
                        lblFProgramada.Text = info.CriticaLecturaInfo.FECHA_PROGRAMADA;
                        lblDial.Text = info.CriticaLecturaInfo.CODIGODIAL == null ? muestraBd.dial.ToString() : info.CriticaLecturaInfo.CODIGODIAL;
                        lblCodAnomaliaLect.Text = info.CriticaLecturaInfo.CODIGOANOMALIA;
                        lblAnomaliaLect.Text = info.CriticaLecturaInfo.DESCRIPCION;
                        lblTipoLect.Text = info.CriticaLecturaInfo.tipolect;

                        if (muestraBd.Resuelto)
                        {
                            btnAceptar.Visible = false;
                        }
                        else
                        {
                            btnAceptar.Visible = true;
                        }

                        lblMsjSave.Text = string.Empty;
                    }
                }
                else
                {
                    UpdatePanel1.Visible = false;
                    lblMsjError.Visible = true;
                    lblMsjError.Text = "Ocurrió un error al cargar datos, por favor vuelva a intentar.";
                    SetClearValues();
                    btnAceptar.Visible = false;
                }
            }
            hfObj.Value = string.Empty;
            mpResolver.Show();
        }
        void SetClearValues()
        {
            literalControl.Text = null;
            lblNisrad.Text = "S/D";
            lblMedidor.Text = "S/D";
            lblUbicacion.Text = "S/D";
            lblTarifa.Text = "S/D";
            lblCliente.Text = "S/D";
            lblDireccion.Text = "S/D";
            lblLecturaEncontrada.Text = "S/D";
            lblFLectura.Text = "S/D";
            lblFProgramada.Text = "S/D";
            lblDial.Text = "S/D";
            lblCodAnomaliaLect.Text = "S/D";
            lblAnomaliaLect.Text = "S/D";
            lblTipoLect.Text = "S/D";
        }
        protected void gvRepoM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToUpper() == "RESOLVER")
            {
                try
                {
                    cargarDatos(e.CommandArgument.ToString());
                }
                catch (Exception ex)
                {

                }
            }
        }
        private ClienteRoot GetCliente(string clave)
        {
            var clienteRoot = _clCtrl.CargarInfoCliente(clave);
            try
            {
                if (clienteRoot != null)
                {
                    List<imagePreview> imageUrlList = new List<imagePreview>();
                    List<PrintConfigPreviewViewModel> imageList = new List<PrintConfigPreviewViewModel>();

                    if (clienteRoot.cliFotos != null)
                    {
                        foreach (var (item, index) in clienteRoot.cliFotos.Select((v, i) => (v, i)))
                        {
                            try
                            {
                                if (item.origen == "Logistics")
                                {
                                    string path = Request.PhysicalApplicationPath;
                                    byte[] bytes = Convert.FromBase64String(item.imagen);

                                    MemoryStream ms = new MemoryStream(bytes);
                                    ms.Position = 0;
                                    imageList.Add(new PrintConfigPreviewViewModel() { Name = item.fecha, Picture = bytes, clave = item.clave });
                                }
                                else
                                {
                                    byte[] img1 = System.IO.File.ReadAllBytes(item.imagen);
                                    imageList.Add(new PrintConfigPreviewViewModel() { Name = item.fecha, Picture = img1, clave = item.clave });
                                }
                            }
                            catch (Exception)
                            {
                                imageUrlList = null;
                                imageList = null;

                            }
                        }
                        imageUrlList = GuardarCopiaImagenes(imageList, clave);
                        clienteRoot.ImagenesInnerText = GetImagenesMosaico(imageUrlList).Text;
                    }
                    else
                    {
                        imageUrlList = null;
                        clienteRoot.ImagenesInnerText = string.Empty;
                    }
                }
                else
                {
                    clienteRoot.cliFotos = null;
                }
                clienteRoot.ListaHistoricoLectura = null;
                clienteRoot.ListaHistoricoConsumo = null;
                return clienteRoot;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public class imagePreview
        {
            public string Name { get; set; }
            public string url { get; set; }
        }
        public class PrintConfigPreviewViewModel
        {
            public string Name { get; set; }
            public byte[] Picture { get; set; }
            public string clave { get; set; }
        }
        public LiteralControl GetImagenesMosaico(List<imagePreview> images)
        {
            LiteralControl html = new LiteralControl();

            string folderProject = Request.Url.AbsolutePath.ToString().Trim();

            string content = @"<div id=""galley"" runat=""server"" ClientIDMode=""static""><ul class='pictures'>";

            try
            {
                int Dividendo = images.Count(), divisor = 6, q, r, totalFilas = 0;
                q = (int)(Dividendo / divisor);
                r = Dividendo % divisor;
                totalFilas = q + (r > 0 ? 1 : 0);

                string imageneshtml = "";

                foreach (var (item, index) in images.Select((v, i) => (v, i)))
                {
                    if (item.url != "")
                    {
                        string path = HttpContext.Current.Request.Url.Authority.ToString();
                        //imageneshtml += $@"<li><img data-original='{item.Name}' src ='{folderProject}/ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                        imageneshtml += $@"<li><img data-original='{item.Name}' src ='../ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                    }
                }
                content += imageneshtml;
                content += " </ul></div>";
            }
            catch (Exception)
            {
                content = "<H3>SIN IMÁGENES</H3>";
            }
            
            html.Text = content;
            return html;
        }
        public List<imagePreview> GuardarCopiaImagenes(List<PrintConfigPreviewViewModel> imagenes, string clave)
        {
            try
            {
                List<imagePreview> ImgFiles = new List<imagePreview>();
                //foreach (var item in imagenes)
                foreach (var (item, index) in imagenes.Select((v, i) => (v, i)))
                {
                    string path = Request.PhysicalApplicationPath;
                    string nombreArchivo = $"{path}ImgCli\\img{index}_{item.Name}_C{item.clave}.Jpeg";

                    //string resolvedUrl = System.Web.HttpContext.Current.Request.Url.Authority.ToString();//VirtualPathUtility.ToAbsolute("~/");

                    ImgFiles.Add(new imagePreview() { url = $"img{index}_{item.Name}_C{item.clave}.Jpeg", Name = $"img{index}_{item.Name}_C{item.clave}" });

                    Bitmap objBitMap = new Bitmap(byteArrayToImage(item.Picture));
                    objBitMap.Save(nombreArchivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                return ImgFiles;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
                ms.Position = 0;
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
            catch (Exception)
            {
                return null;
            }
        }
        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                try
                {
                    var m = _cCtrl.GetAsignacionById(hfIdM.Value);
                    //m.idRow =Guid.Parse(hfIdM.Value);
                    m.fechaResolucion = DateTime.Now;
                    m.Resuelto = true;
                    m.P1RefacturaCorrecta = ddlP1RefacturaCorrecta.SelectedValue;
                    m.P2DocSubida= ddlP2DocSubida.SelectedValue;
                    m.P3ProcesoAutorizacionCorrecto = ddlP3ProcesoAutorizacionCorrecto.SelectedValue;
                    m.P4CorreccionProcedente= ddlP4CorreccionProcedente.SelectedValue;
                    m.P5DatosResoCorrecto = ddlP5DatosResoCorrecto.SelectedValue;
                    m.P6DocCorrecta = ddlP6DocCorrecta.SelectedValue;
                    m.P7ObsCierreCorrecto= ddlP7ObsCierreCorrecto.SelectedValue;
                    m.P8RepoSicra= ddlP8RepoSicra.SelectedValue;
                    m.P9OSRevGen= ddlP9OSRevGen.SelectedValue;
                    _cCtrl.PutMuestraCritica(m);
                }
                catch (Exception ex)
                {
                    string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                    lblMsjSave.Text = msj;
                }
                
                int fitroCiclo = int.Parse(hfFilCiclo.Value);
                int fitroDial = int.Parse(hfFilDial.Value);
                Fun.ClearControls(this);

                hfFilCiclo.Value = fitroCiclo.ToString();
                hfFilDial.Value = fitroDial.ToString();

                CargarMuestrasBd(int.Parse(hfFilCiclo.Value), int.Parse(hfFilDial.Value));

                ddlCiclo.SelectedValue = hfFilCiclo.Value;
                ddldial.SelectedValue = hfFilDial.Value;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //Fun.ClearControls(this);
        }
        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarMuestrasBd(int.Parse(ddlCiclo.SelectedValue),int.Parse(ddldial.SelectedValue));
            }
            catch (Exception ex)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }
            
        }
        protected void ddlCiclo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CargarddlDiales(int.Parse(ddlCiclo.SelectedValue));
                if (hfFilDial.Value != "")
                {
                    ddldial.SelectedValue = hfFilDial.Value;
                }
            }
            catch (Exception)
            {
                CargarddlCiclos();
                ddldial.Items.Clear();
                ddldial.DataSource = null;
                ddldial.DataBind();
                ddldial.SelectedIndex = -1;
            }
            
        }
    }
}