﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SoporteRefacturacion.aspx.cs" Inherits="SICRA.Views.SoporteRefacturacion" %>
<%@ Register Src="~/Modals/MsjModal.ascx" TagPrefix="uc1" TagName="MsjModal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <a href="#"><span>SICRA</span></a> <%--agregar al menu que pertenece--%>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
    <a class="UrlActual" href='<%= ResolveUrl("SoporteRefacturacion.aspx") %>'>Soporte de Refacturación</a>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="form-horizontal" style="text-align:justify">
        <div class="form-group col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Solicitud de Rectificación de Factura</div>
                <div class="panel-body">
                    <%--********************************formulario de filtros**************************--%>
                    <br />
                    <asp:Panel id="pnlFiltro" runat="server">
                        <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#demo">FILTROS</button>
                        <div id="demo" class="collapse">
                            <div class="panel panel-success">
                                <div class="panel-body">
                                    <div class="form-horizontal">
                                        <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                            <label for="txtNisrad">NIS:</label>
                                            <asp:TextBox ID="txtClave" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                            <label for="txtPeriodo">Periodo [Año(4) Mes(2)]:</label>
                                            <asp:TextBox ID="txtPeriodo" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:Button ID="btnFiltrar" runat="server" Text="Buscar" CssClass="btn btn-warning" OnClick="btnFiltrar_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--********************************FIN formulario de filtros**************************--%>

                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <asp:Label ID="lblMsj" runat="server" Visible="False" Style="font-size: medium" CssClass="label label-danger"></asp:Label>
                        <input id="hfNisrad" type="hidden" name="hfIdM" runat="server" />
                        <input id="hfPeriodo" type="hidden" name="hfIdM" runat="server" />
                    </div>

                    <%--********************************Panel de Informacion Soportes**************************--%>
                    <asp:Panel ID="pnlInfo" runat="server" Visible="false">
                        <asp:HiddenField ID="idSopSet" runat="server" />
                        <%--********DATOS GENERALES********--%>
                        <div class="form-horizontal">
                            <div class="form-group-lg col-sm-6 col-md-6 col-lg-6">
                                <label for="txtUsuario">Usuario:</label>
                                <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div class="form-group-lg col-sm-6 col-md-6 col-lg-6">
                                <label for="txtPqr">PQR:</label>
                                <asp:TextBox ID="txtPqr" runat="server" CssClass="form-control" ReadOnly="false"></asp:TextBox>
                            </div>
                            <div class="form-group-lg col-sm-6 col-md-6 col-lg-6">
                                <label for="lblCliente">Nombre:</label>
                                <asp:Label ID="lblCliente" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-lg col-sm-2 col-md-2 col-lg-2">
                                <label for="lblClave">Clave:</label>
                                <asp:Label ID="lblClave" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-lg col-sm-2 col-md-2 col-lg-2">
                                <label for="lblUbicacion">Ubicación:</label>
                                <asp:Label ID="lblUbicacion" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-lg col-sm-2 col-md-2 col-lg-2">
                                <label for="lblFechaFact">Fecha:</label>
                                <asp:Label ID="lblFechaFact" runat="server"></asp:Label>
                            </div>
                            <asp:HiddenField ID="hfSecNis" runat="server" />
                            <asp:HiddenField ID="hfSecRec" runat="server" />
                            <asp:HiddenField ID="hfSecRecAnul" runat="server" />
                            <asp:HiddenField ID="hfFfactAnul" runat="server" />
                            <asp:HiddenField ID="hfFfactAnt" runat="server" />
                            <asp:HiddenField ID="hfOrden" runat="server" />
                        </div>
                        <div class="form-horizontal">
                            <div class="form-group-lg col-sm-4 col-md-4 col-lg-4">
                                <label for="lblCicloFact">Ciclo de Facturación:</label>
                                <asp:Label ID="lblCicloFact" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-lg col-sm-4 col-md-4 col-lg-4">
                                <label for="lblCicloRefact">Ciclo de Refacturación:</label>
                                <asp:Label ID="lblCicloRefact" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-lg col-sm-4 col-md-4 col-lg-4">
                                <label for="lblTarifa">Tarifa:</label>
                                <asp:Label ID="lblTarifa" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-lg col-sm-12 col-md-12 col-lg-12">
                                <hr class="hr-success" />
                            </div>
                        </div>
                        <%--********DATOS LECTURAS Y CONSUMOS********--%>
                        <div class="form-horizontal">
                            <div class="form-group-sm col-sm-3 col-md-3 col-lg-3">
                                <label for="lblConsumoAnt">Consumo Facturado:</label>
                                <asp:Label ID="lblConsumoAnt" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-sm col-sm-3 col-md-3 col-lg-3">
                                <label for="lblLectActivaAnt">Lectura Activa Facturado:</label>
                                <asp:Label ID="lblLectActivaAnt" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-sm col-sm-3 col-md-3 col-lg-3">
                                <label for="lblLectReactivaAnt">Lectura Reactiva Facturado:</label>
                                <asp:Label ID="lblLectReactivaAnt" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-sm col-sm-3 col-md-3 col-lg-3">
                                <label for="lblLectDemandaAnt">Lectura Demanda Facturado:</label>
                                <asp:Label ID="lblLectDemandaAnt" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="form-horizontal">
                            <div class="form-group-sm col-sm-3 col-md-3 col-lg-3">
                                <label for="lblConsumoAct">Consumo Rectificado:</label>
                                <asp:Label ID="lblConsumoAct" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-sm col-sm-3 col-md-3 col-lg-3">
                                <label for="lblLectActivaAct">Lectura Activa Rectificado:</label>
                                <asp:Label ID="lblLectActivaAct" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-sm col-sm-3 col-md-3 col-lg-3">
                                <label for="lblLectReactivaAct">Lectura Reactiva Rectificado:</label>
                                <asp:Label ID="lblLectReactivaAct" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-sm col-sm-3 col-md-3 col-lg-3">
                                <label for="lblLectDemandaAct">Lectura Demanda Rectificado:</label>
                                <asp:Label ID="lblLectDemandaAct" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="form-horizontal">
                            <div class="form-group-sm col-sm-3 col-md-3 col-lg-3">
                                <label for="lblDiferencia">Diferencia:</label>
                                <asp:Label ID="lblDiferencia" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-sm col-sm-3 col-md-3 col-lg-3">
                                <label for="lblValDoc">Valor Documento:</label>
                                <asp:Label ID="lblValDoc" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-sm col-sm-6 col-md-6 col-lg-6">
                                <label for="lblDebitoCredito">DB/CR:</label>
                                <asp:Label ID="lblDebitoCredito" runat="server"></asp:Label>
                            </div>
                            <div class="form-group-lg col-sm-12 col-md-12 col-lg-12">
                                <hr class="hr-success" />
                            </div>
                        </div>

                        <%--********OTRAS RECTIFICACIONES (IMPORTES)********--%>
                        <div class="form-horizontal">
                            <div class="form-group-sm col-sm-12 col-md-12 col-lg-12" style="text-align: center">
                                <h2>OTRAS RECTIFICACIONES</h2>
                            </div>
                            <div class="form-group-sm col-sm-12 col-md-12 col-lg-12">
                                <asp:GridView ID="gvRectificaciones" runat="server" Width="100%" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="dato1" HeaderText="Importe"></asp:BoundField>
                                        <asp:BoundField DataField="dato2" HeaderText="Corregido"></asp:BoundField>
                                        <asp:BoundField DataField="dato3" HeaderText="Facturado"></asp:BoundField>
                                        <asp:BoundField DataField="dato4" HeaderText="Diferencia"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="form-group-sm col-sm-12 col-md-12 col-lg-12" style="text-align: center">
                                <h3 runat="server" id="lbldbcr"></h3>
                            </div>
                        </div>
                        <div class="form-horizontal">
                            <div class="form-group-lg col-sm-12 col-md-12 col-lg-12">
                                <hr class="hr-success" />
                            </div>
                            <div class="form-group-lg col-sm-12 col-md-12 col-lg-12">
                                <label for="txtObsAdi">Observaciones Adicionales:</label>
                                <asp:TextBox ID="txtObsAdi" runat="server" CssClass="form-control" ReadOnly="false" TextMode="MultiLine" Rows="10"></asp:TextBox>
                            </div>
                            <div class="form-group-lg col-sm-12 col-md-12 col-lg-12">
                                <hr class="hr-warning" />
                            </div>
                        </div>
                        <div class="form-horizontal">
                            <div class="form-group-lg col-sm-12 col-md-12 col-lg-12">
                                <asp:Button ID="btnGuardar" runat="server" Text="Guardar Soporte" CssClass="btn btn-warning" OnClick="btnGuardar_Click" OnClientClick="Confirm()" />
                            </div>
                            <div class="form-group-lg col-sm-12 col-md-12 col-lg-12">
                                <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" Visible="false" CssClass="btn btn-primary" OnClick="btnActualizar_Click" OnClientClick="Confirm()" />
                            </div>
                        </div>

                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>

    <script>
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("¿Está seguro de realizar esta acción?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>

<%--    <style>

        .file{
            margin-left:25%;
            margin-top:4%;
            margin-bottom:4%;
        }

        .titulo{
            margin-left: 35%;
            margin-bottom: 4%;
        }
        .total{
            margin-left:60%;
            margin-bottom: 1.5%;
            margin-top: 1.5%;
        }

        .btnCargar{
            margin-left: 65%;
            margin-top: 2%;
            margin-bottom: 2%;
        }

        .loading{
            position:fixed;
            top:0px;
            right:0px;
            width:100%;
            height:100%;
            background-color:#ffffff;
            background-repeat:no-repeat;
            background-position:center;
            z-index:10000000;
            opacity: 0.80;
            filter: alpha(opacity=0); 
        }

    </style>
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updPanSoporte">
        <ContentTemplate>
            <asp:UpdateProgress runat="server" ID="progress" DynamicLayout="true" AssociatedUpdatePanelID="updPanSoporte">
                <ProgressTemplate>
                    <div style="text-align:center" class="loading">
                        <img src='<%= ResolveUrl("~/Imagenes/brickLoading.gif") %>'/>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Panel runat="server" ID="panelPrincipal">
            </asp:Panel>
            <uc1:MsjModal runat="server" ID="MsjModal"></uc1:MsjModal>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>
