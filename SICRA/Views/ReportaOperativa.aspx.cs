﻿using SICRA.Controllers;
using SICRA.Models;
using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SICRA.Models.SicraBD;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Drawing;
using System.Net;

namespace SICRA.Views
{
    public partial class ReportaOperativa : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        int valMultiMedida = int.Parse(ConfigurationManager.AppSettings["Multi_Medida"].ToString());
        private ClienteController _cCtrl = new ClienteController();
        private LogisticsController _lCtrl = new LogisticsController();
        private RepoOperativaController _rCtrl = new RepoOperativaController();
        private RegionesController _rgCtrl = new RegionesController();
        private SoeehController _sCtrl = new SoeehController();
        public readonly int MaxDiasRepoOpe = int.Parse(ConfigurationManager.AppSettings["MaxDiasRepoOpe"]);
        public readonly string pathImgRepOpe = ConfigurationManager.AppSettings["pathImgRepOpe"].ToString();
        private string _nombreArchivo = "";

        /**********************************************Eventos*************************************************/
        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                string nis = string.Empty;
                string ciclo = string.Empty;

                nis = Request.QueryString.Get("n");
                ciclo = Request.QueryString.Get("c");

                if (!string.IsNullOrEmpty(nis) && !string.IsNullOrEmpty(ciclo))
                {
                    txtClave.Text = nis;
                    txtCicloRefa.Text = ciclo;
                    CargarDatosCli();
                }
            }
        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            CargarDatosCli();
        }
        protected void btnReportar_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                try
                {
                    var SaveOk = new Rep_Operativa_M();
                    bool SaveOkImg = false;

                    var r = _rCtrl.GetReporteOperativaByClaveCiclo(Convert.ToInt32(txtClave.Text.Trim()), Convert.ToInt32(txtCicloRefa.Text.Trim()));

                    if (r != null)
                    {
                        r.IdRow_M = r.IdRow_M;
                        r.LecturaActiva = !string.IsNullOrEmpty(txtLecActiva.Text) ? long.Parse(txtLecActiva.Text.Trim()) : (long?)null;
                        r.ConsumoActivaFact = !string.IsNullOrEmpty(txtConsumoActivoFact.Text) ? long.Parse(txtConsumoActivoFact.Text.Trim()) : (long?)null;
                        r.LecturaActivaRetira = !string.IsNullOrEmpty(txtLecActivaRet.Text) ? long.Parse(txtLecActivaRet.Text.Trim()) : (long?)null;
                        r.LecturaReactiva = !string.IsNullOrEmpty(txtLecReactiva.Text) ? long.Parse(txtLecReactiva.Text.Trim()) : (long?)null;
                        r.ConsumoReactivaFact = !string.IsNullOrEmpty(txtConsumoReactivoFact.Text) ? long.Parse(txtConsumoReactivoFact.Text.Trim()) : (long?)null;
                        r.LecturaReactivaRetira = !string.IsNullOrEmpty(txtLecReactivaRet.Text) ? long.Parse(txtLecReactivaRet.Text.Trim()) : (long?)null;
                        r.LecturaDemanda = !string.IsNullOrEmpty(txtLecDemanda.Text) ? decimal.Parse(txtLecDemanda.Text.Trim()) : (decimal?)null;
                        r.ConsumoDemandaFact = !string.IsNullOrEmpty(txtConsumoDemandaFact.Text) ? decimal.Parse(txtConsumoDemandaFact.Text.Trim()) : (decimal?)null;
                        r.MultiplicadorAnt = !string.IsNullOrEmpty(txtMulAnt.Text) ? decimal.Parse(txtMulAnt.Text.Trim()) : (decimal?)null;
                        r.MultiplicadorAct = !string.IsNullOrEmpty(txtMulActual.Text) ? decimal.Parse(txtMulActual.Text.Trim()) : (decimal?)null;
                        r.Anomalia = int.Parse(ddlAnomalia.SelectedValue.ToString());
                        //r.SolcitudCausaVerificacion = txtCausa.Text.Trim();
                        r.dial = int.Parse(ddlDial.SelectedValue.ToString());
                        r.FechaCambioMedidor = !string.IsNullOrEmpty(txtFcambioMedidor.Text) ? DateTime.Parse(txtFcambioMedidor.Text) : (DateTime?)null;
                        r.FechaRegistro = DateTime.Parse(txtFechaRepo.Text);
                        r.FechaMaxResol = DateTime.Now.AddDays(MaxDiasRepoOpe);
                        r.Ciclo = r.Ciclo;
                        r.Resuelto = false;
                        r.UsuarioReporta = _usrLog.user;
                        r.Rep_Operativa_D = r.Rep_Operativa_D;
                        r.Tarifa = txtTarifa.Text;
                        r.MedidorAnt = txtMedAnt.Text;
                        r.MedidorAct = txtMedAct.Text;
                        r.OS = string.IsNullOrWhiteSpace(txtOs.Text.Trim()) ? (int?)null : int.Parse(txtOs.Text.Trim());
                        r.IdPerfilResoRepo = _usrLog.usuSicra.IdPerfilResoRepo;

                        SaveOk = _rCtrl.PutReporteOperativaM(r);


                        //guardar detalle

                        var rd = new Rep_Operativa_D
                        {
                            IdRow_M = SaveOk.IdRow_M,
                            Estado = "Reporte",
                            Resolucion = txtCausa.Text.Trim(),
                            FechaResolucion = DateTime.Now,
                            UsuarioResuelve = _usrLog.user
                        };
                        _rCtrl.PutReporteOperativaDetalle(rd);
                    }
                    else
                    {
                        var ReporteClave = new Rep_Operativa_M
                        {
                            IdRow_M = 0,
                            Nis_Rad = decimal.Parse(txtClave.Text.Trim()),
                            idSector = int.Parse(hfIdSector.Value),
                            Ubicacion = txtUbicacion.Text.Trim(),
                            LecturaActiva = !string.IsNullOrEmpty(txtLecActiva.Text) ? long.Parse(txtLecActiva.Text.Trim()) : (long?)null,
                            ConsumoActivaFact = !string.IsNullOrEmpty(txtConsumoActivoFact.Text) ? long.Parse(txtConsumoActivoFact.Text.Trim()) : (long?)null,
                            LecturaActivaRetira = !string.IsNullOrEmpty(txtLecActivaRet.Text) ? long.Parse(txtLecActivaRet.Text.Trim()) : (long?)null,
                            LecturaReactiva = !string.IsNullOrEmpty(txtLecReactiva.Text) ? long.Parse(txtLecReactiva.Text.Trim()) : (long?)null,
                            ConsumoReactivaFact = !string.IsNullOrEmpty(txtConsumoReactivoFact.Text) ? long.Parse(txtConsumoReactivoFact.Text.Trim()) : (long?)null,
                            LecturaReactivaRetira = !string.IsNullOrEmpty(txtLecReactivaRet.Text) ? long.Parse(txtLecReactivaRet.Text.Trim()) : (long?)null,
                            LecturaDemanda = !string.IsNullOrEmpty(txtLecDemanda.Text) ? decimal.Parse(txtLecDemanda.Text.Trim()) : (decimal?)null,
                            ConsumoDemandaFact = !string.IsNullOrEmpty(txtConsumoDemandaFact.Text) ? decimal.Parse(txtConsumoDemandaFact.Text.Trim()) : (decimal?)null,
                            MultiplicadorAnt = !string.IsNullOrEmpty(txtMulAnt.Text) ? decimal.Parse(txtMulAnt.Text.Trim()) : (decimal?)null,
                            MultiplicadorAct = !string.IsNullOrEmpty(txtMulActual.Text) ? decimal.Parse(txtMulActual.Text.Trim()) : (decimal?)null,
                            Medida = txtMedida.Text.Trim(),
                            Anomalia = int.Parse(ddlAnomalia.SelectedValue.ToString()),
                            SolcitudCausaVerificacion = txtCausa.Text.Trim(),
                            dial = int.Parse(ddlDial.SelectedValue.ToString()),
                            FechaCambioMedidor = !string.IsNullOrEmpty(txtFcambioMedidor.Text) ? DateTime.Parse(txtFcambioMedidor.Text) : (DateTime?)null,
                            FechaRegistro = DateTime.Now,
                            FechaMaxResol = DateTime.Parse(txtFechaResol.Text),
                            Ciclo = Convert.ToInt32(txtCicloRefa.Text.Trim()),//_rCtrl.GetCicloFactura(),
                            Resuelto = false,
                            UsuarioReporta = _usrLog.user,
                            //Rep_Operativa_D = new Rep_Operativa_D(),
                            Tarifa = txtTarifa.Text,
                            MedidorAnt = txtMedAnt.Text,
                            MedidorAct = txtMedAct.Text,
                            OS = string.IsNullOrWhiteSpace(txtOs.Text.Trim()) ? (int?)null : int.Parse(txtOs.Text.Trim()),
                            IdPerfilResoRepo = _usrLog.usuSicra.IdPerfilResoRepo
                        };
                        SaveOk = _rCtrl.PutReporteOperativaM(ReporteClave);
                    }

                    //GUARDADO FINAL - IMAGEN
                    if (SaveOk != null)
                    {
                        GuardarFilesBd(SaveOk);
                    }

                    lblMsj.Visible = true;
                    if (r != null)
                    {
                        lblMsj.Text = "ATENCIÓN: Esta Clave ya ha sido reportada anteriormente, se realizó actualización.";
                        lblMsj.Attributes.Add("class", "label label-warning");
                    }
                    else
                    {
                        lblMsj.Text = "Los datos se han guardado correctamente.";
                        lblMsj.Attributes.Add("class", "label label-success");
                    }
                }
                catch (Exception ex)
                {
                    string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                    lblMsj.Visible = true;
                    lblMsj.Text = msj;
                    lblMsj.Attributes.Add("class", "label label-danger");
                }
            }
        }
        protected void ddlDial_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtFechaRepo.Text = DateTime.Now.ToShortDateString();
                txtFechaResol.Text = DateTime.Now.AddDays(MaxDiasRepoOpe).ToShortDateString();
            }
            catch (Exception)
            {
                txtFechaRepo.Text = "";
                txtFechaResol.Text = "";
            }
        }
        protected void txtOs_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtOs.Text.Trim() != "")
                {
                    var Os = _sCtrl.GetExisteOS(int.Parse(txtOs.Text.Trim()));

                    if (Os.Texto1 == "0")
                    {
                        lblOs.ForeColor = Color.Red;
                    }
                    else
                    {
                        lblOs.ForeColor = Color.Green;
                    }
                    lblOs.Text = Os.Texto2;
                }
                else
                {
                    lblOs.ForeColor = Color.Orange;
                    lblOs.Text = "Sin OS";
                }
            }
            catch (Exception)
            {
                lblOs.ForeColor = Color.Red;
                lblOs.Text = "OS no valida";
            }
        }

        /***********************************************METDOS**********************************************/
        private void CargarDatosCli()
        {
            try
            {
                if (Convert.ToInt32(txtClave.Text.Trim()) > 0 && Convert.ToInt32(txtCicloRefa.Text.Trim())>0)
                {
                    //consultar si existe registro en bd
                    var reporExiste = _rCtrl.GetReporteOperativaByClaveCiclo(Convert.ToInt32(txtClave.Text.Trim()), Convert.ToInt32(txtCicloRefa.Text.Trim()));

                    if (reporExiste != null)
                    {
                        lblRepe.Visible = true;
                        lblRepe.Text = "ATENCIÓN: Esta Clave ya ha sido reportada anteriormente.!!";
                        ddlAnomalia.SelectedValue = reporExiste.Anomalia.ToString();
                        ddlDial.SelectedValue = reporExiste.dial.ToString();
                        hfIdM.Value = reporExiste.IdRow_M.ToString();
                        txtCausa.Text = reporExiste.SolcitudCausaVerificacion;
                        txtUbicacion.Text = reporExiste.Ubicacion;
                        txtRegion.Text = reporExiste.Sectores.Regiones.Region;
                        txtSector.Text = reporExiste.Sectores.Sector;
                        txtMedida.Text = reporExiste.Medida;
                        txtTarifa.Text = reporExiste.Tarifa;
                        txtMulAnt.Text = reporExiste.MultiplicadorAnt.ToString();
                        txtMulActual.Text = reporExiste.MultiplicadorAct.ToString();
                        txtLecActiva.Text = reporExiste.LecturaActiva.ToString();
                        txtLecReactiva.Text = reporExiste.LecturaReactiva.ToString();
                        txtLecDemanda.Text = reporExiste.LecturaDemanda.ToString();
                        txtFcambioMedidor.Text = reporExiste.FechaCambioMedidor != null ? reporExiste.FechaCambioMedidor.Value.ToShortDateString() : "N/A";
                        txtLecActivaRet.Text = reporExiste.LecturaActivaRetira.ToString();
                        txtConsumoActivoFact.Text = reporExiste.ConsumoActivaFact.ToString();
                        txtLecReactivaRet.Text = reporExiste.LecturaReactivaRetira.ToString();
                        txtConsumoReactivoFact.Text = reporExiste.ConsumoReactivaFact.ToString();
                        txtConsumoDemandaFact.Text = reporExiste.ConsumoDemandaFact.ToString();
                        txtMedAnt.Text = reporExiste.MedidorAnt;
                        txtMedAct.Text = reporExiste.MedidorAct;
                        btnReportar.Enabled = true;
                        txtFechaRepo.Text = reporExiste.FechaRegistro.Value.ToShortDateString();
                        txtFechaResol.Text = reporExiste.FechaMaxResol.Value.ToShortDateString();
                        txtOs.Text = reporExiste.OS != null ? reporExiste.OS.ToString() : string.Empty;
                    }
                    else
                    {
                        //Si no existe en bd, consultar datos en WS.
                        ClienteRoot cliente = _cCtrl.GetInfoCliente(txtClave.Text.Trim());
                        string cicloLect = txtCicloRefa.Text.Trim();

                        int CicloActual = Fun.GetCicloActual();
                        bool esActual = int.Parse(cicloLect) < CicloActual ? false : true;

                        Fun.ClearControls(this);
                        lblRepe.Visible = false;
                        if (cliente != null)
                        {
                            txtClave.Text = cliente.cliente.nis_rad;
                            txtCicloRefa.Text = cicloLect;//cliente.cliente.cicloLectura;

                            if (!string.IsNullOrEmpty(cliente.cliente.dial))
                            {
                                ddlDial.SelectedValue = cliente.cliente.dial;
                                txtFechaRepo.Text = DateTime.Now.ToShortDateString();
                                txtFechaResol.Text = DateTime.Now.AddDays(MaxDiasRepoOpe).ToShortDateString();
                            }
                            lblRepe.Visible = false;
                            hfIdM.Value = "0";
                            txtUbicacion.Text = cliente.cliente.ubicacion;
                            txtRegion.Text = cliente.cliente.region;
                            txtSector.Text = cliente.cliente.area;
                            hfIdSector.Value = _rgCtrl.GetSectorByNombre(cliente.cliente.area).idSector.ToString();
                            txtTarifa.Text = cliente.cliente.nom_tarifa;
                            txtMedida.Text = int.Parse(cliente.cliente.multiplicador) > valMultiMedida ? "Medida Especial" : "Medida Directa";
                            txtMulActual.Text = esActual ? cliente.cliente.multiplicador : null;
                            txtLecActiva.Text = esActual ? cliente.cliente.lectura_activa_campo : null;
                            txtLecReactiva.Text = esActual ? cliente.cliente.lectura_reactiva_campo : null;
                            txtLecDemanda.Text = esActual ? cliente.cliente.lectura_demanda_actual : null;
                            txtMedAnt.Text = cliente.cliente.num_medidor_ant;
                            txtMedAct.Text = cliente.cliente.num_medidor;
                            btnReportar.Enabled = true;
                        }
                        else
                        {

                            Fun.ClearControls(this);
                            lblRepe.Visible = true;
                            lblRepe.Text = "ATENCIÓN: No existen registros con los parámetros ingresados.";
                        }
                    }

                    CargarDropDown();
                }
                else
                {
                    Fun.ClearControls(this);
                    lblRepe.Visible = true;
                    lblRepe.Text = "Error en entrada de parámetros de búsqueda.";
                }
            }
            catch (Exception ex)
            {
                lblRepe.Text = $"Ha ocurrido una Excepción: {ex.Message}";
            }
        }
        private void CargarDropDown()
        {
            ddlAnomalia.DataSource = null;
            ddlAnomalia.DataSource = _lCtrl.GetListaAnomalias();
            ddlAnomalia.DataTextField = "descripcion";
            ddlAnomalia.DataValueField = "idLecturaAnomalia";
            ddlAnomalia.DataBind();

            ddlDial.DataSource = null;
            ddlDial.Items.Clear();
            for (int i = 1; i <= 23; i++)
            {
                ListItem li = new ListItem(i.ToString(), i.ToString());
                ddlDial.Items.Add(li);
            }
            ddlDial.DataBind();
            ddlDial.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
        }
        private void GuardarFilesBd(Rep_Operativa_M r)
        {
            //imagenes a reporte
            var listaImg = new List<ImagenReporteOperativa>();
            //foreach (var archivo in fUpload.PostedFiles)
            foreach (var (item, index) in fUpload.PostedFiles.Select((v, i) => (v, i)))
            {
                if (item.ContentLength > 0)
                {
                    string nom = "File_" + index + "_" + txtClave.Text;
                    var guardaImg = GuardarBitacoraSoporte(item, nom);
                    guardaImg.IdRow_M = r.IdRow_M;
                    string ext = Path.GetExtension(item.FileName).ToLower();
                    if (ext == ".jfif" || ext == ".png" || ext == ".jpg" || ext == ".jpeg")
                    {
                        guardaImg.esImg = true;
                    }
                    else
                    {
                        guardaImg.esImg = false;
                    }
                    _rCtrl.PutImgReporteOperativa(guardaImg);
                }
            }
        }
        private ImagenReporteOperativa GuardarBitacoraSoporte(HttpPostedFile file, string nombre)
        {
            _nombreArchivo = Path.GetFileNameWithoutExtension(nombre) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(file.FileName);

            //string filePath = Server.MapPath("~/Files/") + nombre;
            //string path = Request.PhysicalApplicationPath;
            string filePath = $"{pathImgRepOpe}\\{_nombreArchivo}";

            //string filePath = Server.MapPath("~/Files/") + "Files\\" + _nombreArchivo;
            hfRutaArchivo.Value = filePath;
            file.SaveAs(filePath);

            var imgRepoOpe = new ImagenReporteOperativa
            {
                nombre = _nombreArchivo,
                ruta = filePath,
                fechaCarga = DateTime.Now,
                idRow = Guid.NewGuid()
            };

            return imgRepoOpe;
        }

        
    }
}