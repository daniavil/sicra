﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
//using Epplus;

namespace SICRA.Views
{
    public partial class AsignaMuestraFact : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private CriticaFacturacionController _cCtrl = new CriticaFacturacionController();

        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                CargarChkListUsuarios();
            }
        }

        private void CargarChkListUsuarios()
        {
            try
            {
                var usuarios = _cCtrl.GetUsuariosFacturacion();

                foreach (var usuario in usuarios)
                {
                    ListItem item = new ListItem();
                    item.Text = usuario.nombre;
                    item.Value = usuario.codUsr;
                    cklUsuarios.Items.Add(item);
                }
                cklUsuarios.DataBind();
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;

                lblMsjSave.Text = $"Ocurrió una excepción: {msj}";
            }
        }

        private void CargarChkListAnomalias()
        {
            try
            {
                var anomalias = _cCtrl.GetAnomaliasFacturacion();

                foreach (var anomalia in anomalias)
                {
                    ListItem item = new ListItem();
                    item.Text = anomalia.nombre;
                    item.Value = anomalia.id;
                    cklAnomalia.Items.Add(item);
                }
                cklAnomalia.DataBind();
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;

                lblMsjSave.Text = $"Ocurrió una excepción: {msj}";
            }
        }

        private void CargarChkListObservCritica()
        {
            try
            {
                var observs = _cCtrl.GetCriticaObservaciones();

                foreach (var observ in observs)
                {
                    ListItem item = new ListItem();
                    item.Text = observ.valor;
                    item.Value = observ.id;
                    cklObservaciones.Items.Add(item);
                }
                cklObservaciones.DataBind();
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                lblMsjSave.Text = $"Ocurrió una excepción: {msj}";
            }
        }

        private void CargarChkListUsuariosSoeeh()
        {
            try
            {
                string date = DateTime.Parse(ddlFecha.SelectedValue.ToString().ToString()).ToString("yyyy/MM/dd");
                var usuarios = _cCtrl.GetUsuariosSoeehCritica(date);

                foreach (var usuario in usuarios)
                {
                    ListItem item = new ListItem();
                    item.Text = usuario.valor;
                    item.Value = usuario.id;
                    cklUsrCritica.Items.Add(item);
                }
                cklUsrCritica.DataBind();
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;

                lblMsjSave.Text = $"Ocurrió una excepción: {msj}";
            }
        }
        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                pnlAsignado.Visible = true;
                lblFiltros.Text = string.Empty;
                if (ddlFecha.SelectedIndex > 0)
                {
                    if (ExisteMuestraFecha())
                    {
                        lblMsj.Text = "Ya existen registros de este tipo de muestra para la fecha seleccionada.!";
                        lblMsj.Visible = true;
                        gvRepoM.DataSource = null;
                        gvRepoM.DataBind();
                    }
                    else
                    {
                        //Sectores sector = _regCtrl.GetFirstSectorByUser(_usrLog.usuSicra.idUsuario);
                        List<string> codAnomalias = new List<string>();
                        foreach (ListItem li in cklAnomalia.Items)
                        {
                            if (li.Selected == true)
                            {
                                codAnomalias.Add("'" + li.Value + "'");
                            }
                        }

                        List<int> CodObservaciones = new List<int>();
                        foreach (ListItem li in cklObservaciones.Items)
                        {
                            if (li.Selected == true)
                            {
                                CodObservaciones.Add(int.Parse(li.Value));
                            }
                        }

                        List<string> codUsrCri = new List<string>();
                        foreach (ListItem li in cklUsrCritica.Items)
                        {
                            if (li.Selected == true)
                            {
                                codUsrCri.Add("'" + li.Value + "'");
                            }
                        }
                        string UsuCri = "";
                        UsuCri = string.Join(",", codUsrCri);

                        string anomalias = "";
                        anomalias = string.Join(",", codAnomalias);
                        string observaciones = "";
                        observaciones = string.Join(",", CodObservaciones);

                        string date = DateTime.Parse(ddlFecha.SelectedValue.ToString().ToString()).ToString("yyyy/MM/dd");

                        CargarMuestra(anomalias, observaciones, UsuCri, date);

                        btnAsignar.Enabled = true;
                        lblMsjSave.Visible = false;
                        lblMsjSave.Text = string.Empty;
                    }
                }
                else
                {
                    lblMsjSave.Text = "ERROR EN PARÁMETROS.";
                    lblMsjSave.Attributes["CssClass"] = "label label - danger";
                    lblMsjSave.Visible = true;
                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblFiltros.Text = $"Excepción: {ex.Message}";
                //lblMsjSave.Text = "ERROR EN PARÁMETROS.";
                //lblMsjSave.Attributes["CssClass"] = "label label - danger";
                //lblMsjSave.Visible = true;
            }
            
        }

        private void CargarMuestra(string anomalias, string observs, string UsuCri, string fecha)
        {
            List<string> listUsuarios = new List<string>();
            listUsuarios = cklUsuarios.Items.Cast<ListItem>().Where(li => li.Selected).Select(y => y.Value).ToList();
            int porcentaje = int.Parse(txtPorcentaje.Text);
            var muestra = _cCtrl.GetMuestraBd(fecha, anomalias, observs, UsuCri, porcentaje, listUsuarios);
            gvRepoM.DataSource = null;
            gvRepoM.DataSource = muestra;
            gvRepoM.DataBind();

            //lblFiltros.Text = muestra.RespWs;

            if (muestra != null)
            {
                if (muestra.Count > 0)
                {
                    spnConteo.InnerText = muestra.Count.ToString();
                    spnFecha.InnerText = DateTime.Parse(ddlFecha.SelectedValue.ToString()).ToShortDateString();
                    spnTipo.InnerText = ckbSesgada.Checked ? "MUESTRA SESGADA" : "MUESTRA TOTAL";
                    pnlAsignado.Visible = true;
                    lblFiltros.Visible = false;
                }
                else
                {
                    lblFiltros.Visible = true;
                    pnlAsignado.Visible = false;
                    lblFiltros.Text = $"Atención, no se produjeron datos.";
                }
            }
            else
            {
                lblFiltros.Visible = true;
                pnlAsignado.Visible = false;
                lblFiltros.Text = $"Atención, no se produjeron datos.";
            }            
        }

        protected void btnAsignar_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                try
                {
                    List<MuestraCritica> muestras = new List<MuestraCritica>();
                    foreach (GridViewRow row in gvRepoM.Rows)
                    {
                        try
                        {
                            muestras.Add(new MuestraCritica
                            {
                                idRow = Guid.NewGuid(),
                                nisrad = int.Parse(row.Cells[1].Text),
                                EsTotal = ckbTotal.Checked ? true : false,
                                dial = int.Parse(row.Cells[2].Text),
                                Ciclo = int.Parse(row.Cells[8].Text),
                                CodAnomaliaCritica = HttpUtility.HtmlDecode(row.Cells[3].Text),
                                AnomaliaCritica = HttpUtility.HtmlDecode(row.Cells[4].Text),
                                CodAnomaliaLectura = HttpUtility.HtmlDecode(row.Cells[5].Text),
                                AnomaliaLectura = HttpUtility.HtmlDecode(row.Cells[6].Text),
                                lecturaActiva = int.Parse(row.Cells[9].Text),
                                lecturaReactiva = HttpUtility.HtmlDecode(row.Cells[10].Text).Trim() == "" ? (int?)null : int.Parse(HttpUtility.HtmlDecode(row.Cells[10].Text)),
                                lecturaDemanda = HttpUtility.HtmlDecode(row.Cells[11].Text).Trim() == "" ? (decimal?)null : decimal.Parse(HttpUtility.HtmlDecode(row.Cells[11].Text)),
                                usuarioGenera = _usrLog.user,
                                fechaRegistro = DateTime.Now,
                                Resuelto = false,
                                FechaLectura = Convert.ToDateTime(ddlFecha.SelectedValue),
                                usuarioResuelve = HttpUtility.HtmlDecode(row.Cells[7].Text),
                                usuarioResIncms = HttpUtility.HtmlDecode(row.Cells[13].Text)
                            });
                        }
                        catch (Exception ex)
                        {
                        }

                        
                    }
                    RespMsj resp = GuardarMuestra(muestras);
                    btnAsignar.Enabled = false;
                    lblMsjSave.Visible = true;
                    lblMsjSave.Text = resp.mensaje;
                    lblMsjSave.Attributes["CssClass"] = "label label - success";

                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                }
                catch (Exception ex)
                {
                }
            }
        }

        private RespMsj GuardarMuestra(List<MuestraCritica> muestras)
        {
            var resp = _cCtrl.PutAsingarMuestras(muestras);
            return resp;

        }

        protected void ddlCiclo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ckbSesgada.Checked)
            {
                CargarFechasLecturasTotalSesgada(0);
            }
            else if (ckbTotal.Checked)
            {
                CargarFechasLecturasTotalSesgada(1);
            }
        }

        protected void ddlFecha_SelectedIndexChanged(object sender, EventArgs e)
        {
            string fecha = ""; //DateTime.Parse(ddlFecha.SelectedValue.ToString()).ToShortDateString();
            if (ddlFecha.SelectedIndex > 0)
            {
                fecha = DateTime.Parse(ddlFecha.SelectedValue.ToString()).ToShortDateString();
                if (ExisteMuestraFecha())
                {
                    lblMsj.Text = "Ya existen registros de este tipo de muestra para la fecha seleccionada.!";
                    lblMsj.Visible = true;
                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                }
                else
                {
                    CargarChkListUsuariosSoeeh();
                    lblMsj.Text = string.Empty;
                    lblMsj.Visible = false;
                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                    pnlAsignado.Visible = false;
                }
            }
        }

        private bool ExisteMuestraFecha()
        {
            bool tipo = ckbTotal.Checked ? true : false;

            var existe = _cCtrl.GetCountMuestrasbyFecha(DateTime.Parse(ddlFecha.SelectedValue.ToString()), tipo);
            return existe;
        }

        protected void ckbSesgada_CheckedChanged(object sender, EventArgs e)
        {
            gvRepoM.DataSource = null;
            gvRepoM.DataBind();
            var ckb = (CheckBox)sender;
            if (ckb.Checked)
            {
                CargarCiclos();
                CargarChkListAnomalias();
                CargarChkListObservCritica();
                hfFiltra.Value = "0";

                ckbTotal.Checked = false;

                divAnomalia.Visible = true;
                div1.Visible = true;
                div2.Visible = true;
                divPorcentaje.Visible = true;

                txtPorcentaje.Text = "80";
            }
        }

        protected void ckbTotal_CheckedChanged(object sender, EventArgs e)
        {
            gvRepoM.DataSource = null;
            gvRepoM.DataBind();
            var ckb = (CheckBox)sender;
            if (ckb.Checked)
            {
                CargarCiclos();
                lblMsj.Text = string.Empty;
                hfFiltra.Value = "1";

                ckbSesgada.Checked = false;

                divAnomalia.Visible = false;
                div1.Visible = false;
                div2.Visible = false;
                //divPorcentaje.Visible = false;

                cklAnomalia.Items.Clear();
                cklAnomalia.DataSource = null;
                cklAnomalia.DataBind();

                cklObservaciones.Items.Clear();
                cklObservaciones.DataSource = null;
                cklObservaciones.DataBind();

                cklUsrCritica.Items.Clear();
                cklUsrCritica.DataSource = null;
                cklUsrCritica.DataBind();

                gvRepoM.DataSource = null;
                gvRepoM.DataBind();

                txtPorcentaje.Text = "100";
            }
        }

        private void CargarFechasLecturasTotalSesgada(int esTotal)
        {
            var fechas = _cCtrl.GetMuestrasFechaTotalSesgada(esTotal,int.Parse(ddlCiclo.SelectedValue));

            ddlFecha.DataSource = null;
            ddlFecha.DataSource = fechas;
            ddlFecha.DataTextField = "FECHA_LECTURA";
            ddlFecha.DataValueField = "FECHA_LECTURA";
            ddlFecha.DataBind();
            ddlFecha.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
        }

        protected void ddlAnomalia_SelectedIndexChanged(object sender, EventArgs e)
        {
            int conteoCheck = 0;
            foreach (ListItem item in cklAnomalia.Items)
            {
                if (item.Selected)
                {
                    conteoCheck = conteoCheck + 1;
                }
            }

            if (conteoCheck == 3)
            {
                hfFiltra.Value = "1";
            }
            else
            {
                hfFiltra.Value = "0";
            }
        }

        protected void ckTodosAnom_CheckedChanged(object sender, EventArgs e)
        {
            var ckb = (CheckBox)sender;
            if (ckb.Checked)
            {
                foreach (ListItem li in cklAnomalia.Items)
                {
                    li.Selected = true;
                }
            }
            else
            {
                foreach (ListItem li in cklAnomalia.Items)
                {
                    li.Selected = false;
                }
            }
        }

        protected void ckObserv_CheckedChanged(object sender, EventArgs e)
        {
            var ckb = (CheckBox)sender;
            if (ckb.Checked)
            {
                foreach (ListItem li in cklObservaciones.Items)
                {
                    li.Selected = true;
                }
            }
            else
            {
                foreach (ListItem li in cklObservaciones.Items)
                {
                    li.Selected = false;
                }
            }
        }

        private void CargarCiclos()
        {
            ddlCiclo.DataSource = null;
            ddlCiclo.DataSource = _cCtrl.GetPeriodosLectura();
            ddlCiclo.DataBind();
            ddlCiclo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlFecha.DataSource = null;
            ddlFecha.Items.Clear();
            ddlFecha.DataBind();
        }

        protected void chlUsrs_CheckedChanged(object sender, EventArgs e)
        {
            var ckb = (CheckBox)sender;
            if (ckb.Checked)
            {
                foreach (ListItem li in cklUsuarios.Items)
                {
                    li.Selected = true;
                }
            }
            else
            {
                foreach (ListItem li in cklUsuarios.Items)
                {
                    li.Selected = false;
                }
            }
        }

        protected void cklTodosUsrSoeeh_CheckedChanged(object sender, EventArgs e)
        {
            var ckb = (CheckBox)sender;
            if (ckb.Checked)
            {
                foreach (ListItem li in cklUsrCritica.Items)
                {
                    li.Selected = true;
                }
            }
            else
            {
                foreach (ListItem li in cklUsrCritica.Items)
                {
                    li.Selected = false;
                }
            }
        }
    }
}