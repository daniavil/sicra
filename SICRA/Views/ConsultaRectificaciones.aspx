﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConsultaRectificaciones.aspx.cs" Inherits="SICRA.Views.ConsultaRectificaciones" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="DevExpress.XtraReports.v21.1.Web.WebForms, Version=21.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <a href="#"><span>SICRA</span></a>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>   
    <a class="UrlActual" href='<%= ResolveUrl("AutorizacionRectificacion.aspx") %>'>Autorizacion de Rectificaciones</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>

    <%--formulario flotante de actualizacion--%>
    <cc1:ModalPopupExtender ID="mpResolver" ClientIDMode="Static" runat="server" Enabled="true"
        CancelControlID="btnCancel" OkControlID="btnCancel" PopupControlID="pnResolver"
        TargetControlID="hfIdM" BackgroundCssClass="ModalPopupBG">
    </cc1:ModalPopupExtender>

    <div ID="pnResolver" runat="server" class="panel panel-success pnlresolv">
        <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left" style="padding-top: 7.5px;">Estado Solicitud Refactura</h4>
                <div class="input-group" style="float:right">
                    <asp:Button ID="btnCancel" Text="Cerrar" runat="server" CssClass="btn btn-danger active" />
                </div>
        </div>
        
        <input id="hfIdM" type="hidden" name="hddclick" runat="server" />
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Datos Generales</div>
                        <div class="panel-body">
                            <asp:HiddenField ID="hfIdAsigGes" runat="server" />
                            <asp:HiddenField ID="hfIdGestionRefa" runat="server" />
                            <asp:HiddenField ID="hfUrlFile" ClientIDMode="Static" runat="server" />
                            <asp:HiddenField ID="hfNomFile" runat="server" />

                            <div class="col-sm-3 b-r">
                                <label for="lblNisrad">NIS:</label>
                                <asp:Label ID="lblNisrad" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-3 b-r">
                                <label for="lblNisrad">Origen:</label>
                                <asp:Label ID="lblOrigen" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-3 b-r">
                                <label for="lblNisrad">Id Gestión:</label>
                                <asp:Label ID="lblCodGestion" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-3 b-r">
                                <label for="lblTipoRect">Tipo:</label>
                                <asp:Label ID="lblTipoRect" runat="server"></asp:Label>
                            </div>

                            <div class="col-sm-4 b-r">
                                <label for="lblMonto">Valor kWh:</label>
                                <asp:Label ID="lblKwh" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-4 b-r">
                                <label for="lblMonto">Monto LPS:</label>
                                <asp:Label ID="lblMonto" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-4 b-r">
                                <label for="lblDbcr">DBCR:</label>
                                <asp:Label ID="lblDbcr" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-12 b-r">
                                <label for="lblCiclos">Ciclo(s):</label>
                                <asp:Label ID="lblCiclos" runat="server"></asp:Label>
                            </div>

                            <div class="col-sm-4 b-r">
                                <label for="lblSector">Sector:</label>
                                <asp:Label ID="lblSector" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-4 b-r">
                                <label for="lblMercado">Mercado:</label>
                                <asp:Label ID="lblMercado" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-4 b-r">
                                <label for="lblFechaSoli">Fecha de Solicitud:</label>
                                <asp:Label ID="lblFechaSoli" runat="server"></asp:Label>
                            </div>

                            <div class="col-sm-12 b-r">
                                <label for="lblSolicita">Nombre Solicitante:</label>
                                <asp:Label ID="lblSolicita" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-12 b-r">
                                <label for="txtRazonSolicitud">Razón de Solicitud:</label>
                                <asp:TextBox runat="server" class="form-control" ID="txtCausa" ReadOnly="true" TextMode="MultiLine" Rows="2"></asp:TextBox>
                            </div>

                            <div class="col-sm-4 b-r">
                                <label for="lblEstado">Estado:</label>
                                <asp:Label ID="lblEstado" runat="server"></asp:Label>
                            </div>
                            <div class="col-sm-4 b-r">
                                <label for="lblFinalizado">Finalizado:</label>
                                <asp:Label ID="lblFinalizado" runat="server"></asp:Label>
                            </div>

                            <div class="col-sm-12 b-r">
                                <asp:HyperLink runat="server" NavigateUrl="~/Views/Reportes/FrmViewReport.aspx" Target="_blank">Ver Reporte</asp:HyperLink>
                            </div>

                            <div class="col-sm-12 b-r">
                                <asp:Label ID="lblErrorDatos" runat="server" Visible="false"></asp:Label>
                            </div>
                            <asp:HiddenField ID="hfObj" runat="server" Value="" />
                        </div>
                    </div>
                </div>

                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Resoluciones</div>
                        <div class="panel-body" style="text-align: left">

                            <%--COMERCIAL--%>
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">COMERCIAL</div>
                                    <div class="panel-body" style="text-align: left">
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:Literal ID="literalComercial" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--CONTROL DE ENERGIA--%>
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-warning">
                                    <div class="panel-heading">CONTROL DE ENERGÍA</div>
                                    <div class="panel-body" style="text-align: left">
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:Literal ID="literalCE" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%--GRID RESOLUCIONES Y DOCUMENTOS--%>
                            <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Resoluciones/Documentos</div>
                                    <div class="panel-body" style="text-align: left">
                                        <asp:GridView ID="gvResoluciones" runat="server" AllowSorting="False" AutoGenerateColumns="False" CssClass="mydatagrid"
                                            Width="100%" DataKeyNames="IdGestDet" Font-Size="Small" HeaderStyle-CssClass="header" RowStyle-CssClass="rows"
                                            OnRowDataBound="gvResoluciones_RowDataBound">
                                            <HeaderStyle Font-Bold="True" ForeColor="White" />
                                            <EditRowStyle BackColor="Yellow" />
                                            <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                            <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="#">
                                                    <ItemTemplate>
                                                        <img alt="" class="imgPlusRow" style="cursor: pointer" src="../Imagenes/plus.png" />
                                                        <asp:Panel ID="pnlDocs" runat="server" Style="display: none">
                                                            <asp:GridView ID="gvDocs" runat="server" Width="100%" AutoGenerateColumns="false" AllowPaging="false" CssClass="Grid">
                                                                <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                                                                <Columns>
                                                                    <asp:BoundField DataField="IdDoc" HeaderText="IdDoc" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" Visible="true"></asp:BoundField>
                                                                    <asp:BoundField DataField="NomDoc" HeaderText="Nombre Archivo" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="hlDownload" runat="server" CommandArgument='<%# Eval("Ruta") %>' OnCommand="hlDownload_Command" CommandName='<%# Eval("NomDoc") %>' Text="Descargar" Style="color: black !important;"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IdGestDet" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="idAsigRefa" ReadOnly="True" Visible="true" />
                                                <asp:BoundField DataField="ComentarioResol" HeaderText="Resolución" />
                                                <asp:BoundField DataField="FechaHoraResuelto" HeaderText="F/H Resuelto" />
                                                <asp:BoundField DataField="NomResolvio" HeaderText="Resolvió" />
                                                <asp:BoundField DataField="Perfil" HeaderText="Perfil" />
                                                <asp:BoundField DataField="Estado" HeaderText="Estado" />
                                            </Columns>
                                        </asp:GridView>

                                        <hr class="my-12"/>

                                        <dx:ASPxWebDocumentViewer ID="ASPxWebDocumentViewer1" runat="server" ></dx:ASPxWebDocumentViewer>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--</asp:Panel>--%>
    <%--FIN formulario flotante de actualizacion--%>

    <div class="col-sm-12">
        <h2 class="titulo">Solicitudes de Rectificación</h2>
        <div class="panel panel-primary">
            <div class="panel-body">
                <br />
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <span style="font-weight: bold">Conteo de Registros:</span>
                    <span id="spnNoResueltas" runat="server" style="font-weight: bold"></span>
                </div>
                <asp:Panel ID="pnlAsignado" runat="server">
                    <asp:GridView ID="gvRepoM" runat="server" OnRowCommand="gvRepoM_RowCommand"
                    AllowSorting="False" AutoGenerateColumns="False" CssClass="mydatagrid" Width="100%"
                    DataKeyNames="IdGesRefMae" Font-Size="Small" HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="Yellow" />
                    <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                    <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="IdGesRefMae" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdGesRefMae" ReadOnly="True" Visible="true" />
                        <asp:BoundField DataField="nisrad" HeaderText="NIS" />
                        <asp:BoundField DataField="ComentarioSolicitud" HeaderText="COMENTARIO" />
                        <asp:BoundField DataField="FechaCreacion" HeaderText="Fecha/Hora" />
                        <asp:BoundField DataField="NomPersona" HeaderText="Solicitante" />
                        <asp:BoundField DataField="Estado" HeaderText="Estado" />
                        <asp:BoundField DataField="Finalizado" HeaderText="Finalizado" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Acción">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkResolver" runat="server" CommandName="resolver" 
                                    CommandArgument='<%# Eval("IdGesRefMae") %>' Text="VER" ForeColor="blue">
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </asp:Panel>

                <div class="form-horizontal col-sm-12 col-md-12 col-lg-12">
                    <asp:Label ID="lblError" runat="server" Visible="false"></asp:Label>
                </div>
            </div>
        </div>
    </div>

    

</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolderJS">
    <script src="../Scripts/GridCells.js"></script>        
    <script type="text/javascript">
        $(document).on("click", "[class^=imgPlusRow]", function () {
            $(this).attr("class", "imgMinusRow");
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
            $(this).attr("src", "../Imagenes/minus.png");
        });
        $(document).on("click", "[class^=imgMinusRow]", function () {
            $(this).attr("class", "imgPlusRow");
            $(this).attr("src", "../Imagenes/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>
    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            if (confirm("¿Está seguro de realizar esta acción?")) {
                confirm_value.value = "Si";
            }
            else {
                confirm_value.value = "No";
            }

            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="ContentPlaceHolderStyle">
    <link rel="stylesheet" href="../GalleryRoot/css/viewer.css" />
    <style>
        .pnlresolv {
            max-width:-webkit-fill-available !important; 
            overflow-y:scroll;
            top:10px !important;
            height:-webkit-fill-available !important;
            left:30px !important;
            right:30px !important;
        }
    </style>
    <style>
        .pictures {
            margin: 0;
            padding: 0;
            list-style: none;
            width:100%
        }

            .pictures > li {
                float: left;
                width: 50px;
                height: 50px;
                margin: 0 -1px -1px;
                border: 1px solid transparent;
                overflow: hidden;
            }

                .pictures > li > img {
                    width: 50px;
                    height: 50px;
                    cursor: -webkit-zoom-in;
                    cursor: zoom-in;
                }

        .viewer-download {
            color: #fff;
            font-family: FontAwesome;
            font-size: .75rem;
            line-height: 1.5rem;
            text-align: center;
        }

            .viewer-download::before {
                content: "\f019";
            }

        #FotoContainer {
            margin-top: 10px !important;
            display: inline-block;
        }
    </style>
    <style type="text/css">
        .tableFont {font-size:14px !important;}
        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }
    </style>
    <style type="text/css">
        .timeline {
            list-style-type: none;
            display: flex;
            align-items: center;
            justify-content: center;
            font-size:small;
            /*overflow-x: auto;*/
            flex-flow: wrap;
            list-style: decimal;
        }

        .li {
            transition: all 200ms ease-in;
        }

        .timestamp {
            margin-bottom: 20px;
            padding: 0px 40px;
            display: flex;
            flex-direction: column;
            align-items: center;
            font-weight: 100;
        }

        .status {
            padding: 0px 40px;
            display: flex;
            justify-content: center;
            border-top: 2px solid #D6DCE0;
            position: relative;
            transition: all 200ms ease-in;
        }

            .status h4 {
                font-weight: 600;
            }

            .status:before {
                content: "";
                width: 25px;
                height: 25px;
                background-color: white;
                border-radius: 25px;
                border: 1px solid #ddd;
                position: absolute;
                top: -15px;
                left: 42%;
                transition: all 200ms ease-in;
            }

            .li.complete .status {
                border-top: 2px solid #66DC71;
            }
        
            .li.complete .status:before {
                background-color: #66DC71;
                border: none;
                transition: all 200ms ease-in;
            }

            .li.complete .status h4 {
                color: #66DC71;
            }

            .li.denegate .status {
                border-top: 2px solid #db3535;
            }
            .li.denegate .status:before {
                background-color: #db3535;
                border: none;
                transition: all 200ms ease-in;
            }
            .li.denegate .status h4 {
                color: #db3535;
            }

            .li.vacio .status {
                border-top: 2px solid #ffcc00;
            }
            .li.vacio .status:before {
                background-color: #ffcc00;
                border: none;
                transition: all 200ms ease-in;
            }
            .li.vacio .status h4 {
                color: #ffcc00;
            }


        @media (min-device-width: 320px) and (max-device-width: 700px) {
            .timeline {
                list-style-type: none;
                display: block;
            }

            .li {
                transition: all 200ms ease-in;
                display: flex;
                width: inherit;
            }

            .timestamp {
                width: 100px;
            }

            .status:before {
                left: -8%;
                top: 30%;
                transition: all 200ms ease-in;
            }
        }

        html, body {
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
            font-family: "Titillium Web", sans serif;
            color: #758D96;
        }

        button {
            position: absolute;
            width: 100px;
            min-width: 100px;
            padding: 20px;
            margin: 20px;
            font-family: "Titillium Web", sans serif;
            border: none;
            color: white;
            font-size: 16px;
            text-align: center;
        }

        #toggleButton {
            position: absolute;
            left: 50px;
            top: 20px;
            background-color: #75C7F6;
        }
        .auto-style1 {
            width: 100%;
        }
    </style>
</asp:Content>
