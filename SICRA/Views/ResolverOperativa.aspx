﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResolverOperativa.aspx.cs" Inherits="SICRA.Views.ResolverOperativa" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <a href="#"><span>SICRA</span></a>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>   
    <a class="UrlActual" href='<%= ResolveUrl("ResolverClaveRepo.aspx") %>'>Resolver Clave Reportada</a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>

    <%--formulario flotante de actualizacion--%>
    <cc1:ModalPopupExtender ID="mpResolver" ClientIDMode="Static" runat="server" Enabled="true"
        CancelControlID="btnCancel" OkControlID="btnCancel" PopupControlID="pnResolver"
        TargetControlID="hfIdM" BackgroundCssClass="ModalPopupBG">
    </cc1:ModalPopupExtender>

        <div ID="pnResolver" runat="server" class="panel panel-info" style="width:80%; overflow-y:scroll;top:10px !important;height:-webkit-fill-available !important;">
            <div class="panel-heading">NIS A RESOLVER: <span id="spNisrad" runat="server"></span> - CICLO: <span id="spCiclo" runat="server"></span></div>
            <div class="panel-body">
                <input id="hfIdM" type="hidden" name="hfIdM" runat="server" />
                <div class="form-inline">
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <span class="label label-success">Valores de Reporte Operativa</span>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <br />
                    </div>
                    <%------------------------------------------%>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtUbicacion">Ubicación:</label>
                        <asp:Label runat="server" ID="txtUbicacion"></asp:Label>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtRegion">Region:</label>
                        <asp:Label runat="server" ID="txtRegion"></asp:Label>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtSector">Sector:</label>
                        <asp:Label runat="server" ID="txtSector"></asp:Label>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtTarifa">Tarifa:</label>
                        <asp:Label runat="server" ID="txtTarifa"></asp:Label>
                    </div>
                    <%------------------------------------------%>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtMulAnt">Mul. Anterior:</label>
                        <asp:Label runat="server" ID="txtMulAnt"></asp:Label>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtMulActual">Mul. Actual:</label>
                        <asp:Label runat="server" ID="txtMulActual"></asp:Label>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtMedAnt">Medidor Anterior:</label>
                        <asp:Label runat="server" ID="txtMedAnt"></asp:Label>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtMedAct">Medidor Actual:</label>
                        <asp:Label runat="server" ID="txtMedAct"></asp:Label>
                    </div>
                    <%------------------------------------------%>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtMedida">Medida:</label>
                        <asp:Label runat="server" ID="txtMedida"></asp:Label>
                    </div>
                    <div class="form-group col-sm-6 col-md-6 col-lg-6">
                        <label for="txtAnomalia">Anomalía:</label>
                        <asp:Label runat="server" ID="txtAnomalia"></asp:Label>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtFcambioMedidor">Fecha Cambio de Medidor:</label>
                        <asp:Label runat="server" ID="txtFcambioMedidor"></asp:Label>
                    </div>
                    <%------------------------------------------%>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <label for="txtLecActiva">Lectura Activa:</label>
                        <asp:Label runat="server" ID="txtLecActiva"></asp:Label>
                    </div>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <label for="txtLecActivaRet">Lectura Activa Retiro:</label>
                        <asp:Label runat="server" ID="txtLecActivaRet"></asp:Label>
                    </div>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <label for="txtConsumoActivoFact">Consumo Activa Facturar:</label>
                        <asp:Label runat="server" ID="txtConsumoActivoFact"></asp:Label>
                    </div>
                    <%------------------------------------------%>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <label for="txtLecReactiva">Lectura Reactiva:</label>
                        <asp:Label runat="server" ID="txtLecReactiva"></asp:Label>
                    </div>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <label for="txtLecActiva">Lectura Reactiva Retiro:</label>
                        <asp:Label runat="server" ID="txtLecReactivaRet"></asp:Label>
                    </div>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <label for="txtLecActiva">Consumo Reactiva Facturar:</label>
                        <asp:Label runat="server" ID="txtConsumoReactivoFact"></asp:Label>
                    </div>
                    <%------------------------------------------%>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtLecDemanda">Lectura Demanda:</label>
                        <asp:Label runat="server" ID="txtLecDemanda"></asp:Label>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtConsumoDemandaFact">Consumo Demanda Facturar:</label>
                        <asp:Label runat="server" ID="txtConsumoDemandaFact"></asp:Label>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                        <label for="txtDial">Dial Lectura:</label>
                        <asp:Label runat="server" ID="txtDial"></asp:Label>
                    </div>
                    <div class="form-group col-sm-3 col-md-3 col-lg-3">
                            <label for="txtOs">OS Referencia:</label>
                            <asp:Label runat="server" ID="txtOs"></asp:Label>
                        </div>
                    <%------------------------------------------%>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <label for="txtCausa">Solicitud O Causa de Verificación:</label>
                        <asp:Label runat="server" ID="txtCausa"></asp:Label>
                    </div>
                    <div class="form-group col-sm-6 col-md-6 col-lg-6">
                        <label for="txtSolicitante">Solicitante:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtSolicitante" ReadOnly="true" Width="100%" ></asp:TextBox>
                    </div>
                    <div class="form-group col-sm-6 col-md-6 col-lg-6">
                        <label for="txtPerfilResol">Perfil Resolución:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtPerfilResol" ReadOnly="true" Width="100%" ></asp:TextBox>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <br /><br />
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <span class="label label-primary">Valores de Resolución</span>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <br />
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <label for="txtCausa">Estado de Resolución:</label>
                        <asp:DropDownList runat="server" class="form-control" ID="ddlEstadoRepo">
                            <asp:ListItem Text="Resuelto" Value="Resuelto" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Corregir" Value="Corregir"></asp:ListItem>
                            <asp:ListItem Text="Improcedente" Value="Improcedente"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <label for="txtResolucion">Resolución:</label>
                        <asp:TextBox runat="server" class="form-control" ID="txtResolucion" Width="100%" TextMode="MultiLine" Rows="2"></asp:TextBox>
                        <br /><br />
                    </div>
                    <div class="form-group col-sm-4 col-md-4 col-lg-4">
                        <asp:Button ID="btnAceptar" Text="Aceptar" runat="server" CssClass="btn btn-primary active" OnClientClick="Confirm()" OnClick="btnAceptar_Click" />
                    </div>
                    <div class="form-group col-sm-8 col-md-8 col-lg-8">
                        <asp:Button ID="btnCancel" Text="Cancelar" runat="server" CssClass="btn btn-warning active" />
                    </div>
                </div>
            </div>
        </div>
    <%--</asp:Panel>--%>
    <%--FIN formulario flotante de actualizacion--%>
    
    
    <div class="col-sm-12">
        <h2 class="titulo">Resolver Clave Reportada</h2>
        <div class="panel panel-primary">
            <div class="panel-body">
                <%--********************************formulario de filtros**************************--%>
                <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#demo">FILTROS</button>
                <div id="demo" class="collapse">
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <%--****************************************************************************--%>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="txtNisrad">NIS:</label>
                                            <asp:TextBox ID="txtClave" runat="server" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="txtOsFiltro">OS Referencia:</label>
                                            <asp:TextBox ID="txtOsFiltro" runat="server" TextMode="Number" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlEstado">¿Tiene OS?:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlTieneOS">
                                                <asp:ListItem Text="--Seleccionar--" Value=""></asp:ListItem>
                                                <asp:ListItem Text="SI" Value="SI"></asp:ListItem>
                                                <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlfechasRegistro">Ciclo Refactura:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlCiclo"></asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlRegion">Región:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlSector">Sector:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlSector" OnSelectedIndexChanged="ddlSector_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlMedida">Medida:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlMedida"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlEstado">Estado:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlEstado">
                                                <asp:ListItem Text="--Seleccionar--" Value=""></asp:ListItem>
                                                <asp:ListItem Text="RESUELTO" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="NO RESUELTO" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="ddlAnomalia">Anomalía:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlAnomalia"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlPerfilReso">Perfil de Reporte:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlPerfilReso">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="ddlDial">Dial:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlDial"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlVencido">Fecha Vencimiento :</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlVencido">
                                                <asp:ListItem Text="--SIN FILTRO--" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="FECHA VENCIDA" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="FECHA NO VENCIDA" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlfechasRegistro">Fechas de Registro:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlfechasRegistro" DataTextFormatString="{0:dd/MM/yyyy}">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlFechavencimiento">Fechas de vencimiento:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlFechavencimiento" DataTextFormatString="{0:dd/MM/yyyy}">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlUsuarioReporta">Usuario Reporta:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlUsuarioReporta">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlUsuarioResuelve">Usuario Resuelve:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlUsuarioResuelve">
                                            </asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlCicloRepo">Ciclo de Reporte:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlCicloRepo">
                                            </asp:DropDownList>
                                        </div>
                                        <%--****************************************************************************--%>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlRegion" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlSector" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                    <asp:Button ID="btnFiltrar" runat="server" Text="Buscar" CssClass="btn btn-warning" OnClick="btnFiltrar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--********************************FIN formulario de filtros**************************--%>
                <br />
                <br />
                <br />

                <div class="form-group col-sm-2 col-md-2 col-lg-2">
                    <span style="background-color: lightcoral">No resueltas:</span>
                    <span id="spnNoResueltas" runat="server" style="font-weight: bold"></span>
                </div>
                <div class="form-group col-sm-2 col-md-2 col-lg-2">
                    <span style="background-color: lightgreen">Resueltas:</span>
                    <span id="spnResueltas" runat="server" style="font-weight: bold"></span>
                </div>
                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                    <asp:Label ID="lblmsj" runat="server" Visible="False" Style="font-size: medium" CssClass="label label-danger"></asp:Label>
                </div>

<%--                <div class="form-group col-sm-8 col-md-8 col-lg-8" style="text-align:right">
                    <asp:Button id="btnExportar" runat="server" CssClass="btn btn-primary" Text="Exportar" OnClick="btnExportar_Click" />
                </div>--%>

                <asp:GridView ID="gvRepoM" runat="server" OnRowCommand="gvRepoM_RowCommand"
                    AllowSorting="False" AutoGenerateColumns="False" CssClass="mydatagrid" Width="100%"
                    DataKeyNames="IdRow_M" OnRowDataBound="gvRepoM_RowDataBound" Font-Size="Small"
                    HeaderStyle-CssClass="header" RowStyle-CssClass="rows">
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="Yellow" />
                    <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                    <EmptyDataTemplate>No hay elementos a mostrar</EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <img alt="" class="imgPlusRow" style="cursor: pointer" src="../Imagenes/plus.png" />
                                <asp:Panel ID="pnlRegistros" runat="server" Style="display: none">
                                    <asp:GridView ID="gvRepoD" runat="server" CssClass="Grid" AutoGenerateColumns="false" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="IdRow_D" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdRow_D" ReadOnly="True" Visible="true" />
                                            <asp:BoundField DataField="Estado" HeaderText="Estado" ReadOnly="True" />
                                            <asp:BoundField DataField="Resolucion" HeaderText="Resolución" ReadOnly="True" />
                                            <asp:BoundField DataField="FechaResolucion" HeaderText="Fecha Resolución" ReadOnly="True" />
                                            <asp:BoundField DataField="UsuarioResuelve" HeaderText="Usuario Resolvió" ReadOnly="True" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="IdRow_M" HeaderText="IdRow_M" />
                        <asp:BoundField DataField="Nis_Rad" HeaderText="NIS" />
                        <asp:BoundField DataField="Region" HeaderText="Región" />
                        <asp:BoundField DataField="Sector" HeaderText="Sector" />
                        <asp:BoundField DataField="Medida" HeaderText="Medida" />
                        <asp:BoundField DataField="UsuarioFacturacion" HeaderText="Reporta/Resuelve" />
                        <asp:BoundField DataField="PerfilAsignado" HeaderText="Perfil" />
                        <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha Reporte" DataFormatString = "{0:dd/MM/yyyy}"/>
                        <asp:BoundField DataField="Anomalia" HeaderText="Anomalía" />
                        <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="Resuelto" HeaderText="Resuelto" />
                        <asp:BoundField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" DataField="VisibleResolver" HeaderText="vencido" />
                        <asp:BoundField DataField="FechaMaxResol" DataFormatString = "{0:dd/MM/yyyy}" HeaderText="Fecha Máxima" />
                        <asp:BoundField DataField="dial" HeaderText="Dial" />
                        <asp:BoundField DataField="Ciclo" HeaderText="Ciclo" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Acción">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkResolver" runat="server" CommandName="resolver" Visible='<%# Eval("VisibleResolver") %>'
                                    CommandArgument='<%# Eval("IdRow_M") %>' Text="Resolver" ForeColor="blue">
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <style type="text/css">
        .tableFont {font-size:14px !important;}
        .ModalPopupBG
        {
            background-color: #666699;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }

        .HellowWorldPopup
        {
            min-width:200px;
            min-height:150px;
            background:white;
        }
        .Controls {
            padding-top:10px;
            padding-bottom:10px;
        }
        
        .link
        {
         color: #428bca !important; 
         text-decoration: underline;
        }
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    <script src="../Scripts/GridCells.js"></script>    
    <script type="text/javascript">
        $(document).on("click", "[class^=imgPlusRow]", function () {
            $(this).attr("class", "imgMinusRow");
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
            $(this).attr("src", "../Imagenes/minus.png");
        });
        $(document).on("click", "[class^=imgMinusRow]", function () {
            $(this).attr("class", "imgPlusRow");
            $(this).attr("src", "../Imagenes/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>

    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("¿Está seguro de realizar esta acción?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>