﻿using ClosedXML.Excel;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Epplus;

namespace SICRA.Views
{
    public partial class AdmonUser : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];

        public readonly string pathImgRepOpe = ConfigurationManager.AppSettings["pathImgRepOpe"].ToString();
        private UsuarioController _uCtrl = new UsuarioController();


        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                CargarDdl();
            }
            else if (Request.Form["__EVENTTARGET"] == "MainContent_txtUsuario")
            {
                ValidarUsuario(txtUsuario.Text.Trim());
            }
        }

        private void ValidarUsuario(string usr)
        {

            var usuario = _uCtrl.GetUsuarioByUser(usr);
            Fun.ClearControls(this);
            txtUsuario.Text = usr.Trim();

            if (usuario != null)
            {
                txtUsuario.ReadOnly = true;
                txtPersona.ReadOnly = true;
                btnGuardar.Visible = false;
                BtnActualizar.Visible = true;
                txtPersona.Text = usuario.NomPersona;
                txtCorreo.Text = usuario.correo;
                hfIdUsr.Value = usuario.idUsuario.ToString();
                
                try
                {
                    ddlperfil.SelectedValue = usuario.idPerfil.ToString();
                }
                catch (Exception)
                {
                    ddlperfil.ClearSelection();
                    ddlperfil.SelectedIndex = -1;
                }

                try
                {
                    ddlPerfilReportes.SelectedValue = usuario.IdPerfilResoRepo.ToString();
                }
                catch (Exception)
                {
                    ddlPerfilReportes.ClearSelection();
                    ddlPerfilReportes.SelectedIndex = -1;
                }


                chkAutoriza.Checked = usuario.Aut;
                chkAdmin.Checked = usuario.AdminUser;
                chkActivo.Checked = usuario.Activo;
            }
            else
            {
                txtUsuario.ReadOnly = false;
                txtPersona.ReadOnly = false;
                hfIdUsr.Value = "0";
                btnGuardar.Visible = true;
                BtnActualizar.Visible = false;
            }
            CargarSectores(usr);
            CargarMercados(usr);
        }

        private void CargarDdl()
        {
            ddlperfil.DataSource = null;
            ddlperfil.DataSource = _uCtrl.GetListaPerfilAutoziacionSicra();
            ddlperfil.DataTextField = "Nombre";
            ddlperfil.DataValueField = "idPerfil";
            ddlperfil.DataBind();
            ddlperfil.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
            ddlperfil.SelectedIndex = 0;

            ddlPerfilReportes.DataSource = null;
            ddlPerfilReportes.DataSource = _uCtrl.GetListaPerfilRepo();
            ddlPerfilReportes.DataTextField = "NombrePerfil";
            ddlPerfilReportes.DataValueField = "idPerfilResoRepo";
            ddlPerfilReportes.DataBind();
            ddlPerfilReportes.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
            ddlPerfilReportes.SelectedIndex = 0;
        }

        private void CargarSectores(string usr)
        {
            try
            {
                var SectoresUsuario = _uCtrl.GetSectoresUsuarioDto(usr);
                cklSectores.Items.Clear();
                foreach (var sector in SectoresUsuario)
                {
                    ListItem item = new ListItem();
                    item.Text = sector.Sector;
                    item.Value = sector.IdSector.ToString();
                    cklSectores.Items.Add(item);
                }
                cklSectores.DataBind();

                foreach (ListItem li in cklSectores.Items)
                {
                    li.Selected = SectoresUsuario.Where(x => x.IdSector == int.Parse(li.Value)).FirstOrDefault().Asig;
                }
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;

                //lblMsjSave.Text = $"Ocurrió una excepción: {msj}";
            }
        }

        private void CargarMercados(string usr)
        {
            try
            {
                var MercadosUsuario = _uCtrl.GetMercadosUsuarioDto(usr);
                cklMercados.Items.Clear();
                foreach (var mercado in MercadosUsuario)
                {
                    ListItem item = new ListItem();
                    item.Text = mercado.Nombre;
                    item.Value = mercado.IdMercado.ToString();
                    cklMercados.Items.Add(item);
                }
                cklMercados.DataBind();

                foreach (ListItem li in cklMercados.Items)
                {
                    li.Selected = MercadosUsuario.Where(x => x.IdMercado == int.Parse(li.Value)).FirstOrDefault().Asig;
                }
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            GuardarActualizarUsuario(true);
        }

        protected void BtnActualizar_Click(object sender, EventArgs e)
        {
            GuardarActualizarUsuario(false);
        }

        private void GuardarActualizarUsuario(bool EsNuevo)
        {
            try
            {
                var usuario = new Usuarios();
                if (EsNuevo)
                {
                    usuario.Usuario = txtUsuario.Text.Trim();
                    usuario.NomPersona = txtPersona.Text.Trim();
                    usuario.AdminUser = chkAdmin.Checked;
                    usuario.correo = txtCorreo.Text.Trim();
                    usuario.Aut = chkAutoriza.Checked;
                    usuario.idPerfil = string.IsNullOrWhiteSpace(ddlperfil.SelectedValue) ? (int?)null : int.Parse(ddlperfil.SelectedValue.ToString());
                    usuario.IdPerfilResoRepo = ddlPerfilReportes.SelectedValue.ToString() == "" ? (int?)null : int.Parse(ddlPerfilReportes.SelectedValue.ToString());
                    usuario.Activo = chkActivo.Checked;
                }
                else
                {
                    usuario = _uCtrl.GetUsuarioByUser(txtUsuario.Text.Trim());
                    usuario.AdminUser = chkAdmin.Checked;
                    usuario.correo = txtCorreo.Text.Trim();
                    usuario.Aut = chkAutoriza.Checked;
                    usuario.idPerfil = ddlperfil.SelectedValue == "" ? (int?)null : int.Parse(ddlperfil.SelectedValue.ToString());
                    usuario.IdPerfilResoRepo = ddlPerfilReportes.SelectedValue.ToString() == "" ? (int?)null : int.Parse(ddlPerfilReportes.SelectedValue.ToString());
                    usuario.Activo = chkActivo.Checked;
                }
                var resp = _uCtrl.PutUsuario(usuario);

                if (resp.idUsuario>0)
                {
                    //SECTORES
                    List<int> secAsig = new List<int>();
                    foreach (ListItem li in cklSectores.Items)
                    {
                        if (li.Selected)
                        {
                            secAsig.Add(int.Parse(li.Value));
                        }
                    }
                    _uCtrl.PutSectoresUsuarios(secAsig, resp.idUsuario);

                    //MERCADOS
                    List<int> merAsig = new List<int>();
                    foreach (ListItem li in cklMercados.Items)
                    {
                        if (li.Selected)
                        {
                            merAsig.Add(int.Parse(li.Value));
                        }
                    }
                    _uCtrl.PutMercadosUsuario(merAsig, resp.idUsuario);
                }

                lblMsj.Visible = true;
                lblMsj.Text = "Datos guardados Correctamente.";
            }
            catch (Exception ex)
            {
                lblMsj.Visible = true;
                lblMsj.Text = ex.Message;
            }

        }

    }
}