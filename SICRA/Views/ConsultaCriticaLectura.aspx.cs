﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
//using Epplus;

namespace SICRA.Views
{
    public partial class ConsultaCriticaLectura : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private CriticaLecturaController _cCtrl = new CriticaLecturaController();

        public class imagePreview
        {
            public string Name { get; set; }
            public string url { get; set; }
        }
        public class PrintConfigPreviewViewModel
        {
            public string Name { get; set; }
            public byte[] Picture { get; set; }
            public string clave { get; set; }
        }

        /**********************************************Eventos*************************************************/
        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                Cargarddl();
            }
        }
        protected void gvRepoM_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string IdM = gvRepoM.DataKeys[e.Row.RowIndex].Value.ToString();

                TableCell cellResuelto = e.Row.Cells[7];

                if (bool.Parse(cellResuelto.Text) == true)
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].BackColor = Color.LightGreen;
                    }
                }
                else
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].BackColor = Color.LightCoral;
                    }
                }
            }
        }
        protected void gvRepoM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToUpper() == "RESOLVER")
            {
                int idrow = int.Parse(e.CommandArgument.ToString());
                MuestraLecturasMes muestraBd = new MuestraLecturasMes();
                muestraBd = _cCtrl.GetMuestrasByIdRow(idrow);

                int clave = muestraBd.CLAVE;

                //int clave = int.Parse(e.CommandArgument.ToString());
                int nis = 0;

                if (clave < 1000000)
                {
                    nis = clave + 3000000;
                }
                else
                {
                    nis = clave;
                }
                
                ClienteRoot cliente = GetCliente(nis.ToString());
                if (cliente!=null)
                {
                    literalControl.Text = cliente.ImagenesInnerText;
                    lblNisrad.Text = cliente.cliente.nis_rad;
                    lblMedidor.Text = cliente.cliente.num_medidor;
                    lblUbicacion.Text = cliente.cliente.ubicacion;
                    lblTarifa.Text = cliente.cliente.nom_tarifa;
                    lblCliente.Text = cliente.cliente.nom_cli;
                    lblDireccion.Text = cliente.cliente.direccion;
                }

                //hfObj.Value = string.Empty;

                var info = _cCtrl.GetInfoMuestraWs(clave.ToString(), muestraBd.PERIODO); //; --------> cambiar as ciclo
                if (info.CriticaLecturaInfo == null)
                {
                    info = _cCtrl.GetInfoMuestraWs(clave.ToString(), muestraBd.PERIODO);
                }
                if (info.CriticaLecturaInfo != null)
                {
                    hfEsTelemedido.Value = info.CriticaLecturaInfo.EsTelemedido.ToString();

                    lblMsjSave.Text = string.Empty;

                    lblLecturaEncontrada.Text = info.CriticaLecturaInfo.LECTURAENCONTRADA;
                    lblFLectura.Text = info.CriticaLecturaInfo.FECHALECTURA;
                    lblFProgramada.Text = info.CriticaLecturaInfo.FECHA_PROGRAMADA;
                    lblDial.Text = info.CriticaLecturaInfo.CODIGODIAL;
                    lblCodAnomalia.Text = info.CriticaLecturaInfo.CODIGOANOMALIA;
                    lblAnomalia.Text = info.CriticaLecturaInfo.DESCRIPCION;
                    lblParam.Text = muestraBd.CambioParametrizacion;
                    lblObsInspector.Text = info.CriticaLecturaInfo.ObsInspector;

                    muestraBd.PERIODO = info.CriticaLecturaInfo.PERIODO != null ? int.Parse(info.CriticaLecturaInfo.PERIODO) : int.Parse(DateTime.Now.ToString("yyyyMM"));
                    muestraBd.LECTURAENCONTRADA = info.CriticaLecturaInfo.LECTURAENCONTRADA != null ? int.Parse(info.CriticaLecturaInfo.LECTURAENCONTRADA) : (int?)null;
                    muestraBd.FECHA_PROGRAMADA = info.CriticaLecturaInfo.FECHA_PROGRAMADA;
                    muestraBd.CODIGOANOMALIA = info.CriticaLecturaInfo.CODIGOANOMALIA;
                    muestraBd.CODIGOADMINISTRATIVO = info.CriticaLecturaInfo.CODIGOADMINISTRATIVO == null ? (double?)null : double.Parse(info.CriticaLecturaInfo.CODIGOADMINISTRATIVO);
                    muestraBd.CODIGOGRUPOTRABAJO = info.CriticaLecturaInfo.CODIGOGRUPOTRABAJO == null ? (double?)null : double.Parse(info.CriticaLecturaInfo.CODIGOGRUPOTRABAJO);

                    ddlLectura.SelectedValue = muestraBd.lecturaCorrecta.ToString();
                    ddlAnomalia.SelectedValue = muestraBd.anomaliaCorrecta.ToString();

                    lblRendimiento.Text = muestraBd.Rendimiento;
                    lblLecturaDebioSer.Text= (int?)muestraBd.LecturaDebioSer!=null? muestraBd.LecturaDebioSer.ToString():string.Empty;
                    lblObsFoto.Text= muestraBd.IdObFoto != null ? _cCtrl.GetObservacionFoto(muestraBd.IdObFoto).Observacion : String.Empty;
                    lblAnomaliaNo.Text = muestraBd.anomaliaDebioSer;
                    lblRendimiento.Text = muestraBd.Rendimiento;

                }
                else
                {
                    lblMsjSave.Visible = true;
                    lblMsjSave.Text = $"No se encontró información de lectura: , por favor vuelva a intentar nuevamente. {info.Msj}";
                }

                if (info.histoAnomaliasInfo.Count > 0)
                {
                    gvHistoAnomalia.DataSource = null;
                    gvHistoAnomalia.DataSource = info.histoAnomaliasInfo;
                    gvHistoAnomalia.DataBind();
                }
                else
                {
                    gvHistoAnomalia.DataSource = null;
                    gvHistoAnomalia.DataBind();
                }
                mpResolver.Show();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            CargarMuestrasBd();
        }
        protected void ddlCiclo_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarddlDiales(int.Parse(ddlCiclo.SelectedValue));
        }
        protected void ddldial_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarTipoMuestraFecha();
        }
        /***********************************************METDOS**********************************************/
        private void Cargarddl()
        {
            try
            {
                ddlCiclo.DataSource = null;
                ddlCiclo.DataSource = _cCtrl.GetCiclosMuestras(1);
                ddlCiclo.DataBind();
                ddlCiclo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

                //ddldial.DataSource = null;
                //ddldial.DataSource = _cCtrl.GetDialesMuestras(1);
                //ddldial.DataBind();
                //ddldial.Items.Insert(0, new ListItem("--SELECCIONAR--", "0"));
            }
            catch (Exception)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }

        }
        private void CargarMuestrasBd()
        {
            try
            {
                lblMsjSave.Text = string.Empty;
                var muestras = _cCtrl.GetMuestrasByUser(_usrLog.usuSicra.idUsuario,
                                                    _usrLog.usuSicra.Usuario,
                                                    ddlCiclo.SelectedValue.ToString(),
                                                    ddldial.SelectedValue.ToString(),
                                                    1,
                                                    ddlTipoFiltro.SelectedItem.Text.Trim(),
                                                    false);
                gvRepoM.DataSource = null;
                gvRepoM.DataSource = muestras;
                gvRepoM.DataBind();

                spnResueltas.InnerText = muestras.Count.ToString();
            }
            catch (Exception)
            {
                lblMsjSave.Text = "Error en parámetros.!";
            }

        }
        private void CargarddlDiales(int ciclo)
        {
            try
            {
                ddldial.DataSource = null;
                ddldial.DataSource = _cCtrl.GetDialesResueltos(ciclo, 1);
                ddldial.DataBind();
                ddldial.Items.Insert(0, new ListItem("--SELECCIONAR--", "0"));

                //btnFiltro.Attributes.Add("Class", "btn btn-success");
                //demoFiltros.Attributes.Add("Class", "collapse in");
                //demoFiltros.Style.Remove("style");
            }
            catch (Exception)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }

        }
        private ClienteRoot GetCliente(string clave)
        {
            var clienteRoot = _cCtrl.CargarInfoCliente(clave);
            try
            {
                if (clienteRoot != null)
                {
                    List<imagePreview> imageUrlList = new List<imagePreview>();
                    List<PrintConfigPreviewViewModel> imageList = new List<PrintConfigPreviewViewModel>();

                    foreach (var (item, index) in clienteRoot.cliFotos.Select((v, i) => (v, i)))
                    {
                        try
                        {
                            if (item.origen == "Logistics")
                            {
                                string path = Request.PhysicalApplicationPath;
                                byte[] bytes = Convert.FromBase64String(item.imagen);

                                MemoryStream ms = new MemoryStream(bytes);
                                ms.Position = 0;
                                imageList.Add(new PrintConfigPreviewViewModel() { Name = item.fecha, Picture = bytes, clave = item.clave });
                            }
                            else
                            {
                                byte[] img1 = System.IO.File.ReadAllBytes(item.imagen);
                                imageList.Add(new PrintConfigPreviewViewModel() { Name = item.fecha, Picture = img1, clave = item.clave });
                            }
                        }
                        catch (Exception)
                        {
                            imageUrlList = null;
                            imageList = null;

                        }
                    }
                    imageUrlList = GuardarCopiaImagenes(imageList, clave);
                    clienteRoot.ImagenesInnerText = GetImagenesMosaico(imageUrlList).Text;
                }
                else
                {
                    clienteRoot.cliFotos = null;
                }
                clienteRoot.ListaHistoricoLectura = null;
                clienteRoot.ListaHistoricoConsumo = null;
                return clienteRoot;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public LiteralControl GetImagenesMosaico(List<imagePreview> images)
        {
            LiteralControl html = new LiteralControl();

            string folderProject = Request.Url.AbsolutePath.ToString().Trim();

            string content = @"<div id=""galley"" runat=""server"" ClientIDMode=""static""><ul class='pictures'>";

            try
            {
                int Dividendo = images.Count(), divisor = 6, q, r, totalFilas = 0;
                q = (int)(Dividendo / divisor);
                r = Dividendo % divisor;
                totalFilas = q + (r > 0 ? 1 : 0);

                string imageneshtml = "";

                foreach (var (item, index) in images.Select((v, i) => (v, i)))
                {
                    if (item.url != "")
                    {
                        string path = HttpContext.Current.Request.Url.Authority.ToString();
                        //imageneshtml += $@"<li><img data-original='{item.Name}' src ='{folderProject}/ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                        imageneshtml += $@"<li><img data-original='{item.Name}' src ='../ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                    }
                }
                content += imageneshtml;
                content += " </ul></div>";
            }
            catch (Exception)
            {
                content = "<H3>SIN IMÁGENES</H3>";
            }

            html.Text = content;
            return html;
        }
        public List<imagePreview> GuardarCopiaImagenes(List<PrintConfigPreviewViewModel> imagenes, string clave)
        {
            try
            {
                List<imagePreview> ImgFiles = new List<imagePreview>();
                //foreach (var item in imagenes)
                foreach (var (item, index) in imagenes.Select((v, i) => (v, i)))
                {
                    string path = Request.PhysicalApplicationPath;
                    string nombreArchivo = $"{path}ImgCli\\img{index}_{item.Name}_C{item.clave}.Jpeg";

                    //string resolvedUrl = System.Web.HttpContext.Current.Request.Url.Authority.ToString();//VirtualPathUtility.ToAbsolute("~/");

                    ImgFiles.Add(new imagePreview() { url = $"img{index}_{item.Name}_C{item.clave}.Jpeg", Name = $"img{index}_{item.Name}_C{item.clave}" });

                    Bitmap objBitMap = new Bitmap(byteArrayToImage(item.Picture));
                    objBitMap.Save(nombreArchivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                return ImgFiles;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
                ms.Position = 0;
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
            catch (Exception)
            {
                return null;
            }
        }
        private void CargarTipoMuestraFecha()
        {
            try
            {
                ddlTipoFiltro.DataSource = null;
                ddlTipoFiltro.DataSource = _cCtrl.GetTiposMuetrasFecha(ddldial.SelectedItem.Text.Trim(), _usrLog.usuSicra.idUsuario, false);
                ddlTipoFiltro.DataBind();
            }
            catch (Exception)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }

        }
    }
}