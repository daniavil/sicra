﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
//using Epplus;

namespace SICRA.Views
{
    public partial class CriticaLectura : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private CriticaLecturaController _cCtrl = new CriticaLecturaController();

        public class imagePreview
        {
            public string Name { get; set; }
            public string url { get; set; }
        }

        public class PrintConfigPreviewViewModel
        {
            public string Name { get; set; }
            public byte[] Picture { get; set; }
            public string clave { get; set; }
        }

        /**********************************************Eventos*************************************************/
        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                Cargarddl();
                //CargarMuestrasBd();
            }
        }
        protected void gvRepoM_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string IdM = gvRepoM.DataKeys[e.Row.RowIndex].Value.ToString();

                TableCell cellResuelto = e.Row.Cells[7];

                if (bool.Parse(cellResuelto.Text) == true)
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].BackColor = Color.LightGreen;
                    }
                }
                else
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].BackColor = Color.LightCoral;
                    }
                }
            }
        }
        protected void gvRepoM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToUpper() == "RESOLVER")
            {
                try
                {
                    CargarDatos(e.CommandArgument.ToString(
                        ));
                }
                catch (Exception  ex)
                {

                }
            }
        }
        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                try
                {
                    //MuestraLecturasMes muestra = new MuestraLecturasMes();

                    var m = _cCtrl.GetMuestrasByIdRow(int.Parse(hfIdM.Value.Trim().ToString()));

                    var m2 = JsonConvert.DeserializeObject<MuestraLecturasMes>(hfObj.Value.ToString());

                    m.lecturaCorrecta = hfEsTelemedido.Value.Trim()=="1" ? (int?)null : int.Parse(ddlLectura.SelectedValue.ToString());
                    m.fotoCorrecta = bool.Parse(ddlFoto.SelectedValue.ToString());
                    m.anomaliaCorrecta = bool.Parse(ddlAnomalia.SelectedValue.ToString());
                    m.LECTURAENCONTRADA = string.IsNullOrWhiteSpace(lblLecturaEncontrada.Text.Trim()) ? (int?)null : int.Parse(lblLecturaEncontrada.Text);
                    m.FECHA_PROGRAMADA = m2.FECHA_PROGRAMADA;
                    m.CODIGOANOMALIA = lblCodAnomalia.Text;
                    m.DESCRIPCION = lblAnomalia.Text;
                    m.CODIGOADMINISTRATIVO = m2.CODIGOADMINISTRATIVO;
                    m.CODIGOGRUPOTRABAJO = m2.CODIGOGRUPOTRABAJO;
                    m.fechaResolucion = DateTime.Now;
                    m.IdObFoto = ddlFotoNo.SelectedValue != "" ? Convert.ToInt32(ddlFotoNo.SelectedValue) : (int?)null;
                    m.UsrResuelve = _usrLog.usuSicra.idUsuario;
                    m.LecturaDebioSer = string.IsNullOrWhiteSpace(txtLecturaDebioSer.Text.Trim()) ? (int?)null : int.Parse(txtLecturaDebioSer.Text.Trim());
                    m.EsTelemedido = string.IsNullOrWhiteSpace(hfEsTelemedido.Value.Trim()) ? (int?)null : int.Parse(hfEsTelemedido.Value.Trim());
                    m.anomaliaDebioSer = ddlAnomaliaNo.SelectedValue != "" ? ddlAnomaliaNo.SelectedItem.Text.Trim() : (string)null;
                    m.Rendimiento = ddlRendimiento.SelectedValue;

                    var resp = _cCtrl.PutCriticaMuestra(m);
                    lblMsjSave.Text = resp.mensaje;
                }
                catch (Exception ex)
                {
                    string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                    lblMsjSave.Text = msj;
                }
                ddlLectura.SelectedIndex = -1;
                ddlFoto.SelectedIndex = -1;
                ddlAnomalia.SelectedIndex = -1;
                txtLecturaDebioSer.Text = String.Empty;
                ddlAnomaliaNo.SelectedIndex = -1;
                ddlFotoNo.SelectedIndex = -1;

                CargarMuestrasBd();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlLectDebioSer.Visible = false;
        }
        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            CargarMuestrasBd();
        }
        protected void ddlCiclo_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarddlDiales(int.Parse(ddlCiclo.SelectedValue));
        }
        protected void ddlFoto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!Convert.ToBoolean(ddlFoto.SelectedValue))
                {
                    pnlDdlFotoNo.Visible = true;
                    ddlFotoNo.DataSource = null;
                    ddlFotoNo.Items.Clear();
                    ddlFotoNo.DataSource = _cCtrl.GetListaObservacionFoto();
                    ddlFotoNo.DataTextField = "Observacion";
                    ddlFotoNo.DataValueField = "IdObFoto";
                    ddlFotoNo.DataBind();


                    //Se elimina condición de acuerdo a GLPI 35313, Punto #2

                    ////GLPI 30913: se agrega opción "No determinado" al list de lectura.
                    //ddlLectura.DataSource = null;
                    //ddlLectura.Items.Clear();
                    //ddlLectura.Items.Add(new ListItem("SI","1"));
                    //ddlLectura.Items.Add(new ListItem("NO", "0"));
                    //ddlLectura.Items.Add(new ListItem("No se puede determinar", "2"));
                    //ddlLectura.DataBind();


                }
                else
                {
                    pnlDdlFotoNo.Visible = false;
                    ddlFotoNo.DataSource = null;
                    ddlFotoNo.Items.Clear();
                    ddlFotoNo.DataBind();

                    //Se elimina condición de acuerdo a GLPI 35313, Punto #2
                    //ddlLectura.DataSource = null;
                    //ddlLectura.Items.Clear();
                    //ddlLectura.Items.Add(new ListItem("SI", "1"));
                    //ddlLectura.Items.Add(new ListItem("NO", "0"));
                    //ddlLectura.DataBind();
                }
            }
            catch (Exception ex)
            {
                pnlDdlFotoNo.Visible = false;
                ddlFotoNo.DataSource = null;
                ddlFotoNo.Items.Clear();
                ddlFotoNo.DataBind();
            }
            mpResolver.Show();
        }
        protected void ddldial_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarTipoMuestraFecha();
        }
        protected void ddlLectura_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtLecturaDebioSer.Text = string.Empty;
                if (int.Parse(ddlLectura.SelectedValue) == 0)
                {
                    pnlLectDebioSer.Visible = true;
                }
                else
                {
                    pnlLectDebioSer.Visible = false;
                }
            }
            catch (Exception ex)
            {
                pnlLectDebioSer.Visible = false;
                txtLecturaDebioSer.Text = string.Empty;
            }
            mpResolver.Show();
        }

        protected void ddlAnomalia_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!Convert.ToBoolean(ddlAnomalia.SelectedValue))
                {
                    pnlDdlAnomaliaNo.Visible = true;
                    ddlAnomaliaNo.DataSource = null;
                    ddlAnomaliaNo.Items.Clear();
                    ddlAnomaliaNo.DataSource = _cCtrl.GetAnomaliasLectura();
                    ddlAnomaliaNo.DataTextField = "NOMBRE";
                    ddlAnomaliaNo.DataValueField = "id";
                    ddlAnomaliaNo.DataBind();
                }
                else
                {
                    pnlDdlAnomaliaNo.Visible = false;
                    ddlAnomaliaNo.DataSource = null;
                    ddlAnomaliaNo.Items.Clear();
                    ddlAnomaliaNo.DataBind();
                }
            }
            catch (Exception ex)
            {
                pnlDdlAnomaliaNo.Visible = false;
                ddlAnomaliaNo.DataSource = null;
                ddlAnomaliaNo.Items.Clear();
                ddlAnomaliaNo.DataBind();
            }
            mpResolver.Show();
        }
        /***********************************************METDOS**********************************************/
        private void Cargarddl()
        {
            try
            {
                ddlCiclo.DataSource = null;
                ddlCiclo.DataSource = _cCtrl.GetCiclosMuestras(0);
                ddlCiclo.DataBind();
                ddlCiclo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

                //ddldial.DataSource = null;
                //ddldial.DataSource = _cCtrl.GetDialesMuestras(0);
                //ddldial.DataBind();
                //ddldial.Items.Insert(0, new ListItem("--SELECCIONAR--", "0"));
            }
            catch (Exception)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }

        }
        private void CargarMuestrasBd()
        {
            try
            {
                var muestras = _cCtrl.GetMuestrasByUser(_usrLog.usuSicra.idUsuario, 
                                                    _usrLog.usuSicra.Usuario,
                                                    ddlCiclo.SelectedValue.ToString(),
                                                    ddldial.SelectedValue.ToString(),
                                                    0,
                                                    ddlTipoFiltro.SelectedItem.Text.Trim(),
                                                    true);
                gvRepoM.DataSource = null;
                gvRepoM.DataSource = muestras;
                gvRepoM.DataBind();

                spnNoResueltas.InnerText = muestras.Count.ToString();
            }
            catch (Exception ex)
            {
            }
        }
        private void CargarDatos(string commandargument)
        {
            try
            {
                pnlLectDebioSer.Visible = false;
                pnlDdlFotoNo.Visible = false;
                pnlDdlAnomaliaNo.Visible = false;

                ddlFoto.SelectedIndex = -1;
                ddlAnomalia.SelectedIndex = -1;

                int idrow = int.Parse(commandargument);
                MuestraLecturasMes muestraBd = new MuestraLecturasMes();
                muestraBd = _cCtrl.GetMuestrasByIdRow(idrow);

                int clave = muestraBd.CLAVE;
                hfIdM.Value = muestraBd.idRow.ToString();

                if (muestraBd != null)
                {
                    lblMsjSave.Text = string.Empty;
                    int nis = 0;

                    if (clave < 1000000)
                    {
                        nis = clave + 3000000;
                    }
                    else
                    {
                        nis = clave;
                    }

                    ClienteRoot cliente = GetCliente(nis.ToString());
                    if (cliente != null)
                    {
                        literalControl.Text = cliente.ImagenesInnerText;
                        lblNisrad.Text = cliente.cliente.nis_rad;
                        lblMedidor.Text = cliente.cliente.num_medidor;
                        lblUbicacion.Text = cliente.cliente.ubicacion;
                        lblTarifa.Text = cliente.cliente.nom_tarifa;
                        lblCliente.Text = cliente.cliente.nom_cli;
                        lblDireccion.Text = cliente.cliente.direccion;
                    }

                    hfObj.Value = string.Empty;
                    btnAceptar.Visible = false;

                    var info = _cCtrl.GetInfoMuestraWs(clave.ToString(), muestraBd.PERIODO); //-------->cambiar as ciclo

                    //consulta nuevamente
                    if(info.CriticaLecturaInfo == null)
                    {
                        info = _cCtrl.GetInfoMuestraWs(clave.ToString(), muestraBd.PERIODO);
                    }
                    if (info.CriticaLecturaInfo != null)
                    {
                        if (info.CriticaLecturaInfo.EsTelemedido == 0)
                        {
                            PnlddlLectura.Visible = true;
                        }
                        else if (info.CriticaLecturaInfo.EsTelemedido == 1)
                        {
                            PnlddlLectura.Visible = false;
                        }
                        hfEsTelemedido.Value = info.CriticaLecturaInfo.EsTelemedido.ToString();

                        lblMsjSave.Text = string.Empty;
                        lblError.Visible = true;
                        lblError.Text = info.Msj;

                        lblLecturaEncontrada.Text = info.CriticaLecturaInfo.LECTURAENCONTRADA;
                        lblFLectura.Text = info.CriticaLecturaInfo.FECHALECTURA;
                        lblFProgramada.Text = info.CriticaLecturaInfo.FECHA_PROGRAMADA;
                        lblDial.Text = info.CriticaLecturaInfo.CODIGODIAL;
                        lblCodAnomalia.Text = info.CriticaLecturaInfo.CODIGOANOMALIA;
                        lblAnomalia.Text = info.CriticaLecturaInfo.DESCRIPCION;
                        lblParam.Text = muestraBd.CambioParametrizacion;
                        lblObsInspector.Text = info.CriticaLecturaInfo.ObsInspector;

                        muestraBd.PERIODO = info.CriticaLecturaInfo.PERIODO != null ? int.Parse(info.CriticaLecturaInfo.PERIODO) : int.Parse(DateTime.Now.ToString("yyyyMM"));
                        muestraBd.LECTURAENCONTRADA = info.CriticaLecturaInfo.LECTURAENCONTRADA != null ? int.Parse(info.CriticaLecturaInfo.LECTURAENCONTRADA) : (int?)null;
                        muestraBd.FECHA_PROGRAMADA = info.CriticaLecturaInfo.FECHA_PROGRAMADA;
                        muestraBd.CODIGOANOMALIA = info.CriticaLecturaInfo.CODIGOANOMALIA;
                        muestraBd.CODIGOADMINISTRATIVO = info.CriticaLecturaInfo.CODIGOADMINISTRATIVO == null ? (double?)null : double.Parse(info.CriticaLecturaInfo.CODIGOADMINISTRATIVO);
                        muestraBd.CODIGOGRUPOTRABAJO = info.CriticaLecturaInfo.CODIGOGRUPOTRABAJO == null ? (double?)null : double.Parse(info.CriticaLecturaInfo.CODIGOGRUPOTRABAJO);

                        ddlLectura.SelectedValue = muestraBd.lecturaCorrecta.ToString();
                        ddlAnomalia.SelectedValue = muestraBd.anomaliaCorrecta.ToString();

                        if (muestraBd.Resuelto)
                        {
                            btnAceptar.Visible = false;
                            lblError.Text = info.Msj + " - Muestra Resuelta";
                        }
                        else
                        {
                            ddlLectura.SelectedIndex= 0;
                            ddlFoto.SelectedIndex = -1;
                            ddlAnomalia.SelectedIndex = -1;
                            btnAceptar.Visible = true;
                            lblError.Text = info.Msj + "";
                        }

                        hfObj.Value = JsonConvert.SerializeObject(muestraBd);   
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = $"No se encontró información de lectura: , por favor vuelva a intentar nuevamente. {info.Msj}";
                    }

                    gvHistoAnomalia.DataSource = null;
                    gvHistoAnomalia.DataSource = info.histoAnomaliasInfo;
                    gvHistoAnomalia.DataBind();


                }
                else
                {
                    hfIdM.Value = string.Empty;
                    lblError.Visible = true;
                    lblError.Text = "No se encontró información del Cliente, por favor vuelva a intentar nuevamente.";
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "";
                lblError.Visible = true;
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                lblError.Text = $"Error en cargar datos: {msj}";
            }
            mpResolver.Show();
        }
        private ClienteRoot GetCliente(string clave)
        {
            var clienteRoot = _cCtrl.CargarInfoCliente(clave);
            try
            {
                if (clienteRoot != null)
                {
                    List<imagePreview> imageUrlList = new List<imagePreview>();
                    List<PrintConfigPreviewViewModel> imageList = new List<PrintConfigPreviewViewModel>();

                    if (clienteRoot.cliFotos != null)
                    {
                        foreach (var (item, index) in clienteRoot.cliFotos.Select((v, i) => (v, i)))
                        {
                            try
                            {
                                if (item.origen == "Logistics")
                                {
                                    string path = Request.PhysicalApplicationPath;
                                    byte[] bytes = Convert.FromBase64String(item.imagen);

                                    MemoryStream ms = new MemoryStream(bytes);
                                    ms.Position = 0;
                                    imageList.Add(new PrintConfigPreviewViewModel() { Name = item.fecha, Picture = bytes, clave = item.clave });
                                }
                                else
                                {
                                    byte[] img1 = System.IO.File.ReadAllBytes(item.imagen);
                                    imageList.Add(new PrintConfigPreviewViewModel() { Name = item.fecha, Picture = img1, clave = item.clave });
                                }
                            }
                            catch (Exception)
                            {
                                imageUrlList = null;
                                imageList = null;

                            }
                        }
                        imageUrlList = GuardarCopiaImagenes(imageList, clave);
                        clienteRoot.ImagenesInnerText = GetImagenesMosaico(imageUrlList).Text;
                    }
                    else
                    {
                        imageUrlList = null;
                        clienteRoot.ImagenesInnerText = string.Empty;
                    }
                }
                else
                {
                    clienteRoot.cliFotos = null;
                }
                clienteRoot.ListaHistoricoLectura = null;
                clienteRoot.ListaHistoricoConsumo = null;
                return clienteRoot;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public LiteralControl GetImagenesMosaico(List<imagePreview> images)
        {
            LiteralControl html = new LiteralControl();

            string folderProject = Request.Url.AbsolutePath.ToString().Trim();

            string content = @"<div id=""galley"" runat=""server"" ClientIDMode=""static""><ul class='pictures'>";

            try
            {
                int Dividendo = images.Count(), divisor = 6, q, r, totalFilas = 0;
                q = (int)(Dividendo / divisor);
                r = Dividendo % divisor;
                totalFilas = q + (r > 0 ? 1 : 0);

                string imageneshtml = "";

                foreach (var (item, index) in images.Select((v, i) => (v, i)))
                {
                    if (item.url != "")
                    {
                        string path = HttpContext.Current.Request.Url.Authority.ToString();
                        //imageneshtml += $@"<li><img data-original='{item.Name}' src ='{folderProject}/ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                        imageneshtml += $@"<li><img data-original='{item.Name}' src ='../ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                    }
                }
                content += imageneshtml;
                content += " </ul></div>";

            }
            catch (Exception)
            {
                content = "<H3>SIN IMÁGENES</H3>";
            }
            
            html.Text = content;
            return html;
        }
        public List<imagePreview> GuardarCopiaImagenes(List<PrintConfigPreviewViewModel> imagenes, string clave)
        {
            try
            {
                List<imagePreview> ImgFiles = new List<imagePreview>();
                //foreach (var item in imagenes)
                foreach (var (item, index) in imagenes.Select((v, i) => (v, i)))
                {
                    string path = Request.PhysicalApplicationPath;
                    string nombreArchivo = $"{path}ImgCli\\img{index}_{item.Name}_C{item.clave}.Jpeg";

                    //string resolvedUrl = System.Web.HttpContext.Current.Request.Url.Authority.ToString();//VirtualPathUtility.ToAbsolute("~/");

                    ImgFiles.Add(new imagePreview() { url = $"img{index}_{item.Name}_C{item.clave}.Jpeg", Name = $"img{index}_{item.Name}_C{item.clave}" });

                    Bitmap objBitMap = new Bitmap(byteArrayToImage(item.Picture));
                    objBitMap.Save(nombreArchivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                return ImgFiles;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
                ms.Position = 0;
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
            catch (Exception)
            {
                return null;
            }
        }
        private void CargarddlDiales(int ciclo)
        {
            try
            {
                ddldial.DataSource = null;
                ddldial.DataSource = _cCtrl.GetDialesResueltos(ciclo,0);
                ddldial.DataBind();
                ddldial.Items.Insert(0, new ListItem("--SELECCIONAR--", "0"));
            }
            catch (Exception)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }
        }
        private void CargarTipoMuestraFecha()
        {
            try
            {
                ddlTipoFiltro.DataSource = null;
                ddlTipoFiltro.DataSource = _cCtrl.GetTiposMuetrasFecha(ddldial.SelectedItem.Text.Trim(),_usrLog.usuSicra.idUsuario,true);
                ddlTipoFiltro.DataBind();
                //ddlTipoFiltro.Items.Insert(0, new ListItem("--SELECCIONAR--", "0"));
            }
            catch (Exception)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }

        }

        protected void mpResolver_Disposed(object sender, EventArgs e)
        {

        }
    }
}