﻿using SICRA.Controllers;
using SICRA.Models;
using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SICRA.Models.SicraBD;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using Newtonsoft.Json;

namespace SICRA.Views
{
    public partial class SolicitudGestionRefactura : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private RefacturaController _rCtrl = new RefacturaController();
        public readonly string pathDocSopRect = ConfigurationManager.AppSettings["pathDocSopRect"].ToString();
        public readonly int idPerfilNoAsig = int.Parse(ConfigurationManager.AppSettings["idPerfilNoAsig"].ToString());
        private ClienteController _cCtrl = new ClienteController();
        private UsuarioController _uCtrl = new UsuarioController();

        //-------------------------------------EVENTOS-------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            if (_usrLog.usuSicra.idPerfil == idPerfilNoAsig)
            {
                PermitirBloquearCrear(false);
                Fun.HideControls(this);
                lblErrorUsr.Visible = true;
                lblErrorUsr.Text = "Usuario No tiene un perfil asignado para continuar con esta acción.";
            }
            else
            {
                lblErrorUsr.Visible = false;
                lblErrorUsr.Text = string.Empty;
                PermitirBloquearCrear(true);

                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                if (!IsPostBack)
                {
                    CargaData();
                }
            }

            
        }
        protected void txtIdGestion_TextChanged(object sender, EventArgs e)
        {
            Validaciones();
        }        
        protected void txtNis_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ClienteRoot cliente = _cCtrl.GetInfoCliente(txtNis.Text.Trim());
                txtSector.Text = cliente.cliente.area;
                txtMercado.Text = cliente.cliente.mercado;
                hfIdSector.Value = _cCtrl.GetSectorByIdIncms(Convert.ToInt32(cliente.cliente.codarea)).idSector.ToString();
                hfIdMercado.Value = _cCtrl.GetMercadoByIdIncms(cliente.cliente.codmercado).IdMercado.ToString();

                lblError.Visible = false;
                lblError.Text = string.Empty;
                PermitirBloquearCrear(true);
                CargarCiclosFacturados();
                Validaciones();
            }
            catch (Exception ex)
            {
                PermitirBloquearCrear(false);
            }
        }
        protected void btnCrear_Click(object sender, EventArgs e)
        {
            Validaciones();

            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                try
                {
                    RespMsj resp = null;

                    /* Guardar Ciclos Tabla *************************************************/
                    var ciclos = new List<CicloRefactura>();
                    string[] ciclosInt = hfCiclos.Value.Trim().Split(new char[] { ',' });

                    foreach (var ciclo in ciclosInt)
                    {
                        var c = new CicloRefactura
                        {
                            ciclo = int.Parse(ciclo)
                        };
                        ciclos.Add(c);
                    }

                    /* Guardar Documentos Soporte *************************************************/
                    var DocsSopRefac = new List<DocGestionSoporteRefactura>();
                    try
                    {
                        foreach (var (item, index) in fUpload.PostedFiles.Select((v, i) => (v, i)))
                        {
                            string ext = Path.GetExtension(item.FileName).ToLower();
                            var doc = GuardarDocsSoporte(item, $"DocRef_{index + 1}_{txtNis.Text.Trim()}");
                            if (doc != null)
                            {
                                DocsSopRefac.Add(doc);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        DocsSopRefac = null;
                    }

                    int? idPerfilActualAsignado = (int?)_uCtrl.GetUsuarioByUser(_usrLog.user).PerfilAutorizacion.IdPerfilSuperior;
                    /* Guardar Gestion Detalle *************************************************/
                    var GestionDet = new GestionRefacturaDet
                    {
                        idPerfilAsignado = idPerfilActualAsignado,
                        IdUsuarioAutoriza = _usrLog.usuSicra.idUsuario,
                        idPerfilAutoriza = _usrLog.usuSicra.idPerfil,
                        ComentarioResuelve = $"Creación Solicitud Refactura {ddlTipoRect.SelectedItem.Text}, NIS {txtNis.Text.Trim()}, solicitado por {_usrLog.usuSicra.NomPersona}",
                        FechaCreacion = DateTime.Now,
                        FechaHoraResuelto = DateTime.Now,
                        Denego = false,
                        DocGestionSoporteRefactura = new List<DocGestionSoporteRefactura>(DocsSopRefac)
                    };

                    /* Guardar Gestion Maestra *************************************************/
                    if (DocsSopRefac != null)
                    {
                        var GestionMae = new GestionRefacturaMae
                        {
                            codGestion = txtIdGestion.Text.Trim(),
                            idTipoOrigen = int.Parse(ddlOrigenGestion.SelectedValue),
                            nisrad = decimal.Parse(txtNis.Text.Trim()),
                            ciclo = hfCiclos.Value.Trim(),
                            kWhCv = int.Parse(txtKwhCv.Text),
                            LpsCv = decimal.Parse(txtLpsCv.Text),
                            kWhRef = int.Parse(txtKwhRefa.Text),
                            LpsRef = decimal.Parse(txtLpsRefa.Text),
                            kWhTotal = int.Parse(txtKwhCv.Text) + int.Parse(txtKwhRefa.Text),
                            LpsTotal = decimal.Parse(txtLpsCv.Text) + decimal.Parse(txtLpsRefa.Text),
                            dbcr = rbDbcr.SelectedValue,
                            IdSector = int.Parse(hfIdSector.Value),
                            IdMercado = int.Parse(hfIdMercado.Value),
                            GesCom = true,
                            idUsuario = _usrLog.usuSicra.idUsuario,
                            IdPerfilCrea = _usrLog.usuSicra.idPerfil,
                            IdPerfilActualAsignado = idPerfilActualAsignado,
                            idTipoRect = int.Parse(ddlTipoRect.SelectedValue),
                            FechaCreacion = DateTime.Now,
                            ComentarioSolicitud = txtRazonRefa.Text.Trim(),
                            Denegada = false,
                            Finalizada = false,
                            CicloRefactura = new List<CicloRefactura>(ciclos),
                            GestionRefacturaDet = new List<GestionRefacturaDet>() { GestionDet }
                        };

                        resp = _rCtrl.PutGestionRefactura_Mae_Det_DocGes(GestionMae);

                        lblError.Visible = true;
                        lblError.Text = resp.mensaje;

                        if (resp.estado == "ok")
                        {
                            lblError.Attributes.Add("class", "label label-success");
                        }
                        else
                        {
                            lblError.Attributes.Add("class", "label label-danger");
                        }
                    }
                    else
                    {
                        lblError.Attributes.Add("class", "label label-success");
                        lblError.Text = "Error al cargar arhivo.";
                    }
                    btnCrear.Visible = false;
                }
                catch (Exception ex)
                {
                    string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                    lblError.Visible = true;
                    lblError.Text = msj;
                    lblError.Attributes.Add("class", "label label-danger");
                }
            }
        }
        //-------------------------------------MÉTODOS-------------------------------------
        private void CargaData()
        {
            try
            {
                var permiteAutorizar = _rCtrl.GetPermiteAutorizar(_usrLog.usuSicra.idUsuario);

                if (!permiteAutorizar)
                {
                    ddlCiclos.DataSource = null;
                    ddlCiclos.Items.Clear();
                    ddlCiclos.DataBind();

                    Fun.ClearControls(this);
                    PermitirBloquearCrear(false);
                    lblError.Visible = true;
                    lblError.Text = $"Atención, su usuario no esta autorizado para realizar Solicitudes de Ajustes o Rectificaciones.";
                    lblError.Attributes.Add("class", "label label-danger");
                }
                else
                {
                    ddlOrigenGestion.DataSource = null;
                    ddlOrigenGestion.DataSource = _rCtrl.GetAllTiposOrigen();
                    ddlOrigenGestion.DataTextField = "nombre";
                    ddlOrigenGestion.DataValueField = "idTipoOrigen";
                    ddlOrigenGestion.DataBind();
                    ddlOrigenGestion.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

                    ddlTipoRect.DataSource = null;
                    ddlTipoRect.DataSource = _rCtrl.GetAllTiposRectificacion();
                    ddlTipoRect.DataTextField = "nombre";
                    ddlTipoRect.DataValueField = "idTipoRect";
                    ddlTipoRect.DataBind();
                    ddlTipoRect.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

                    lblError.Visible = false;
                    lblError.Text = "";

                    lblError.Visible = false;
                    lblError.Text = string.Empty;
                    PermitirBloquearCrear(true);
                }
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = $"Error en carga de datos: {ex.Message}";
                PermitirBloquearCrear(false);
            }
        }
        private void Validaciones()
        {
            //**************************Id Gestiones**************************
            rfvGestion.IsValid = false;
            //**************************ciclos**************************
            try
            {
                if (hfCiclos.Value.Trim().Length > 0)
                {
                    rfvCiclos.IsValid = true;
                }
            }
            catch (Exception ex)
            {
                PermitirBloquearCrear(false);
                rfvCiclos.IsValid = false;
            }
        }
        private void CargarCiclosFacturados()
        {
            try
            {
                var obj = _rCtrl.GetCiclosFacturadosYMultiByNis(int.Parse(txtNis.Text.Trim()));

                if (obj.periodo.Count > 0)
                {
                    ddlCiclos.DataSource = null;
                    ddlCiclos.Items.Clear();
                    ddlCiclos.DataSource = obj.periodo;
                    ddlCiclos.DataBind();
                }
            }
            catch (Exception ex)
            {
                PermitirBloquearCrear(false);
                lblError.Visible = true;
                lblError.Text = "Error: " + ex.Message;
            }
        }
        private void PermitirBloquearCrear(bool permite)
        {
            btnCrear.Visible = permite;
        }
        private DocGestionSoporteRefactura GuardarDocsSoporte(HttpPostedFile file, string indexClave)
        {
            try
            {
                string _nombreArchivo = "";
                _nombreArchivo = indexClave + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(file.FileName);

                string filePath = $"{pathDocSopRect}\\{_nombreArchivo}";
                file.SaveAs(filePath);

                var DocSoporte = new DocGestionSoporteRefactura
                {
                    NomDoc = _nombreArchivo,
                    Ruta = filePath,
                    FechaHora = DateTime.Now
                };

                return DocSoporte;
            }
            catch (Exception ex)
            {
                lblError.Text = ex.ToString();

                return null;
            }
        }
    }
}