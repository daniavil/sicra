﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
//using Epplus;

namespace SICRA.Views
{
    public partial class ViewCriticaRefactura : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private CriticaRefacturaController _cCtrl = new CriticaRefacturaController();
        private CriticaLecturaController _clCtrl = new CriticaLecturaController();
        private UsuarioController _uCtrl = new UsuarioController();

        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                CargarddlCiclos();
            }
        }
        private void CargarddlCiclos()
        {
            try
            {
                ddlCiclo.DataSource = null;
                ddlCiclo.DataSource = _cCtrl.GetCiclosMuestras();
                ddlCiclo.DataBind();
                ddlCiclo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
            }
            catch (Exception)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }

        }
        private void CargarddlDiales(int ciclo)
        {
            try
            {
                ddldial.DataSource = null;
                ddldial.DataSource = _cCtrl.GetDialesResueltos(ciclo);
                ddldial.DataBind();
                ddldial.Items.Insert(0, new ListItem("--SELECCIONAR--", "0"));
            }
            catch (Exception)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }

        }
        private void CargarMuestrasBd(int ciclo, int dial)
        {
            int size = tvMuestrasUsr.CheckedNodes.Count;
            TreeNode[] list = new TreeNode[size];
            tvMuestrasUsr.CheckedNodes.CopyTo(list, 0);
            List<string> usuarios = new List<string>();

            if (size>0)
            {
                foreach (var item in list)
                {
                    usuarios.Add(item.Value);
                }

                var muestras = _cCtrl.GetResueltasByUser(usuarios, ciclo, dial);
                gvRepoM.DataSource = null;
                gvRepoM.DataSource = muestras;
                gvRepoM.DataBind();

                spnNoResueltas.InnerText = muestras.Count.ToString();
            }


            
        }
        protected void gvRepoM_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    string IdM = gvRepoM.DataKeys[e.Row.RowIndex].Value.ToString();

            //    TableCell cellResuelto = e.Row.Cells[7];

            //    if (bool.Parse(cellResuelto.Text) == true)
            //    {
            //        for (int i = 0; i < e.Row.Cells.Count; i++)
            //        {
            //            e.Row.Cells[i].BackColor = Color.LightGreen;
            //        }
            //    }
            //    else
            //    {
            //        for (int i = 0; i < e.Row.Cells.Count; i++)
            //        {
            //            e.Row.Cells[i].BackColor = Color.LightCoral;
            //        }
            //    }
            //}
        }
        private void cargarDatos(string commandargument)
        {
            string idrow = commandargument;
            hfIdM.Value = string.Empty;
            var muestraBd = _cCtrl.GetAsignacionById(idrow);
            hfIdM.Value = idrow;
            int clave = muestraBd.nisrad;

            hfFilCiclo.Value = ddlCiclo.SelectedValue;
            hfFilDial.Value = ddldial.SelectedValue;

            ClienteRoot cliente = GetCliente(clave.ToString());
            CriticaRefacturaDtoInfo infoCritica = null;
            SetClearValues();

            if (cliente != null)
            {
                literalControl.Text = cliente.ImagenesInnerText;
                lblNisrad.Text = cliente.cliente.nis_rad;
                lblMedidor.Text = cliente.cliente.num_medidor;
                lblUbicacion.Text = cliente.cliente.ubicacion;
                lblTarifa.Text = cliente.cliente.nom_tarifa;
                lblCliente.Text = cliente.cliente.nom_cli;
                lblDireccion.Text = cliente.cliente.direccion;

                //202008

                int cicloIncms = int.Parse("20" + muestraBd.Ciclo);
                var info = _clCtrl.GetInfoMuestraWs(cliente.cliente.clave.ToString(), cicloIncms);
                infoCritica = _cCtrl.GetCriticaFactInfo(cliente.cliente.nis_rad.ToString(), muestraBd.Ciclo);

                if (infoCritica != null)
                {
                    lblMsjError.Visible = false;
                    lblMsjError.Text = string.Empty;
                    //lblRepSicra.Text = infoCritica.repoSicra;
                    //lblCsmoCri.Text = infoCritica.csmocri;
                    //lblCsmoFact.Text = infoCritica.csmofact;
                    lblAccion.Text = infoCritica.Origen;
                    lblUsrCri.Text = infoCritica.codUsuarioRef;
                    lblObsCri.Text = infoCritica.Observacion;
                    var usrCri = _uCtrl.GetUsuarioByUser(infoCritica.codUsuarioRef);
                    lblNomUsr.Text = usrCri == null ? "Usuario No existe en SICRA." : usrCri.NomPersona;

                    if (info.CriticaLecturaInfo != null)
                    {
                        lblLecturaEncontrada.Text = info.CriticaLecturaInfo.LECTURAENCONTRADA;
                        lblFLectura.Text = info.CriticaLecturaInfo.FECHALECTURA;
                        lblFProgramada.Text = info.CriticaLecturaInfo.FECHA_PROGRAMADA;
                        lblDial.Text = info.CriticaLecturaInfo.CODIGODIAL == null ? muestraBd.dial.ToString() : info.CriticaLecturaInfo.CODIGODIAL;
                        lblCodAnomaliaLect.Text = info.CriticaLecturaInfo.CODIGOANOMALIA;
                        lblAnomaliaLect.Text = info.CriticaLecturaInfo.DESCRIPCION;
                        lblTipoLect.Text = info.CriticaLecturaInfo.tipolect;
                    }
                }
                else
                {
                    lblMsjError.Visible = true;
                    lblMsjError.Text = "Ocurrió un error al cargar datos, por favor vuelva a intentar.";
                    SetClearValues();
                }

                /************************************resultado resolucion muestra***********************************/
                ddlP1RefacturaCorrecta.Text = muestraBd.P1RefacturaCorrecta;
                ddlP2DocSubida.Text = muestraBd.P2DocSubida;
                ddlP3ProcesoAutorizacionCorrecto.Text = muestraBd.P3ProcesoAutorizacionCorrecto;
                ddlP4CorreccionProcedente.Text = muestraBd.P4CorreccionProcedente;
                ddlP5DatosResoCorrecto.Text = muestraBd.P5DatosResoCorrecto;
                ddlP6DocCorrecta.Text = muestraBd.P6DocCorrecta;
                ddlP7ObsCierreCorrecto.Text = muestraBd.P7ObsCierreCorrecto;
                ddlP8RepoSicra.Text = muestraBd.P8RepoSicra;
                ddlP9OSRevGen.Text = muestraBd.P9OSRevGen;
                /************************************Cargar Datos Evaluacion***********************************/
                lblRefaCorrecta.Text = muestraBd.P1RefacturaCorrecta?.ToString();
                lblPorRefaCorrecta.Text = $"{muestraBd.CaliRefaCorrecta?.ToString()} %";

                lblDocSoporteCorrecta.Text = muestraBd.P6DocCorrecta;
                lblPorDocSoporteCorrecta.Text = $"{muestraBd.CaliDocSoporteCorrecta?.ToString()} %";

                lblObsCierreCorrecta.Text = muestraBd.P7ObsCierreCorrecto;
                lblPorObsCierreCorrecta.Text = $"{muestraBd.CaliObsCierreCorrecta?.ToString()} %";

                lblProcAutCorrecta.Text = muestraBd.P3ProcesoAutorizacionCorrecto;
                lblPorProcAutCorrecta.Text = $"{muestraBd.CaliProcAutCorrecta?.ToString()} %";

                lblDatosResolCorrecta.Text = muestraBd.P5DatosResoCorrecto;
                lblPorDatosResolCorrecta.Text = $"{muestraBd.CaliDatosResolCorrecta?.ToString()} %";

                lbltotCali.Text = $"{muestraBd.Calificacion?.ToString()} %";
            }
            hfObj.Value = string.Empty;
            mpResolver.Show();
        }
        void SetClearValues()
        {
            //pnlP1.Visible = false;
            //pnl.Visible = false;
            //pnlp31.Visible = false;
            //pnlp32.Visible = false;
            literalControl.Text = null;
            lblNisrad.Text = "S/D";
            lblMedidor.Text = "S/D";
            lblUbicacion.Text = "S/D";
            lblTarifa.Text = "S/D";
            lblCliente.Text = "S/D";
            lblDireccion.Text = "S/D";
            lblCodAnoCri.Text = "S/D";
            lblAnoCritica.Text = "S/C";
            lblRepSicra.Text = "S/C";
            lblLecturaEncontrada.Text = "S/D";
            lblFLectura.Text = "S/D";
            lblFProgramada.Text = "S/D";
            lblDial.Text = "S/D";
            lblCodAnomaliaLect.Text = "S/D";
            lblAnomaliaLect.Text = "S/D";
            lblTipoLect.Text = "S/D";
        }
        protected void gvRepoM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToUpper() == "RESOLVER")
            {
                try
                {
                    cargarDatos(e.CommandArgument.ToString());
                }
                catch (Exception ex)
                {

                }
            }
        }
        private ClienteRoot GetCliente(string clave)
        {
            var clienteRoot = _clCtrl.CargarInfoCliente(clave);
            try
            {
                if (clienteRoot != null)
                {
                    List<imagePreview> imageUrlList = new List<imagePreview>();
                    List<PrintConfigPreviewViewModel> imageList = new List<PrintConfigPreviewViewModel>();

                    if (clienteRoot.cliFotos != null)
                    {
                        foreach (var (item, index) in clienteRoot.cliFotos.Select((v, i) => (v, i)))
                        {
                            try
                            {
                                if (item.origen == "Logistics")
                                {
                                    string path = Request.PhysicalApplicationPath;
                                    byte[] bytes = Convert.FromBase64String(item.imagen);

                                    MemoryStream ms = new MemoryStream(bytes);
                                    ms.Position = 0;
                                    imageList.Add(new PrintConfigPreviewViewModel() { Name = item.fecha, Picture = bytes, clave = item.clave });
                                }
                                else
                                {
                                    byte[] img1 = System.IO.File.ReadAllBytes(item.imagen);
                                    imageList.Add(new PrintConfigPreviewViewModel() { Name = item.fecha, Picture = img1, clave = item.clave });
                                }
                            }
                            catch (Exception)
                            {
                                imageUrlList = null;
                                imageList = null;

                            }
                        }
                        imageUrlList = GuardarCopiaImagenes(imageList, clave);
                        clienteRoot.ImagenesInnerText = GetImagenesMosaico(imageUrlList).Text;
                    }
                    else
                    {
                        imageUrlList = null;
                        clienteRoot.ImagenesInnerText = string.Empty;
                    }
                }
                else
                {
                    clienteRoot.cliFotos = null;
                }
                clienteRoot.ListaHistoricoLectura = null;
                clienteRoot.ListaHistoricoConsumo = null;
                return clienteRoot;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public class imagePreview
        {
            public string Name { get; set; }
            public string url { get; set; }
        }
        public class PrintConfigPreviewViewModel
        {
            public string Name { get; set; }
            public byte[] Picture { get; set; }
            public string clave { get; set; }
        }
        public LiteralControl GetImagenesMosaico(List<imagePreview> images)
        {
            LiteralControl html = new LiteralControl();

            string folderProject = Request.Url.AbsolutePath.ToString().Trim();

            string content = @"<div id=""galley"" runat=""server"" ClientIDMode=""static""><ul class='pictures'>";

            try
            {
                int Dividendo = images.Count(), divisor = 6, q, r, totalFilas = 0;
                q = (int)(Dividendo / divisor);
                r = Dividendo % divisor;
                totalFilas = q + (r > 0 ? 1 : 0);

                string imageneshtml = "";

                foreach (var (item, index) in images.Select((v, i) => (v, i)))
                {
                    if (item.url != "")
                    {
                        string path = HttpContext.Current.Request.Url.Authority.ToString();
                        //imageneshtml += $@"<li><img data-original='{item.Name}' src ='{folderProject}/ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                        imageneshtml += $@"<li><img data-original='{item.Name}' src ='../ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                    }
                }
                content += imageneshtml;
                content += " </ul></div>";
            }
            catch (Exception)
            {
                content = "<H3>SIN IMÁGENES</H3>";
            }

            
            html.Text = content;
            return html;
        }
        public List<imagePreview> GuardarCopiaImagenes(List<PrintConfigPreviewViewModel> imagenes, string clave)
        {
            try
            {
                List<imagePreview> ImgFiles = new List<imagePreview>();
                //foreach (var item in imagenes)
                foreach (var (item, index) in imagenes.Select((v, i) => (v, i)))
                {
                    string path = Request.PhysicalApplicationPath;
                    string nombreArchivo = $"{path}ImgCli\\img{index}_{item.Name}_C{item.clave}.Jpeg";

                    //string resolvedUrl = System.Web.HttpContext.Current.Request.Url.Authority.ToString();//VirtualPathUtility.ToAbsolute("~/");

                    ImgFiles.Add(new imagePreview() { url = $"img{index}_{item.Name}_C{item.clave}.Jpeg", Name = $"img{index}_{item.Name}_C{item.clave}" });

                    Bitmap objBitMap = new Bitmap(byteArrayToImage(item.Picture));
                    objBitMap.Save(nombreArchivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                return ImgFiles;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
                ms.Position = 0;
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
            catch (Exception)
            {
                return null;
            }
        }
        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarMuestrasBd(int.Parse(ddlCiclo.SelectedValue),int.Parse(ddldial.SelectedValue));
            }
            catch (Exception ex)
            {

            }
            
        }
        protected void ddlCiclo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tvMuestrasUsr.Nodes.Clear();

                CargarddlDiales(int.Parse(ddlCiclo.SelectedValue));
                if (hfFilDial.Value != "")
                {
                    ddldial.SelectedValue = hfFilDial.Value;
                }
            }
            catch (Exception)
            {
                CargarddlCiclos();
                ddldial.Items.Clear();
                ddldial.DataSource = null;
                ddldial.DataBind();
                ddldial.SelectedIndex = -1;
            }
            
        }
        protected void ddldial_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tvMuestrasUsr.Nodes.Clear();
                var muestrasInfo = _cCtrl.GetMuestrasUsuariosEstados(int.Parse(ddlCiclo.SelectedValue), int.Parse(ddldial.SelectedValue));

                lblTotGen.Text = muestrasInfo.CantTotalGen.ToString();
                lblResueltos.Text = muestrasInfo.CantResueltas.ToString();
                lblNoRes.Text = muestrasInfo.CantNoResueltas.ToString();

                foreach (var usr in muestrasInfo.ListaMuestras)
                {
                    TreeNode node2 = new TreeNode($"Resueltos: {usr.CantResueltas}")
                    {
                        ShowCheckBox = usr.CantResueltas > 0 ? true : false,
                        SelectAction = TreeNodeSelectAction.None,
                        Value = usr.usuario
                    };

                    TreeNode node3 = new TreeNode($"No Resueltos: {usr.CantNoResueltas}")
                    {
                        ShowCheckBox = false,
                        SelectAction = TreeNodeSelectAction.None
                    };

                    var treeNode = new TreeNode($"{usr.usuario} - {usr.NomUsr}")
                    {
                        ShowCheckBox = false,
                        Expanded = true,
                        SelectAction = TreeNodeSelectAction.None
                    };
                    treeNode.ChildNodes.Add(node2);
                    treeNode.ChildNodes.Add(node3);
                    tvMuestrasUsr.Nodes.Add(treeNode);

                    tvMuestrasUsr.SelectedNodeStyle.ForeColor = Color.Black;
                }
            }
            catch (Exception)
            {
                lblTotGen.Text = "0";
                lblResueltos.Text = "0";
                lblNoRes.Text = "0";
            }
            
        }
  
    }
}