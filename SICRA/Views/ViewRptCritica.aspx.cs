﻿using SICRA.Controllers;
using SICRA.Dto;
using System;
using System.Configuration;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Services;
using System.Web.UI.WebControls;
using Newtonsoft;
using Newtonsoft.Json;
using System.Linq;
//using Epplus;

namespace SICRA.Views
{
    public partial class ViewRptCritica : System.Web.UI.Page
    {
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private static CriticaFacturacionController _cCtrl = new CriticaFacturacionController();

        /**************************************************Metodos************************************************/
        private string LoadTables(Filtro f)
        {
            object resultData = new object();
            if (f.TipoRepo == 1)
            {
                resultData = _cCtrl.GetDatosGeneral(f);
                return Json.Encode(resultData);
            }
            else if (f.TipoRepo == 2)
            {
                resultData = _cCtrl.GetDatosDetalle(f);
                return Json.Encode(resultData);
            }
            else if (f.TipoRepo == 3)
            {
                resultData = _cCtrl.GetDatosControl(f);
                return Json.Encode(resultData);
            }
            else
            {
                return null;
            }
        }

        /**************************************************Eventos************************************************/
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarCiclos();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["jsn"]))
            {

                Filtro obj = JsonConvert.DeserializeObject<Filtro>(Request.QueryString["jsn"]);

                if (!string.IsNullOrWhiteSpace(obj.FechasLectura))
                {
                    char[] delimiters = new char[] { ',' };
                    string[] fechas = obj.FechasLectura.Trim().Split(delimiters);
                    var fechasConcat = "'" + string.Join("','", fechas.Select(x => x.ToString()).ToArray()) + "'";
                    obj.FechasLectura = fechasConcat;
                }
                string responseJson = LoadTables(obj);

                //Check for CallBack function.
                if (!string.IsNullOrEmpty(Request.QueryString["callback"]))
                {
                    //Wrap a call to CallBack function with the JSON string as parameter.
                    responseJson = string.Format("{0}({1});", Request.QueryString["callback"], responseJson);
                }

                //Send the Response in JSON format to Client.
                Response.ContentType = "text/json";
                Response.Write(responseJson);
                Response.End();
            }
        }

        private void CargarCiclos()
        {
            try
            {
                ddlCiclo.DataSource = null;
                ddlCiclo.DataSource = _cCtrl.GetCiclosMuestras();
                ddlCiclo.DataBind();
                ddlCiclo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
            }
            catch (Exception)
            {

            }
        }

        protected void ddlCiclo_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarddlFechas(int.Parse(ddlCiclo.SelectedValue));
        }

        private void CargarddlFechas(int ciclo)
        {
            try
            {
                ddlFechasLect.DataSource = null;
                ddlFechasLect.Items.Clear();
                ddlFechasLect.DataSource = _cCtrl.GetDialesResueltos(ciclo);
                ddlFechasLect.DataBind();
            }
            catch (Exception)
            {
            }

        }
    }
}