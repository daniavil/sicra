﻿using ClosedXML.Excel;
using DevExpress.XtraPrinting.Caching;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using SICRA.Views.Reportes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
//using Epplus;

namespace SICRA.Views
{
    public partial class ConsultaRectificaciones : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private RefacturaController _rCtrl = new RefacturaController();


        //-------------------------------------EVENTOS-------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                CargarSolicitudesAutorizaciones();
            }
        }
        protected void gvRepoM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToUpper() == "RESOLVER")
            {
                CargarDatos(e.CommandArgument.ToString());
            }
        }
        protected void hlDownload_Click(object sender, EventArgs e)
        {
            ReadPDFFile();
        }
        protected void gvResoluciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int IdM = int.Parse(gvResoluciones.DataKeys[e.Row.RowIndex].Value.ToString());
                GridView gvDocs = e.Row.FindControl("gvDocs") as GridView;
                var listaDocs = _rCtrl.GetDocumentosByIdGestDet(IdM);
                gvDocs.DataSource = listaDocs;
                gvDocs.DataBind();

            }
        }
        //-------------------------------------MÉTODOS-------------------------------------

        private void CargarSolicitudesAutorizaciones()
        {
            var muestras = _rCtrl.GetAllSolicitudesRefaGrid();
            gvRepoM.DataSource = null;
            gvRepoM.DataSource = muestras;
            gvRepoM.DataBind();

            spnNoResueltas.InnerText = muestras != null ? muestras.Count.ToString() : "0";
        }
        private void CargarDatos( string commandargument)
        {
            try
            {
                int idGestion = int.Parse(commandargument);

                var g = _rCtrl.GetInfoSolicitudRefaByidGestion(idGestion);

                if (g != null)
                {
                    hfIdAsigGes.Value = g.IdAsigRefa.ToString();
                    hfIdGestionRefa.Value = g.IdGestionRefa.ToString();
                    lblNisrad.Text = g.Nis.ToString();
                    lblOrigen.Text = g.Origen;
                    lblCodGestion.Text = g.IdGestionOrigen;
                    lblTipoRect.Text = g.TipoRefa;
                    lblKwh.Text = g.kWhTotal;
                    lblMonto.Text = g.LpsTotal.ToString();
                    lblDbcr.Text = g.Dbcr;
                    lblCiclos.Text = g.Ciclos;
                    lblSector.Text = g.Sector;
                    lblMercado.Text = g.Mercado;
                    lblFechaSoli.Text = g.FechaHoraCreado.ToShortDateString();
                    lblSolicita.Text = g.NomSolicita;
                    txtCausa.Text = g.RazonSolicita;
                    lblEstado.Text = g.Estado;
                    lblEstado.Attributes.Add("class", g.Denegada ? "label label-danger" : "label label-success");
                    lblFinalizado.Text = g.FinalizadoString;
                    lblFinalizado.Attributes.Add("class", g.FinalizadoString=="NO" ? "label label-warning" : "label label-success");

                    literalComercial.Text = GetTimelineHtml(g.TimeLineCO).Text;
                    literalCE.Text = GetTimelineHtml(g.TimeLineCE).Text;

                    gvResoluciones.DataSource = null;
                    gvResoluciones.DataSource = g.Resoluciones;
                    gvResoluciones.DataBind();

                    if (g.Finalizado && !g.Denegada)
                    {
                        var data = _rCtrl.GetRptAutorizacion(g.IdGestionRefa);

                        rptAutorizaRefa report = new rptAutorizaRefa();
                        var list = new List<RepoAutorizaDto>();
                        list.Add(data);
                        report.DataSource = list;
                        ASPxWebDocumentViewer1.OpenReport(report);
                    }
                    else
                    {
                        ASPxWebDocumentViewer1.Visible = false;
                    }
                }
                lblErrorDatos.Visible = false;
            }
            catch (Exception ex)
            {
                lblErrorDatos.Visible = true;
                lblErrorDatos.Text = $"Se produjo una excepción: {ex.Message}";
            }
            mpResolver.Show();
        }
        public LiteralControl GetTimelineHtml(List<TimeLineAutorizacion> data)
        {
            LiteralControl html = new LiteralControl();
            string content = $@"<ul class='timeline' id='timeline'>";

            try
            {
                var li = string.Empty;
                foreach (var (item, index) in data.Select((v, i) => (v, i)))
                {
                    var stateColor = item.Denego == 2 ? "vacio" : (item.Denego == 1 ? "denegate" : "complete");

                    li += $@"<li class='li {stateColor}'>
                                <div class='timestamp'>
                                    <span class='author'>{item.NomPersona}</span>
                                    <span class='date'>{item.Fecha}<span>
                                </div>
                                <div class='status'>
                                    <h4>{item.Perfil}</h4>
                                </div>
                            </li>";
                }
                content += li;
                content += " </ul>";
            }
            catch (Exception)
            {
                content = "<H3>SIN DATOS</H3>";
            }

            html.Text = content;
            return html;
        }
        public void ReadPDFFile()
        {
            string path = hfUrlFile.Value.ToString();
            string archivo = hfNomFile.Value.ToString();
            try
            {
                FileInfo file = new FileInfo(path);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", "attachment; filename=" + archivo);
                    Response.AddHeader("Content-Type", "application/Excel");
                    Response.ContentType = "application/vnd.xls";
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.WriteFile(file.FullName);
                    Response.End();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Error Message", "alert('Archivo No existe.!!!')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Error Message", "alert('" + ex.Message.ToString() + "')", true);
            }
        }
        protected void hlDownload_Command(object sender, CommandEventArgs e)
        {
            ReadFile(e.CommandArgument.ToString(), e.CommandName.ToString());
        }
        public void ReadFile(string path, string archivo)
        {
            try
            {
                WebClient client = new WebClient();
                Byte[] buffer = client.DownloadData(path);

                if (buffer != null)
                {
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-length", buffer.Length.ToString());
                    Response.AddHeader("content-disposition", $"attachment;filename={archivo}");
                    Response.BinaryWrite(buffer);
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "window.open('application/octet-stream','_newtab');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Error Message", "alert('" + ex.Message.ToString() + "')", true);
            }

        }
    }
}