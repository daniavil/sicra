﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
//using Epplus;

namespace SICRA.Views
{
    public partial class AsignacionMuestra : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private RegionesController _regCtrl = new RegionesController();
        private CriticaLecturaController _cCtrl = new CriticaLecturaController();
        private UsuarioController _UCtrl = new UsuarioController();

        /**********************************************Eventos*************************************************/
        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            //rbTipoCount.SelectedIndex = 0;
            //rbTipoCount.Items[1].Enabled = false;
            //if (int.Parse(ddlTipoFiltro.SelectedValue) == 4)
            //{
            //    rbTipoCount.SelectedIndex = 0;
            //    rbTipoCount.Items[1].Enabled = true;
            //}


            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {

                //ckbTipoFiltro.Enabled = false;
            }
        }
        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                pnlAsignado.Visible = true;
                lblFiltros.Text = string.Empty;
                if (ddlFecha.SelectedIndex > 0)
                {
                    if (ExisteMuestraFecha())
                    {
                        lblMsj.Text = "Ya existen registros de este tipo de muestra para la fecha seleccionada.!";
                        lblMsj.Visible = true;
                        gvRepoM.DataSource = null;
                        gvRepoM.DataBind();
                    }
                    else
                    {
                        Sectores sector = _regCtrl.GetFirstSectorByUser(_usrLog.usuSicra.idUsuario);
                        List<string> codAnomalias = new List<string>();

                        txtTipoFiltro.Text = string.Empty;
                        string concat = string.Empty;
                        string ids = "";

                        int op = int.Parse(ddlTipoFiltro.SelectedValue);
                        if (op == 1)
                        {
                            foreach (ListItem li in ckbTipoFiltro.Items)
                            {
                                if (li.Selected == true)
                                {
                                    codAnomalias.Add("'" + li.Value + "'");
                                    concat = concat + li.Value + " - " + li.Text + Environment.NewLine;
                                    ids = "''" + string.Join("','", codAnomalias) + "''";
                                }
                            }
                        }
                        else if (op == 2)
                        {
                            foreach (ListItem li in ckbTipoFiltro.Items)
                            {
                                if (li.Selected == true)
                                {
                                    codAnomalias.Add(li.Value);
                                    concat = concat + li.Text + Environment.NewLine;
                                    ids = "'''" + string.Join("'',''", codAnomalias) + "'''";
                                }
                            }
                        }
                        else if (op == 3)
                        {
                            codAnomalias.Clear();
                            ckbTipoFiltro.Items.Clear();
                            concat = string.Empty;

                        }
                        else if (op == 4)
                        {
                            foreach (ListItem li in ckbTipoFiltro.Items)
                            {
                                if (li.Selected == true)
                                {
                                    codAnomalias.Add(li.Value);
                                    concat = concat + li.Text + Environment.NewLine;
                                }
                            }
                        }

                        txtTipoFiltro.Text = concat;

                        string date = DateTime.Parse(ddlFecha.SelectedValue.ToString().ToString()).ToString("yyyy/MM/dd");
                        CargarReportes(ids, date, sector.CodSector.ToString());
                        btnAsignar.Enabled = true;
                        lblMsjSave.Visible = false;
                        lblMsjSave.Text = string.Empty;
                    }
                }
                else
                {
                    lblMsjSave.Text = "ERROR EN PARÁMETROS.";
                    lblMsjSave.Attributes["CssClass"] = "label label - danger";
                    lblMsjSave.Visible = true;
                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblFiltros.Text = $"Excepción: {ex.Message}";
            }

        }
        protected void btnAsignar_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                try
                {
                    List<MuestraLecturasMes> muestras = new List<MuestraLecturasMes>();
                    foreach (GridViewRow row in gvRepoM.Rows)
                    {
                        try
                        {
                            for (int i = 0; i < gvRepoM.Columns.Count; i++)
                            {
                                //string header = gvRepoM.Columns[i].HeaderText;
                                string cellText = row.Cells[i].Text;
                                cellText = row.Cells[i].Text.Trim().Replace("&nbsp;", string.Empty);
                                row.Cells[i].Text = cellText;
                            }

                            string CodGrupoTrab = row.Cells[5].Text.Trim().Replace("&nbsp;", string.Empty);
                            string CodAdmin = row.Cells[9].Text.Trim().Replace("&nbsp;", string.Empty);

                            int? SectorId = (int?)int.Parse(row.Cells[10].Text.Trim());

                            if (SectorId == null)
                            {
                                SectorId = _regCtrl.GetFirstSectorByUser(_usrLog.usuSicra.idUsuario).CodSector;
                            }

                            muestras.Add(new MuestraLecturasMes
                            {
                                CLAVE = int.Parse(row.Cells[1].Text),
                                CODIGODIAL = int.Parse(row.Cells[2].Text),
                                SUBCLASE = row.Cells[3].Text,
                                DESCRIPCION = HttpUtility.HtmlDecode(row.Cells[4].Text),
                                CODIGOGRUPOTRABAJO = CodGrupoTrab == string.Empty ? (double?)null : double.Parse(row.Cells[5].Text),
                                CambioParametrizacion = row.Cells[7].Text.Trim() + " --> " + row.Cells[8].Text.Trim(),
                                TipoFiltro = ckbTotal.Checked ? "MUESTRA TOTAL" : ddlTipoFiltro.SelectedItem.Text.Trim(),
                                FECHALECTURA = DateTime.Parse(spnFecha.InnerText),
                                EsTotal = spnTipo.InnerText.Trim() == "MUESTRA TOTAL" ? true : false,
                                fechaRegistro = DateTime.Now,
                                usuario = _usrLog.user,
                                Resuelto = false,
                                lecturaCorrecta = 0,
                                fotoCorrecta = false,
                                anomaliaCorrecta = false,
                                SECTOR_ID = SectorId,
                                PERIODO = int.Parse(DateTime.Parse(spnFecha.InnerText).ToString("yyyyMM")),
                                CODIGOADMINISTRATIVO = CodAdmin == string.Empty ? (double?)null : double.Parse(row.Cells[9].Text),
                            });
                        }
                        catch (Exception ex)
                        {
                        }
                    }

                    if (ddlTipoFiltro.SelectedItem.Text.Trim() == "INTERSECTORIAL")
                    {
                        muestras = GetListaUsuariosMuestras(muestras);
                    }
                    else
                    {
                        muestras.ToList().ForEach(x => x.UsrResuelve = _usrLog.usuSicra.idUsuario);
                    }

                    RespMsj resp = GuardarMuestra(muestras);
                    btnAsignar.Enabled = false;
                    lblMsjSave.Visible = true;
                    Fun.ClearControls(this);
                    lblMsjSave.Text = resp.mensaje;
                    lblMsjSave.Attributes["CssClass"] = "label label - success";

                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();


                }
                catch (Exception ex)
                {
                }
            }
        }
        protected void ddlFecha_SelectedIndexChanged(object sender, EventArgs e)
        {
            string fecha = ""; //DateTime.Parse(ddlFecha.SelectedValue.ToString()).ToShortDateString();
            ckbTipoFiltro.ClearSelection();
            if (ddlFecha.SelectedIndex > 0)
            {
                fecha = DateTime.Parse(ddlFecha.SelectedValue.ToString()).ToShortDateString();

                if (ExisteMuestraFecha())
                {
                    lblMsj.Text = "Ya existen registros de este tipo de muestra para la fecha seleccionada.!";
                    lblMsj.Visible = true;
                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                }
                else
                {
                    lblMsj.Text = string.Empty;
                    lblMsj.Visible = false;
                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                    pnlAsignado.Visible = false;
                }
            }
        }
        protected void ckbSesgada_CheckedChanged(object sender, EventArgs e)
        {
            var ckb = (CheckBox)sender;
            Fun.ClearControls(this);
            ckb.Checked = true;
            txtPorcentaje.Text = "2";
            hfFiltra.Value = "0";
            CargarCiclos();
            ckbTotal.Checked = false;
            divTipoFiltro.Visible = true;
            divAnomalia2.Visible = true;
            txtTipoFiltro.Visible = true;
            filtroTipo.Visible = true;
        }
        protected void ckbTotal_CheckedChanged(object sender, EventArgs e)
        {
            var ckb = (CheckBox)sender;
            Fun.ClearControls(this);
            ckb.Checked = true;
            txtPorcentaje.Text = "2";
            lblMsj.Text = string.Empty;
            hfFiltra.Value = "1";
            CargarCiclos();
            ckbSesgada.Checked = false;
            divTipoFiltro.Visible = false;
            divAnomalia2.Visible = false;
            txtTipoFiltro.Visible = false;
            filtroTipo.Visible = false;
        }
        protected void ckbTipoFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            AgregarAListboxFinal();
        }
        protected void ddlCiclo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ckbTotal.Checked)
            {
                CargarFechasLecturas(1);
            }
            else if (ckbSesgada.Checked)
            {
                CargarFechasLecturas(0);
            }
        }
        protected void ddlTipoFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {

            var ddl = (DropDownList)sender;
            int op = int.Parse(ddl.SelectedValue);

            ckbTipoFiltro.DataSource = null;
            ckbTipoFiltro.Items.Clear();
            txtTipoFiltro.Text = string.Empty;
            txtTipoFiltro.BackColor = Color.Empty;

            rbTipoCount.SelectedIndex = 0;
            rbTipoCount.Items[1].Enabled = false;

            if (op == 1)
            {
                CargarListaAnomalias();
                divTipoFiltro.Visible = true;
                divAnomalia2.Visible = true;
                hfFiltra.Value = "1";
            }
            else if (op == 2)
            {
                CargarListaInspector();
                divTipoFiltro.Visible = true;
                divAnomalia2.Visible = true;
                hfFiltra.Value = "2";
            }
            else if (op == 3)
            {
                divTipoFiltro.Visible = false;
                divAnomalia2.Visible = false;
                hfFiltra.Value = "3";
            }
            else if (op == 4)
            {
                CargarListaUsuarios();
                divTipoFiltro.Visible = true;
                divAnomalia2.Visible = true;
                hfFiltra.Value = "4";

                rbTipoCount.SelectedIndex= 0;
                rbTipoCount.Items[1].Enabled = true;
            }
            else
            {
                hfFiltra.Value = "0";
                ckbTipoFiltro.DataSource = null;
                ckbTipoFiltro.Items.Clear();
                txtTipoFiltro.Text = string.Empty;
                txtTipoFiltro.BackColor = Color.Empty;
            }

            chlTipoFiltro.Checked = false;
            foreach (ListItem li in ckbTipoFiltro.Items)
            {
                li.Selected = false;
            }

        }
        
        /***********************************************METDOS**********************************************/
        private void CargarReportes(string idFiltros, string fecha, string codSector)
        {
            int esTotal = ckbSesgada.Checked ? 0 : 1;

            var filtros = new FiltroMuestralectura
            {
                Sector = codSector,
                FechaLect = fecha,
                ConcatIds = idFiltros,
                EsTotal = esTotal,
                Porcentaje = int.Parse(txtPorcentaje.Text),
                TipoFiltro = int.Parse(ddlTipoFiltro.SelectedValue),
                Usr = _usrLog.user,
                TipoCant = rbTipoCount.SelectedValue.ToString()
            };

            var muestra = _cCtrl.GetMuestraCriticaLectura(filtros);
            gvRepoM.DataSource = null;
            gvRepoM.DataSource = muestra;
            gvRepoM.DataBind();

            lblFiltros.Text = "";//muestra.RespWs;

            if (muestra != null)
            {
                spnConteo.InnerText = muestra.Count.ToString();
                spnFecha.InnerText = DateTime.Parse(ddlFecha.SelectedValue.ToString()).ToShortDateString();
                spnTipo.InnerText = ckbSesgada.Checked ? "MUESTRA SESGADA" : "MUESTRA TOTAL";
                pnlAsignado.Visible = true;
                lblFiltros.Visible = false;
                lblBusqueda.Visible = false;
            }
            else
            {
                lblFiltros.Visible = true;
                pnlAsignado.Visible = false;
                lblBusqueda.Visible = true;
                lblBusqueda.Text = "Los filtros seleccionados no produjeron ningún resultado.";
            }
        }
        private RespMsj GuardarMuestra(List<MuestraLecturasMes> muestras)
        {
            var resp = _cCtrl.PutAsingarMuestras(muestras);
            return resp;
        }
        private bool ExisteMuestraFecha()
        {
            bool tipo = ckbTotal.Checked ? true : false;

            var existe = _cCtrl.GetCountMuestrasbyFecha(DateTime.Parse(ddlFecha.SelectedValue.ToString()), tipo,_usrLog.user);
            return existe;
        }
        private void CargarFechasLecturas(int esTotal)
        {
            var fechas = _cCtrl.GetReporteFecha(_usrLog.user, esTotal,ddlCiclo.SelectedValue);

            ddlFecha.DataSource = null;
            ddlFecha.DataSource = fechas;
            ddlFecha.DataTextField = "FECHA_LECTURA";
            ddlFecha.DataValueField = "FECHA_LECTURA";
            ddlFecha.DataBind();
            ddlFecha.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
        }
        private void CargarCiclos()
        {
            ddlCiclo.DataSource = null;
            ddlCiclo.DataSource = _cCtrl.GetPeriodosLectura();
            ddlCiclo.DataBind();
            ddlCiclo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlFecha.DataSource = null;
            ddlFecha.Items.Clear();
            ddlFecha.DataBind();
        }
        private void CargarListaAnomalias()
        {
            try
            {
                lblTipoFiltro.Text = "Anomalías";
                ckbTipoFiltro.Items.Clear();
                var lista = _cCtrl.GetAnomaliasLectura();

                foreach (var i in lista)
                {
                    ListItem item = new ListItem();
                    item.Text = i.nombre;
                    item.Value = i.id;
                    ckbTipoFiltro.Items.Add(item);
                }
                ckbTipoFiltro.DataBind();
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                lblMsjSave.Visible = true;
                lblMsjSave.Text = $"Ocurrió una excepción: {msj}";
            }
        }
        private void CargarListaInspector()
        {
            try
            {
                lblTipoFiltro.Text = "Inspectores";
                ckbTipoFiltro.Items.Clear();

                int? idSector = _regCtrl.GetSectoresByUsuario(_usrLog.user).FirstOrDefault().CodSector;
                var lista = _cCtrl.GetInspectoresLectura(idSector);

                foreach (var i in lista)
                {
                    ListItem item = new ListItem();
                    item.Text = i.nombre;
                    item.Value = i.id;
                    ckbTipoFiltro.Items.Add(item);
                }
                ckbTipoFiltro.DataBind();
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;

                lblMsjSave.Text = $"Ocurrió una excepción: {msj}";
            }
        }
        private void CargarListaUsuarios()
        {
            try
            {
                lblTipoFiltro.Text = "Usuarios Lecturas";
                ckbTipoFiltro.Items.Clear();

                int idPerfil = _UCtrl.GetPerfilRepoByUser(_usrLog.user).IdPerfilResoRepo;

                var lista = _UCtrl.GetUsuariosByIdPerfilResol(idPerfil);

                foreach (var i in lista)
                {
                    ListItem item = new ListItem
                    {
                        Text = i.NomPersona,
                        Value = i.idUsuario.ToString()
                    };
                    ckbTipoFiltro.Items.Add(item);
                }
                ckbTipoFiltro.DataBind();
            }
            catch (Exception ex)
            {
                string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;

                lblMsjSave.Text = $"Ocurrió una excepción: {msj}";
            }
        }
        private List<MuestraLecturasMes> GetListaUsuariosMuestras(List<MuestraLecturasMes> muestra)
        {
            int conteoPorUsuario = 0;
            List<int> listUsuarios = new List<int>();
            listUsuarios = ckbTipoFiltro.Items.Cast<ListItem>().Where(li => li.Selected).Select(y => Convert.ToInt32(y.Value)).ToList();

            conteoPorUsuario = muestra.Count() / listUsuarios.Count();

            List<MuestraLecturasMes> listaFinal = new List<MuestraLecturasMes>();

            foreach (var usr in listUsuarios)
            {
                var listSinUsrs = muestra
                    .FindAll(x => !listaFinal.Select(y => y.UsrResuelve).Contains(x.UsrResuelve))
                    .Take(conteoPorUsuario)
                    .ToList();

                foreach (var mues in listSinUsrs)
                {
                    mues.UsrResuelve = usr;

                    listaFinal.Add(mues);

                }
            }

            return listaFinal;
        }

        protected void chlTipoFiltro_CheckedChanged(object sender, EventArgs e)
        {
            var ckb = (CheckBox)sender;
            if (ckb.Checked)
            {
                foreach (ListItem li in ckbTipoFiltro.Items)
                {
                    li.Selected = true;
                }
            }
            else
            {
                foreach (ListItem li in ckbTipoFiltro.Items)
                {
                    li.Selected = false;
                }
            }

            AgregarAListboxFinal();
        }

        private void AgregarAListboxFinal()
        {
            int conteoCheck = 0;
            txtTipoFiltro.Text = string.Empty;
            string concat = string.Empty;
            foreach (ListItem item in ckbTipoFiltro.Items)
            {
                if (item.Selected)
                {
                    concat = concat + item.Value + " - " + item.Text + Environment.NewLine;
                    conteoCheck = conteoCheck + 1;
                }
            }
            txtTipoFiltro.Text = concat;

            int op = int.Parse(ddlTipoFiltro.SelectedValue);

            btnFiltrar.Visible = true;
            if (op == 1)
            {
                if (conteoCheck == 3)
                {
                    hfFiltra.Value = "1";
                    txtTipoFiltro.BackColor = Color.LightGreen;
                    btnFiltrar.Visible = true;
                }
                else
                {
                    hfFiltra.Value = "0";
                    txtTipoFiltro.BackColor = Color.LightCoral;
                    btnFiltrar.Visible = false;
                }
            }
            else if (op == 2)
            {
                txtTipoFiltro.BackColor = Color.LightGreen;
            }
            else if (op == 3)
            {
                txtTipoFiltro.BackColor = Color.LightGreen;
            }
            else if (op == 4)
            {
                txtTipoFiltro.BackColor = Color.LightGreen;
            }
            else
            {
                txtTipoFiltro.Text = string.Empty;
                txtTipoFiltro.BackColor = Color.Empty;
            }
        }

    }
}