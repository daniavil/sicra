﻿using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SICRA.Models.SicraBD;
//using Epplus;

namespace SICRA.Views
{
    public partial class HistoricoClavesReportadas : System.Web.UI.Page
    {
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private RepoAnomaliaController _rCtrl = new RepoAnomaliaController();
        private RegionesController _regCtrl = new RegionesController();
        private UsuarioController _uCtrl = new UsuarioController();

        protected void Page_Load(object sender, EventArgs e)
        {
            //_usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            //_usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                CargarDdl();
            }
        }

        private void CargarDdl()
        {
            ddlRegion.DataSource = null;
            ddlRegion.DataSource = _regCtrl.GetRegionesByUsuario("");
            ddlRegion.DataTextField = "Region";
            ddlRegion.DataValueField = "Region";
            ddlRegion.DataBind();
            ddlRegion.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlSector.DataSource = null;
            ddlSector.DataSource = _regCtrl.GetSectoresByUsuario("");
            ddlSector.DataTextField = "Sector";
            ddlSector.DataValueField = "Sector";
            ddlSector.DataBind();
            ddlSector.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlMedida.DataSource = null;
            ddlMedida.DataSource = _regCtrl.GetMedidaByUsuario("");
            ddlMedida.DataBind();
            ddlMedida.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlPerfilReso.DataSource = null;
            ddlPerfilReso.DataSource = _uCtrl.GetListaPerfilRepo();
            ddlPerfilReso.DataTextField = "NombrePerfil";
            ddlPerfilReso.DataValueField = "IdPerfilResoRepo";
            ddlPerfilReso.DataBind();
            ddlPerfilReso.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
            ddlPerfilReso.SelectedIndex = 0;

            ddlDial.DataSource = null;
            ddlDial.Items.Clear();
            for (int i = 1; i <= 23; i++)
            {
                ListItem li = new ListItem(i.ToString(), i.ToString());
                ddlDial.Items.Add(li);
            }
            ddlDial.DataBind();
            ddlDial.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlAnomalia.DataSource = null;
            ddlAnomalia.DataSource = _rCtrl.GetAnomalias();
            ddlAnomalia.DataTextField = "descripcion";
            ddlAnomalia.DataValueField = "idLecturaAnomalia";
            ddlAnomalia.DataBind();
            ddlAnomalia.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlCiclo.DataSource = null;
            ddlCiclo.DataSource = _rCtrl.GetCiclosAnomaliaHistorico();
            ddlCiclo.DataBind();
            ddlCiclo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
        }

        private void CargarReportes()
        {
            try
            {
                FiltroObj filtros = null;
                List<RepoAnomaliaMaestroDto> listaReportadas = null;

                if (txtClave.Text.Trim() == "" && ddlRegion.SelectedValue == ""
                    && ddlSector.SelectedValue == "" && ddlMedida.SelectedValue == "" 
                    && ddlEstado.SelectedValue == "" && ddlAnomalia.SelectedValue == ""
                    && ddlCiclo.SelectedValue == "" && ddlDial.SelectedValue == ""
                    && ddlPerfilReso.SelectedValue == "")
                {
                    filtros = null;
                    spnConteo.InnerText = " 0 - Debe de Aplicar Filtros.";
                    gvRepoM.DataSource = null;
                    gvRepoM.DataBind();
                }
                else
                {
                    filtros = new FiltroObj
                    {
                        Nisrad = txtClave.Text.Trim(),
                        Region = ddlRegion.SelectedValue,
                        Sector = ddlSector.SelectedValue,
                        Medida = ddlMedida.SelectedValue,
                        Resuelto = ddlEstado.SelectedValue,
                        idAnomalia = ddlAnomalia.SelectedValue,
                        CicloFact = ddlCiclo.SelectedValue,
                        Dial = ddlDial.SelectedValue,
                        idPerfilReso = ddlPerfilReso.SelectedValue
                    };

                    listaReportadas = _rCtrl.listaRepoMaestroAnomaliaHistorico(filtros);

                    gvRepoM.DataSource = null;
                    gvRepoM.DataSource = listaReportadas;
                    gvRepoM.DataBind();
                    spnConteo.InnerText = listaReportadas.Count().ToString();
                }
            }
            catch (Exception ex)
            {
                spnConteo.InnerText = " 0 - Debe de Aplicar Filtros.";
            }
        }

        protected void gvRepoM_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string IdM = gvRepoM.DataKeys[e.Row.RowIndex].Value.ToString();
                    GridView gvReDetails = e.Row.FindControl("gvRepoD") as GridView;
                    TableCell cellCiclo = e.Row.Cells[12];

                    var listaDetalle = _rCtrl.RepoDetailAnomaliaByIdMHistorico(long.Parse(IdM.Trim()),int.Parse(cellCiclo.Text));

                    gvReDetails.DataSource = listaDetalle;
                    gvReDetails.DataBind();

                    TableCell cellResuelto = e.Row.Cells[14];
                    TableCell cellResueltoString = e.Row.Cells[15];
                    if (cellResuelto.Text == "1")
                    {
                        cellResueltoString.BackColor = Color.LightGreen;
                    }
                    else if (cellResuelto.Text == "0")
                    {
                        cellResueltoString.BackColor = Color.LightCoral;
                    }
                    else if (cellResuelto.Text == "2")
                    {
                        cellResueltoString.BackColor = System.Drawing.ColorTranslator.FromHtml("#F4FA58");
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void gvRepoM_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CargarReportes();
            gvRepoM.PageIndex = e.NewPageIndex;
            gvRepoM.DataBind();
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            CargarReportes();
        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSector.SelectedIndex = 0;
        }

        protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlRegion.SelectedIndex = 0;
        }

        protected void gvRepoM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string[] NisCiclo = e.CommandArgument.ToString().Trim().Split(new char[] { ',' });

            if (NisCiclo.Length > 1)
            {
                var r = _rCtrl.GetRepoAnomaliaDTOHistorico(long.Parse(NisCiclo[0]), int.Parse(NisCiclo[1]));

                if (r != null)
                {
                    //maestro
                    spNisrad.InnerText = r.Nis_Rad.ToString();
                    spCiclo.InnerText = r.Ciclo.ToString();
                    txtUbicacion.Text = r.Ubicacion;
                    txtRegion.Text = r.Region;
                    txtSector.Text = r.Sector;
                    txtMedida.Text = r.Medida;
                    txtAnomalia.Text = r.Anomalia;
                    txtResuelto.Text = r.ResueltoStng;
                    txtLecActiva.Text = r.LecturaActiva.ToString();
                    txtLecReactiva.Text = r.LecturaReactiva.ToString();
                    txtLecDemanda.Text = r.LecturaDemanda.ToString();
                    txtCiclo.Text = r.Ciclo.ToString();
                    txtFechaRepo.Text = r.FechaRegistro.ToString();
                    txtFechaMaxResol.Text = r.FechaMaxResol.ToString();
                    txtCategoria.Text = r.categoriaResol;
                    txtRepo.Text = r.tipo;
                    txtDial.Text = r.dial.ToString();
                    txtCausa.Text = r.SolcitudCausaVerificacion;

                    //detalle
                    gvRepoD.DataSource = r.repoAnomaliaDetalleDto; //listaDetalle;
                    gvRepoD.DataBind();

                    /**********************************IMAGENES**********************************/
                    List<imagePreview> imageUrlList = new List<imagePreview>();
                    List<PrintConfigPreviewViewModel> imageList = new List<PrintConfigPreviewViewModel>();

                    foreach (var (item, index) in r.imagenReporteAnomalias.Select((v, i) => (v, i)))
                    {
                        try
                        {
                            byte[] img1 = File.ReadAllBytes(item.ruta);
                            imageList.Add(new PrintConfigPreviewViewModel() { Name = item.nombre, Picture = img1, clave = r.Nis_Rad.ToString(), fecha = item.fechaCarga.ToString("ddMMyyyy") });
                        }
                        catch (Exception)
                        {
                            imageUrlList = null;
                            imageList = null;

                        }
                    }

                    if (imageList != null && imageUrlList != null)
                    {
                        imageUrlList = GuardarCopiaImagenes(imageList);
                        literalControl.Text = GetImagenesMosaico(imageUrlList).Text;
                        lblmsjErrorImg.Visible = false;
                    }
                    else
                    {
                        lblmsjErrorImg.Visible = true;
                        lblmsjErrorImg.Text = "Atención: Ocurrió un error al intentar cargar imágenes.";
                    }
                    /********************************************************************/
                    mpver.Show();
                }
            }
        }

        public class imagePreview
        {
            public string Name { get; set; }
            public string url { get; set; }
        }

        public class PrintConfigPreviewViewModel
        {
            public string Name { get; set; }
            public byte[] Picture { get; set; }
            public string clave { get; set; }
            public string fecha { get; set; }
        }

        public LiteralControl GetImagenesMosaico(List<imagePreview> images)
        {
            LiteralControl html = new LiteralControl();

            string folderProject = Request.Url.AbsolutePath.ToString().Trim();

            //string content = @"<div id=""galley"" runat=""server"" ClientIDMode=""static""><ul class='pictures'>";
            string content = @"<ul class='pictures'>";

            try
            {
                int Dividendo = images.Count(), divisor = 6, q, r, totalFilas = 0;
                q = (int)(Dividendo / divisor);
                r = Dividendo % divisor;
                totalFilas = q + (r > 0 ? 1 : 0);

                string imageneshtml = "";

                foreach (var (item, index) in images.Select((v, i) => (v, i)))
                {
                    if (item.url != "")
                    {
                        string path = HttpContext.Current.Request.Url.Authority.ToString();
                        //imageneshtml += $@"<li><img data-original='{item.Name}' src ='{folderProject}/ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                        imageneshtml += $@"<li><img data-original='{item.Name}' src ='../ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                    }
                }
                content += imageneshtml;
                content += " </ul>";
            }
            catch (Exception)
            {
                content = "<H3>SIN IMÁGENES</H3>";
            }            
            html.Text = content;
            return html;
        }

        public List<imagePreview> GuardarCopiaImagenes(List<PrintConfigPreviewViewModel> imagenes)
        {
            try
            {
                List<imagePreview> ImgFiles = new List<imagePreview>();
                //foreach (var item in imagenes)
                foreach (var (item, index) in imagenes.Select((v, i) => (v, i)))
                {
                    string path = Request.PhysicalApplicationPath;
                    string nombreArchivo = $"{path}ImgCli\\img{index}_ope{item.clave}.Jpeg";

                    //string resolvedUrl = System.Web.HttpContext.Current.Request.Url.Authority.ToString();//VirtualPathUtility.ToAbsolute("~/");

                    ImgFiles.Add(new imagePreview() { url = $"img{index}_ope{item.clave}.Jpeg", Name = $"img{index}_ope{item.clave}_{item.fecha}" });

                    Bitmap objBitMap = new Bitmap(byteArrayToImage(item.Picture));
                    objBitMap.Save(nombreArchivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                return ImgFiles;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
                ms.Position = 0;
                System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
                return returnImage;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}