﻿using ClosedXML.Excel;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Epplus;

namespace SICRA.Views
{
    public partial class ResolverOperativa : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private RepoOperativaController _rCtrl = new RepoOperativaController();
        private RegionesController _regCtrl = new RegionesController();
        private UsuarioController _uCtrl = new UsuarioController();
        public readonly int idPerFact = int.Parse(ConfigurationManager.AppSettings["idPerfilFacturacion"]);
        /**********************************************Eventos*************************************************/
        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                CargarDdl();
            }
        }
        protected void gvRepoM_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string IdM = gvRepoM.DataKeys[e.Row.RowIndex].Value.ToString();
                GridView gvReDetails = e.Row.FindControl("gvRepoD") as GridView;

                var listaDetalle = _rCtrl.RepoDetaOpeByIdM(long.Parse(IdM.Trim()));
                gvReDetails.DataSource = listaDetalle;
                gvReDetails.DataBind();

                TableCell cellResuelto = e.Row.Cells[10];

                if (cellResuelto.Text == "1")
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].BackColor = Color.LightGreen;
                    }
                }
                else if (cellResuelto.Text == "0")
                {
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].BackColor = Color.LightCoral;
                    }
                }
            }
        }
        protected void gvRepoM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToUpper() == "RESOLVER")
            {
                LimpiarFormResolver();
                long id = long.Parse(e.CommandArgument.ToString());

                var r = _rCtrl.GetRepoMaeOpeDTOById(id);

                hfIdM.Value = r.IdRow_M.ToString();
                spNisrad.InnerText = r.Nis_Rad.ToString();
                spCiclo.InnerText = r.Ciclo.ToString();
                txtUbicacion.Text = r.Ubicacion;
                txtRegion.Text = r.Region;
                txtSector.Text = r.Sector;
                txtTarifa.Text = r.Tarifa;
                txtMulAnt.Text = r.MultiplicadorAnt.ToString();
                txtMulActual.Text = r.MultiplicadorAct.ToString();
                txtMedAnt.Text = r.MedidorAnt;
                txtMedAct.Text = r.MedidorAct;
                txtMedida.Text = r.Medida;
                txtAnomalia.Text = r.Anomalia;
                txtFcambioMedidor.Text = r.FechaCambioMedidor != null ? r.FechaCambioMedidor.Value.ToShortDateString() : "";
                txtLecActiva.Text = r.LecturaActiva.ToString();
                txtLecActivaRet.Text = r.LecturaActivaRetira.ToString();
                txtConsumoActivoFact.Text = r.ConsumoActivaFact.ToString();
                txtLecReactiva.Text = r.LecturaReactiva.ToString();
                txtLecReactivaRet.Text = r.LecturaReactivaRetira.ToString();
                txtConsumoReactivoFact.Text = r.ConsumoReactivaFact.ToString();
                txtLecDemanda.Text = r.LecturaDemanda.ToString();
                txtDial.Text = r.dial.ToString();
                txtCausa.Text = r.SolcitudCausaVerificacion;
                txtResolucion.Text = string.Empty;
                txtSolicitante.Text = r.Solicitante;
                txtPerfilResol.Text = r.PerfilUsuario;
                txtOs.Text = r.OS != null ? r.OS.ToString() : string.Empty;

                if (r.rep_Operativa_D != null)
                {
                    ddlEstadoRepo.SelectedValue = r.rep_Operativa_D.Estado;
                    txtResolucion.Text = r.rep_Operativa_D.Resolucion;
                    //txtResolucion.Text = r.rep_Operativa_D.FechaResolucion != null ? r.rep_Operativa_D.FechaResolucion.Value.ToShortDateString() : null;
                }

                mpResolver.Show();
            }
        }
        protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlRegion.SelectedIndex = 0;
        }
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSector.SelectedIndex = 0;
        }
        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            CargarReportes();
        }
        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                try
                {
                    var repoMae = _rCtrl.GetReporteOperativaByClaveCiclo(long.Parse(spNisrad.InnerText), int.Parse(spCiclo.InnerText));
                    if (ddlEstadoRepo.SelectedValue == "Corregir")
                    {
                        repoMae.Resuelto = false;
                    }
                    else
                    {
                        repoMae.Resuelto = true;
                    }

                    repoMae.Rep_Operativa_D = new List<Rep_Operativa_D>
                    {
                        new Rep_Operativa_D
                        {
                            IdRow_M = repoMae.IdRow_M,
                            Estado = ddlEstadoRepo.SelectedValue,
                            Resolucion = txtResolucion.Text,
                            FechaResolucion = DateTime.Now,
                            UsuarioResuelve = _usrLog.user
                        }
                    };

                    var SaveOk = _rCtrl.PutReporteOperativaMDI(repoMae);
                    CargarReportes();
                }
                catch (Exception ex)
                {
                    lblmsj.Visible = true;
                    lblmsj.Text = ex.Message;
                }
            }
        }

        /***********************************************METDOS**********************************************/
        private void CargarDdl()
        {
            ddlRegion.DataSource = null;
            ddlRegion.DataSource = _regCtrl.GetRegionesByUsuario(_usrLog.user);
            ddlRegion.DataTextField = "Region";
            ddlRegion.DataValueField = "Region";
            ddlRegion.DataBind();
            ddlRegion.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlSector.DataSource = null;
            ddlSector.DataSource = _regCtrl.GetSectoresByUsuario(_usrLog.user);
            ddlSector.DataTextField = "Sector";
            ddlSector.DataValueField = "Sector";
            ddlSector.DataBind();
            ddlSector.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlMedida.DataSource = null;
            ddlMedida.DataSource = _regCtrl.GetMedidaByUsuario(_usrLog.user);
            ddlMedida.DataBind();
            ddlMedida.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlDial.DataSource = null;
            ddlDial.Items.Clear();
            for (int i = 1; i <= 23; i++)
            {
                ListItem li = new ListItem(i.ToString(), i.ToString());
                ddlDial.Items.Add(li);
            }
            ddlDial.DataBind();
            ddlDial.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlAnomalia.DataSource = null;
            ddlAnomalia.DataSource = _rCtrl.GetAnomalias();
            ddlAnomalia.DataTextField = "descripcion";
            ddlAnomalia.DataValueField = "idLecturaAnomalia";
            ddlAnomalia.DataBind();
            ddlAnomalia.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlCiclo.DataSource = null;
            ddlCiclo.DataSource = _rCtrl.GetCiclosOperativaHistorico();
            ddlCiclo.DataBind();
            ddlCiclo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            //nuevos filtros ----------------------------------------------------------------------------------------------------------------------------------------
            ddlPerfilReso.DataSource = null;
            ddlPerfilReso.DataSource = _usrLog.usuSicra.AdminUser ? _uCtrl.GetListaPerfilRepo() : _uCtrl.GetListaPerfilRepoById(_usrLog.usuSicra.IdPerfilResoRepo);
            ddlPerfilReso.DataTextField = "NombrePerfil";
            ddlPerfilReso.DataValueField = "IdPerfilResoRepo";
            ddlPerfilReso.DataBind();
            if (_usrLog.usuSicra.AdminUser || _usrLog.usuSicra.IdPerfilResoRepo == idPerFact)
            {
                ddlPerfilReso.Items.Insert(0, new ListItem("--TODOS--", ""));
            }
            ddlPerfilReso.SelectedIndex = 0;

            ddlfechasRegistro.DataSource = null;
            ddlfechasRegistro.DataSource = _rCtrl.GetFechasRegistro("o");
            ddlfechasRegistro.DataBind();
            ddlfechasRegistro.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlFechavencimiento.DataSource = null;
            ddlFechavencimiento.DataSource = _rCtrl.GetFechasVencimiento("o");
            ddlFechavencimiento.DataBind();
            ddlFechavencimiento.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlUsuarioReporta.DataSource = null;
            ddlUsuarioReporta.DataSource = _rCtrl.GetUsuariosReporteOp();
            ddlUsuarioReporta.DataTextField = "NomPersona";
            ddlUsuarioReporta.DataValueField = "Usuario";
            ddlUsuarioReporta.DataBind();
            ddlUsuarioReporta.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlUsuarioResuelve.DataSource = null;
            ddlUsuarioResuelve.DataSource = _rCtrl.GetUsuariosRespuestaOp();
            ddlUsuarioResuelve.DataTextField = "NomPersona";
            ddlUsuarioResuelve.DataValueField = "Usuario";
            ddlUsuarioResuelve.DataBind();
            ddlUsuarioResuelve.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));

            ddlCicloRepo.DataSource = null;
            ddlCicloRepo.DataSource = _rCtrl.GetCiclosRegistro();
            ddlCicloRepo.DataBind();
            ddlCicloRepo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
        }
        private void CargarReportes()
        {
            try
            {
                FiltroObj filtros = null;

                if (txtClave.Text.Trim() == "" && ddlRegion.SelectedValue == ""
                    && ddlSector.SelectedValue == "" && ddlMedida.SelectedValue == ""
                    && ddlEstado.SelectedValue == "" && ddlAnomalia.SelectedValue == ""
                    && ddlDial.SelectedValue == "" && ddlPerfilReso.SelectedValue == ""
                    && ddlfechasRegistro.SelectedValue == "" && ddlFechavencimiento.SelectedValue == ""
                    && ddlUsuarioReporta.SelectedValue == "" && ddlUsuarioResuelve.SelectedValue == ""
                    && txtOsFiltro.Text.Trim() == "" && ddlTieneOS.SelectedValue == ""
                    && ddlVencido.SelectedValue == "" && ddlCiclo.SelectedValue == ""
                    && ddlCicloRepo.SelectedValue == "")
                {
                    filtros = null;
                }
                else
                {
                    filtros = new FiltroObj
                    {
                        Nisrad = txtClave.Text.Trim(),
                        Region = ddlRegion.SelectedValue,
                        Sector = ddlSector.SelectedValue,
                        Medida = ddlMedida.SelectedValue,
                        Resuelto = ddlEstado.SelectedValue,
                        idAnomalia = ddlAnomalia.SelectedValue,
                        Dial = ddlDial.SelectedValue,
                        idPerfilReso = ddlPerfilReso.SelectedValue,
                        FechaRegistro = ddlfechasRegistro.SelectedValue,
                        FechaMaxResol = ddlFechavencimiento.SelectedValue,
                        UsuarioFacturacion = ddlUsuarioReporta.SelectedValue,
                        UsuarioResuelve = ddlUsuarioResuelve.SelectedValue,
                        OS = txtOsFiltro.Text.Trim(),
                        TieneOS = ddlTieneOS.SelectedValue,
                        Vencido = Convert.ToInt32(ddlVencido.SelectedValue),
                        CicloFact = ddlCiclo.SelectedValue,
                        CicloRepo = ddlCicloRepo.SelectedValue
                    };
                }

                List<RepoAnomaliaMaestroDto> listaReportadas = _rCtrl.listaRepoMaestroOperativa(filtros);

                gvRepoM.DataSource = null;
                gvRepoM.DataSource = listaReportadas;
                gvRepoM.DataBind();

                spnNoResueltas.InnerText = listaReportadas.Where(r => r.Resuelto==0).Count().ToString();
                spnResueltas.InnerText = listaReportadas.Where(r => r.Resuelto==1).Count().ToString();
            }
            catch (Exception ex)
            {
            }
        }
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
        private void LimpiarFormResolver()
        {
            hfIdM.Value = string.Empty;
            spNisrad.InnerText = string.Empty;
            spCiclo.InnerText = string.Empty;
            txtUbicacion.Text = string.Empty;
            txtRegion.Text = string.Empty;
            txtSector.Text = string.Empty;
            txtTarifa.Text = string.Empty;
            txtMulAnt.Text = string.Empty;
            txtMulActual.Text = string.Empty;
            txtMedAnt.Text = string.Empty;
            txtMedAct.Text = string.Empty;
            txtMedida.Text = string.Empty;
            txtAnomalia.Text = string.Empty;
            txtFcambioMedidor.Text = string.Empty;
            txtLecActiva.Text = string.Empty;
            txtLecActivaRet.Text = string.Empty;
            txtConsumoActivoFact.Text = string.Empty;
            txtLecReactiva.Text = string.Empty;
            txtLecReactivaRet.Text = string.Empty;
            txtConsumoReactivoFact.Text = string.Empty;
            txtLecDemanda.Text = string.Empty;
            txtDial.Text = string.Empty;
            txtCausa.Text = string.Empty;
            txtResolucion.Text = string.Empty;
        }
    }
}