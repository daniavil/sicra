﻿using SICRA.Controllers;
using SICRA.Models;
using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SICRA.Models.SicraBD;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json;
using SICRA.Views.Reportes;
using DevExpress.XtraPrinting;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Drawing;
using Image = System.Drawing.Image;

namespace SICRA.Views
{
    public partial class CargaFirmaUsr : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private UsuarioController _uCtrl = new UsuarioController();

        public readonly string pathImgFirma = ConfigurationManager.AppSettings["pathImgFirma"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            lblUser.InnerText = _usrLog.usuSicra.Usuario;

            loadFirma();

        }

        private void loadFirma()
        {
            var firma = _uCtrl.GetFirmaUsuario(_usrLog.usuSicra.idUsuario);
            if (firma != null)
            {
                string path = Request.PhysicalApplicationPath;
                string nombreArchivo = $"{path}ImgCli\\{firma.nombreImg}";

                WebClient client = new WebClient();
                Stream stream = client.OpenRead(firma.firma);
                Bitmap objBitMap = new Bitmap(stream);
                objBitMap.Save(nombreArchivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                imgFirma.ImageUrl = $"~\\ImgCli\\{firma.nombreImg}";
            }
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
                ms.Position = 0;
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
            catch (Exception)
            {
                return null;
            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            GuardarFirma();
        }

        private bool GuardarFirma()
        {
            var ImgFirma = new FirmaUsuario();
            try
            {
                foreach (var (item, index) in fUpload.PostedFiles.Select((v, i) => (v, i)))
                {
                    string ext = Path.GetExtension(item.FileName).ToLower();

                    if (ext == ".png")
                    {
                        var guardaImg = GuardarImgFisico(item);

                        if (guardaImg!=null)
                        {
                            var resp = _uCtrl.PutFirmaUsuario(guardaImg);

                            if (resp.estado == "ok")
                            {
                                lblmsj.Text = resp.mensaje;
                                lblmsj.Attributes.Add("class", "label label-success");
                            }
                            else
                            {
                                lblmsj.Text = resp.mensaje;
                                lblmsj.Attributes.Add("class", "label label-danger");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblmsj.Text = ex.Message;
            }



            return true;
        }

        private FirmaUsuario GuardarImgFisico(HttpPostedFile file)
        {
            try
            {
                string _nombreArchivo = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);

                string folderLocation = $"{pathImgFirma}\\{_usrLog.user}\\";
                bool exists = System.IO.Directory.Exists(folderLocation);

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(folderLocation);
                }


                string filePath = $"{pathImgFirma}\\{_usrLog.user}\\{_nombreArchivo}";
                file.SaveAs(filePath);

                var imgFirma = new FirmaUsuario
                {
                    idUsuario = _usrLog.usuSicra.idUsuario,
                    nombreImg = _nombreArchivo,
                    firma = filePath,
                    FechaCarga = DateTime.Now
                };

                return imgFirma;
            }
            catch (Exception ex)
            {
                lblmsj.Text = "Error al crear ruta de Imagen.";
                lblmsj.Attributes.Add("class", "label label-danger");
                return null;
            }
        }

    }
}