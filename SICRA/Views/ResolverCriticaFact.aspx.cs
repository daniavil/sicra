﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
//using Epplus;

namespace SICRA.Views
{
    public partial class ResolverCriticaFact : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private CriticaFacturacionController _cCtrl = new CriticaFacturacionController();
        private CriticaLecturaController _clCtrl = new CriticaLecturaController();
        private UsuarioController _uCtrl = new UsuarioController();

        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (!IsPostBack)
            {
                CargarddlCiclos();
                //CargarMuestrasBd();
            }
        }
        private void CargarddlCiclos()
        {
            try
            {
                ddlCiclo.DataSource = null;
                ddlCiclo.DataSource = _cCtrl.GetCiclosByUser(_usrLog.user);
                ddlCiclo.DataBind();
                ddlCiclo.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));
            }
            catch (Exception)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }

        }
        private void CargarddlDiales(int ciclo)
        {
            try
            {
                ddldial.DataSource = null;
                ddldial.DataSource = _cCtrl.GetDialesByUser(_usrLog.user, ciclo);
                ddldial.DataBind();
                ddldial.Items.Insert(0, new ListItem("--SELECCIONAR--", "0"));
            }
            catch (Exception ex)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }

        }
        private void CargarMuestrasBd(int ciclo, DateTime dial)
        {
            var muestras = _cCtrl.GetAsignacionByUser(_usrLog.user, ciclo, dial);
            gvRepoM.DataSource = null;
            gvRepoM.DataSource = muestras;
            gvRepoM.DataBind();

            spnNoResueltas.InnerText = muestras.Count.ToString();
        }
        protected void gvRepoM_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    string IdM = gvRepoM.DataKeys[e.Row.RowIndex].Value.ToString();

            //    TableCell cellResuelto = e.Row.Cells[7];

            //    if (bool.Parse(cellResuelto.Text) == true)
            //    {
            //        for (int i = 0; i < e.Row.Cells.Count; i++)
            //        {
            //            e.Row.Cells[i].BackColor = Color.LightGreen;
            //        }
            //    }
            //    else
            //    {
            //        for (int i = 0; i < e.Row.Cells.Count; i++)
            //        {
            //            e.Row.Cells[i].BackColor = Color.LightCoral;
            //        }
            //    }
            //}
        }
        private void cargarDatos(string commandargument)
        {
            string idrow = commandargument;
            hfIdM.Value = string.Empty;
            var muestraBd = _cCtrl.GetAsignacionById(idrow);
            hfIdM.Value = muestraBd.idRow.ToString();
            int clave = muestraBd.nisrad;

            hfFilCiclo.Value = ddlCiclo.SelectedValue;
            hfFilDial.Value = ddldial.SelectedValue;

            ClienteRoot cliente = GetCliente(clave.ToString());
            SetClearValues();

            if (cliente != null)
            {
                literalControl.Text = cliente.ImagenesInnerText;
                lblNisrad.Text = cliente.cliente.nis_rad;
                lblMedidor.Text = cliente.cliente.num_medidor;
                lblUbicacion.Text = cliente.cliente.ubicacion;
                lblTarifa.Text = cliente.cliente.nom_tarifa;
                lblCliente.Text = cliente.cliente.nom_cli;
                lblDireccion.Text = cliente.cliente.direccion;

                int cicloIncms = int.Parse(muestraBd.FechaLectura.Value.Year + "" + muestraBd.FechaLectura.Value.Month.ToString().PadLeft(2, '0'));

                var info = _clCtrl.GetInfoMuestraWs(cliente.cliente.clave.ToString(), cicloIncms);
                
                CriticaFactInfo infoCritica = _cCtrl.GetCriticaFactInfo(cliente.cliente.nis_rad.ToString(), cicloIncms);

                if (infoCritica != null)
                {
                    UpdatePanel1.Visible = true;
                    btnAceptar.Visible = true;
                    lblMsjError.Visible = false;
                    lblMsjError.Text = string.Empty;
                    lblCodAnoCri.Text = infoCritica.CodAnomalia;
                    lblAnoCritica.Text = infoCritica.DescripcionAnomalia;
                    lblRepSicra.Text = infoCritica.repoSicra;
                    lblCsmoCri.Text = infoCritica.csmocri;
                    lblCsmoFact.Text = infoCritica.csmofact;
                    lblAccion.Text = infoCritica.accion;
                    lblUsrCri.Text = infoCritica.usrcri;
                    lblObsCri.Text = infoCritica.ObsCri;
                    var usrCri = _uCtrl.GetUsuarioByUser(infoCritica.usrcri);
                    lblNomUsr.Text = usrCri == null ? "Usuario No existe en SICRA." : usrCri.NomPersona;
                    chblP3Hacer.Visible = true;

                    ddlP3EnSicra.SelectedValue = infoCritica.repoSicra;
                    CambiarddlP3EnSicra();

                    if (_usrLog.usuSicra.AdminUser)
                    {
                        pnlUsrCri.Visible = true;
                    }
                    else
                    {
                        pnlUsrCri.Visible = false;
                    }

                    if (info.CriticaLecturaInfo != null)
                    {
                        lblLecturaEncontrada.Text = info.CriticaLecturaInfo.LECTURAENCONTRADA;
                        lblFLectura.Text = info.CriticaLecturaInfo.FECHALECTURA;
                        lblFProgramada.Text = info.CriticaLecturaInfo.FECHA_PROGRAMADA;
                        lblDial.Text = info.CriticaLecturaInfo.CODIGODIAL == null ? muestraBd.dial.ToString() : info.CriticaLecturaInfo.CODIGODIAL;
                        lblCodAnomaliaLect.Text = info.CriticaLecturaInfo.CODIGOANOMALIA;
                        lblAnomaliaLect.Text = info.CriticaLecturaInfo.DESCRIPCION;
                        lblTipoLect.Text = info.CriticaLecturaInfo.tipolect;

                        if (muestraBd.Resuelto)
                        {
                            btnAceptar.Visible = false;
                        }
                        else
                        {
                            btnAceptar.Visible = true;
                        }

                        lblMsjSave.Text = string.Empty;
                        //hfObj.Value = JsonConvert.SerializeObject(muestraBd);
                    }
                    ddlP2DescipcionObs.DataSource = null;
                    ddlP2DescipcionObs.DataSource = _cCtrl.GetCriticaObservaciones();
                    ddlP2DescipcionObs.DataTextField = "valor";
                    ddlP2DescipcionObs.DataValueField = "id";
                    ddlP2DescipcionObs.DataBind();
                    //ddlP2DescipcionObs.Items.Insert(0, new ListItem("--SELECCIONAR--", ""));


                    chblP3Hacer.Items.Clear();
                    chblP3Hacer.Items.Add("Solicitar el estado de medidor");
                    chblP3Hacer.Items.Add("Corroborar las lecturas del mes anterior");
                    chblP3Hacer.Items.Add("Corroborar las lecturas del mes actual");
                    chblP3Hacer.Items.Add("Solicitar fotos mas legibles");
                    chblP3Hacer.Items.Add("Solicitar serie del medidor");
                    chblP3Hacer.Items.Add("Solicitar Lectura de control");

                }
                else
                {
                    UpdatePanel1.Visible = false;
                    lblMsjError.Visible = true;
                    lblMsjError.Text = "Ocurrió un error al cargar datos, por favor vuelva a intentar.";
                    SetClearValues();
                    btnAceptar.Visible = false;
                }
            }
            ddlP4DebioRepoSicra.SelectedIndex = 0;
            PnSeguimientoFact.Visible = false;
            hfObj.Value = string.Empty;
            mpResolver.Show();
        }
        void SetClearValues()
        {
            pnlP1.Visible = false;
            pnlP2.Visible = false;
            pnlp31.Visible = false;
            pnlp32.Visible = false;
            literalControl.Text = null;
            lblNisrad.Text = "S/D";
            lblMedidor.Text = "S/D";
            lblUbicacion.Text = "S/D";
            lblTarifa.Text = "S/D";
            lblCliente.Text = "S/D";
            lblDireccion.Text = "S/D";
            lblCodAnoCri.Text = "S/D";
            lblAnoCritica.Text = "S/C";
            lblRepSicra.Text = "S/C";
            lblLecturaEncontrada.Text = "S/D";
            lblFLectura.Text = "S/D";
            lblFProgramada.Text = "S/D";
            lblDial.Text = "S/D";
            lblCodAnomaliaLect.Text = "S/D";
            lblAnomaliaLect.Text = "S/D";
            lblTipoLect.Text = "S/D";
        }
        protected void gvRepoM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToUpper() == "RESOLVER")
            {
                try
                {
                    cargarDatos(e.CommandArgument.ToString());
                }
                catch (Exception ex)
                {

                }
            }
        }
        private ClienteRoot GetCliente(string clave)
        {
            var clienteRoot = _clCtrl.CargarInfoCliente(clave);
            try
            {
                if (clienteRoot != null)
                {
                    List<imagePreview> imageUrlList = new List<imagePreview>();
                    List<PrintConfigPreviewViewModel> imageList = new List<PrintConfigPreviewViewModel>();

                    if (clienteRoot.cliFotos != null)
                    {
                        foreach (var (item, index) in clienteRoot.cliFotos.Select((v, i) => (v, i)))
                        {
                            try
                            {
                                if (item.origen == "Logistics")
                                {
                                    string path = Request.PhysicalApplicationPath;
                                    byte[] bytes = Convert.FromBase64String(item.imagen);

                                    MemoryStream ms = new MemoryStream(bytes);
                                    ms.Position = 0;
                                    imageList.Add(new PrintConfigPreviewViewModel() { Name = item.fecha, Picture = bytes, clave = item.clave });
                                }
                                else
                                {
                                    byte[] img1 = System.IO.File.ReadAllBytes(item.imagen);
                                    imageList.Add(new PrintConfigPreviewViewModel() { Name = item.fecha, Picture = img1, clave = item.clave });
                                }
                            }
                            catch (Exception)
                            {
                                imageUrlList = null;
                                imageList = null;

                            }
                        }
                        imageUrlList = GuardarCopiaImagenes(imageList, clave);
                        clienteRoot.ImagenesInnerText = GetImagenesMosaico(imageUrlList).Text;
                    }
                    else
                    {
                        imageUrlList = null;
                        clienteRoot.ImagenesInnerText = string.Empty;
                    }
                }
                else
                {
                    clienteRoot.cliFotos = null;
                }
                clienteRoot.ListaHistoricoLectura = null;
                clienteRoot.ListaHistoricoConsumo = null;
                return clienteRoot;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public class imagePreview
        {
            public string Name { get; set; }
            public string url { get; set; }
        }
        public class PrintConfigPreviewViewModel
        {
            public string Name { get; set; }
            public byte[] Picture { get; set; }
            public string clave { get; set; }
        }
        public LiteralControl GetImagenesMosaico(List<imagePreview> images)
        {
            LiteralControl html = new LiteralControl();

            string folderProject = Request.Url.AbsolutePath.ToString().Trim();

            string content = @"<div id=""galley"" runat=""server"" ClientIDMode=""static""><ul class='pictures'>";

            try
            {
                int Dividendo = images.Count(), divisor = 6, q, r, totalFilas = 0;
                q = (int)(Dividendo / divisor);
                r = Dividendo % divisor;
                totalFilas = q + (r > 0 ? 1 : 0);

                string imageneshtml = "";

                foreach (var (item, index) in images.Select((v, i) => (v, i)))
                {
                    if (item.url != "")
                    {
                        string path = HttpContext.Current.Request.Url.Authority.ToString();
                        //imageneshtml += $@"<li><img data-original='{item.Name}' src ='{folderProject}/ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                        imageneshtml += $@"<li><img data-original='{item.Name}' src ='../ImgCli/{item.url}' alt='Imagen:{item.Name}' title='{item.Name}'></ li >";
                    }
                }
                content += imageneshtml;
                content += " </ul></div>";
            }
            catch (Exception)
            {
                content = "<H3>SIN IMÁGENES</H3>";
            }
            
            html.Text = content;
            return html;
        }
        public List<imagePreview> GuardarCopiaImagenes(List<PrintConfigPreviewViewModel> imagenes, string clave)
        {
            try
            {
                List<imagePreview> ImgFiles = new List<imagePreview>();
                //foreach (var item in imagenes)
                foreach (var (item, index) in imagenes.Select((v, i) => (v, i)))
                {
                    string path = Request.PhysicalApplicationPath;
                    string nombreArchivo = $"{path}ImgCli\\img{index}_{item.Name}_C{item.clave}.Jpeg";

                    //string resolvedUrl = System.Web.HttpContext.Current.Request.Url.Authority.ToString();//VirtualPathUtility.ToAbsolute("~/");

                    ImgFiles.Add(new imagePreview() { url = $"img{index}_{item.Name}_C{item.clave}.Jpeg", Name = $"img{index}_{item.Name}_C{item.clave}" });

                    Bitmap objBitMap = new Bitmap(byteArrayToImage(item.Picture));
                    objBitMap.Save(nombreArchivo, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                return ImgFiles;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
                ms.Position = 0;
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
            catch (Exception)
            {
                return null;
            }
        }
        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                try
                {
                    var m = _cCtrl.GetAsignacionById(hfIdM.Value);
                    m.fechaResolucion = DateTime.Now;
                    m.Resuelto = true;
                    m.P1AccionCorrecta = ddlP1accionCorrecta.SelectedValue;
                    m.P1DebioDe = pnlP1.Visible ? ddlP1DebioDe.SelectedValue : null;
                    m.P2ObservacionCorrecta = ddlP2ObservacionCorrecta.SelectedValue;
                    m.P2DescipcionObs = pnlP2.Visible ? ddlP2DescipcionObs.SelectedItem.Text : null;
                    m.P3EnSicra = ddlP3EnSicra.SelectedValue;
                    m.P3HechoCorrecto = pnlp31.Visible ? ddlP3HechoCorrecto.SelectedValue : null;
                    m.P3Hacer = pnlp32.Visible ? string.Join(", ", chblP3Hacer.Items.Cast<ListItem>().Where(i => i.Selected)) : null;
                    m.P4DebioRepoSicra = ddlP4DebioRepoSicra.SelectedValue;
                    m.usuarioResIncms = lblUsrCri.Text.Trim();
                    _cCtrl.PutMuestraCritica(m);


                    if (ddlP4DebioRepoSicra.SelectedValue == "SISeg")
                    {
                        var RefaCritica = new FACTRefacturaDto
                        {
                            NIS_RAD = m.nisrad,
                            CICLO = m.Ciclo,
                            DIAL = m.dial,
                            CO_AN = m.CodAnomaliaCritica,
                            LECT_ACT_ACTIVA = m.lecturaActiva,
                            LECT_ACT_REACTIVA = m.lecturaReactiva,
                            LECT_ACT_DEMANDA = (int?)m.lecturaDemanda,
                            COD_ESTADO = 1,
                            MESES = Convert.ToInt32(ddlMesRefaCri.SelectedValue),
                            TXT_COMENTARIO_INICIAL = txtComentSeg.Text.Trim(),
                            COD_ORIGEN = 4,
                            COD_TIPO_CAMBIO = Convert.ToInt32(ddlTipoCambio.SelectedValue),
                            FEC_REGISTRO = DateTime.Now,
                            COD_USUARIO_REGISTRO = _usrLog.usuSicra.Usuario,
                            IdRowMuestraCritica = m.idRow
                        };


                        SoeehController soeehController = new SoeehController();
                        soeehController.PutFACTRefactura(RefaCritica);
                    }

                    

                }
                catch (Exception ex)
                {
                    string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                    lblMsjSave.Text = msj;
                }
                
                int fitroCiclo = int.Parse(hfFilCiclo.Value);
                DateTime fitroDial = DateTime.Parse(hfFilDial.Value);
                Fun.ClearControls(this);
                CargarMuestrasBd(fitroCiclo, fitroDial);

                hfFilCiclo.Value = fitroCiclo.ToString();
                hfFilDial.Value = fitroDial.ToString("yyyy-MM-dd");

                ddlCiclo.SelectedValue = hfFilCiclo.Value;
                ddldial.SelectedValue = hfFilDial.Value;

                pnlP1.Visible = false;
                pnlP2.Visible = false;
                pnlp31.Visible = false;
                pnlp32.Visible = true;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //Fun.ClearControls(this);
        }
        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarMuestrasBd(int.Parse(ddlCiclo.SelectedValue),DateTime.Parse(ddldial.SelectedValue));
            }
            catch (Exception)
            {
                gvRepoM.DataSource = null;
                gvRepoM.DataBind();
            }
            
        }
        protected void ddlCiclo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CargarddlDiales(int.Parse(ddlCiclo.SelectedValue));
                if (hfFilDial.Value != "")
                {
                    ddldial.SelectedValue = hfFilDial.Value;
                }
            }
            catch (Exception)
            {
                CargarddlCiclos();
                ddldial.Items.Clear();
                ddldial.DataSource = null;
                ddldial.DataBind();
                ddldial.SelectedIndex = -1;
            }
            
        }
        protected void ddlP1accionCorrecta_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlP1accionCorrecta.SelectedValue == "NO")
            {
                pnlP1.Visible = true;
            } 
            else
                pnlP1.Visible = false;


        }
        protected void ddlP2ObservacionCorrecta_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlP2ObservacionCorrecta.SelectedValue == "NO")
            {
                pnlP2.Visible = true;
            }
            else
            {
                ddlP2DescipcionObs.SelectedIndex = -1;
                ddlP2DescipcionObs.SelectedValue = null;
                pnlP2.Visible = false;
            }
                
        }
        protected void ddlP3EnSicra_SelectedIndexChanged(object sender, EventArgs e)
        {
            CambiarddlP3EnSicra();
        }
        void CambiarddlP3EnSicra()
        {
            if (ddlP3EnSicra.SelectedValue == "SI")
            {
                pnlp31.Visible = true;
                pnlp32.Visible = false;
                ddlP3HechoCorrecto.SelectedIndex = -1;
                ddlP3HechoCorrecto.SelectedValue = null;
                foreach (ListItem li in chblP3Hacer.Items)
                {
                    li.Selected = false;
                }
            }
            else if (ddlP3EnSicra.SelectedValue == "NO")
            {
                pnlp31.Visible = false;
                pnlp32.Visible = true;
                ddlP3HechoCorrecto.SelectedIndex = -1;
                ddlP3HechoCorrecto.SelectedValue = null;
                foreach (ListItem li in chblP3Hacer.Items)
                {
                    li.Selected = false;
                }
            }
            else
            {
                ddlP3HechoCorrecto.SelectedIndex = 1;
                ddlP3HechoCorrecto.SelectedValue = null;
                foreach (ListItem li in chblP3Hacer.Items)
                {
                    li.Selected = false;
                }
                pnlp31.Visible = false;
                pnlp32.Visible = false;
            }
        }

        protected void ddlP4DebioRepoSicra_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlP4DebioRepoSicra.SelectedValue == "SISeg")
            {
                PnSeguimientoFact.Visible = true;
            }
            else
            {
                PnSeguimientoFact.Visible = false;
            }
        }
    }
}