﻿using SICRA.Controllers;
using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SICRA.Models.SicraBD;
using System.IO;
using System.Diagnostics;
using System.Net;

namespace SICRA.Views
{
    public partial class CargarSoporteRefa : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        private SoportesController _sCtrl = new SoportesController();
        public readonly string pathSoporte = ConfigurationManager.AppSettings["pathsoporterefact"].ToString();

        private string _nombreArchivo = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarSopotesGrid();
            }
            catch (Exception)
            {
                gvbitacora.DataSource = null;
                gvbitacora.DataBind();

                pnlCarga.Visible = false;
                lblMsj.Visible = true;
                lblMsj.Text = "No hay rectficaciones para la NIS ingresada";
            }
        }

        private void CargarSopotesGrid()
        {
            var rectificaciones = _sCtrl.GetSoportesFacturacionBd(decimal.Parse(txtClave.Text.Trim()), _usrLog.usuSicra.Usuario);
            
            if (rectificaciones.Count > 0)
            {
                var bitacora = _sCtrl.GetSoporteSubidoByNisrad(decimal.Parse(txtClave.Text.Trim()));

                gvbitacora.DataSource = bitacora;
                gvbitacora.DataBind();

                spnconteo.InnerText = _sCtrl.GetConteoSoporteSubidoByNisrad(decimal.Parse(txtClave.Text.Trim())).ToString();

                pnlCarga.Visible = true;
                lblMsj.Visible = false;
                lblMsj.Text = string.Empty;
            }
            else
            {
                pnlCarga.Visible = false;
                lblMsj.Visible = true;
                lblMsj.Text = "No hay rectficaciones para la NIS ingresada";
            }
        }

        private bool SubirArchivo()
        {
            try
            {
                _nombreArchivo = Path.GetFileNameWithoutExtension(fUpload.FileName) + "_" +DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(fUpload.FileName); //Path.GetFileName(fUpload.FileName) ; //Path.GetFileNameWithoutExtension(fUpload.FileName) + ".csv";
                //string filePath = Server.MapPath("~/Files/") + nombre;
                //string path = Request.PhysicalApplicationPath;
                string filePath = $"{pathSoporte}\\{_nombreArchivo}";

                //string filePath = Server.MapPath("~/Files/") + "Files\\" + _nombreArchivo;
                hfRutaArchivo.Value = filePath;
                fUpload.SaveAs(filePath);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        private void GuardarBitacoraSoporte()
        {
            lblMsj.Visible = false;
            if (SubirArchivo())
            {
                //string nombre = Path.GetFileNameWithoutExtension(fUpload.FileName) + DateTime.Now.ToString("ddMMyyyHmmss") + Path.GetExtension(fUpload.FileName); //Path.GetFileName(fUpload.FileName) ; //Path.GetFileNameWithoutExtension(fUpload.FileName) + ".csv"; //Path.GetFileName(fUpload.FileName);
                //string filePath = Server.MapPath("~/Files/") + nombre;

                //string path = Request.PhysicalApplicationPath;
                //string filePath = $"{path}Files\\{_nombreArchivo}";
                string filePath = $"{pathSoporte}\\{_nombreArchivo}";

                //string filePath = Server.MapPath("~/Files/") + "Files\\" + _nombreArchivo;

                var bitasoporte = new BitacoraCargaSoporte
                {
                    nisrad = decimal.Parse(txtClave.Text.Trim()),
                    archivo = _nombreArchivo,
                    ruta = filePath,
                    fechaCarga = DateTime.Now,
                    usuario = _usrLog.user
                };

                lblMsj.Visible = true;
                lblMsj.Text = _sCtrl.PutBitacoraSoporte(bitasoporte).mensaje;

                spnconteo.InnerText = _sCtrl.GetConteoSoporteSubidoByNisrad(decimal.Parse(txtClave.Text.Trim())).ToString();
                CargarSopotesGrid();
            }
        }

        protected void btncargar_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);
            if (confirmValue == "Si")
            {
                GuardarBitacoraSoporte();
            }
        }

        protected void hlDownload_Command(object sender, CommandEventArgs e)
        {
            //openFile(e.CommandArgument.ToString());
            ReadPDFFile(e.CommandArgument.ToString(),e.CommandName.ToString());
        }

        public void openFile(string _uri)
        {
            try
            {
                lblerror.Visible = false;
                lblerror.Text = string.Empty;
                Process.Start(@_uri.ToString());
            }
            catch (Exception ex)
            {
                lblerror.Visible = true;
                lblerror.Text = $"Error: {ex.Message}";
            }
        }

        public void ReadPDFFile(string path,string archivo)
        {
            try
            {
                WebClient client = new WebClient();
                Byte[] buffer = client.DownloadData(path);

                if (buffer != null)
                {
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-length", buffer.Length.ToString());
                    Response.AddHeader("content-disposition", $"attachment;filename={archivo}");
                    Response.BinaryWrite(buffer);
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "window.open('application/pdf','_newtab');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Error Message", "alert('" + ex.Message.ToString() + "')", true);
            }
        }
    }
}