﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using SICRA.Controllers;
using SICRA.Dto;
using SICRA.Models;
using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
//using Epplus;

namespace SICRA.Views
{
    public partial class AutorizacionRectificacion : System.Web.UI.Page
    {
        private AdminLogin _usrLog = new AdminLogin();
        public readonly string app = ConfigurationManager.AppSettings["app"];
        public readonly string pathDocSopRect = ConfigurationManager.AppSettings["pathDocSopRect"].ToString();
        private RefacturaController _rCtrl = new RefacturaController();
        private UsuarioController _uCtrl = new UsuarioController();


        //-------------------------------------EVENTOS-------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            _usrLog = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            _usrLog.EvalSession(_usrLog);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);

            if (!IsPostBack)
            {
                CargarSolicitudesAutorizaciones();
            }
        }
        protected void gvRepoM_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToUpper() == "RESOLVER")
            {
                CargarDatos(e.CommandArgument.ToString());
            }
        }
        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            GuardarAutorizacion(false, false, (int?)_uCtrl.GetUsuarioByUser(_usrLog.user).PerfilAutorizacion.IdPerfilSuperior);
        }
        protected void btnDenegar_Click(object sender, EventArgs e)
        {
            GuardarAutorizacion(true, true);
        }
        protected void ddlPerfilRetorna_SelectedIndexChanged(object sender, EventArgs e)
        {
            GuardarAutorizacion(true, false,int.Parse(ddlPerfilRetorna.SelectedValue));
        }
        protected void hlDownload_Command(object sender, CommandEventArgs e)
        {
            ReadFile(e.CommandArgument.ToString(), e.CommandName.ToString());
        }
        protected void gvResoluciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int IdM = int.Parse(gvResoluciones.DataKeys[e.Row.RowIndex].Value.ToString());
                GridView gvDocs = e.Row.FindControl("gvDocs") as GridView;
                var listaDocs = _rCtrl.GetDocumentosByIdGestDet(IdM);
                gvDocs.DataSource = listaDocs;
                gvDocs.DataBind();

            }
        }
        //-------------------------------------MÉTODOS-------------------------------------
        private void CargarSolicitudesAutorizaciones()
        {
            var muestras = _rCtrl.GetSolicitudesRefaByPerfil(_usrLog.usuSicra);
            gvRepoM.DataSource = null;
            gvRepoM.DataSource = muestras;
            gvRepoM.DataBind();

            spnNoResueltas.InnerText = muestras != null ? muestras.Count.ToString() : "0";
        }
        private void CargarDatos( string commandargument)
        {
            try
            {
                int idGestion = int.Parse(commandargument);

                var g = _rCtrl.GetInfoSolicitudRefaByidGestion(idGestion,_usrLog.usuSicra.idPerfil);

                if (g != null)
                {
                    hfIdAsigGes.Value = g.IdAsigRefa.ToString();
                    hfIdGestionRefa.Value = g.IdGestionRefa.ToString();
                    lblNisrad.Text = g.Nis.ToString();
                    lblOrigen.Text = g.Origen;
                    lblCodGestion.Text = g.IdGestionOrigen;
                    lblTipoRect.Text = g.TipoRefa;
                    lblKwh.Text = g.kWhTotal;
                    lblMonto.Text = g.LpsTotal.ToString();
                    lblDbcr.Text = g.Dbcr;
                    lblCiclos.Text = g.Ciclos;
                    lblSector.Text = g.Sector;
                    lblMercado.Text = g.Mercado;
                    lblFechaSoli.Text = g.FechaHoraCreado.ToShortDateString();
                    lblSolicita.Text = g.NomSolicita;
                    txtCausa.Text = g.RazonSolicita;
                    lblEstado.Text = g.Estado;
                    lblEstado.Attributes.Add("class", g.Denegada ? "label label-danger" : "label label-success");
                    lblFinalizado.Text = g.FinalizadoString;
                    lblFinalizado.Attributes.Add("class", g.FinalizadoString == "NO" ? "label label-warning" : "label label-success");

                    gvResoluciones.DataSource = null;
                    gvResoluciones.DataSource = g.Resoluciones;
                    gvResoluciones.DataBind();

                    //Cargar Perfiles anteriores para retornar gestion
                    ddlPerfilRetorna.DataSource = null;
                    ddlPerfilRetorna.DataSource = g.PerfilesHijos;
                    ddlPerfilRetorna.DataTextField = "Nombre";
                    ddlPerfilRetorna.DataValueField = "idPerfil";
                    ddlPerfilRetorna.DataBind();
                    ddlPerfilRetorna.Items.Insert(0, new ListItem("Perfil a Retornar", ""));
                }
                lblErrorDatos.Visible = false;
            }
            catch (Exception ex)
            {
                lblErrorDatos.Visible = true;
                lblErrorDatos.Text = $"Se produjo una excepción: {ex.Message}";
            }
            mpResolver.Show();
        }
        /// <summary>
        /// Parametros: Denegar, Finalizar, Perfil Asignado
        /// </summary>
        /// <param name="GestionDenegada"></param>
        /// <param name="GestionFinalizada"></param>
        /// <param name="IdPerfilAsignado"></param>
        private void GuardarAutorizacion(bool GestionDenegada, bool GestionFinalizada, int? IdPerfilAsignado = null)
        {
            string confirmValue = Request.Form["confirm_value"].Substring(Request.Form["confirm_value"].ToString().Length - 2);

            if (confirmValue == "Si")
            {
                try
                {
                    var resp = new RespMsj();
                    /* Crear objeto Documentos_Soporte *************************************************/
                    var DocsSopRefac = new List<DocGestionSoporteRefactura>();
                    try
                    {
                        foreach (var (item, index) in fUpload.PostedFiles.Select((v, i) => (v, i)))
                        {
                            if (item.ContentLength > 0)
                            {
                                string ext = Path.GetExtension(item.FileName).ToLower();
                                var doc = GuardarDocsSoporte(item, $"DocRef_{index + 1}_{lblNisrad.Text.Trim()}");
                                if (doc != null)
                                {
                                    DocsSopRefac.Add(doc);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        DocsSopRefac = null;
                    }

                    if (!DocsSopRefac.Any())
                    {
                        DocsSopRefac = null;
                    }

                    /* Obtener y modificar Gestion Maestra *************************************************/
                    var GestionMaestra = _rCtrl.GetGestionByIdGestion(int.Parse(hfIdGestionRefa.Value));
                    GestionMaestra.IdPerfilActualAsignado = IdPerfilAsignado;
                    //GestionMaestra.IdPerfilCrea = _usrLog.usuSicra.idPerfil;
                    GestionMaestra.Denegada = GestionDenegada;
                    GestionMaestra.Finalizada = GestionFinalizada;
                    GestionMaestra.FechaFinalizada = IdPerfilAsignado == null ? DateTime.Now : (DateTime?)null;
                    
                    /* attach Gestion Detalle *************************************************/
                    GestionMaestra.GestionRefacturaDet = new List<GestionRefacturaDet>
                    {
                        new GestionRefacturaDet
                        {
                            IdGesRefMae=GestionMaestra.IdGesRefMae,
                            idPerfilAsignado = (int?)IdPerfilAsignado,
                            IdUsuarioAutoriza = _usrLog.usuSicra.idUsuario,
                            idPerfilAutoriza = _usrLog.usuSicra.idPerfil,
                            ComentarioResuelve = !string.IsNullOrWhiteSpace(txtResolucion.Text.Trim())
                                                ? txtResolucion.Text.Trim()
                                                : $"Seguimiento Solicitud Refactura, realizado por {_usrLog.usuSicra.NomPersona}",
                            FechaCreacion = DateTime.Now,
                            FechaHoraResuelto = DateTime.Now,
                            Denego = GestionDenegada,
                            /* attach Documentos Soporte *************************************************/
                            DocGestionSoporteRefactura = DocsSopRefac!=null? new List<DocGestionSoporteRefactura>(DocsSopRefac):null
                        },
                    };

                    _rCtrl.PutGestionRefactura_Mae_Det_DocGes(GestionMaestra);

                    lblError.Visible = true;
                    lblError.Text = resp.mensaje;
                    lblError.Attributes.Add("class", "label label-success");
                }
                catch (Exception ex)
                {
                    string msj = ex.InnerException == null ? ex.Message : ex.InnerException.GetBaseException().Message;
                    lblError.Visible = true;
                    lblError.Text = msj;
                    lblError.Attributes.Add("class", "label label-danger");
                }
                CargarSolicitudesAutorizaciones();
            }
        }
        public void ReadFile(string path, string archivo)
        {
            try
            {
                WebClient client = new WebClient();
                Byte[] buffer = client.DownloadData(path);

                if (buffer != null)
                {
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-length", buffer.Length.ToString());
                    Response.AddHeader("content-disposition", $"attachment;filename={archivo}");
                    Response.BinaryWrite(buffer);
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Message", "window.open('application/octet-stream','_newtab');", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Error Message", "alert('" + ex.Message.ToString() + "')", true);
            }

        }
        private DocGestionSoporteRefactura GuardarDocsSoporte(HttpPostedFile file, string indexClave)
        {
            try
            {
                string _nombreArchivo = "";
                _nombreArchivo = indexClave + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(file.FileName);

                string filePath = $"{pathDocSopRect}\\{_nombreArchivo}";
                file.SaveAs(filePath);

                var DocSoporte = new DocGestionSoporteRefactura
                {
                    NomDoc = _nombreArchivo,
                    Ruta = filePath,
                    FechaHora = DateTime.Now
                };

                return DocSoporte;
            }
            catch (Exception ex)
            {
                lblError.Text = ex.ToString();

                return null;
            }
        }
    }
}