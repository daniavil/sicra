﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportarClaveAnomalia.aspx.cs" Inherits="SICRA.Views.ReportarClaveAnomalia" %>
<%@ Register Src="~/Modals/MsjModal.ascx" TagPrefix="uc1" TagName="MsjModal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="UrlSeguimientoContent" runat="server">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <a href="#"><span>SICRA</span></a> <%--agregar al menu que pertenece--%>
    <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
    <a class="UrlActual" href='<%= ResolveUrl("ReportarClaveAnomalia.aspx") %>'>Reportar NIS - Anomalía</a>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updPanReportar">
        <ContentTemplate>
            <asp:UpdateProgress runat="server" ID="progress" DynamicLayout="true" AssociatedUpdatePanelID="updPanReportar">
                <ProgressTemplate>
                    <div style="text-align: center" class="loading">
                        <img src='<%= ResolveUrl("~/Imagenes/brickLoading.gif") %>' />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Panel runat="server" ID="panelPrincipal">
                <div class="col-sm-12">
                    <h2 class="titulo">Reportar NIS - Anomalía.</h2>
                    <div class="form-group">
                        <asp:Panel runat="server" ID="panelContent">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Información NIS</div>
                                <div class="panel-body">
                                    <%--***************************************************************************************************************************************************************************************************--%>
                                    <div class="form-inline">
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <asp:HiddenField runat="server" ID="hfClave390" />
                                            <label for="txtClave">NIS:</label>
                                            <asp:TextBox ID="txtClave" runat="server" CssClass="form-control" onkeypress="EnterEvent(event);return ValidateNumeros(event)"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-3">
                                            <label for="ddlTipoRepo">Tipo Reporte:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlTipoRepo" OnSelectedIndexChanged="ddlTipoRepo_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddlTipoRepo"  ErrorMessage="Campo Requerido" ForeColor="Red" InitialValue="" runat="server" />
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:Label ID="lblRepe" runat="server" Style="font-size: medium"></asp:Label>
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <br />
                                        </div>
                                    </div>

                                    <%-------------------------------------------------------------------------------------------------------------%>
                                    <input id="hfIdM" type="hidden" value="0" runat="server" />
                                    <div class="form-horizontal">
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="txtClave">Ciclo Reporte:</label>
                                            <asp:TextBox ID="txtCicloFact" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="txtOs">OS Referencia:</label>
                                            <asp:TextBox ID="txtOs" runat="server" CssClass="form-control" ReadOnly="true" ></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hfMsjOs" />
                                        </div>

                                        <asp:Panel class="form-group col-sm-2 col-md-2 col-lg-2" runat="server" id="pnlGenOS">
                                            <label>¿ Desea Crear OS ?</label>
                                            <label class="switch switch-slide form-control">
                                                <input class="switch-input" type="checkbox" id="ChkGenOs" runat="server"/>
                                                <span class="switch-label" data-on="SI" data-off="NO"></span>
                                                <span class="switch-handle"></span>
                                            </label>
                                        </asp:Panel>

                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="txtFechaRepo">Fecha Reporte:</label>
                                            <asp:TextBox ID="txtFechaRepo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtFechaRepo" ForeColor="Red"  ErrorMessage="Campo Requerido" InitialValue="" runat="server" />
                                        </div>
                                        <div class="form-group col-sm-2 col-md-2 col-lg-2">
                                            <label for="txtFechaResol">Fecha Maxíma:</label>
                                            <asp:TextBox ID="txtFechaResol" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtFechaResol" ForeColor="Red"  ErrorMessage="Campo Requerido"  InitialValue="" runat="server" />
                                        </div>
                                        <%-------------------------------------------------------------------------------------------------------------%>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="txtUbicacion">Ubicación:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtUbicacion" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="txtRegion">Region:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtRegion" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="txtSector">Sector:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtSector" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <%-------------------------------------------------------------------------------------------------------------%>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="txtMultiplicador">Multiplicador:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtMultiplicador" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="txtMedida">Medida:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtMedida" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                            <label for="ddlAnomalia">Anomalía:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlAnomalia"></asp:DropDownList>
                                        </div>
                                        <%-------------------------------------------------------------------------------------------------------------%>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="txtLecActiva">Lectura Activa:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtLecActiva" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="txtLecReactiva">Lectura Reativa:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtLecReactiva" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-sm-4 col-md-4 col-lg-4">
                                            <label for="txtLecDemanda">Lectura Demanda:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtLecDemanda" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <%-------------------------------------------------------------------------------------------------------------%>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlDial">Dial Lectura:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlDial" OnSelectedIndexChanged="ddlDial_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            <%--<asp:DropDownList runat="server" class="form-control" ID="ddlDial"></asp:DropDownList>--%>
                                            <asp:RequiredFieldValidator ID="rfddlDial" ControlToValidate="ddlDial" InitialValue="" ForeColor="Red"   ErrorMessage="Campo Requerido"  runat="server" />
                                        </div>
                                        <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                            <label for="ddlCategoria">Categorización:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlCategoria">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="ddlCategoria"  ErrorMessage="Campo Requerido" ForeColor="Red" InitialValue="" runat="server" />
                                        </div>
                                        <div class="form-group col-sm-3 col-md-3 col-lg-3">
                                            <label for="ddlPerfilReso">Perfil de Resolución:</label>
                                            <asp:DropDownList runat="server" class="form-control" ID="ddlPerfilReso">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="ddlPerfilReso"  ErrorMessage="Campo Requerido" ForeColor="Red" InitialValue="" runat="server" />
                                        </div>
                                        <%-------------------------------------------------------------------------------------------------------------%>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <label for="txtCausa">Solicitud O Causa de Verificación:</label>
                                            <asp:TextBox runat="server" class="form-control" ID="txtCausa" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                        </div>
                                        <br />
                                        <br />
                                        <br />
                                        <%-------------------------------------------------------------------------------------------------------------%>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:Button ID="btnReportar" runat="server" Text="Reportar NIS" CssClass="btn btn-info" OnClick="btnReportar_Click" OnClientClick="Confirm()" />
                                        </div>
                                        <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                            <asp:TextBox ID="lblMsj" runat="server" ReadOnly="true" Visible="False" Style="font-size: medium" Width="100%" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                        </div>
                                        <%-------------------------------------------------------------------------------------------------------------%>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDetalle">
                <div class="col-sm-12">
                    <div class="panel panel-collapse panel-success">
                    <div class="panel-heading">Resoluciones</div>
                    <div class="panel-body">
                        <asp:Panel ID="pnlRegistros" runat="server" Style="width:100%">
                            <asp:GridView ID="gvRepoD" runat="server" CssClass="Grid" AutoGenerateColumns="false" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="IdRow_D" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" HeaderText="IdRow_D" ReadOnly="True" Visible="true" />
                                    <asp:BoundField DataField="UsuarioResuelve" HeaderText="Usuario Reportó / Resolvió" ReadOnly="True" />
                                    <asp:BoundField DataField="LecturaActiva" HeaderText="Lectura Activa" ReadOnly="True" />
                                    <asp:BoundField DataField="LecturaReactiva" HeaderText="Lectura Reactiva" ReadOnly="True" />
                                    <asp:BoundField DataField="LecturaDemanda" HeaderText="Lectura Demanda" ReadOnly="True" />
                                    <asp:BoundField DataField="Resolucion" HeaderText="Reporte / Resolución" ReadOnly="True" />
                                    <asp:BoundField DataField="FechaResolucion" HeaderText="Fecha Reporte / Resolución" ReadOnly="True" />
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </div>
                </div>
            </asp:Panel>
            <uc1:MsjModal runat="server" ID="MsjModal"></uc1:MsjModal>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnReportar" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderStyle" runat="server">
    <style>

        .file{
            margin-left:25%;
            margin-top:4%;
            margin-bottom:4%;
        }

        .titulo{
            margin-left: 35%;
            margin-bottom: 4%;
        }
        .total{
            margin-left:60%;
            margin-bottom: 1.5%;
            margin-top: 1.5%;
        }

        .btnCargar{
            margin-left: 65%;
            margin-top: 2%;
            margin-bottom: 2%;
        }

        .loading{
            position:fixed;
            top:0px;
            right:0px;
            width:100%;
            height:100%;
            background-color:#ffffff;
            background-repeat:no-repeat;
            background-position:center;
            z-index:10000000;
            opacity: 0.80;
            filter: alpha(opacity=0); 
        }

    </style>
    <style>
        .switch {
            position: relative;
            width: auto;
            margin: 0;
            background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
            background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
            border-radius: 18px;
            box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
            cursor: pointer;
            box-sizing: content-box;
            padding:0 !important;
        }

        .switch-input {
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
            box-sizing: content-box;
        }

        .switch-label {
            position: relative;
            display: block;
            height: inherit;
            font-size: 14px;
            text-transform: uppercase;
            background: #FF7F50;
            border-radius: inherit;
            box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
            box-sizing: content-box;
            text-align:center;
        }

            .switch-label:before, .switch-label:after {
                position: absolute;
                top: 50%;
                margin-top: -.5em;
                line-height: 1;
                -webkit-transition: inherit;
                -moz-transition: inherit;
                -o-transition: inherit;
                transition: inherit;
                box-sizing: content-box;
            }

            .switch-label:before {
                content: attr(data-off);
                right: 11px;
                color: #ffffff;
                text-shadow: 0 1px rgba(255, 255, 255, 0.5);
            }

            .switch-label:after {
                content: attr(data-on);
                left: 11px;
                color: #FFFFFF;
                text-shadow: 0 1px rgba(0, 0, 0, 0.2);
                opacity: 0;
            }

        .switch-input:checked ~ .switch-label {
            background: #E1B42B;
            box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
        }

            .switch-input:checked ~ .switch-label:before {
                opacity: 0;
            }

            .switch-input:checked ~ .switch-label:after {
                opacity: 1;
            }

        .switch-handle {
            position: absolute;
            top: 4px;
            left: 4px;
            width: 28px;
            height: 28px;
            background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
            background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
            border-radius: 100%;
            box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
        }

            .switch-handle:before {
                content: "";
                position: absolute;
                top: 50%;
                left: 50%;
                margin: -6px 0 0 -6px;
                width: 12px;
                height: 12px;
                background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
                background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
                border-radius: 6px;
                box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
            }

        .switch-input:checked ~ .switch-handle {
            left: 74px;
            box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
        }

        /* Switch Slide
==========================*/
        .switch-slide {
            padding: 0;
            background: #FFF;
            border-radius: 0;
            background-image: none;
        }

            .switch-slide .switch-label {
                box-shadow: none;
                background: none;
                overflow: hidden;
            }

                .switch-slide .switch-label:after, .switch-slide .switch-label:before {
                    width: 100%;
                    height: 100%;
                    top: 0px;
                    left: 0;
                    text-align: center;
                    padding-top: 7%;
                    box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.2), inset 0 0 3px rgba(0, 0, 0, 0.1);
                }

                .switch-slide .switch-label:after {
                    color: #FFFFFF;
                    background: #32CD32;
                    left: -100px;
                }

                .switch-slide .switch-label:before {
                    background: #FF7F50;
                }

            .switch-slide .switch-handle {
                display: none;
            }

            .switch-slide .switch-input:checked ~ .switch-label {
                background: #FFF;
                border-color: #0088cc;
            }

                .switch-slide .switch-input:checked ~ .switch-label:before {
                    left: 100px;
                }

                .switch-slide .switch-input:checked ~ .switch-label:after {
                    left: 0;
                }

        /* Transition
============================================================ */
        .switch-label, .switch-handle {
            transition: All 0.3s ease;
            -webkit-transition: All 0.3s ease;
            -moz-transition: All 0.3s ease;
            -o-transition: All 0.3s ease;
        }
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolderJS" runat="server">
    <script src="../Scripts/GridCells.js"></script>
    <script>
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("¿Está seguro de realizar esta acción?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }

        function EnterEvent(e) {
            if (e.keyCode == 13) {
                __doPostBack('<%=txtClave.ClientID%>', "");
            }        
        }
    </script>
</asp:Content>