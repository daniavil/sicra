﻿function renderLocal() {

    $(document).on("click", "[class^=imgPlusRow]", function () {
        $(this).attr("class", "imgMinusRow");
        $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
        $(this).attr("src", "../Imagenes/minus.png");
    });
    $(document).on("click", "[class^=imgMinusRow]", function () {
        $(this).attr("class", "imgPlusRow");
        $(this).attr("src", "../Imagenes/plus.png");
        $(this).closest("tr").next().remove();
    });

    var ciclos = $("#hfCiclos").val();

    var ddl = $(".js-example-basic-multiple").select2({
        placeholder: "Seleccionar Ciclo",
        allowClear: true,
        multiple: "multiple",
        closeOnSelect: false
    });

    ddl.val("");
    ddl.trigger("change");
    ddl.on('change', function (e) {
        var cMax = Math.max.apply(null, ddl.val());
        var cMin = Math.min.apply(null, ddl.val());
        $("#lblCicloIni").html(cMin);
        $("#lblCicloFin").html(cMax);
        $("#hfCicloIni").val(cMin);
        $("#hfCicloFin").val(cMax);

        $("#hfCiclos").val(ddl.val());

    });

    CalcularDifDias();
}

function CalcularDifDias() {
    try {
        var fini = $("#txtFechaIniCdr").val();
        var ffin = $("#txtFechaFinCdr").val();

        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        var firstDate = new Date(fini);
        var secondDate = new Date(ffin);
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / oneDay));
        $("#lblCantDiasCdr").html(diffDays);

        $("#hfCantDiasCdr").val(diffDays);
        

    } catch (e) {
        $("#lblCantDiasCdr").html('0');
        $("#hfCantDiasCdr").val("");
    }
}

function ReprocesoCorregido() {
    $('#third_tab').trigger('click');
}