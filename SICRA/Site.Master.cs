﻿using SICRA.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SICRA
{
    public partial class SiteMaster : MasterPage
    {
        public readonly string app = ConfigurationManager.AppSettings["app"];
        public readonly string keyEncrypt = ConfigurationManager.AppSettings["PassPhrase"];
        public readonly string entornoSys = ConfigurationManager.AppSettings["entorno"];
        UsuarioController _uCtrl = new UsuarioController();

        AdminLogin admin = new AdminLogin();
        protected void Page_Load(object sender, EventArgs e)
        {
            string paginaDefault = FormsAuthentication.DefaultUrl;
            spEntorno.InnerText = entornoSys;
            try
            {
                admin = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);

                string data = admin.GetLogin().Replace("\"", "");
                data = (new EEHCryptoClass.eehCrypto()).EEHDecrypt(data, keyEncrypt);
                ((Label)HeadLoginView.FindControl("LoginName")).Text = admin.user;


                if (admin.UserAutenticated(data) == "OK")
                {
                    //*******verificar si es activo en sicra******
                    if (_uCtrl.GetActivoSicra(admin.user))
                    {
                        string url = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath != "/" ? (Request.Url.Host + Request.Url.LocalPath).Replace(Request.Url.Host + System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath, "") : Request.Url.LocalPath;

                        /******************NO BORRAR*****************/
                        if (!FormsAuthentication.DefaultUrl.Contains(url))
                        {
                            if (!admin.IsEnabledWebform(url))
                            {
                                Response.Redirect(FormsAuthentication.DefaultUrl, false);
                                return;
                            }
                        }

                        menu.InnerHtml = admin.GetMenuAcceso("Inicio").Text;
                        /*******************NO BORRAR***********************/
                    }
                    //*********************************************
                }
            }
            catch (Exception ex)
            {

            }
        }       

        protected void LoginStatus_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            AdminLogin login = new AdminLogin(((FormsIdentity)Context.User.Identity).Ticket.UserData, app);
            login.EliminarLogin();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }
    }
}