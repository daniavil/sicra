﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace SICRA
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            DevExpress.XtraReports.Web.WebDocumentViewer.Native.WebDocumentViewerBootstrapper.SessionState = System.Web.SessionState.SessionStateBehavior.Disabled;
            // Código que se ejecuta al iniciar la aplicación
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
            DevExpress.XtraReports.Web.ASPxWebDocumentViewer.StaticInitialize();
            DevExpress.XtraReports.Web.ASPxReportDesigner.StaticInitialize();
            DevExpress.XtraReports.Web.Extensions.ReportStorageWebExtension.RegisterExtensionGlobal(new ReportStorageWebExtension1());
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            string keyEncrypt = ConfigurationManager.AppSettings["PassPhrase"];

            bool cookieFound = false;
            HttpCookie authCookie = null;
            Data ticket;
            HttpCookie cookie;
            for (int i = 0; i < Request.Cookies.Count; i++)
            {
                cookie = Request.Cookies[i];
                if (cookie.Name == FormsAuthentication.FormsCookieName)
                {
                    cookieFound = true;
                    authCookie = cookie;
                    break;
                }
            }

            if (authCookie != null)
            {
                HttpCookie authcookie = FormsAuthentication.GetAuthCookie(FormsAuthentication.FormsCookieName, true);
                authcookie.Value = authCookie.Value;

                ticket = JsonConvert.DeserializeObject<Data>(authcookie.Value);


                if (ticket.UserData != "")
                {
                    FormsAuthenticationTicket newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration, ticket.IsPersistent, ticket.UserData);

                    if (ValidoUsuario(ticket.UserData))
                    {
                        string cookiestr = JsonConvert.SerializeObject(newTicket).ToString();//(new EEHCryptoClass.eehCrypto()).EEHEncrypt(JsonConvert.SerializeObject(tkt).ToString(), keyEncrypt);

                        authcookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookiestr);
                        Response.SetCookie(authcookie);
                        if (Context.User == null) CrearUserContext(newTicket);
                    }
                    else
                        FormsAuthentication.SignOut();
                }
                else
                {
                    // No cookie found, we can redirect to the Windows auth site if we want, or let it pass through so
                    // that the forms auth system redirects to the logon page for us.
                }
            }
        }             

        private void CrearUserContext(FormsAuthenticationTicket tkt)
        {
            string[] roles = tkt.UserData.Split(';');
            FormsIdentity formsId = new FormsIdentity(tkt);
            System.Security.Principal.GenericPrincipal princ = new System.Security.Principal.GenericPrincipal(formsId, roles);
            HttpContext.Current.User = princ;
        }

        private bool ValidoUsuario(string usuario)
        {
            string keyEncrypt = ConfigurationManager.AppSettings["PassPhrase"];
            string app = ConfigurationManager.AppSettings["app"];


            string usuarioDecrypt = (new EEHCryptoClass.eehCrypto()).EEHDecrypt(usuario, keyEncrypt);
            AdminLogin admin = new AdminLogin(usuario, app);
            string data = admin.GetLogin().Replace("\"", "");

            if (data == "") return false;
            //UsuarioDto info = admin.GetInfoLogin();

            string contrasena = (new EEHCryptoClass.eehCrypto()).EEHDecrypt(data, keyEncrypt);
            string resultado = admin.UserAutenticated(contrasena);

            if (resultado == "OK" && admin.usuSicra!=null)
            {   return true;
            }
            else
                return false;
        }

        public class Data
        {
            public int Version { get; set; }
            public string Name { get; set; }
            public DateTime IssueDate { get; set; }
            public DateTime Expiration { get; set; }
            public bool IsPersistent { get; set; }
            public string UserData { get; set; }
        }

    }
       
    }