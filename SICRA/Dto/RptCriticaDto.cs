﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Dto
{
    public class Filtro
    {
        public int Ciclo { get; set; }
        public string FechasLectura { get; set; }
        public int TipoRepo { get; set; }
    }

    public class FiltroRefa
    {
        public int Ciclo { get; set; }
        public string Dial { get; set; }
        public int TipoRepo { get; set; }
    }

    public class SerieGeneral
    {
        public int id { get; set; }
        public string Concepto { get; set; }
        public int Valor { get; set; }
    }
    public class SerieDetalle
    {
        public string NomPersona { get; set; }
        public int Asignados { get; set; }
        public int PromCalidadMuestra { get; set; }
        public int CantAccionCorrecta { get; set; }
        public int CantObservCorrecta { get; set; }
        public int CantSicraSiDebioRepoSi { get; set; }
        public int TotalMuestra { get; set; }
        public string FechaLect { get; set; }
    }
    public class SerieDetalleRefa
    {
        public string NomPersona { get; set; }
        public int Asignados { get; set; }
        public int PromCalidadMuestra { get; set; }
        public int CantRefacturaCorrecta { get; set; }
        public int CantDocCorrecta { get; set; }
        public int CantObsCierreCorrecto { get; set; }
        public int CantProcesoAutorizacionCorrecto { get; set; }
        public int CantDatosResoCorrecto { get; set; }
        public int TotalMuestra { get; set; }
        public string Dial { get; set; }
    }

    public class SerieControl
    {
        public string NomPersona { get; set; }
        public int Asignados { get; set; }
        public int CantResueltos { get; set; }
        public int CantNoResueltos { get; set; }
    }

    /**************************************************************************************************/
    public class DatosRptDetalle
    {
        public Filtro Filtros { get; set; }
        public List<SerieDetalle> SeriesDatos { get; set; }
        public FiltroRefa FiltrosRefa { get; set; }
        public List<SerieDetalleRefa> SeriesDatosRefa { get; set; }
    }

    public class DatosRptGeneral
    {
        public Filtro Filtros { get; set; }
        public List<SerieGeneral> SeriesDatos { get; set; }
        public List<SerieGeneral> SeriesDatosRefa { get; set; }
        public FiltroRefa FiltrosRefa { get; set; }
    }

    public class DatosRptControl
    {
        public Filtro Filtros { get; set; }
        public List<SerieControl> SeriesDatos { get; set; }
        public List<SerieControl> SeriesDatosRefa { get; set; }
        public FiltroRefa FiltrosRefa { get; set; }
    }
}