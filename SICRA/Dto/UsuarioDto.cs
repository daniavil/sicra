﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SICRA.Dto
{
    public class UsuarioDto
    {
        [StringLength(100)]
        public string UserName { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public int SectorId { get; set; }
        public int PuestoId { get; set; }
        public string Puesto { get; set; }
        public string identidad { get; set; }
        public string email { get; set; }
        public string telefono { get; set; }
        public int id { get; set; }
    }

    public class SectoresUsuarioDto
    {
        public int IdSector { get; set; }
        public string Sector { get; set; }
        public bool Asig { get; set; }
    }
    public class MercadosUsuarioDto
    {
        public int IdMercado { get; set; }
        public string Nombre { get; set; }
        public bool Asig { get; set; }
    }
}