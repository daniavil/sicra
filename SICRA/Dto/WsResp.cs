﻿using SICRA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Dto
{
    public class WsResp
    {
        public string id { get; set; }
        public string cadena { get; set; }
    }

    public class HistoAnomaliaInfo
    {
        public int CLAVE { get; set; }
        public string LECTURAACTIVA { get; set; }
        public string CONSUMOACTIVA { get; set; }
        public string CONSUMOREACTIVA { get; set; }
        public string ANOMALIA { get; set; }
        public string OBSERVACION { get; set; }
        public int CICLO { get; set; }
    }

    public class Resp_CriticaLecturaInfo
    {
        public string Msj { get; set; }
        public CriticaLecturaInfo CriticaLecturaInfo { get; set; }
        public List<HistoAnomaliaInfo> histoAnomaliasInfo { get; set; }
    }
}