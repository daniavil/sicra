﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Dto
{
    public class CriticaRefacturaDtoInfo
    {
        public int nisrad { get; set; } //(int, null)
        public int CICLO { get; set; } //(int, null)
        public int dial { get; set; } //(int, null)
        public string codUsuario { get; set; } //(varchar(50), null)
        public string LectActiva { get; set; } //(varchar(12), null)
        public int? LectReactiva { get; set; } //(int, null)
        public int? LectDemanda { get; set; } //(int, null)
        public int? CodObs { get; set; } //(int, null)
        public string Observacion { get; set; } //(varchar(200), null)
        public int? CodOrigen { get; set; } //(int, null)
        public string Origen { get; set; } //(varchar(100), null)
        public string codUsuarioRef { get; set; } //(varchar(50), null)
    }

}