﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Dto
{
    public class RepoAutorizaDto
    {
        public DateTime FechaCreacion { get; set; } //(datetime, not null)
        public DateTime FechaFinalizada { get; set; } //(datetime, null)
        public int IdGesRefMae { get; set; } //(int, not null)
        public string codGestion { get; set; } //(nvarchar(max), not null)
        public string Usuario { get; set; } //(nvarchar(15), not null)
        public string NomPersona { get; set; } //(nvarchar(200), null)
        public string tipopqr { get; set; } //(varchar(11), not null)
        public int IdArea { get; set; } //(int, not null)
        public string NombreArea { get; set; } //(nvarchar(50), not null)
        public string ComentarioSolicitud { get; set; } //(nvarchar(max), null)
        public int kWhRef { get; set; } //(int, not null)
        public decimal LpsRef { get; set; } //(decimal(18,2), not null)
        public int kWhCv { get; set; } //(int, not null)
        public decimal LpsCv { get; set; } //(decimal(18,2), not null)
        public int kWhTotal { get; set; } //(int, not null)
        public decimal LpsTotal { get; set; } //(decimal(18,2), not null)
    }

}