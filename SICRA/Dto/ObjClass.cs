﻿namespace SICRA.Dto
{
    public class ObjClass
    {

        public string Texto1 { get; set; }
        public string Texto2 { get; set; }
        public string Texto3 { get; set; }
        public string Texto4 { get; set; }
    }

    public class FiltroObj
    {
        public string Region { get; set; }
        public string Sector { get; set; }
        public string Nisrad { get; set; }
        public string Medida { get; set; }
        public string CicloFact { get; set; }
        public string Dial { get; set; }
        public string Resuelto { get; set; }
        public string idAnomalia { get; set; }
        public string idPerfilReso { get; set; }
        public string FechaMaxResol { get; set; }
        public string FechaRegistro { get; set; }
        public string UsuarioFacturacion { get; set; }
        public string UsuarioResuelve { get; set; }
        public string OS { get; set; }
        public string TieneOS { get; set; }
        /// <summary>
        /// valores: 0: sin filtro, 1:vencido, 2: no vencido
        /// </summary>
        public int Vencido { get; set; }
        public string CicloRepo { get; set; }
    }

    public class OS_Obj
    {
        public bool CreaOs { get; set; }
        public int Clave { get; set; }
        public string Comentario { get; set; }
        public string UsuarioCrea { get; set; }
        public int? IdOS { get; set; }
        public string Resultadotext { get; set; }
        public bool Correcto { get; set; }
    }

    public class FiltroMuestralectura
    {
        public string Sector { get; set; }
        public string FechaLect { get; set; }
        public string ConcatIds { get; set; }
        public int EsTotal { get; set; }
        public int Porcentaje { get; set; }
        /// <summary>
        /// 1-> ANOMALÍA
        /// 2-> INSPECTOR
        /// 3-> CAMBIO PARAMETRIZACIÓN
        /// </summary>
        public int TipoFiltro { get; set; }
        public string TipoCant { get; set; }
        public string Usr { get; set; }
    }
}