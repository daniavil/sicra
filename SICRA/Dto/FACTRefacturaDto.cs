﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Dto
{
    public class FACTRefacturaDto
    {
        public int? NIS_RAD { get; set; } //(int, null)
        public int? CICLO { get; set; } //(int, null)
        public int? DIAL { get; set; } //(int, null)
        public string CO_AN { get; set; } //(varchar(5), null)
        public int? LECT_ACT_ACTIVA { get; set; } //(int, null)
        public int? LECT_ACT_REACTIVA { get; set; } //(int, null)
        public int? LECT_ACT_DEMANDA { get; set; } //(int, null)
        public int? COD_ESTADO { get; set; } //(int, null)
        public int? MESES { get; set; } //(int, null)
        public string TXT_COMENTARIO_INICIAL { get; set; } //(varchar(1500), null)
        public int? COD_ORIGEN { get; set; } //(int, null)
        public int? COD_TIPO_CAMBIO { get; set; } //(int, null)
        public DateTime? FEC_REGISTRO { get; set; } //(datetime, null)
        public string COD_USUARIO_REGISTRO { get; set; } //(varchar(50), null)
        public Guid? IdRowMuestraCritica { get; set; }
    }

}