﻿using SICRA.Models.SicraBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Dto
{
    public class RefacturaGrid
    {
        public int IdGesRefDet { get; set; }
        public int IdGesRefMae { get; set; }
        public decimal nisrad { get; set; }
        public string ComentarioSolicitud { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string NomPersona { get; set; }
        public string Estado { get; set; }
        public string Finalizado { get; set; }
        public string CicloRegistro { get; set; }
    }

    public class RefacturaInfo
    {
        public int IdAsigRefa { get; set; }
        public int IdGestionRefa { get; set; }
        public decimal Nis { get; set; }
        public string Origen { get; set; }
        public string IdGestionOrigen { get; set; }
        public string TipoRefa { get; set; }
        public string kWhCv { get; set; }
        public string LpsCv { get; set; }
        public string kWhRef { get; set; }
        public string LpsRef { get; set; }
        public string kWhTotal { get; set; }
        public string LpsTotal { get; set; }
        public string Dbcr { get; set; }
        public string Ciclos { get; set; }
        public string Sector { get; set; }
        public string Mercado { get; set; }
        public string NomSolicita { get; set; }
        public string RazonSolicita { get; set; }
        public DateTime FechaHoraCreado { get; set; }
        public List<Resolucion> Resoluciones { get; set; }
        public bool Finalizado { get; set; }
        public bool Denegada { get; set; }
        public string Estado { get; set; }
        public string FinalizadoString { get; set; }
        public string CicloRegistro { get; set; }
        public List<TimeLineAutorizacion> TimeLineCO { get; set; }
        public List<TimeLineAutorizacion> TimeLineCE { get; set; }
        public List<DocumentosAutorizacion> DocsAuto { get; set; }
        public List<PerfilAutorizacion> PerfilesHijos { get; set; }
    }

    public class TimeLineAutorizacion
    {
        public int IdPerfil { get; set; }
        public string Perfil { get; set; }
        public string NomPersona { get; set; }
        public string Fecha { get; set; }
        public int Denego { get; set; }
    }
    public class DocumentosAutorizacion
    {
        public int IdGesRefDet { get; set; }
        public int IdDoc { get; set; }
        public string NomPersona { get; set; }
        public string NomDoc { get; set; }
        public string Ruta { get; set; }
    }

    public class Resolucion
    {
        public int IdGestDet { get; set; }
        public string ComentarioResol { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaHoraResuelto { get; set; }
        public string NomResolvio { get; set; }
        public string Perfil { get; set; }
        public string Estado { get; set; }
    }

    //*******************************************************************************************

    public class DatosRefactura
    {
        public string nisrad { get; set; }
        public string nombretarifa { get; set; }
        public string codtarifa { get; set; }
        public string codtarifaofi { get; set; }
        public string secrec { get; set; }
        public string secnis { get; set; }
        public DateTime ffactura { get; set; }
        public DateTime frefactura { get; set; }
        public string totalimporte { get; set; }
        public string consumoactivafact { get; set; }
        public string consumoreactivafact { get; set; }
        public DateTime ffactanterior { get; set; }
        public string periodofacturado { get; set; }
        public string periodorefacturado { get; set; }
        public string JsonImp { get; set; }
        public string ffacturaint { get; set; }
        
    }

    public class PeriodosMulti
    {
        public decimal multiplicador { get; set; }
        public List<int> periodo { get; set; }
        public string RespCallBack { get; set; }
    }
    public class FiltrosLectPeriodo
    {
        public int Nisrad { get; set; }
        public int Ciclo { get; set; }
    }
    public class LecturaPeriodo
    {
        public int? Csmo { get; set; }
        public int? Lect { get; set; }
        /// <summary>
        /// CO011 activa
        /// CO015 reactiva
        /// </summary>
        public string Tip_csmo { get; set; }
        public int Cte { get; set; }
        public string Tip_Lect { get; set; }
        public string Nom_Tip_Lect { get; set; }
        public string LectTipo { get; set; }
    }
    public class FiltrosImportes
    {
        public int Nisrad { get; set; }
        public int Ciclo { get; set; }
        public int FechaFact { get; set; }
    }
    public class FiltroCalculoAlumbrado
    {
        public int Nisrad { get; set; }
        public int CicloAnt { get; set; }
        public int PeriodoAnt { get; set; }
        public int CsmoAnt { get; set; }
    }
    public class DtoCalculoCorregido
    {
        public int Nisrad { get; set; }
        public int Periodo { get; set; }
        public int CicloAnt { get; set; }
        public int PeriodoAnt { get; set; }
        public decimal ValorCobrar { get; set; }
        public int kwh { get; set; }
        public int Csmo { get; set; }
        public string FechaFact { get; set; }
        public string codtarifa { get; set; }
        public string codconcepto { get; set; }
        public string DescripcionImporte { get; set; }
        public int IsNew { get; set; }
    }
    public class FiltroCalculoConceptos
    {
        public int Nisrad { get; set; }
        public int CicloAnt { get; set; }
        //public int PeriodoAnt { get; set; }
        public int CsmoAnt { get; set; }
        public int PeriodoAct { get; set; }
        public int CsmoAct { get; set; }
        //public DateTime FechaFact { get; set; }
        public int FechaFactInt { get; set; }
        public string CodTarifa { get; set; }
    }
}