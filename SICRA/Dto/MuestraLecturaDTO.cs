﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Dto
{
    public class MuestraLecturaDTO
    {
        public int idRow { get; set; }
        public int CLAVE { get; set; }
        public bool EsTotal { get; set; }

        public int? CODIGODIAL { get; set; }
        public int PERIODO { get; set; }

        public int? SECTOR_ID { get; set; }

        public int? LECTURAENCONTRADA { get; set; }
        public DateTime FECHALECTURA { get; set; }
        public string FECHA_PROGRAMADA { get; set; }
        public string CODIGOANOMALIA { get; set; }
        public string SUBCLASE { get; set; }
        public string DESCRIPCION { get; set; }

        public double? CODIGOADMINISTRATIVO { get; set; }

        public double? CODIGOGRUPOTRABAJO { get; set; }
        public string CambioParametrizacion { get; set; }
        public string TipoFiltro { get; set; }

        public int? lecturaCorrecta { get; set; }

        public int? LecturaDebioSer { get; set; }

        public bool fotoCorrecta { get; set; }

        public bool anomaliaCorrecta { get; set; }
        public string usuario { get; set; }

        public DateTime? fechaRegistro { get; set; }

        public DateTime? fechaResolucion { get; set; }

        public bool Resuelto { get; set; }

        public int? IdObFoto { get; set; }

        public int? UsrResuelve { get; set; }
        public string UsrResuelveString { get; set; }

        public int? EsTelemedido { get; set; }
    }
}