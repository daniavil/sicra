﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Dto
{
    public class ObjetoGenerico
    {
        public string Dato1 { get; set; }
        public string Dato2 { get; set; }
        public string Dato3 { get; set; }
        public string Dato4 { get; set; }
        public string Dato5 { get; set; }
        public string Dato6 { get; set; }
        public string Dato7 { get; set; }
        public string Dato8 { get; set; }
        public string Dato9 { get; set; }
        public string Dato10 { get; set; }
    }
}