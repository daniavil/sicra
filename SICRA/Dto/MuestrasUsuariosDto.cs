﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SICRA.Dto
{
    public class MuestrasInfo
    {
        public int CantTotalGen { get; set; }
        public int CantResueltas { get; set; }
        public int CantNoResueltas { get; set; }
        public List<ListaMuestrasUsuarios> ListaMuestras { get; set; }
    }
    public class ListaMuestrasUsuarios
    {
        public string usuario { get; set; }
        public string NomUsr { get; set; }
        public int CantResueltas { get; set; }
        public int CantNoResueltas { get; set; }
    }
}