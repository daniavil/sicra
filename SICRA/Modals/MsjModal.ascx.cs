﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SICRA.Modals
{
    public partial class MsjModal : System.Web.UI.UserControl
    {

        public string MsjBody { get; set; }
        public UpdatePanel UptModal { get; set; }
        public string Estado { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void MostrarModal()
        {
            if (Estado == "ok")
            {
                hPanel.Style.Add("background-color", "#088A29");
                lblTitle.Style.Add("color", "#ffffff;");
                lblTitle.Text = "Completado!";
            }
            else if (Estado == "error")
            {
                hPanel.Style.Add("background-color", "#FE2E2E");
                lblTitle.Style.Add("color", "#ffffff;");
                lblTitle.Text = "Error!";
            }
            else if (Estado == "warning")
            {
                hPanel.Style.Add("background-color", "#ff9100");
                lblTitle.Style.Add("color", "#ffffff;");
                lblTitle.Text = "Advertencia!";
            }
            else
            {
                hPanel.Style.Add("background-color", "#0174DF");
                //lblModalTitle.Style.Add("color", "#000000;");
                lblTitle.Style.Add("color", "#ffffff;");
            }

            lblTitle.Text = "Informacion!";
            lblMensaje.Text = MsjBody;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(), "$('#MsjModal').modal();", true);
            upModal.Update();
        }
    }
}