﻿using EEHCryptoClass;
using SICRA.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web.UI;
using System.Xml.Serialization;
using Crypto;
using SICRA.Controllers;
using SICRA.Models.SicraBD;
using System.Web.Security;

namespace SICRA
{
    public class AdminLogin
    {
        public readonly string SP_NAME = "SP_authentication";
        public readonly string strWS = ConfigurationManager.AppSettings["wsuser"];
        public readonly string keyEncrypt = ConfigurationManager.AppSettings["PassPhrase"];
        public readonly string tokenKey = ConfigurationManager.AppSettings["TokenSys"];
        private UsuarioController _uCtrl = new UsuarioController();
        public string appid { get; set; }
        public bool habilitado { get; set; }
        public string user { get; set; }
        public string accesos { get; set; }
        public Usuarios usuSicra { get; set; }


        private ACCESOS objAccesos;
        [XmlRoot("ACCESOS")]

        public class ACCESOS
        {
            [XmlElement("ID")]
            public List<string> ID { get; set; }
        }

        public AdminLogin(string userCrypt = "", string app = "")
        {
            string data = (userCrypt != "" ? (new eehCrypto()).EEHDecrypt(userCrypt, keyEncrypt) : "");
            var usr = _uCtrl.GetUsuarioByUser(data);
            if (usr != null)
            {
                this.appid = app;
                this.user = data;
                this.habilitado = false;
                this.objAccesos = new ACCESOS();
                this.usuSicra = usr;
            }
        }

        public List<ObjClass> ValidateUser(string password, string user)
        {
            List<SqlParameter> paramsIn = new List<SqlParameter>();
            List<SqlParameter> paramsOut = new List<SqlParameter>();

            paramsIn.Add(new SqlParameter("@aplicacion", appid));
            paramsIn.Add(new SqlParameter("@usuario", user));
            paramsIn.Add(new SqlParameter("@password", password));
            paramsIn.Add(new SqlParameter("@debug", 0));

            paramsOut.Add(new SqlParameter("@retorno", System.Data.SqlDbType.Int));
            paramsOut.Add(new SqlParameter("@mensaje", System.Data.SqlDbType.NVarChar, 200));
            paramsOut.Add(new SqlParameter("@accesos", System.Data.SqlDbType.VarChar, int.MaxValue));
            paramsOut.Add(new SqlParameter("@opciones", System.Data.SqlDbType.VarChar, int.MaxValue));


            List<ObjClass> Resultado = Fun.ExecPaQueryResult(SP_NAME, paramsIn, paramsOut, 30, "SegConn");

            return Resultado;
        }

        public void GetListAccesos(string xml)
        {
            string path = string.Empty;
            string xmlInputData = string.Empty;
            string xmlOutputData = string.Empty;

            xmlInputData = xml;

            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ACCESOS));

            using (StringReader sr = new StringReader(xml))
            {
                objAccesos = (ACCESOS)ser.Deserialize(sr);
            }

        }

        public LiteralControl GetMenuAcceso(string Titulo)
        {
            LiteralControl Menu = new LiteralControl();
            DataSet ds = MenuController.GetAllWebformsByAccesos(objAccesos.ID, this.appid);

            string strdivMenu = @"     <h2 class=icon icon-world>" + Titulo + "</h2>" +
                                 "         <ul id =menus >";
            Menu.Text += strdivMenu;

            int i = 0;
            string stropcionMenuEnd = @"";
            string stropcionMenu = @"";
            string stropcion = @"";
            string strendDivMenu = @"            </ul>";

            foreach (DataRow row in ds.Tables[0].Rows)
            {

                if (row[0].ToString() == "0" && row[3].ToString() == "#") //row[3].ToString() == "#"
                {
                    if (i > 0) { stropcionMenuEnd = @"</ul></div></li>"; Menu.Text += stropcionMenuEnd; }
                    stropcionMenu = @"<li  class='icon icon-arrow-left'>" +
                                         "           <a class='icon icon-display' href='#'>" + row[2].ToString() + "</a>" +
                                         "           <div class='mp-level'>" +
                                         "               <h2 class='icon icon-display'>" + row[2].ToString() + "</h2>" +
                                         "               <a class='mp-back' href='#'>Atrás</a>" +
                                         "               <ul>";

                    Menu.Text += stropcionMenu;
                    i += 1;
                }
                else
                {
                    string urlres = (new Control()).ResolveUrl(row[3].ToString());

                    if (row[0].ToString() == "0")
                    {
                        if (i > 0) { stropcionMenuEnd = @"</ul></div></li>"; Menu.Text += stropcionMenuEnd; }
                        stropcion = @"<li class='icon icon-arrow-left'>" +
                                   "<a class='icon icon-wallet' href='" + urlres + "'>" + row[2].ToString() + "</a>" +
                                   "</li>";
                        i = 0;
                    }
                    else
                        stropcion = @" <li><a href='" + urlres + "' >" + row[2].ToString() + "</a></li>";

                    Menu.Text += stropcion;
                }


            }

            if (i > 0) { stropcionMenuEnd = @"</ul></div></li>"; Menu.Text += stropcionMenuEnd; }

            Menu.Text += strendDivMenu;

            return Menu;
        }

        public string UserAutenticated(string contrasena, string usuario = "")
        {
            string userValidate = this.user;
            try
            {
                if (usuario != "") userValidate = usuario;
                List<ObjClass> Resultado = ValidateUser(contrasena, userValidate);
                if (Resultado[1].Texto2 == "OK") GetListAccesos(Resultado[2].Texto2);
                accesos = Resultado[2].Texto2;

                return Resultado[1].Texto2;

            }
            catch (Exception ex)
            {
                return ex.Message; 
}
        }

        public bool IsEnabledWebform(string url)
        {
            return MenuController.GetWebformByAccesos(objAccesos.ID, this.appid,url);
        }

        #region
        public string GetLogin()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(strWS);

            string data = client.GetStringAsync("api/Login?user1=" + user).Result;

            return data;
        }

        public bool EliminarLogin()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(strWS);

            string data = client.GetStringAsync("api/Login?user3=" + user).Result;
            if (data == "true")
                return true;
            else
                return false;
        }

        public void EvalSession(AdminLogin login)
        {
            if (!login.usuSicra.Activo)
            {
                login.EliminarLogin();
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        #endregion
    }
}